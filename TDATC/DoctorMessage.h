//
//  DoctorMessage.h
//  TDATC
//
//  Created by Vinay on 14/10/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DoctorMessage : NSObject

@property(nonatomic, strong) NSString* subject;
@property(nonatomic, strong) NSString* doctor_msg;
@property(nonatomic, strong) NSString* date;
@property(nonatomic, strong) NSString* msg_id;
@property(nonatomic, strong) NSString* patient_id;

@property(nonatomic, strong) NSString* patientimag_path;
@property(nonatomic, strong) NSString* full_name;
@property(nonatomic, strong) NSString* gender;
@property(nonatomic, strong) NSString* attchment;



- (id) initWithDictionary:(NSDictionary*)dictionary;

@end
