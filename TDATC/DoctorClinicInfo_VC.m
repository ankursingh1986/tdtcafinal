//
//  DoctorClinicInfo_VC.m
//  TDATC
//
//  Created by iWeb on 9/20/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "DoctorClinicInfo_VC.h"
#import "DoctorClinicInfo_TVC.h"
#import "DataManager.h"
#import "Utility.h"
#import "MyLoader.h"
#import "DataParser.h"
#import "Defines.h"
#import "ClinicViewModel.h"
#import "DoctorClinicInfoSecond_TVC.h"
#import "DoctorClinicInfoThird_TVC.h"
#import "DoctorClinicInfoFourth_TVC.h"


@interface DoctorClinicInfo_VC ()
{
    NSMutableArray *clinicViewArray;
}
@end

@implementation DoctorClinicInfo_VC

- (void)viewDidLoad {
    [super viewDidLoad];
  //  [self loadData];
    clinicViewArray= [[NSMutableArray alloc]init];
    [self leftButton];
    self.lbl_clinicName.text=[self.clinicData.clicnic_name uppercaseString];
    self.lbl_address.text=self.clinicData.locality;
    
    
    NSString *imagePath=[NSString stringWithFormat:@"%@",_gDataManager.doctorProfileImagePath];
    if([imagePath isEqualToString:@"(null)(null)"])
    {
        
    }
    else
    {
        [self downloadImage:imagePath];
        
    }
    

    
    [self getDataFromServer];
    indexArrayId = 0;
    height_Cell1=0;
    height_Cell2=0;
    height_Cell3=0;
    height_Cell4=0;
  //  [tbl_View setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];

    // Do any additional setup after loading the view.
}

-(void)getDataFromServer{
    
    NSString *postData=[NSString stringWithFormat:@"clinic_id=%@",self.clinicData.clinic_id];
    
    
    [MyLoader showLoadingView];
    [DataParser DappointmentClinictViewHit:postData WithCompletionHandler:^(NSMutableArray *array, BOOL isSuccessful)
     {
         if (isSuccessful)
         {
             [MyLoader hideLoadingView];
             clinicViewArray = [array mutableCopy];
             if(clinicViewArray.count==0)
             {
                 _lbl_Date.text=@"No time slot alloted";
                 tbl_View.userInteractionEnabled=NO;

             }
             else
             {
                 ClinicViewModel *objData= [clinicViewArray objectAtIndex:indexArrayId];
                 
                 df = [[NSDateFormatter alloc] init];
                 gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
                 components = [[NSDateComponents alloc] init];
                 
                 
                 
                 
                 
                 
                // _lbl_Date.text=convertedString;
                 _lbl_Date.text=[self dateFomateString:objData.availabledate];

              
                 
                // _lbl_Date.text=objData.availabledate;
                 tbl_View.userInteractionEnabled=YES;

                 [tbl_View reloadData];
             }
         //    [tbl_View reloadData];
         }
         else
         {
             [MyLoader hideLoadingView];
             [Utility alertWithTitle:@"" withMessage:kNetworkErrorTitle onVC:self];
         }
     }];
}
-(NSString *)dateFomateString:(NSString *)dataString
{
    NSArray* foo = [dataString componentsSeparatedByString: @"T"];
    NSString *str = [foo objectAtIndex: 0];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormatter dateFromString: str];
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM dd, YYYY"];
    return [dateFormatter stringFromDate:date];

}
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationItem.title = @"CLINIC INFORMATION";
    
    [super viewWillAppear:animated];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView DataSource and delegates

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4 ;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DoctorClinicInfo_TVC *cell1;
    DoctorClinicInfoSecond_TVC *cell2;
    DoctorClinicInfoThird_TVC *cell3;
    DoctorClinicInfoFourth_TVC *cell4;

    if(indexPath.row==0)
    {
        cell1 = [tableView dequeueReusableCellWithIdentifier:@"DoctorClinicInfo_Cell1" forIndexPath:indexPath];
        cell1.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
        if(clinicViewArray.count>0)
        {
            ClinicViewModel *objData= [clinicViewArray objectAtIndex:indexArrayId];
            NSMutableArray *arrayData=objData.doctor_avail_timesArray;
            int x=53,y=51;
            
            for (int i=0; i< arrayData.count; i++) {
                //label for message
                NSDictionary *dicData=[arrayData objectAtIndex:i];
                NSString *time1 = dicData[@"time"];
                NSString *time2 = @"12:00:00";
                
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"HH:mm:ss"];
                
                NSDate *date1= [formatter dateFromString:time1];
                NSDate *date2 = [formatter dateFromString:time2];
                
                NSComparisonResult result = [date1 compare:date2];
                if(result == NSOrderedAscending)
                {
                    NSLog(@"date2 is later than date1");
                    UILabel *lblMsg = [[UILabel alloc] initWithFrame:CGRectZero];
                    lblMsg.tag = i;
                    lblMsg.backgroundColor = [UIColor clearColor];
                    [lblMsg setFont:[UIFont fontWithName:@"Verdana" size:12]];
                    [lblMsg setLineBreakMode:UILineBreakModeWordWrap];
                    
                    if([dicData[@"status"] isEqualToString:@"0"])
                    {
                      //  lblMsg.textColor = [UIColor colorWithRed:(215/255.f) green:(29/255.f) blue:(32/255.f) alpha:.5];
                        
                        lblMsg.textColor = [UIColor colorWithRed:(29/255.f) green:(29/255.f) blue:(38/255.f) alpha:1.0];


                    }
                    else
                    {
                        lblMsg.textColor = [UIColor blueColor];

                    }

                    
                    lblMsg.text=[self timeFormateChangeAmPm:dicData[@"time"]];
                    lblMsg.frame = CGRectMake(x,y, 66, 18);
                    x=x+65;
                    int k=i+1;
                    if(k % 4==0)
                    {
                        x=53;
                        y=y+35;
                    }
                    
                    
                    UITapGestureRecognizer *lblAction = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(lblClickFromCell1:)];
                    lblAction.numberOfTapsRequired = 1;
                    lblMsg.userInteractionEnabled = YES;
                    [lblMsg addGestureRecognizer:lblAction];
                    
                    
                    [cell1 addSubview:lblMsg];
                }
        
            }
            //y=y+35;
            height_Cell1=y+2;
            
            UIView *paintView=[[UIView alloc]initWithFrame:CGRectMake(10, y, 300, 1)];
            [paintView setBackgroundColor:[UIColor lightGrayColor]];
            [cell1 addSubview:paintView];
           
        }
        
        return cell1;


    }
    else if (indexPath.row==1)
    {
        cell2 = [tableView dequeueReusableCellWithIdentifier:@"DoctorClinicInfo_Cell2" forIndexPath:indexPath];
        cell2.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
        if(clinicViewArray.count>0)
        {
            ClinicViewModel *objData= [clinicViewArray objectAtIndex:indexArrayId];
            NSMutableArray *arrayData=objData.doctor_avail_timesArray;
            int x=53,y=51;
            
            for (int i=0; i< arrayData.count; i++) {
                NSDictionary *dicData=[arrayData objectAtIndex:i];
                NSString *time1 = dicData[@"time"];
                NSString *time2 = @"11:59:59";
                NSString *time3 = @"18:00:00";

                
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"HH:mm:ss"];
                
                NSDate *date1= [formatter dateFromString:time1];
                NSDate *date2 = [formatter dateFromString:time2];
                NSDate *date3 = [formatter dateFromString:time3];

                
                NSComparisonResult result1 = [date1 compare:date2];
                NSComparisonResult result2 = [date1 compare:date3];
                if(result1 == NSOrderedDescending)
                {
                    if(result2 == NSOrderedAscending)
                    {
                        NSLog(@"date2 is later than date1");
                        UILabel *lblMsg = [[UILabel alloc] initWithFrame:CGRectZero];
                        lblMsg.tag = i;
                        lblMsg.backgroundColor = [UIColor clearColor];
                        [lblMsg setFont:[UIFont fontWithName:@"Verdana" size:12]];
                        [lblMsg setLineBreakMode:UILineBreakModeWordWrap];
                        
                        
                        if([dicData[@"status"] isEqualToString:@"0"])
                        {
                            //lblMsg.textColor = [UIColor colorWithRed:(215/255.f) green:(29/255.f) blue:(32/255.f) alpha:0.5];
                            lblMsg.textColor = [UIColor colorWithRed:(29/255.f) green:(29/255.f) blue:(38/255.f) alpha:1.0];
                            
                        }
                        else
                        {
                            lblMsg.textColor = [UIColor blueColor];

                            
                        }
                        
                        lblMsg.text=[self timeFormateChangeAmPm:dicData[@"time"]];
                        lblMsg.frame = CGRectMake(x,y, 66, 18);
                        x=x+65;
                        int k=i+1;
                        if(k % 4==0)
                        {
                            x=53;
                            y=y+35;
                        }
                        
                        
                        UITapGestureRecognizer *lblAction = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(lblClickFromCell2:)];
                        lblAction.numberOfTapsRequired = 1;
                        lblMsg.userInteractionEnabled = YES;
                        [lblMsg addGestureRecognizer:lblAction];
                        
                        [cell2 addSubview:lblMsg];
                    }
                }
                
            }
           // y=y+35;
            height_Cell2=y+2;

            
            UIView *paintView=[[UIView alloc]initWithFrame:CGRectMake(10, y, 300, 1)];
            [paintView setBackgroundColor:[UIColor lightGrayColor]];
            [cell2 addSubview:paintView];
        }
        
        
        
        return cell2;


    }
    else if (indexPath.row==2)
    {
        cell3 = [tableView dequeueReusableCellWithIdentifier:@"DoctorClinicInfo_Cell3" forIndexPath:indexPath];
        cell3.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
        if(clinicViewArray.count>0)
        {
            ClinicViewModel *objData= [clinicViewArray objectAtIndex:indexArrayId];
            NSMutableArray *arrayData=objData.doctor_avail_timesArray;
            int x=53,y=51;
            
            for (int i=0; i< arrayData.count; i++) {
                NSDictionary *dicData=[arrayData objectAtIndex:i];
                NSString *time1 = dicData[@"time"];
                NSString *time2 = @"17:59:59";
                NSString *time3 = @"21:00:00";
                
                
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"HH:mm:ss"];
                
                NSDate *date1= [formatter dateFromString:time1];
                NSDate *date2 = [formatter dateFromString:time2];
                NSDate *date3 = [formatter dateFromString:time3];
                
                
                NSComparisonResult result1 = [date1 compare:date2];
                NSComparisonResult result2 = [date1 compare:date3];
                if(result1 == NSOrderedDescending)
                {
                    if(result2 == NSOrderedAscending)
                    {
                        NSLog(@"date2 is later than date1");
                        UILabel *lblMsg = [[UILabel alloc] initWithFrame:CGRectZero];
                        lblMsg.tag = i;
                        lblMsg.backgroundColor = [UIColor clearColor];
                        [lblMsg setFont:[UIFont fontWithName:@"Verdana" size:12]];
                        [lblMsg setLineBreakMode:UILineBreakModeWordWrap];
                        lblMsg.text=[self timeFormateChangeAmPm:dicData[@"time"]];
                        
                        
                        if([dicData[@"status"] isEqualToString:@"0"])
                        {
                          //  lblMsg.textColor = [UIColor colorWithRed:(215/255.f) green:(29/255.f) blue:(32/255.f) alpha:.5];
                            
                             lblMsg.textColor = [UIColor colorWithRed:(29/255.f) green:(29/255.f) blue:(38/255.f) alpha:1.0];

                            
                        }
                        else
                        {
                            lblMsg.textColor = [UIColor blueColor];

                            
                        }

                        
                        lblMsg.frame = CGRectMake(x,y, 66, 18);
                        x=x+65;
                        int k=i+1;
                        if(k%4==0)
                        {
                            x=53;
                            y=y+35;
                        }
                        
                        
                        UITapGestureRecognizer *lblAction = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(lblClickFromCell3:)];
                        lblAction.numberOfTapsRequired = 1;
                        lblMsg.userInteractionEnabled = YES;
                        [lblMsg addGestureRecognizer:lblAction];
                        
                        [cell3 addSubview:lblMsg];
                    }
                }
                
            }
            
          //  y=y+35;
            height_Cell3=y+2;

            
            UIView *paintView=[[UIView alloc]initWithFrame:CGRectMake(10, y, 300, 1)];
            [paintView setBackgroundColor:[UIColor lightGrayColor]];
            [cell3 addSubview:paintView];
        }
        
        
        return cell3;

        
    }
    else if (indexPath.row==3)
    {
        cell4 = [tableView dequeueReusableCellWithIdentifier:@"DoctorClinicInfo_Cell4" forIndexPath:indexPath];
        cell4.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
        if(clinicViewArray.count>0)
        {
            int countdata=0;
            ClinicViewModel *objData= [clinicViewArray objectAtIndex:indexArrayId];
            NSMutableArray *arrayData=objData.doctor_avail_timesArray;
            int x=53,y=51;
            
            for (int i=0; i< arrayData.count; i++) {
                NSDictionary *dicData=[arrayData objectAtIndex:i];
                NSString *time1 = dicData[@"time"];
                NSString *time2 = @"20:59:59";
                
                
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"HH:mm:ss"];
                
                NSDate *date1= [formatter dateFromString:time1];
                NSDate *date2 = [formatter dateFromString:time2];
                
                
                NSComparisonResult result1 = [date1 compare:date2];
                if(result1 == NSOrderedDescending)
                {
                  
                        NSLog(@"date2 is later than date1");
                        UILabel *lblMsg = [[UILabel alloc] initWithFrame:CGRectZero];
                        lblMsg.tag = i;
                        lblMsg.backgroundColor = [UIColor clearColor];
                        [lblMsg setFont:[UIFont fontWithName:@"Verdana" size:12]];
                        [lblMsg setLineBreakMode:UILineBreakModeWordWrap];
                        lblMsg.text=[self timeFormateChangeAmPm:dicData[@"time"]];
                    
                    
                        if([dicData[@"status"] isEqualToString:@"0"])
                        {
                           // lblMsg.textColor = [UIColor colorWithRed:(215/255.f) green:(29/255.f) blue:(32/255.f) alpha:.5];
                            lblMsg.textColor = [UIColor colorWithRed:(29/255.f) green:(29/255.f) blue:(38/255.f) alpha:1.0];

                            
                        }
                        else
                        {
                            lblMsg.textColor = [UIColor blueColor];

                          
                            
                        }

                    
                        lblMsg.frame = CGRectMake(x,y, 66, 18);
                        x=x+65;
                        int k=i+1;
                        if(k % 4==0)
                        {
                            x=53;
                            y=y+35;
                        }
                    countdata++;
                        UITapGestureRecognizer *lblAction = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(lblClickFromCell4:)];
                        lblAction.numberOfTapsRequired = 1;
                        lblMsg.userInteractionEnabled = YES;
                        [lblMsg addGestureRecognizer:lblAction];
                    
                        [cell4 addSubview:lblMsg];
                }
                
            }
          
            if(countdata<5)
            {

                height_Cell4=y+2+35;

                UIView *paintView=[[UIView alloc]initWithFrame:CGRectMake(10, y+35, 300, 1)];
                [paintView setBackgroundColor:[UIColor lightGrayColor]];
                [cell4 addSubview:paintView];
            }
            else
            {
                height_Cell4=y+2;
                
                UIView *paintView=[[UIView alloc]initWithFrame:CGRectMake(10, y, 300, 1)];
                [paintView setBackgroundColor:[UIColor lightGrayColor]];
                [cell4 addSubview:paintView];
            }
           
        }
        
        return cell4;


        
    }
    return nil;
}

-(NSString *)timeFormateChangeAmPm:(NSString *)dateData
{
    NSDateFormatter *dateFormatter3 = [[NSDateFormatter alloc] init];
    [dateFormatter3 setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter3 setDateFormat:@"HH:mm:ss"];
    NSDate *date1 = [dateFormatter3 dateFromString:dateData];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"hh:mm a"];
    return [formatter stringFromDate:date1];
}

- (void)lblClickFromCell1:(UITapGestureRecognizer *)tapGesture {
    UILabel *label = (UILabel *)tapGesture.view;
    int index=(int)label.tag;
    
    [self changeTimeAPI:index];
    NSLog(@"Lable tag is ::%ld",(long)label.tag);
}
- (void)lblClickFromCell2:(UITapGestureRecognizer *)tapGesture {
    UILabel *label = (UILabel *)tapGesture.view;
    int index=(int)label.tag;
    
    [self changeTimeAPI:index];
    NSLog(@"Lable tag is ::%ld",(long)label.tag);
}
- (void)lblClickFromCell3:(UITapGestureRecognizer *)tapGesture {
    UILabel *label = (UILabel *)tapGesture.view;
    int index=(int)label.tag;
    
    [self changeTimeAPI:index];
    NSLog(@"Lable tag is ::%ld",(long)label.tag);
}
- (void)lblClickFromCell4:(UITapGestureRecognizer *)tapGesture {
    UILabel *label = (UILabel *)tapGesture.view;
    int index=(int)label.tag;
    
    [self changeTimeAPI:index];
    NSLog(@"Lable tag is ::%ld",(long)label.tag);
}


-(void)changeTimeAPI:(int)index
{
    
    ClinicViewModel *objData= [clinicViewArray objectAtIndex:indexArrayId];
    NSMutableArray *arrayData=objData.doctor_avail_timesArray;

    NSDictionary *dicData=[arrayData objectAtIndex:index];
    
    NSString *postData;
    
    if([dicData[@"status"] isEqualToString:@"1"])
    {
        postData=[NSString stringWithFormat:@"dat_id=%@&status=0&da_id=%@",dicData[@"dat_id"],dicData[@"da_id"]];

    }
    else
    {
        postData=[NSString stringWithFormat:@"dat_id=%@&status=1&da_id=%@",dicData[@"dat_id"],dicData[@"da_id"]];

    }
    
    [MyLoader showLoadingView];
    [DataParser globalHitAPI:doctorChangeTimeAPI detaiInfi:postData WithCompletionHandler:^(NSDictionary *dict, BOOL isSuccessful)
     {
         if (isSuccessful)
         {
             [MyLoader hideLoadingView];
             
             if([dict[@"message"] isEqualToString:@"success"])
             {
                 [self getDataFromServer];

                 
                 
             }
             else
             {

                 [Utility alertWithTitle:kAlertTitle withMessage:@"Fail! Please try Again" onVC:self];
                 
             }
         }
         else
         {
             [MyLoader hideLoadingView];
             [Utility alertWithTitle:@"" withMessage:kNetworkErrorTitle onVC:self];
         }
     }];

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView beginUpdates]; // tell the table you're about to start making changes
    
    if ([indexPath compare:self.expandedIndexPath] == NSOrderedSame)
    {
        self.expandedIndexPath = nil;
    }
    else
    {
        self.expandedIndexPath = indexPath;
    }
    
    [tableView endUpdates];
}

//- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if (indexPath.row == [_searchMedicineResultsArray count] - 1 )
//    {
//        if(_isScrolling)
//        {
//            _initialPageCount = _initialPageCount+10;
//            [self getMedinineList:_searchString withCount:_initialPageCount];
//        }
//    }
//}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([indexPath compare:self.expandedIndexPath] == NSOrderedSame)
    {
        if(indexPath.row==0)
        {
            return height_Cell1;
        }
        if(indexPath.row==1)
        {
            return height_Cell2;
        }
        if(indexPath.row==2)
        {
            return height_Cell3;
        }
        if(indexPath.row==3)
        {
            return height_Cell4;
        }
        return 130; // Expanded height
    }
    return 50; // Normal height
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)loadData{
    
    dateGloble = [NSDate date];
    df = [[NSDateFormatter alloc] init];
    gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    components = [[NSDateComponents alloc] init];

    [df setDateFormat:@"EE, dd MMM"];
    _lbl_Date.text=[df stringFromDate:dateGloble];
}

- (IBAction)touch_Right:(id)sender {
    
    if(clinicViewArray.count>0)
    {
    
        int valur=(int)clinicViewArray.count;
        
        if(valur > indexArrayId)
        {
            indexArrayId++;
            
            if(valur > indexArrayId)
            {
                ClinicViewModel *objData= [clinicViewArray objectAtIndex:indexArrayId];
                _lbl_Date.text=[self dateFomateString:objData.availabledate];
                [tbl_View reloadData];

            }
        }
        else
        {
            [Utility alertWithTitle:@"" withMessage:@"No right date available" onVC:self];

        }
    }
    
//    int daysToAdd = 1;  //
//    // set up date components
//    [components setDay:daysToAdd];
//    dateGloble = [gregorian dateByAddingComponents:components toDate:dateGloble options:0];
//    _lbl_Date.text=[df stringFromDate:dateGloble];

}

- (IBAction)touchLeft:(id)sender {
    if(clinicViewArray.count>0)
    {
        if(indexArrayId>0)
        {
            indexArrayId--;

            ClinicViewModel *objData= [clinicViewArray objectAtIndex:indexArrayId];
            _lbl_Date.text=[self dateFomateString:objData.availabledate];
            [tbl_View reloadData];
        }
        else
        {
            [Utility alertWithTitle:@"" withMessage:@"No less date available" onVC:self];

        }
    }
//    int daysToReduce = -1;  //
//    // set up date components
//    [components setDay:daysToReduce];
//    // create a calendar
//    dateGloble = [gregorian dateByAddingComponents:components toDate:dateGloble options:0];
//    _lbl_Date.text=[df stringFromDate:dateGloble];
    
}

- (IBAction) backButtonAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
    
}
-(void)leftButton{
    UIButton *button =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(backButtonAction:)forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0, 20, 20)];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = barButton;
    self.navigationItem.hidesBackButton=TRUE;
    
}

- (void) downloadImage:(NSString *)imgURL1
{
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       image_manager = [SDWebImageManager sharedManager];
                       [image_manager downloadWithURL:[NSURL URLWithString:imgURL1] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize, NSInteger feedID, NSString *feedString)
                        {
                        } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished)
                        {
                            if (image)
                            {
                                img_profile.image = image;
                            }
                        }];
                   });
    
}

@end
