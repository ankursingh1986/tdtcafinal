//
//  PatientWriteReview_VC.m
//  TDATC
//
//  Created by iWeb on 9/24/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "PatientWriteReview_VC.h"
#import "PatientDataParser.h"
#import "DataManager.h"
#import "Defines.h"
#import "Utility.h"
#import "PatientDataParser.h"
#import "DataParser.h"
#import "PickerTool.h"
#import "Defines.h"
#import "MyLoader.h"
#import "PickerModel.h"

@interface PatientWriteReview_VC ()
{
    PickerTool* _pickerView;
    NSMutableArray* _servicesArray;
    NSMutableArray* _clinicArray;
    
    NSString* _clinicId;
    NSString* _serviceId;
    
}

@end

@implementation PatientWriteReview_VC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self leftButton];
    [self putView];
    [self pickerViews];
    self.clinicServiceTextField.userInteractionEnabled = YES;
    self.clinicNameTextField.userInteractionEnabled = YES;
    
   
    // Do any additional setup after loading the view.
}
-(void)putView
{
    _view_stat1.selectedStar = [UIImage imageNamed:@"redstar"];
    _view_stat1.canEdit = NO;
    _view_stat1.maxRating = 5;
    _view_stat1.rating = [self.patientAppointmentData.rating intValue];;
    
    _view_star2.selectedStar = [UIImage imageNamed:@"redstar"];
    _view_star2.canEdit = YES;
    _view_star2.maxRating = 5;
    _view_star2.rating = 1;
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationItem.title = @"WRITE A REVIEW";
    _servicesArray = [[NSMutableArray alloc] init];
    _clinicArray = [[NSMutableArray alloc] init];
    
    self.doctorNameLabel.text = [self.patientAppointmentData.doctorName uppercaseString];
    self.doctorLocationLabel.text = self.patientAppointmentData.locality;
    
    NSString *imagePath=[NSString stringWithFormat:@"%@%@",kDoctorImagePath,self.patientAppointmentData.image];
    if([imagePath isEqualToString:@"(null)(null)"])
    {
        
    }
    else
    {
        [self downloadImage:imagePath];
        
    }

    
    [self getClinicList];
    [self getServiceList];
}

- (void)didReceiveMemoryWarning
{
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) pickerViews
{
    _pickerView = [PickerTool loadClass];
    self.clinicNameTextField.inputView = _pickerView;
    self.clinicServiceTextField.inputView = _pickerView;
}

- (void) getClinicList
{
    [PatientDataParser doctorClinicList:[NSString stringWithFormat:@"doctor_id=%@",self.patientAppointmentData.doctor_id] WithCompletionHandler:^(NSArray *array, BOOL isSuccessful)
    {
        if (isSuccessful)
        {
            _clinicArray = [array mutableCopy];
            if (_clinicArray.count)
            {
                PickerModel* data = [_clinicArray objectAtIndex:0];
                _clinicId = data.dataId;
                self.clinicNameTextField.text = data.dataName;
            }
        }
        else
        {
            [Utility alertWithTitle:kAlertTitle withMessage:kNetworkErrorTitle onVC:self];
        }
        
    }];
}

- (void) getServiceList
{
    [PatientDataParser getServiceListWithCompletionHandler:^(NSArray *array, BOOL isSuccessful)
     {
         if (isSuccessful)
         {
             _servicesArray = [array mutableCopy];
             
             if (_servicesArray.count)
             {
                 PickerModel* data = [_servicesArray objectAtIndex:0];
                 _serviceId = data.dataId;
                 self.clinicServiceTextField.text = data.dataName;
             }
             
         }
         else
         {
             [Utility alertWithTitle:kAlertTitle withMessage:kNetworkErrorTitle onVC:self];
         }
        
     }];
}

#pragma mark - Submit Review
 
- (IBAction)touchSubmitReview:(id)sender
{
    
//    patient id mean (id)
//    clinic_id
//    doctor_id
//    like_id
//    raiting_id
//    treatment_id
//    pasent_msg
//    
//    patient_name
    
    // [self.navigationController popViewControllerAnimated:YES];
    [MyLoader showLoadingView];
   // NSString* postData = [NSString stringWithFormat:@"id=%@&clinic_id=%@&doctor_id=%@&raiting_id=%@&treatment_id=%@&pasent_msg=%@&patient_name=%@",_gDataManager.loginModel.user_id,_clinicId,self.patientAppointmentData.doctor_id,@"3",_serviceId,self.patientMessageTextView.text,self.patientAppointmentData.patientname];
    
    NSString *dataRat=[NSString stringWithFormat:@"%f",_view_star2.rating];
    
    NSString* postData = [NSString stringWithFormat:@"id=%@&clinic_id=%@&doctor_id=%@&raiting_id=%@&treatment_id=%@&pasent_msg=%@&patient_name=%@",_gDataManager.loginModel.user_id,_clinicId,self.patientAppointmentData.doctor_id,dataRat,_serviceId,self.patientMessageTextView.text,self.patientAppointmentData.patientname];
    
    NSLog(@"post data=%@",postData);
    
    
    [PatientDataParser patientAddFeedback:postData WithCompletionHandler:^(NSDictionary *dict, BOOL isSuccessful)
    {
        [MyLoader hideLoadingView];
        if (isSuccessful)
        {
            [Utility alertWithTitle:@"Success" withMessage:dict[@"message"] onVC:self];
        }
        else
        {
            [Utility alertWithTitle:kAlertTitle withMessage:kNetworkErrorTitle onVC:self];
        }
    }];
}


- (IBAction) backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)leftButton
{
    UIButton *button =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(backButtonAction:)forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0, 20, 20)];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = barButton;
    self.navigationItem.hidesBackButton=TRUE;
}

//-------------------------------------------------------------------------------------------
#pragma mark- Text field delegate and data source
//-------------------------------------------------------------------------------------------

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField==self.clinicNameTextField )
    {
        [self setPickerInfo:self.clinicNameTextField withArray:_clinicArray];
    }
    else if (textField == self.clinicServiceTextField)
    {
        [self setPickerInfo:self.clinicServiceTextField withArray:_servicesArray];
    }
}

#pragma mark- Picker Methods

- (void) setPickerInfo:(UITextField*)textfield  withArray:(NSArray*)array
{
    [_pickerView pickerViewMethod:textfield arr:array];
    _pickerView.completionHandler = ^(PickerModel* detail)
    {
        if(textfield == self.clinicNameTextField)
            _clinicId = detail.dataId;
        else if (textfield == self.clinicServiceTextField)
            _serviceId = detail.dataId;
    };
}
- (void) downloadImage:(NSString *)imgURL1
{
    NSLog(@"Image URL in method=%@",imgURL1);
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       image_manager = [SDWebImageManager sharedManager];
                       [image_manager downloadWithURL:[NSURL URLWithString:imgURL1] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize, NSInteger feedID, NSString *feedString)
                        {
                        } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished)
                        {
                            if (image)
                            {
                                _img_profile.image = image;
                            }
                        }];
                   });
    
}

@end
