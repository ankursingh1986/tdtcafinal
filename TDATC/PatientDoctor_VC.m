//
//  PatientDoctor_VC.m
//  TDATC
//
//  Created by iWeb on 9/23/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "PatientDoctor_VC.h"
#import "PatientDoctor_TVC.h"
#import "PatientDoctorDeatil_VC.h"
#import "DataManager.h"
#import "DataParser.h"
#import "PatientDataParser.h"
#import "Utility.h"
#import "MyLoader.h"
#import "FindDoctorModel.h"

@interface PatientDoctor_VC ()
{
    NSMutableArray* _doctorArray;
}

@end

@implementation PatientDoctor_VC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self leftButton];
    [self rightButton];
    [tbl_doctor setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    self->tbl_doctor.tableFooterView = [[UIView alloc] init];
    

    // Do any additional setup after loading the view.
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationItem.title = @"DOCTORS";
    _doctorArray = [[NSMutableArray alloc] init];
    [self getDoctorList];
}

- (void) getDoctorList
{
    [MyLoader showLoadingView];
    NSString* postData = [NSString stringWithFormat:@"id=%@&country_id=%@&state_id=%@&city_id=%@&service_id=%@",_gDataManager.loginModel.user_id,self.countryId,self.stateId,self.cityId,self.serviceId];
    [PatientDataParser patientSearchDoctorList:postData WithCompletionHandler:^(NSArray* array, BOOL isSuccessful)
    {
         [MyLoader hideLoadingView];
        if (isSuccessful)
        {
            _doctorArray = [array mutableCopy];
            [tbl_doctor reloadData];
        }
        else
        {
            [Utility alertWithTitle:kAlertTitle withMessage:kNetworkErrorTitle onVC:self];
        }
    }];
}

#pragma mark - table view delegates and data socurce


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _doctorArray.count;
}
//

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    PatientDoctor_TVC *cell = [tableView dequeueReusableCellWithIdentifier:@"PatientDoctor_Cell" forIndexPath:indexPath];
    cell.view_star.selectedStar = [UIImage imageNamed:@"redstar"];

    cell.view_star.canEdit = NO;
    cell.view_star.maxRating = 5;
    
    cell.btn_Book.tag = indexPath.row;
    [cell.btn_Book addTarget:self action:@selector(btnBookAction:) forControlEvents:UIControlEventTouchUpInside];
    
    FindDoctorModel* data = [_doctorArray objectAtIndex:indexPath.row];
    
    cell.view_star.rating = [data.rating intValue];

    [cell setUpData:data];

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}
- (void) btnBookAction:(id)sender
{
    
    UIButton* btn = (UIButton*)sender;
    
    FindDoctorModel* data = [_doctorArray objectAtIndex:btn.tag];
    
    UIStoryboard *storyBoard12 = [UIStoryboard storyboardWithName:@"Patient" bundle:nil];
    PatientDoctorDeatil_VC* destVC = [storyBoard12 instantiateViewControllerWithIdentifier:@"PatientDoctorDeatil_VC_id"];
    destVC.doctorData = data;
    
    [self.navigationController pushViewController:destVC animated:YES];

    
    NSLog(@"calcel click");
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    DoctoreMessageDetailViewController* destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"DoctoreMessageDetailViewController_id"];
    //
    //    [self.navigationController pushViewController:destVC animated:YES];
    
    
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)rightButton
{
    button1 =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button1 setImage:[UIImage imageNamed:@"notification"] forState:UIControlStateNormal];
    [button1 addTarget:self action:@selector(nitificatioButtonAction:)forControlEvents:UIControlEventTouchUpInside];
    [button1 setFrame:CGRectMake(0, 0, 20, 20)];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button1];
    self.navigationItem.rightBarButtonItem = barButton;
}
- (IBAction) nitificatioButtonAction:(id)sender
{
    
    //    button.selected = !button.selected;
    //    if(button.selected)
    //    {
    //        self.view_1.hidden=NO;
    //        tbl_FeedBack.hidden=YES;
    //    }
    //    else
    //    {
    //        self.view_1.hidden=YES;
    //        tbl_FeedBack.hidden=NO;
    //    }
    
}

-(void)leftButton
{
    UIButton *button =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(backButtonAction:)forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0, 20, 20)];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = barButton;
    
}
- (IBAction) backButtonAction:(id)sender
{
    
    [self.navigationController popViewControllerAnimated:YES];

    
//    UIStoryboard *storyBoard12 = [UIStoryboard storyboardWithName:@"Patient" bundle:nil];
//    
//    UIViewController *vc = [storyBoard12 instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
//    [self presentViewController:vc animated:NO completion:nil];
    
}


@end
