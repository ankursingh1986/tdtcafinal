//
//  AppDelegate.h
//  TDATC
//
//  Created by iWeb on 9/13/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <Accounts/Accounts.h>
#import <Twitter/Twitter.h>





@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)getTwitterAccountOnCompletion:(void(^)(ACAccount *))completionHandler;


- (void)saveContext;


@end

