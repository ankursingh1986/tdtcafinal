//
//  PatientWriteReview_VC.h
//  TDATC
//
//  Created by iWeb on 9/24/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASStarRatingView.h"
#import "PatientFeedbackModel.h"
#import "PatientAppointmentModel.h"
#import "Global.pch"


@interface PatientWriteReview_VC : UIViewController
{
    SDWebImageManager *image_manager;

}
@property (weak, nonatomic) IBOutlet UIImageView *img_profile;

@property (nonatomic, strong) PatientFeedbackModel* feedbackData;

@property (nonatomic, strong) PatientAppointmentModel* patientAppointmentData;

@property (weak, nonatomic) IBOutlet ASStarRatingView *view_stat1;
@property (weak, nonatomic) IBOutlet ASStarRatingView *view_star2;

@property (weak, nonatomic) IBOutlet UITextField *clinicNameTextField;

@property (weak, nonatomic) IBOutlet UITextField *clinicServiceTextField;
@property (weak, nonatomic) IBOutlet UITextView *messageTextView;
@property (weak, nonatomic) IBOutlet UILabel *doctorNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *doctorLocationLabel;
@property (weak, nonatomic) IBOutlet UITextView *patientMessageTextView;

- (IBAction)touchSubmitReview:(id)sender;

@end
