//
//  ClinicTableViewCell.h
//  TDATC
//
//  Created by Vinay on 03/11/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ClinicInfoModel.h"

@interface ClinicTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *btn_View;
@property (weak, nonatomic) IBOutlet UILabel *clinicNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;

- (void) setUpData:(ClinicInfoModel*)data;

@end
