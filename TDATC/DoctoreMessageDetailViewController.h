//
//  DoctoreMessageDetailViewController.h
//  TDATC
//
//  Created by iWeb on 9/18/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DoctorMessage.h"
#import "SDWebImageManager.h"
#import "Global.pch"


@interface DoctoreMessageDetailViewController : UIViewController
{
    SDWebImageManager *image_manager;

}
@property (weak, nonatomic) IBOutlet UIImageView *img_profile;
@property (weak, nonatomic) IBOutlet UILabel *lbl_name;
@property (weak, nonatomic) IBOutlet UILabel *lbl_age;
@property (weak, nonatomic) IBOutlet UILabel *lbl_date;
@property (weak, nonatomic) IBOutlet UILabel *lbl_subject;

@property(nonatomic, strong) NSString* patientId;
@property (nonatomic, strong) NSString* doctorMessage;

@property (weak, nonatomic) IBOutlet UITextView *doctorMessageTextView;

@property (nonatomic,strong) DoctorMessage *messageData;

@property (nonatomic,strong) NSMutableArray *inboxArray;




- (IBAction)touch_Reply:(id)sender;

@end
