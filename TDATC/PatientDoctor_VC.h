//
//  PatientDoctor_VC.h
//  TDATC
//
//  Created by iWeb on 9/23/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Global.pch"

@interface PatientDoctor_VC : UIViewController
{
    IBOutlet UITableView *tbl_doctor;
    UIButton *button1;
    SDWebImageManager *image_manager;

}

@property (nonatomic, strong) NSString* countryId;
@property (nonatomic, strong) NSString* stateId;
@property (nonatomic, strong) NSString* cityId;
@property (nonatomic, strong) NSString* serviceId;

@end
