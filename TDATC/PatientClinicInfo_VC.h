//
//  PatientClinicInfo_VC.h
//  TDATC
//
//  Created by iWeb on 9/23/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Global.pch"

@interface PatientClinicInfo_VC : UIViewController
{
    IBOutlet UITableView *tbl_View;
    NSDate *dateGloble;
    NSDateFormatter *df;
    NSCalendar *gregorian;
    NSDateComponents *components;
    SDWebImageManager *image_manager;
    
    int indexArrayId;
    int height_Cell1,height_Cell2,height_Cell3,height_Cell4;

}
@property (weak, nonatomic) IBOutlet UILabel *lbl_address;
@property (weak, nonatomic) IBOutlet UILabel *_lbl_hospitalname;
@property (weak, nonatomic) IBOutlet UILabel *lbl_clinicName;
@property (weak, nonatomic) IBOutlet UIImageView *img_profile;

@property (nonatomic, strong) ClinicInfoModel* clinicData;

@property (nonatomic, strong) FindDoctorModel* doctorProfileData;

@property (nonatomic, strong) PatientAppointmentModel* patientAppointmentData;



@property (strong, nonatomic) NSIndexPath *expandedIndexPath;

@property (weak, nonatomic) IBOutlet UILabel *lbl_Date;
- (IBAction)touch_Right:(id)sender;
- (IBAction)touchLeft:(id)sender;

@end
