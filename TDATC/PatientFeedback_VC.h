//
//  PatientFeedback_VC.h
//  TDATC
//
//  Created by iWeb on 9/23/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Global.pch"

@interface PatientFeedback_VC : UIViewController
{
    IBOutlet UITableView *tbl_FeedBack;
    SDWebImageManager *image_manager;

}
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UITextView *messageLabel;

@end
