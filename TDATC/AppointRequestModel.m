//
//  AppointRequestModel.m
//  TDATC
//
//  Created by iWeb on 10/14/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "AppointRequestModel.h"

@implementation AppointRequestModel
- (id) initWithDictionary:(NSDictionary*)dict
{
    self = [super init];
    if (self)
    {
        self.patient_id = [NSString stringWithFormat:@"%@",dict[@"patient_id"]];
        self.clinic_id =[NSString stringWithFormat:@"%@",dict[@"clinic_id"]];
        self.booking_id=[NSString stringWithFormat:@"%@",dict[@"booking_id"]];
        
        if (![dict[@"visit"] isKindOfClass:[NSNull class]])
            self.visit= dict[@"visit"];
        else
            self.visit = @"";
        
        
        if (![dict[@"patientname"] isKindOfClass:[NSNull class]])
            self.patientname= dict[@"patientname"];
        else
            self.patientname = @"";
        
        if (![dict[@"mobile"] isKindOfClass:[NSNull class]])
            self.mobile= dict[@"mobile"];
        else
            self.mobile = @"";
        //self.mobile = dict[@"mobile"];
        if (![dict[@"createddate"] isKindOfClass:[NSNull class]])
            self.createddate= dict[@"createddate"];
        else
            self.createddate = @"";
        //self.createddate = dict[@"createddate"];
        if (![dict[@"sickness"] isKindOfClass:[NSNull class]])
            self.sickness= dict[@"sickness"];
        else
            self.sickness = @"";
        // self.sickness=dict[@"sickness"];
        if (![dict[@"status"] isKindOfClass:[NSNull class]])
            self.status=[NSString stringWithFormat:@"%@",dict[@"status"]];
        else
            self.status = @"";
        //self.status=[NSString stringWithFormat:@"%@",dict[@"status"]];
        if (![dict[@"consultingmode"] isKindOfClass:[NSNull class]])
            self.consultingmode=dict[@"consultingmode"];
        else
            self.consultingmode = @"";
        //self.consultingmode=dict[@"consultingmode"];
        if (![dict[@"payementway"] isKindOfClass:[NSNull class]])
            self.payementway=dict[@"payementway"];
        else
            self.payementway = @"";
        //self.payementway=dict[@"payementway"];
        if (![dict[@"image"] isKindOfClass:[NSNull class]])
            self.image=dict[@"image"];
        else
            self.image = @"";
        //self.image=dict[@"image"];
        
    }
    return self;
}

@end
