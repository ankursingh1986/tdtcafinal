//
//  PatientDataParser.m
//  TDATC
//
//  Created by Vinay on 01/11/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "PatientDataParser.h"
#import "PickerModel.h"
#import "FindDoctorModel.h"
#import "ClinicInfoModel.h"
#import "PatientFeedbackModel.h"
#import "PatientAppointmentModel.h"
#import "PatientMessageModel.h"
#import "Global.pch"

@implementation PatientDataParser

// Add to cart list API call
+ (void) loginAPIHit:(NSString*)details WithCompletionHandler:(void (^)(NSDictionary* dict , BOOL isSuccessful)) handler
{
    NSURLRequest* request = [WebServiceManager postRequestWithService:loginAPI withPayload:details];
    [WebServiceManager sendRequest:request completion:^(NSData* responseData , NSError* error)
     {
         if (responseData == nil || error)
         {
             handler(nil,NO);
         }
         else
         {
             NSDictionary* responseDict = [PatientDataParser dataObject:responseData];
             
             if ([responseDict[@"status"] isEqualToString:@"true"])
             {
                 // LoginModel* obj = [[LoginModel alloc] initWithDictionary:responseDict[@"user"]];
                 _gDataManager.loginModel = [[LoginModel alloc] initWithDictionary:responseDict[@"user"]];
                 NSLog(@"login response=%@", responseDict);
                 handler(responseDict,YES);
             }
             else
             {
                 handler(nil, YES);
             }
         }
     }];
}


// Get Patient Profile
+ (void) getPatientProfile:(NSString*)details WithCompletionHandler:(void (^)(NSDictionary* dict , BOOL isSuccessful)) handler
{
    NSURLRequest* request = [WebServiceManager postRequestWithService:patientProfileInfoAPI withPayload:details];
    [WebServiceManager sendRequest:request completion:^(NSData* responseData , NSError* error)
     {
         if (responseData == nil || error)
         {
             handler(nil,NO);
         }
         else
         {
             NSDictionary* responseDict = [PatientDataParser dataObject:responseData];
             
             if ([responseDict[@"status"] isEqualToString:@"true"])
             {
                 // LoginModel* obj = [[LoginModel alloc] initWithDictionary:responseDict[@"user"]];
                 _gDataManager.loginModel = [[LoginModel alloc] initWithDictionary:responseDict[@"user"]];
                 NSLog(@"login response=%@", responseDict);
                 handler(responseDict,YES);
             }
             else
             {
                 handler(nil, YES);
             }
         }
     }];
}


// Update patient profile
+ (void) updatePatientProfile:(NSString*)details WithCompletionHandler:(void (^)(NSDictionary* dict , BOOL isSuccessful)) handler
{
    NSURLRequest* request = [WebServiceManager postRequestWithService:patientProfileEditAPI withPayload:details];
    [WebServiceManager sendRequest:request completion:^(NSData* responseData , NSError* error)
     {
         if (responseData == nil || error)
         {
             handler(nil,NO);
         }
         else
         {
             NSDictionary* responseDict = [PatientDataParser dataObject:responseData];
             
             if ([responseDict[@"status"] isEqualToString:@"true"])
             {
                 // LoginModel* obj = [[LoginModel alloc] initWithDictionary:responseDict[@"user"]];
                // _gDataManager.loginModel = [[LoginModel alloc] initWithDictionary:responseDict[@"user"]];
                 NSLog(@"login response=%@", responseDict);
                 handler(responseDict,YES);
             }
             else
             {
                 handler(nil, YES);
             }
         }
     }];
}

+ (void) sentPatientMessage:(NSString*)details WithCompletionHandler:(void (^)(NSArray* array , BOOL isSuccessful)) handler
{
    NSURLRequest* request = [WebServiceManager postRequestWithService:patientMessageSentAPI withPayload:details];
    [WebServiceManager sendRequest:request completion:^(NSData* responseData , NSError* error)
     {
         if (responseData == nil || error)
         {
             handler(nil,NO);
         }
         else
         {
             NSDictionary* responseDict = [PatientDataParser dataObject:responseData];
             
             if ([responseDict[@"status"] isEqualToString:@"true"])
             {
                 NSLog(@"login response=%@", responseDict);
                 NSMutableArray* messageArray = [[NSMutableArray alloc] init];
                 
                 NSArray* dataArray = responseDict[@"messagedetail"];
                 
                 for (NSDictionary* dict in dataArray)
                 {
                     NSString *userid=[NSString stringWithFormat:@"%@",dict[@"dr"][@"user_id"]];
                     NSString *useriddata=[NSString stringWithFormat:@"%@",_gDataManager.doctorMessageId];
                     NSLog(@"data=%@ %@",userid,useriddata);
                     
                     if([userid isEqualToString:useriddata])
                     {
                         PatientMessageModel* obj = [[PatientMessageModel alloc] initWithDictionary:dict];
                         [messageArray addObject:obj];
                     }
                 }
                 handler(messageArray,YES);
             }
             else
             {
                 handler(nil, YES);
             }
         }
     }];
}

+ (void) inboxPatientMessage:(NSString*)details WithCompletionHandler:(void (^)(NSArray* array , BOOL isSuccessful)) handler
{
    NSURLRequest* request = [WebServiceManager postRequestWithService:patientMessageInboxAPI withPayload:details];
    [WebServiceManager sendRequest:request completion:^(NSData* responseData , NSError* error)
     {
         if (responseData == nil || error)
         {
             handler(nil,NO);
         }
         else
         {
             NSDictionary* responseDict = [PatientDataParser dataObject:responseData];
             
             if ([responseDict[@"message"] isEqualToString:@"success"])
             {
                 NSLog(@"login response=%@", responseDict);
                 NSMutableArray* messageArray = [[NSMutableArray alloc] init];
                 
                 NSArray* dataArray = responseDict[@"users"];
                 
                 for (NSDictionary* dict in dataArray)
                 {
                     NSString *userid=[NSString stringWithFormat:@"%@",dict[@"dr"][@"user_id"]];
                     NSString *useriddata=[NSString stringWithFormat:@"%@",_gDataManager.doctorMessageId];
                     NSLog(@"data=%@ %@",userid,useriddata);
                     
                     if([userid isEqualToString:useriddata])
                     {
                     
                         PatientMessageModel* obj = [[PatientMessageModel alloc] initWithDictionary:dict];
                         [messageArray addObject:obj];
                     }
                 }
                 handler(messageArray,YES);
             }
             else
             {
                 handler(nil, YES);
             }
         }
     }];
}

+ (void) createPatientMessage:(NSString*)details WithCompletionHandler:(void (^)(NSDictionary* dict , BOOL isSuccessful)) handler
{
    NSURLRequest* request = [WebServiceManager postRequestWithService:patientComposeMessageAPI withPayload:details];
    [WebServiceManager sendRequest:request completion:^(NSData* responseData , NSError* error)
     {
         if (responseData == nil || error)
         {
             handler(nil,NO);
         }
         else
         {
             NSDictionary* responseDict = [PatientDataParser dataObject:responseData];
             
             if ([responseDict[@"status"] isEqualToString:@"true"])
             {
                NSLog(@"login response=%@", responseDict);
                 handler(responseDict,YES);
             }
             else
             {
                 handler(nil, YES);
             }
         }
     }];
}

+ (void) patientFeedback:(NSString*)details WithCompletionHandler:(void (^)(NSArray *array , BOOL isSuccessful)) handler
{
    NSURLRequest* request = [WebServiceManager postRequestWithService:patientFeedbackListAPI withPayload:details];
    [WebServiceManager sendRequest:request completion:^(NSData* responseData , NSError* error)
     {
         if (responseData == nil || error)
         {
             handler(nil,NO);
         }
         else
         {
             NSDictionary* responseDict = [PatientDataParser dataObject:responseData];
             
             if ([responseDict[@"message"] isEqualToString:@"success"])
             {
                 NSMutableArray* feedbackArray = [[NSMutableArray alloc] init];
                 NSArray* dataArray = responseDict[@"Feedbacks"];
                 
                 for (NSDictionary* dict in dataArray)
                 {
                     PatientFeedbackModel* obj = [[PatientFeedbackModel alloc] initWithDictionary:dict];
                     [feedbackArray addObject:obj];
                 }
                 

                 handler(feedbackArray,YES);
             }
             else
             {
                 handler(nil, YES);
             }
         }
     }];
}

+ (void) patientAddFeedback:(NSString*)details WithCompletionHandler:(void (^)(NSDictionary* dict , BOOL isSuccessful)) handler
{
    NSURLRequest* request = [WebServiceManager postRequestWithService:patientFeedbackAddAPI withPayload:details];
    [WebServiceManager sendRequest:request completion:^(NSData* responseData , NSError* error)
     {
         if (responseData == nil || error)
         {
             handler(nil,NO);
         }
         else
         {
             NSDictionary* responseDict = [PatientDataParser dataObject:responseData];
             
             if ([responseDict[@"status"] isEqualToString:@"true"])
             {
                 // LoginModel* obj = [[LoginModel alloc] initWithDictionary:responseDict[@"user"]];
                 //_gDataManager.loginModel = [[LoginModel alloc] initWithDictionary:responseDict[@"user"]];
                 NSLog(@"login response=%@", responseDict);
                 handler(responseDict,YES);
             }
             else
             {
                 handler(nil, YES);
             }
         }
     }];
}

+ (void) patientAppointmentList:(NSString*)details WithCompletionHandler:(void (^)(NSArray *array , BOOL isSuccessful)) handler
{
    NSURLRequest* request = [WebServiceManager postRequestWithService:patientAppointmentlistAPI withPayload:details];
    [WebServiceManager sendRequest:request completion:^(NSData* responseData , NSError* error)
     {
         if (responseData == nil || error)
         {
             handler(nil,NO);
         }
         else
         {
             NSDictionary* responseDict = [PatientDataParser dataObject:responseData];
             
             if ([responseDict[@"status"] isEqualToString:@"success"])
             {
                 // LoginModel* obj = [[LoginModel alloc] initWithDictionary:responseDict[@"user"]];
                 
                 NSMutableArray* appointmentArray = [[NSMutableArray alloc] init];
                 NSArray* dataArray = responseDict[@"appointmentlist"];
                 
                 for (NSDictionary* dict in dataArray)
                 {
                     PatientAppointmentModel* obj = [[PatientAppointmentModel alloc] initWithDictionary:dict];
                     [appointmentArray addObject:obj];
                 }
                 handler(appointmentArray,YES);
             }
             else
             {
                 handler(nil, YES);
             }
         }
     }];
}

+ (void) patientFindDoctorList:(NSString*)details WithCompletionHandler:(void (^)(NSDictionary* dict , BOOL isSuccessful)) handler
{
    NSURLRequest* request = [WebServiceManager postRequestWithService:patientFindDoctorAPI withPayload:details];
    [WebServiceManager sendRequest:request completion:^(NSData* responseData , NSError* error)
     {
         if (responseData == nil || error)
         {
             handler(nil,NO);
         }
         else
         {
             NSDictionary* responseDict = [PatientDataParser dataObject:responseData];
             
             if ([responseDict[@"status"] isEqualToString:@"true"])
             {
                 // LoginModel* obj = [[LoginModel alloc] initWithDictionary:responseDict[@"user"]];
                 //_gDataManager.loginModel = [[LoginModel alloc] initWithDictionary:responseDict[@"user"]];
                 NSLog(@"login response=%@", responseDict);
                 handler(responseDict,YES);
             }
             else
             {
                 handler(nil, YES);
             }
         }
     }];
}

+ (void) patientSearchDoctorList:(NSString*)details WithCompletionHandler:(void (^)(NSArray* array , BOOL isSuccessful)) handler
{
    NSURLRequest* request = [WebServiceManager postRequestWithService:patientDoctorSearchResultAPI withPayload:details];
    [WebServiceManager sendRequest:request completion:^(NSData* responseData , NSError* error)
     {
         if (responseData == nil || error)
         {
             handler(nil,NO);
         }
         else
         {
             NSDictionary* responseDict = [PatientDataParser dataObject:responseData];
             
             if ([responseDict[@"status"] isEqualToString:@"success"])
             {
                 NSMutableArray* doctorArray = [[NSMutableArray alloc] init];
                 NSArray* dataArray = responseDict[@"doctor"];
                 
                 for (NSDictionary* dict in dataArray)
                 {
                     FindDoctorModel* obj = [[FindDoctorModel alloc] initWithDictionary:dict];
                     [doctorArray addObject:obj];
                 }
                 handler(doctorArray,YES);
             }
             else
             {
                 handler(nil, YES);
             }
         }
     }];
}

+ (void) getDoctorList:(NSString*)details WithCompletionHandler:(void (^)(NSArray* array , BOOL isSuccessful)) handler
{
    NSURLRequest* request = [WebServiceManager postRequestWithService:kGetDoctorListAPI withPayload:details];
    [WebServiceManager sendRequest:request completion:^(NSData* responseData , NSError* error)
     {
         if (responseData == nil || error)
         {
             handler(nil,NO);
         }
         else
         {
             NSDictionary* responseDict = [PatientDataParser dataObject:responseData];
             
             if ([responseDict[@"status"] isEqualToString:@"true"])
             {
                 NSMutableArray* doctorArray = [[NSMutableArray alloc] init];
                 NSArray* dataArray = responseDict[@"doctorlist"];
                 
                 for (NSDictionary* dict in dataArray)
                 {
                     DoctorList* obj = [[DoctorList alloc] initWithDictionary:dict];
                     [doctorArray addObject:obj];
                 }
                 handler(doctorArray,YES);
             }
             else
             {
                 handler(nil, YES);
             }
         }
     }];
}


+ (void) patientConfirmAppointment:(NSString*)details WithCompletionHandler:(void (^)(NSDictionary* dict , BOOL isSuccessful)) handler
{
    NSURLRequest* request = [WebServiceManager postRequestWithService:patientConfirmAppointmentAPI withPayload:details];
    [WebServiceManager sendRequest:request completion:^(NSData* responseData , NSError* error)
     {
         if (responseData == nil || error)
         {
             handler(nil,NO);
         }
         else
         {
             NSDictionary* responseDict = [PatientDataParser dataObject:responseData];
             
             if ([responseDict[@"status"] isEqualToString:@"true"])
             {
                 // LoginModel* obj = [[LoginModel alloc] initWithDictionary:responseDict[@"user"]];
                 _gDataManager.loginModel = [[LoginModel alloc] initWithDictionary:responseDict[@"user"]];
                 NSLog(@"login response=%@", responseDict);
                 handler(responseDict,YES);
             }
             else
             {
                 handler(nil, YES);
             }
         }
     }];
}


+ (void) patientCancelAppointment:(NSString*)details WithCompletionHandler:(void (^)(NSDictionary* dict , BOOL isSuccessful)) handler
{
    NSURLRequest* request = [WebServiceManager postRequestWithService:patientCancelAppointmentAPI withPayload:details];
    [WebServiceManager sendRequest:request completion:^(NSData* responseData , NSError* error)
     {
         if (responseData == nil || error)
         {
             handler(nil,NO);
         }
         else
         {
             NSDictionary* responseDict = [PatientDataParser dataObject:responseData];
             
             if ([responseDict[@"status"] isEqualToString:@"true"])
             {
                 // LoginModel* obj = [[LoginModel alloc] initWithDictionary:responseDict[@"user"]];
//                 _gDataManager.loginModel = [[LoginModel alloc] initWithDictionary:responseDict[@"user"]];
//                 NSLog(@"login response=%@", responseDict);
                 handler(responseDict,YES);
             }
             else
             {
                 handler(nil, YES);
             }
         }
     }];
}

+ (void) patientClinicInfo:(NSString*)details WithCompletionHandler:(void (^)(NSArray *array , BOOL isSuccessful)) handler
{
    NSURLRequest* request = [WebServiceManager postRequestWithService:patientClinicInformationAPI withPayload:details];
    [WebServiceManager sendRequest:request completion:^(NSData* responseData , NSError* error)
     {
         if (responseData == nil || error)
         {
             handler(nil,NO);
         }
         else
         {
             NSDictionary* responseDict = [PatientDataParser dataObject:responseData];
             
             if ([responseDict[@"status"] isEqualToString:@"success"])
             {
                 NSArray* dataArray = responseDict[@"clinicinfo"];
                 NSMutableArray* clinicInfoArray = [[NSMutableArray alloc] init];
                 
                 for (NSDictionary* dict in dataArray)
                 {
                     ClinicInfoModel* obj = [[ClinicInfoModel alloc] initWithDictionary:dict];
                     [clinicInfoArray addObject:obj];
                 }
                 handler(clinicInfoArray,YES);
             }
             else
             {
                 handler(nil, YES);
             }
         }
     }];
}

+ (void) doctorClinicList:(NSString*)details WithCompletionHandler:(void (^)(NSArray *array , BOOL isSuccessful)) handler
{
    NSURLRequest* request = [WebServiceManager postRequestWithService:patientClinicInformationAPI withPayload:details];
    [WebServiceManager sendRequest:request completion:^(NSData* responseData , NSError* error)
     {
         if (responseData == nil || error)
         {
             handler(nil,NO);
         }
         else
         {
             NSDictionary* responseDict = [PatientDataParser dataObject:responseData];
             
             if ([responseDict[@"status"] isEqualToString:@"success"])
             {
                 NSArray* dataArray = responseDict[@"clinicinfo"];
                 NSMutableArray* clinicInfoArray = [[NSMutableArray alloc] init];
                 
                 for (NSDictionary* dict in dataArray)
                 {
                     PickerModel* obj = [[PickerModel alloc] initWithClinicDictionary:dict];
                     [clinicInfoArray addObject:obj];
                 }
                 handler(clinicInfoArray,YES);
             }
             else
             {
                 handler(nil, YES);
             }
         }
     }];
}



+ (void) termsAndConditions:(NSString*)details WithCompletionHandler:(void (^)(NSDictionary* dict , BOOL isSuccessful)) handler
{
    NSURLRequest* request = [WebServiceManager postRequestWithService:patientTermAndConditionAPI withPayload:details];
    [WebServiceManager sendRequest:request completion:^(NSData* responseData , NSError* error)
     {
         if (responseData == nil || error)
         {
             handler(nil,NO);
         }
         else
         {
             NSDictionary* responseDict = [PatientDataParser dataObject:responseData];
             
             if ([responseDict[@"status"] isEqualToString:@"true"])
             {
                 // LoginModel* obj = [[LoginModel alloc] initWithDictionary:responseDict[@"user"]];
                 _gDataManager.loginModel = [[LoginModel alloc] initWithDictionary:responseDict[@"user"]];
                 NSLog(@"login response=%@", responseDict);
                 handler(responseDict,YES);
             }
             else
             {
                 handler(nil, YES);
             }
         }
     }];
}


+ (void) getServiceListWithCompletionHandler:(void (^)(NSArray* array , BOOL isSuccessful)) handler
{
    NSURLRequest* request = [WebServiceManager requestWithServiceGET:serviceListAPI];
    [WebServiceManager sendRequest:request completion:^(NSData* responseData , NSError* error)
     {
         if (responseData == nil || error)
         {
             handler(nil,NO);
         }
         else
         {
             NSDictionary* responseDict = [PatientDataParser dataObject:responseData];
             if([responseDict[@"status"] isEqualToString:@"success"])
             {
                 NSArray* dataArray = responseDict[@"services"];
                 NSMutableArray* servicesArray = [[NSMutableArray alloc] init];
                 for (NSDictionary* dict in dataArray)
                 {
                     PickerModel* obj = [[PickerModel alloc] initWithServiceDictionary:dict];
                     [servicesArray addObject:obj];
                 }
                 handler(servicesArray, YES);
             }
             else
             {
                 handler(nil,YES);
             }
         }
     }];

}

+ (void) getBloodGroupsListWithCompletionHandler:(void (^)(NSArray* array , BOOL isSuccessful)) handler
{
    NSURLRequest* request = [WebServiceManager requestWithServiceGET:bloodGroupListAPI];
    [WebServiceManager sendRequest:request completion:^(NSData* responseData , NSError* error)
     {
         if (responseData == nil || error)
         {
             handler(nil,NO);
         }
         else
         {
             NSDictionary* responseDict = [PatientDataParser dataObject:responseData];
             if([responseDict[@"message"] isEqualToString:@"success"])
             {
                 NSArray* dataArray = responseDict[@"bloodgroups"];
                 NSMutableArray* bloodGroupsArray = [[NSMutableArray alloc] init];
                 for (NSDictionary* dict in dataArray)
                 {
                     PickerModel* obj = [[PickerModel alloc] initWithBloodGroupsDictionary:dict];
                     [bloodGroupsArray addObject:obj];
                 }
                 handler(bloodGroupsArray, YES);
             }
             else
             {
                 handler(nil,YES);
             }
         }
     }];
    
}

+ (void) PatientClinictViewHit:(NSString*)details WithCompletionHandler:(void (^)(NSMutableArray* array , BOOL isSuccessful)) handler{
    
    NSURLRequest* request = [WebServiceManager postRequestWithService:patientClinicViewAPI withPayload:details];
    [WebServiceManager sendRequest:request completion:^(NSData* responseData , NSError* error)
     {
         if (responseData == nil || error)
         {
             handler(nil,NO);
         }
         else
         {
             NSDictionary* responseDict = [PatientDataParser dataObject:responseData];
             NSLog(@" response=%@", responseDict);
             if([responseDict[@"message"] isEqualToString:@"success"])
             {
                 NSMutableArray *localArray = responseDict[@"datetime"];
                 
                 NSMutableArray* clinicViewArray = [[NSMutableArray alloc] init];
                 
                 for(int i=0;i<localArray.count;i++)
                 {
                     NSDictionary* parsedic = [localArray objectAtIndex:i];
                     PatientClinicModel* objCli = [[PatientClinicModel alloc] initWithDictionary:parsedic];
                     [clinicViewArray addObject:objCli];
                 }
                 
                 handler(clinicViewArray,YES);
                 
             }
             else{
                 handler(nil,YES);
                 
             }
         }
     }];
    
}



+ (id) dataObject:(NSData*)data
{
    return [ NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
}


@end
