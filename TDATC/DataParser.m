//
//  DataParser.m
//  DawaiBox
//
//Created by Vinay Kumar on 21/11/16.
//  Copyright © 2016 com.garry. All rights reserved.
#import "DataParser.h"
#import "Defines.h"
#import "WebServiceManager.h"
#import "AppointmentModel.h"
#import "AppointRequestModel.h"
#import "ClinicModel.h"
#import "AppointmentCancelModel.h"
#import "DoctorProfileModel.h"
#import "DataManager.h"
#import "DoctorMessage.h"
#import "DoctorFeedback.h"
#import "DoctorSentMessageModel.h"
#import "LoginModel.h"
#import "DataManager.h"
#import "PickerModel.h"
#import "DoctorMemberShipfeaturesModel.h"
#import "DoctorPatientModel.h"
#import "ClinicViewModel.h"



@implementation DataParser
// Get Slider image

// Add to cart list API call
+ (void) loginAPIHit:(NSString*)details WithCompletionHandler:(void (^)(NSDictionary* dict , BOOL isSuccessful)) handler
{
    NSURLRequest* request = [WebServiceManager postRequestWithService:loginAPI withPayload:details];
    [WebServiceManager sendRequest:request completion:^(NSData* responseData , NSError* error)
     {
         if (responseData == nil || error)
         {
             handler(nil,NO);
         }
         else
         {
             NSDictionary* responseDict = [DataParser dataObject:responseData];
             
             if ([responseDict[@"status"] isEqualToString:@"true"])
             {
               

                 _gDataManager.loginModel = [[LoginModel alloc] initWithDictionary:responseDict[@"user"]];
                 NSLog(@"login response=%@", responseDict);
                 handler(responseDict,YES);
             }
             else
             {
                 handler(nil, YES);
             }
         }
     }];
}
+ (void) twitterAPIHit:(NSString*)details WithCompletionHandler:(void (^)(NSDictionary* dict , BOOL isSuccessful)) handler{
    NSURLRequest* request = [WebServiceManager postRequestWithService:twitterApi withPayload:details];
    [WebServiceManager sendRequest:request completion:^(NSData* responseData , NSError* error)
     {
         if (responseData == nil || error)
         {
             handler(nil,NO);
         }
         else
         {
             NSDictionary* responseDict = [DataParser dataObject:responseData];
             
             if ([responseDict[@"status"] isEqualToString:@"success"])
             {
                 NSMutableArray *data=responseDict[@"message"];
                 
                 _gDataManager.loginModel = [[LoginModel alloc] initWithDictionary:[data objectAtIndex:0]];
                 
                 NSLog(@"twitter response=%@", [data objectAtIndex:0]);
                 handler(responseDict,YES);
             }
             else
             {
                 handler(nil, YES);
             }
         }
     }];
}
+ (void) facebookAPIHit:(NSString*)details WithCompletionHandler:(void (^)(NSDictionary* dict , BOOL isSuccessful)) handler
{
    NSURLRequest* request = [WebServiceManager postRequestWithService:faceBookApi withPayload:details];
    [WebServiceManager sendRequest:request completion:^(NSData* responseData , NSError* error)
     {
         if (responseData == nil || error)
         {
             handler(nil,NO);
         }
         else
         {
             NSDictionary* responseDict = [DataParser dataObject:responseData];
             
             if ([responseDict[@"status"] isEqualToString:@"success"])
             {
                 NSMutableArray *data=responseDict[@"message"];
                 
                 _gDataManager.loginModel = [[LoginModel alloc] initWithDictionary:[data objectAtIndex:0]];
                 
                 NSLog(@"facebook response=%@", [data objectAtIndex:0]);
                 handler(responseDict,YES);
             }
             else
             {
                 handler(nil, YES);
             }
         }
     }];
}

+ (void) signupAPI:(NSString*)details WithCompletionHandler:(void (^)(NSDictionary* dict , BOOL isSuccessful)) handler{
    NSURLRequest* request = [WebServiceManager postRequestWithService:signUpAPI withPayload:details];
    [WebServiceManager sendRequest:request completion:^(NSData* responseData , NSError* error)
     {
         if (responseData == nil || error)
         {
             handler(nil,NO);
         }
         else
         {
             NSDictionary* responseDict = [DataParser dataObject:responseData];
             NSLog(@"signupresponse=%@", responseDict);
             handler(responseDict,YES);
         }
     }];
}
+ (void) globalHitAPI:(NSString*)apiname detaiInfi:(NSString*)detail WithCompletionHandler:(void (^)(NSDictionary* dict , BOOL isSuccessful)) handler{
    
    NSURLRequest* request = [WebServiceManager postRequestWithService:apiname withPayload:detail];
    [WebServiceManager sendRequest:request completion:^(NSData* responseData , NSError* error)
     {
         if (responseData == nil || error)
         {
             handler(nil,NO);
         }
         else
         {
             NSDictionary* responseDict = [DataParser dataObject:responseData];
             NSLog(@"response=%@", responseDict);
             handler(responseDict,YES);
         }
     }];
    
}
+ (void) DappointmentDeleteListHit:(NSString*)details WithCompletionHandler:(void (^)(NSMutableArray* array , BOOL isSuccessful)) handler{
    
    NSURLRequest* request = [WebServiceManager postRequestWithService:doctorAppointmentCancelListAPI withPayload:details];
    [WebServiceManager sendRequest:request completion:^(NSData* responseData , NSError* error)
     {
         if (responseData == nil || error)
         {
             handler(nil,NO);
         }
         else
         {
             NSDictionary* responseDict = [DataParser dataObject:responseData];
             NSLog(@"response=%@", responseDict);
             if([responseDict[@"message"] isEqualToString:@"success"])
             {
                 NSMutableArray *localArray = responseDict[@"cancellist"];
                 
                 NSMutableArray* appointDeleteList = [[NSMutableArray alloc] init];
                 
                 for(int i=0;i<localArray.count;i++)
                 {
                     NSDictionary* parsedic = [localArray objectAtIndex:i];
                     AppointmentCancelModel* productObj = [[AppointmentCancelModel alloc] initWithDictionary:parsedic];
                     [appointDeleteList addObject:productObj];
                 }
                 
                 
                 handler(appointDeleteList,YES);
                 
             }
             else{
                 handler(nil,YES);
                 
             }
         }
     }];

    
}
+ (void) DappointmentListHit:(NSString*)details WithCompletionHandler:(void (^)(NSMutableArray* array , BOOL isSuccessful)) handler
{
    
    NSURLRequest* request = [WebServiceManager postRequestWithService:doctorAppointmentapprovedAPI withPayload:details];
    [WebServiceManager sendRequest:request completion:^(NSData* responseData , NSError* error)
     {
         if (responseData == nil || error)
         {
             handler(nil,NO);
         }
         else
         {
             NSDictionary* responseDict = [DataParser dataObject:responseData];
             NSLog(@"response=%@", responseDict);
             if([responseDict[@"message"] isEqualToString:@"success"])
             {
                 NSMutableArray *localArray = responseDict[@"aprove"];
                 
                 NSMutableArray* appointList = [[NSMutableArray alloc] init];
                 
                 for(int i=0;i<localArray.count;i++)
                 {
                     NSDictionary* parsedic = [localArray objectAtIndex:i];
                       AppointmentModel* productObj = [[AppointmentModel alloc] initWithDictionary:parsedic];
                     [appointList addObject:productObj];
                 }
                 

                 handler(appointList,YES);
                 
             }
             else{
                 handler(nil,YES);

             }
         }
     }];
    
}
+ (void) DappointmentRequestListHit:(NSString*)details WithCompletionHandler:(void (^)(NSMutableArray* array , BOOL isSuccessful)) handler{
    
    NSURLRequest* request = [WebServiceManager postRequestWithService:doctorAppointmentrequestAPI withPayload:details];
    [WebServiceManager sendRequest:request completion:^(NSData* responseData , NSError* error)
     {
         if (responseData == nil || error)
         {
             handler(nil,NO);
         }
         else
         {
             NSDictionary* responseDict = [DataParser dataObject:responseData];
             NSLog(@" response=%@", responseDict);
             if([responseDict[@"message"] isEqualToString:@"success"])
             {
                 NSMutableArray *localArray = responseDict[@"request"];
                 
                 NSMutableArray* appointRequestList = [[NSMutableArray alloc] init];
                 
                 for(int i=0;i<localArray.count;i++)
                 {
                     NSDictionary* parsedic = [localArray objectAtIndex:i];
                     AppointRequestModel* productObj = [[AppointRequestModel alloc] initWithDictionary:parsedic];
                     [appointRequestList addObject:productObj];
                 }
                 
                 
                 handler(appointRequestList,YES);
                 
             }
             else{
                 handler(nil,YES);
                 
             }
         }
     }];
    
}
+ (void) DappointmentPatienttListHit:(NSString*)details WithCompletionHandler:(void (^)(NSMutableArray* array , BOOL isSuccessful)) handler{
    
    NSURLRequest* request = [WebServiceManager postRequestWithService:doctorPatientListAPI withPayload:details];
    [WebServiceManager sendRequest:request completion:^(NSData* responseData , NSError* error)
     {
         if (responseData == nil || error)
         {
             handler(nil,NO);
         }
         else
         {
             NSDictionary* responseDict = [DataParser dataObject:responseData];
             NSLog(@" response=%@", responseDict);
             if([responseDict[@"message"] isEqualToString:@"success"])
             {
                 NSMutableArray *localArray = responseDict[@"plisting"];
                 
                 NSMutableArray* patientList = [[NSMutableArray alloc] init];
                 
                 for(int i=0;i<localArray.count;i++)
                 {
                     NSDictionary* parsedic = [localArray objectAtIndex:i];
                     DoctorPatientModel* productObj = [[DoctorPatientModel alloc] initWithDictionary:parsedic];
                     [patientList addObject:productObj];
                 }
                 
                 
                 handler(patientList,YES);
                 
             }
             else{
                 handler(nil,YES);
                 
             }
         }
     }];
    
}
+ (void) DappointmentClinictViewHit:(NSString*)details WithCompletionHandler:(void (^)(NSMutableArray* array , BOOL isSuccessful)) handler{
    
    NSURLRequest* request = [WebServiceManager postRequestWithService:doctorClinicViewAPI withPayload:details];
    [WebServiceManager sendRequest:request completion:^(NSData* responseData , NSError* error)
     {
         if (responseData == nil || error)
         {
             handler(nil,NO);
         }
         else
         {
             NSDictionary* responseDict = [DataParser dataObject:responseData];
             NSLog(@" response=%@", responseDict);
             if([responseDict[@"message"] isEqualToString:@"success"])
             {
                 NSMutableArray *localArray = responseDict[@"datetime"];
                 
                 NSMutableArray* clinicViewArray = [[NSMutableArray alloc] init];
                 
                 for(int i=0;i<localArray.count;i++)
                 {
                     NSDictionary* parsedic = [localArray objectAtIndex:i];
                     ClinicViewModel* objCli = [[ClinicViewModel alloc] initWithDictionary:parsedic];
                     [clinicViewArray addObject:objCli];
                 }
                 
                 handler(clinicViewArray,YES);
                 
             }
             else{
                 handler(nil,YES);
                 
             }
         }
     }];

    
}

+ (void) DappointmentClinictListHit:(NSString*)details WithCompletionHandler:(void (^)(NSMutableArray* array , BOOL isSuccessful)) handler{
    
    NSURLRequest* request = [WebServiceManager postRequestWithService:doctorClinicListAPI withPayload:details];
    [WebServiceManager sendRequest:request completion:^(NSData* responseData , NSError* error)
     {
         if (responseData == nil || error)
         {
             handler(nil,NO);
         }
         else
         {
             NSDictionary* responseDict = [DataParser dataObject:responseData];
             NSLog(@" response=%@", responseDict);
             if([responseDict[@"message"] isEqualToString:@"success"])
             {
                 NSMutableArray *localArray = responseDict[@"cliniclist"];
                 
                 NSMutableArray* clinicList = [[NSMutableArray alloc] init];
                 
                 for(int i=0;i<localArray.count;i++)
                 {
                     NSDictionary* parsedic = [localArray objectAtIndex:i];
                     ClinicModel* productObj = [[ClinicModel alloc] initWithDictionary:parsedic];
                     [clinicList addObject:productObj];
                 }
                 
                 
                 handler(clinicList,YES);
                 
             }
             else{
                 handler(nil,YES);
                 
             }
         }
     }];

    
}


// Doctor Dashboard Profile
+ (void) doctorDashboardProfile:(NSString*)userId WithCompletionHandler:(void(^)(NSDictionary* dict, BOOL isSuccessful))handler
{
    //NSString *strID=[NSString stringWithFormat:@"userID=%@",UserID];
    NSURLRequest* request = [WebServiceManager postRequestWithService:doctorProfileAPI withPayload:userId];
    [WebServiceManager sendRequest:request completion:^(NSData* responseData , NSError* error)
     {
         if (responseData == nil || error)
         {
             handler(nil,NO);
         }
         else
         {
             NSDictionary* responseDict = [DataParser dataObject:responseData];
             NSLog(@"Data from server=%@", responseDict);
             // NSMutableArray* configureDeviceList = [[NSMutableArray alloc] init];
             
             if([responseDict[@"status"] isEqualToString:@"true"])
             {
                 NSMutableArray *data=responseDict[@"profile"];
                 _gDataManager.doctorProfileImagePath=responseDict[@"imgpath"];

                 NSDictionary *dataDic=[data objectAtIndex:0];
                 DoctorProfileModel* doctor = [[DoctorProfileModel alloc] initWithDictionary:dataDic];
                 _gDataManager.doctorModel = doctor;
                 handler(responseDict,YES);
             }
             else
             {
                 
                 handler(nil,YES);
             }
         }
     }];
}

// doctor Appointment request API
+ (void) doctorMessageListing:(NSString*)userId WithCompletionHandler:(void(^)(NSArray* dict, BOOL isSuccessful))handler
{
    //NSString *strID=[NSString stringWithFormat:@"userID=%@",UserID];
    NSURLRequest* request = [WebServiceManager postRequestWithService:doctorMessageListingAPI withPayload:userId];
    [WebServiceManager sendRequest:request completion:^(NSData* responseData , NSError* error)
     {
         if (responseData == nil || error)
         {
             handler(nil,NO);
         }
         else
         {
             NSDictionary* responseDict = [DataParser dataObject:responseData];
             NSLog(@"Data from server in message=%@", responseDict);
             // NSMutableArray* configureDeviceList = [[NSMutableArray alloc] init];
             
             if([responseDict[@"status"] isEqualToString:@"success"])
             {
                 NSArray* dataArray = responseDict[@"message"];
                 _gDataManager.imgmsgpath = responseDict[@"imgmsgpath"];
                 _gDataManager.imgdoctorpath = responseDict[@"imgdoctorpath"];
                 _gDataManager.imgpatientpath = responseDict[@"imgpatientpath"];

                 NSMutableArray* messageArray = [[NSMutableArray alloc] init];
                 
                 for (NSDictionary *dict in dataArray)
                 {
                     DoctorMessage* message = [[DoctorMessage alloc] initWithDictionary:dict];
                     [messageArray addObject:message];
                 }
                 handler(messageArray,YES);
                 
             }
             else
             {
                 handler(nil,YES);
             }
         }
     }];
}


// doctor Appointment request API
+ (void) doctorFeedback:(NSString*)userId WithCompletionHandler:(void(^)(NSArray* array, BOOL isSuccessful))handler
{
    //NSString *strID=[NSString stringWithFormat:@"userID=%@",UserID];
    NSURLRequest* request = [WebServiceManager postRequestWithService:doctorFeedbackAPI withPayload:userId];
    [WebServiceManager sendRequest:request completion:^(NSData* responseData , NSError* error)
     {
         if (responseData == nil || error)
         {
             handler(nil,NO);
         }
         else
         {
             NSDictionary* responseDict = [DataParser dataObject:responseData];
             if([responseDict[@"message"] isEqualToString:@"success"])
             {
                 NSArray* dataArray = responseDict[@"feedback"];
                 NSMutableArray* feedbackArray = [[NSMutableArray alloc] init];
                 
                 for (NSDictionary *dict in dataArray)
                 {
                     DoctorFeedback* feedback =  [[DoctorFeedback alloc] initWithDictionary:dict];
                     [feedbackArray addObject:feedback];
                 }
                 handler(feedbackArray,YES);
             }
             else
             {
                 handler(nil,YES);
             }
         }
     }];
}

// doctor compose message API
+ (void) doctorComposeMsg:(NSString*)postData WithCompletionHandler:(void(^)(NSDictionary* dict, BOOL isSuccessful))handler
{
    //NSString *strID=[NSString stringWithFormat:@"userID=%@",UserID];
    NSURLRequest* request = [WebServiceManager postRequestWithService:doctorComposemsgAPI withPayload:postData];
    [WebServiceManager sendRequest:request completion:^(NSData* responseData , NSError* error)
     {
         if (responseData == nil || error)
         {
             handler(nil,NO);
         }
         else
         {
             NSDictionary* responseDict = [DataParser dataObject:responseData];
             
             
//             NSLog(@"Server data - %@", responseDict);
//             if([responseDict[@"status"] isEqualToString:@"success"])
//             {
//                 NSArray* dataArray = responseDict[@"feedback"];
//                 NSMutableArray* feedbackArray = [[NSMutableArray alloc] init];
//                 
//                 for (NSDictionary *dict in dataArray)
//                 {
//                     DoctorFeedback* feedback =  [[DoctorFeedback alloc] initWithDictionary:dict];
//                     [feedbackArray addObject:feedback];
//                 }
//                 handler(responseDict,YES);
//             }
                handler(responseDict,YES);

//             else
//             {
//                 handler(nil,YES);
//             }
         }
     }];
}


// doctor Message API
+ (void) doctorMsg:(NSString*)postData WithCompletionHandler:(void(^)(NSDictionary* dict, BOOL isSuccessful))handler
{
    //NSString *strID=[NSString stringWithFormat:@"userID=%@",UserID];
    NSURLRequest* request = [WebServiceManager postRequestWithService:doctorComposemsgAPI withPayload:postData];
    [WebServiceManager sendRequest:request completion:^(NSData* responseData , NSError* error)
     {
         if (responseData == nil || error)
         {
             handler(nil,NO);
         }
         else
         {
             NSDictionary* responseDict = [DataParser dataObject:responseData];
             if([responseDict[@"message"] isEqualToString:@"success"])
             {
                 NSArray* dataArray = responseDict[@"feedback"];
                 NSMutableArray* feedbackArray = [[NSMutableArray alloc] init];
                 
                 for (NSDictionary *dict in dataArray)
                 {
                     DoctorFeedback* feedback =  [[DoctorFeedback alloc] initWithDictionary:dict];
                     [feedbackArray addObject:feedback];
                 }
                 handler(responseDict,YES);
             }
             else
             {
                 handler(nil,YES);
             }
         }
     }];
}


// doctor Send Message request API
+ (void) doctorSentMsg:(NSString*)postData WithCompletionHandler:(void(^)(NSArray* array, BOOL isSuccessful))handler
{
    //NSString *strID=[NSString stringWithFormat:@"userID=%@",UserID];
    NSURLRequest* request = [WebServiceManager postRequestWithService:doctorSentmsgAPI withPayload:postData];
    [WebServiceManager sendRequest:request completion:^(NSData* responseData , NSError* error)
     {
         if (responseData == nil || error)
         {
             handler(nil,NO);
         }
         else
         {
             NSDictionary* responseDict = [DataParser dataObject:responseData];
             if([responseDict[@"message"] isEqualToString:@"true"])
             {
                 NSArray* dataArray = responseDict[@"status"];
                 _gDataManager.smsImagePath=responseDict[@"imgpath"];
                 NSMutableArray* messageArray = [[NSMutableArray alloc] init];
                 for (NSDictionary *dict in dataArray)
                 {
                     DoctorSentMessageModel* feedback =  [[DoctorSentMessageModel alloc] initWithDictionary:dict];
                     [messageArray addObject:feedback];
                 }
                 handler(messageArray,YES);
             }
             else
             {
                 handler(nil,YES);
             }
         }
     }];
}


// doctor Send Message request API
+ (void) updateDoctorProfileAPI:(NSString*)postData WithCompletionHandler:(void(^)(NSDictionary* dict, BOOL isSuccessful))handler
{
    //NSString *strID=[NSString stringWithFormat:@"userID=%@",UserID];
    NSURLRequest* request = [WebServiceManager postRequestWithService:doctorEditProfileAPI withPayload:postData];
    [WebServiceManager sendRequest:request completion:^(NSData* responseData , NSError* error)
     {
         if (responseData == nil || error)
         {
             handler(nil,NO);
         }
         else
         {
             NSDictionary* responseDict = [DataParser dataObject:responseData];
             NSLog(@"response data=%@",responseDict);
             handler(responseDict,YES);
//             if([responseDict[@"message"] isEqualToString:@"true"])
//             {
//                 
//                    handler(responseDict,YES);
//                 
//             }
//             else
//             {
//                 handler(nil,YES);
//             }
         }
     }];
}

// doctor Send Message request API
+ (void) updateDoctorFeedback:(NSString*)postData WithCompletionHandler:(void(^)(NSDictionary* dict, BOOL isSuccessful))handler
{
    //NSString *strID=[NSString stringWithFormat:@"userID=%@",UserID];
    NSURLRequest* request = [WebServiceManager postRequestWithService:doctorEditProfileAPI withPayload:postData];
    [WebServiceManager sendRequest:request completion:^(NSData* responseData , NSError* error)
     {
         if (responseData == nil || error)
         {
             handler(nil,NO);
         }
         else
         {
             NSDictionary* responseDict = [DataParser dataObject:responseData];
             if([responseDict[@"message"] isEqualToString:@"true"])
             {
                 handler(responseDict,YES);
             }
             else
             {
                 handler(nil,YES);
             }
         }
     }];
}

+ (void) getCountryListWithCompletionHandler:(void(^)(NSArray* array, BOOL isSuccessful))handler
{
    NSURLRequest* request = [WebServiceManager requestWithServiceGET:kCountriesList];
    [WebServiceManager sendRequest:request completion:^(NSData* responseData , NSError* error)
     {
         if (responseData == nil || error)
         {
             handler(nil,NO);
         }
         else
         {
             NSDictionary* responseDict = [DataParser dataObject:responseData];
             if([responseDict[@"status"] isEqualToString:@"success"])
             {
                 NSArray* dataArray = responseDict[@"countries"];
                 NSMutableArray* countriesArray = [[NSMutableArray alloc] init];
                 
                 for (NSDictionary* dict in dataArray)
                 {
                     PickerModel* obj = [[PickerModel alloc] initWithCountriesDictionary:dict];
                     [countriesArray addObject:obj];
                 }
                 
                 handler(countriesArray,YES);
             }
             else
             {
                 handler(nil,YES);
             }
         }
     }];

}

+ (void) getStateList:(NSString*)countryId WithCompletionHandler:(void(^)(NSArray* array, BOOL isSuccessful))handler
{
    NSURLRequest* request = [WebServiceManager postRequestWithService:kStateListAPI withPayload:countryId];
    [WebServiceManager sendRequest:request completion:^(NSData* responseData , NSError* error)
     {
         if (responseData == nil || error)
         {
             handler(nil,NO);
         }
         else
         {
             NSDictionary* responseDict = [DataParser dataObject:responseData];
             if([responseDict[@"status"] isEqualToString:@"success"])
             {
                 NSArray* dataArray = responseDict[@"states"];
                 NSMutableArray* statesArray = [[NSMutableArray alloc] init];
                 for (NSDictionary* dict in dataArray)
                 {
                     PickerModel* obj = [[PickerModel alloc] initWithStatesDictionary:dict];
                     [statesArray addObject:obj];
                 }
                 handler(statesArray, YES);
             }

             else
             {
                 handler(nil,YES);
             }
         }
     }];
    
}

+ (void) getCityList:(NSString*)stateId WithCompletionHandler:(void(^)(NSArray* array, BOOL isSuccessful))handler
{
    NSURLRequest* request = [WebServiceManager postRequestWithService:kCitiesListAPI withPayload:stateId];
    [WebServiceManager sendRequest:request completion:^(NSData* responseData , NSError* error)
     {
         if (responseData == nil || error)
         {
             handler(nil,NO);
         }
         else
         {
             NSDictionary* responseDict = [DataParser dataObject:responseData];
             if([responseDict[@"status"] isEqualToString:@"success"])
             {
                 NSArray* dataArray = responseDict[@"cities"];
                 NSMutableArray* citiesArray = [[NSMutableArray alloc] init];
                 for (NSDictionary* dict in dataArray)
                 {
                     PickerModel* obj = [[PickerModel alloc] initWithCitiesDictionary:dict];
                     [citiesArray addObject:obj];
                 }
                 handler(citiesArray, YES);
             }
             else
             {
                 handler(nil,YES);
             }
         }
     }];
}


+ (void) getCurrnentMemberShip:(NSString*)doctorId WithCompletionHandler:(void(^)(NSDictionary* dict, BOOL isSuccessful))handler
{
    NSURLRequest* request = [WebServiceManager postRequestWithService:kDoctorCurrnentMemberShip withPayload:doctorId];
    [WebServiceManager sendRequest:request completion:^(NSData* responseData , NSError* error)
     {
         if (responseData == nil || error)
         {
             handler(nil,NO);
         }
         else
         {
             NSDictionary* responseDict = [DataParser dataObject:responseData];
             if([responseDict[@"message"] isEqualToString:@"success"])
             {
                 NSMutableArray* plansArray = responseDict[@"plans"];
                 for (NSDictionary* dict in plansArray)
                 {
                     handler(dict , YES);
                 }
             }
             else
             {
                 handler(nil,YES);
             }
         }
     }];
}
+ (void) getMemberShipFeatureWithCompletionHandler:(void(^)(NSArray* array, BOOL isSuccessful))handler{
    NSURLRequest* request = [WebServiceManager requestWithServiceGET:kDoctorCurrnentMemberShipFeature];
    [WebServiceManager sendRequest:request completion:^(NSData* responseData , NSError* error)
     {
         if (responseData == nil || error)
         {
             handler(nil,NO);
         }
         else
         {
             NSDictionary* responseDict = [DataParser dataObject:responseData];
             if([responseDict[@"message"] isEqualToString:@"success"])
             {
                 NSArray* dataArray = responseDict[@"features"];
                 NSMutableArray* memberShipFeaturesArray = [[NSMutableArray alloc] init];
                 for (NSDictionary* dict in dataArray)
                 {
                     if (![dict[@"membership_right"] isKindOfClass:[NSNull class]])
                     {
                         DoctorMemberShipfeaturesModel* obj = [[DoctorMemberShipfeaturesModel alloc] initWithDictionary:dict];
                         [memberShipFeaturesArray addObject:obj];
                     }
                 }
                 handler(memberShipFeaturesArray, YES);
             }
             else
             {
                 handler(nil,YES);
             }
         }
     }];

}

+ (void) getMemberShipTypeWithCompletionHandler:(void(^)(NSArray* array, BOOL isSuccessful))handler
{
    NSURLRequest* request = [WebServiceManager requestWithServiceGET:kMemberShipDetailsAPI];
    //NSURLRequest* request = [WebServiceManager postRequestWithService:kMemberShipDetailsAPI withPayload:stateId];
    [WebServiceManager sendRequest:request completion:^(NSData* responseData , NSError* error)
     {
         if (responseData == nil || error)
         {
             handler(nil,NO);
         }
         else
         {
             NSDictionary* responseDict = [DataParser dataObject:responseData];
             if([responseDict[@"message"] isEqualToString:@"success"])
             {
                 NSArray* dataArray = responseDict[@"membersipplan"];
                 NSMutableArray* memberShipTypeArray = [[NSMutableArray alloc] init];
                 for (NSDictionary* dict in dataArray)
                 {
                     PickerModel* obj = [[PickerModel alloc] initWithMemberShipPlans:dict];
                     [memberShipTypeArray addObject:obj];
                 }
                 handler(memberShipTypeArray, YES);
             }
             else
             {
                 handler(nil,YES);
             }
         }
     }];
}



+ (id) dataObject:(NSData*)data
{
    return [ NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
}



@end
