//
//  ForgetPasswordViewController.m
//  TDATC
//
//  Created by iWeb on 9/22/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "ForgetPasswordViewController.h"
#import "DataManager.h"
#import "Utility.h"
#import "MyLoader.h"
#import "DataParser.h"
#import "Defines.h"


@interface ForgetPasswordViewController ()

@end

@implementation ForgetPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)submit_Touch:(id)sender{
    if([self validateUserData])
    {
        [self forgetAPI];
    }
}
-(void)forgetAPI
{
    NSString *postData=[NSString stringWithFormat:@"email=%@",txt_Email.text];
    
    [MyLoader showLoadingView];
    [DataParser globalHitAPI:korgotPasswordAPI detaiInfi:postData WithCompletionHandler:^(NSDictionary *dict, BOOL isSuccessful)
     {
         if (isSuccessful)
         {
             [MyLoader hideLoadingView];
             
             if([dict[@"status"] isEqualToString:@"false"])
             {
                 [Utility alertWithTitle:kAlertTitle withMessage:dict[@"message"] onVC:self];
                 
                 //Ankur
             }
             else
             {
                 
                 [Utility alertWithTitle:kAlertTitle withMessage:dict[@"message"] onVC:self];
             }
         }
         else
         {
             [MyLoader hideLoadingView];
             [Utility alertWithTitle:@"" withMessage:kNetworkErrorTitle onVC:self];
         }
     }];
    
    
    
}
- (BOOL) validateUserData
{
    
    NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    
    if ([txt_Email.text isEqualToString:@" "]|| [[txt_Email.text stringByTrimmingCharactersInSet:whitespace] length] == 0  )
    {
        [Utility alertWithTitle:@"" withMessage:@"Please enter Eamil ID." onVC:self];
        return NO;
    }
    if([Utility emailValidation:txt_Email.text])
    {
        
    }
    else
    {
        [Utility alertWithTitle:@"" withMessage:@"Please enter valid email address" onVC:self];
        return NO;
    }
    
    
    
    
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
