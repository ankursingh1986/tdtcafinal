//
//  DoctorMessage.m
//  TDATC
//
//  Created by Vinay on 14/10/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "DoctorMessage.h"

@implementation DoctorMessage

- (id) initWithDictionary:(NSDictionary*)dictionary
{
    self = [super init];
    if (self)
    {
        if (![dictionary[@"subject"] isKindOfClass:[NSNull class]])
            self.subject = dictionary[@"subject"];
        else
            self.subject = @"";
        if (![dictionary[@"doctor_msg"] isKindOfClass:[NSNull class]])
        {
            NSString* result = [dictionary[@"doctor_msg"] stringByReplacingOccurrencesOfString:@"<p>" withString:@""];
            result = [result stringByReplacingOccurrencesOfString:@"</p>" withString:@""];
            self.doctor_msg = result;
        }
        else
        {
            self.doctor_msg =@"";
            
        }
        if (![dictionary[@"date"] isKindOfClass:[NSNull class]])
            self.date = dictionary[@"date"];
        else
            self.date = @"";
        
        self.msg_id = dictionary[@"msg_id"];
        self.patient_id = dictionary[@"us"][@"user_id"];
        
        if (![dictionary[@"attacmet"][@"filename"] isKindOfClass:[NSNull class]])
            self.attchment =[dictionary[@"attacmet"][@"filename"] stringByReplacingOccurrencesOfString:@" " withString:@"%20"] ;
        else
            self.attchment = @"";
        
        self.patientimag_path = [dictionary[@"us"][@"image"] stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        self.full_name = dictionary[@"us"][@"full_name"];
        NSString *genderStr = [[NSString stringWithFormat:@"%@",dictionary[@"us"][@"gender"]] stringByReplacingOccurrencesOfString:@"<null>" withString:@"Male"];

        self.gender =genderStr;

    }
    return self;
}

@end
