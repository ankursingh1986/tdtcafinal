//
//  DoctorClinicList_VC.h
//  TDATC
//
//  Created by iWeb on 9/18/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDWebImageManager.h"
#import "Global.pch"


@interface DoctorClinicList_VC : UIViewController
{
    IBOutlet UITableView *tbl_Clinic;
    SDWebImageManager *image_manager;

}

@end
