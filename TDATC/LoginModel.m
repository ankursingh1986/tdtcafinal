//
//  LoginModel.m
//  TDATC
//
//  Created by iWeb on 10/24/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "LoginModel.h"

@implementation LoginModel

- (id) initWithDictionary:(NSDictionary*)dictionary
{
    self = [super init];
    if (self)
    {
        self.user_id = dictionary[@"user_id"];
        if (![dictionary[@"username"] isKindOfClass:[NSNull class]])
            self.username = dictionary[@"username"];
        else
            self.username = @"";
        
        // self.username = dictionary[@"username"];
        
        if (![dictionary[@"b_id"] isKindOfClass:[NSNull class]])
            self.b_id = dictionary[@"b_id"];
        else
            self.b_id = @"";
        
        //self.b_id = dictionary[@"b_id"];
        
        if (![dictionary[@"full_name"] isKindOfClass:[NSNull class]])
            self.full_name = dictionary[@"full_name"];
        else
            self.full_name = @"";
        // self.full_name = dictionary[@"full_name"];
        
        if (![dictionary[@"role"] isKindOfClass:[NSNull class]])
            self.role = dictionary[@"role"];
        else
            self.role = @"";
        //self.role = dictionary[@"role"];
        
        if (![dictionary[@"image"] isKindOfClass:[NSNull class]])
            self.image = dictionary[@"image"];
        else
            self.image = @"";
        
        //self.image = dictionary[@"image"];
        if (![dictionary[@"dob"] isKindOfClass:[NSNull class]])
            self.dob = dictionary[@"dob"];
        else
            self.dob = @"";
        
        if (![dictionary[@"mobile_number"] isKindOfClass:[NSNull class]])
            self.mobile_number = dictionary[@"mobile_number"];
        else
            self.mobile_number = @"";
        //self.mobile_number = dictionary[@"mobile_number"];
        
        if (![dictionary[@"alternate_mobile_number"] isKindOfClass:[NSNull class]])
            self.alternate_mobile_number = dictionary[@"alternate_mobile_number"];
        else
            self.alternate_mobile_number = @"";
        
        if (![dictionary[@"email"] isKindOfClass:[NSNull class]])
            self.email = dictionary[@"email"];
        else
            self.email = @"";
        //self.email = dictionary[@"email"];
        if (![dictionary[@"zoom_id"] isKindOfClass:[NSNull class]])
            self.zoom_id = dictionary[@"zoom_id"];
        else
            self.zoom_id = @"";
        //self.zoom_id = dictionary[@"zoom_id"];
        
        if (![dictionary[@"zoom_status"] isKindOfClass:[NSNull class]])
            self.zoom_status = dictionary[@"zoom_status"];
        else
            self.zoom_status = @"";
        //self.zoom_status = dictionary[@"zoom_status"];
        
        if (![dictionary[@"user_status"] isKindOfClass:[NSNull class]])
            self.zoom_vedio_url_id = dictionary[@"zoom_vedio_url_id"];
        else
            self.zoom_vedio_url_id = @"";
        //self.zoom_vedio_url_id = dictionary[@"zoom_vedio_url_id"];
        
        
        if (![dictionary[@"user_status"] isKindOfClass:[NSNull class]])
            self.user_status = dictionary[@"user_status"];
        else
            self.user_status = @"";
        
        if (![dictionary[@"gender"] isKindOfClass:[NSNull class]])
            self.gender = dictionary[@"gender"];
        else
            self.gender = @"";
        
        if (![dictionary[@"country_id"] isKindOfClass:[NSNull class]])
            self.country_id = dictionary[@"country_id"];
        else
            self.country_id = @"";
        
        //self.country_id = dictionary[@"country_id"];
        if (![dictionary[@"state_id"] isKindOfClass:[NSNull class]])
            self.state_id = dictionary[@"state_id"];
        else
            self.state_id = @"";
        //self.state_id = dictionary[@"state_id"];
        
        if (![dictionary[@"city_id"] isKindOfClass:[NSNull class]])
            self.city_id = dictionary[@"city_id"];
        else
            self.city_id = @"";
        //self.city_id = dictionary[@"city_id"];
        if (![dictionary[@"facebook_id"] isKindOfClass:[NSNull class]])
            self.facebook_id = dictionary[@"facebook_id"];
        else
            self.facebook_id = @"";
        
        if (![dictionary[@"twitter_id"] isKindOfClass:[NSNull class]])
            self.twitter_id = dictionary[@"twitter_id"];
        else
            self.twitter_id = @"";
        
        if (![dictionary[@"locality"] isKindOfClass:[NSNull class]])
            self.locality = dictionary[@"locality"];
        else
            self.locality = @"";
        
        //self.locality = dictionary[@"locality"];
        
        if (![dictionary[@"pincode"] isKindOfClass:[NSNull class]])
            self.pincode = dictionary[@"pincode"];
        else
            self.pincode = @"";
        //self.pincode = dictionary[@"pincode"];
        
        //0511952348
    }
    return self;
}

@end
