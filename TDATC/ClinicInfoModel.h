//
//  ClinicInfoModel.h
//  TDATC
//
//  Created by Vinay on 02/11/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ClinicInfoModel : NSObject


@property (nonatomic, strong) NSString* clinic_id;
@property (nonatomic, strong) NSString* clicnic_name;
@property (nonatomic, strong) NSString* locality;
@property (nonatomic, strong) NSString* pincode;
@property (nonatomic, strong) NSString* fees;
@property (nonatomic, strong) NSString* avaliabeDayArray;

@property (readwrite, assign) BOOL status;



- (id) initWithDictionary:(NSDictionary*)dictionary;

@end
