//
//  DoctorMembership_VC.m
//  TDATC
//
//  Created by iWeb on 9/19/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "DoctorMembership_VC.h"
#import "DoctorActivePlan_VC.h"
#import "DoctorChangePlan_VC.h"
#import "DoctorUpdateMember_VC.h"
#import "DataManager.h"
#import "Utility.h"
#import "MyLoader.h"
#import "DataParser.h"
#import "Defines.h"

@interface DoctorMembership_VC ()
{
    NSDictionary *localDic;
}

@end

@implementation DoctorMembership_VC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view_head.layer setCornerRadius:10.0f];
    
    // border
    [self.view_head.layer setBorderColor:[UIColor whiteColor].CGColor];
    [self.view_head.layer setBorderWidth:1.5f];
    
    // drop shadow
    [self.view_head.layer setShadowColor:[UIColor blueColor].CGColor];
    [self.view_head.layer setShadowOpacity:0.8];
    [self.view_head.layer setShadowRadius:3.0];
    [self.view_head.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    [self leftButton];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationItem.title = @"MEMBERSHIP";
    [self getCurrentMemberShipPlan];
}

- (void) getCurrentMemberShipPlan
{
    [MyLoader showLoadingView];
   
   //   [DataParser getCurrnentMemberShip:[NSString stringWithFormat:@"id=159"] WithCompletionHandler:^(NSDictionary *dict, BOOL isSuccessful)
     [DataParser getCurrnentMemberShip:[NSString stringWithFormat:@"id=%@",_gDataManager.loginModel.user_id] WithCompletionHandler:^(NSDictionary *dict, BOOL isSuccessful)
     {
         [MyLoader hideLoadingView];
         if (isSuccessful)
         {
             localDic=dict;
             _gDataManager.memberShip_id=[NSString stringWithFormat:@"%@",dict[@"id"]];
             if ([[NSString stringWithFormat:@"%@",dict[@"plan_id"]] isEqualToString:@"1"])
             {
                  self.currentMemberShipTypeLabel.text = @"BASIC";
             }
             else if([[NSString stringWithFormat:@"%@",dict[@"plan_id"]] isEqualToString:@"2"])
             {
                  self.currentMemberShipTypeLabel.text = @"GOLD";
             }
             else if([[NSString stringWithFormat:@"%@",dict[@"plan_id"]] isEqualToString:@"3"])
             {
                  self.currentMemberShipTypeLabel.text = @"DIAMOND";
             }
             
             self.memberShipYearType.text = [dict[@"plan"] uppercaseString];
             self.memberShipPriceTextField.text = [NSString stringWithFormat:@"$%@",dict[@"price"]];
             
             NSString* startDate = [Utility changeDateFormat:dict[@"start_date"]];
             NSString* endDate = [Utility changeDateFormat:dict[@"end_date"]];
             
             self.startDateTextField.text = startDate;
             self.endDateTextField.text = endDate;
         }
         else
         {
             [Utility alertWithTitle:kAlertTitle withMessage:kNetworkErrorTitle onVC:self];
         }
        
     }];
}

-(void)makegradient
{
    self.view_head.layer.cornerRadius=4;
    
    self.view_head.layer.masksToBounds=YES;
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = self.view_head.bounds;
    
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:27/255.0 green:74/255.0 blue:245/255.0 alpha:.8] CGColor], (id)[[UIColor colorWithRed:27/255.0 green:74/255.0 blue:245/255.0 alpha:0.6] CGColor],(id)[[UIColor colorWithRed:27/255.0 green:74/255.0 blue:245/255.0 alpha:.2] CGColor], nil];
    
   // gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:27/255.0 green:74/255.0 blue:245/255.0 alpha:.4] CGColor], (id)[[UIColor colorWithRed:27/255.0 green:74/255.0 blue:245/255.0 alpha:0.1] CGColor], nil];
    
    gradient.startPoint = CGPointMake(0.0, 1.0);
    
    gradient.endPoint = CGPointMake(0.0, 0.0);
    
    CAShapeLayer *shapeLayer =[[CAShapeLayer alloc] init];
    
    shapeLayer.lineWidth = 8; // higher number higher border width
    
    shapeLayer.path = [UIBezierPath bezierPathWithRect:self.view_head.bounds].CGPath;
    
    shapeLayer.fillColor = nil;
    
    shapeLayer.strokeColor = [UIColor blackColor].CGColor;
    
    gradient.mask = shapeLayer;
    
    [self.view_head.layer insertSublayer:gradient atIndex:0];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


- (void) leftButton
{
    UIButton *button =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(backButtonAction:)forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0, 20, 20)];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = barButton;
    
}
- (IBAction) backButtonAction:(id)sender
{
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
    [self presentViewController:vc animated:NO completion:nil];
    
}

- (IBAction)touch_ViewPlan:(id)sender
{
    DoctorActivePlan_VC* destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"DoctorActivePlan_VC_id"];
    [self.navigationController pushViewController:destVC animated:YES];
}

- (IBAction)touch_RenewPlan:(id)sender
{
    DoctorUpdateMember_VC* destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"DoctorUpdateMember_VC_id"];
    [self.navigationController pushViewController:destVC animated:YES];
}

- (IBAction)touch_changeplan:(id)sender
{

        DoctorChangePlan_VC* destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"DoctorChangePlan_VC_id"];
        destVC.dataDic=localDic;
        [self.navigationController pushViewController:destVC animated:YES];
}

@end
