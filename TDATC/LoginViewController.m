//
//  ViewController.m
//  TDATC
//
//  Created by iWeb on 9/13/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "LoginViewController.h"
#import "DataManager.h"
#import "Utility.h"
#import "MyLoader.h"
#import "DataParser.h"
#import "Defines.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    autologin=1;
    [super viewDidLoad];
   
    [btn setImage:[UIImage imageNamed:@"tick"] forState:UIControlStateNormal];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)login_Touch:(id)sender{
    
    if([self validateUserData])
    {
        [self logiAPI];
    }
  }

- (IBAction)facebook:(id)sender {
    
            UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:@"Choose Account type" preferredStyle:UIAlertControllerStyleActionSheet];
            
            [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                                    {
                                        // Cancel button tappped do nothing.
                                        
                                    }]];
            
            [actionSheet addAction:[UIAlertAction actionWithTitle:@"Login as Doctor" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                    {
                                        doctorFlag=2;
                                        [self facebookLogin];
                                    }]];
            
            [actionSheet addAction:[UIAlertAction actionWithTitle:@"Login as Patient" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                    {
                                        doctorFlag=3;
                                        [self facebookLogin];
                                    }]];
            [self presentViewController:actionSheet animated:YES completion:nil];
        
}
-(void)facebookLogin{
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    
    
    //  [login logOut];
    
    
    [login logInWithReadPermissions:@[@"public_profile",@"user_birthday",@"email"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error)
     {
         if (error)
         {
             NSLog(@"error from fb=%@",error);
             [Utility alertWithTitle:@"Alert!" withMessage:@"Some thing went wrong. Please try again." onVC:self];
         }
         else if (result.isCancelled)
         {
             // Handle cancellations
         }
         else
         {
             if ([result.grantedPermissions containsObject:@"email"])
             {
                 //NSLog(@"result is:%@",result);
                 [self fetchUserInfo];
             }
         }
     }];

}
-(void)fetchUserInfo
{
    if ([FBSDKAccessToken currentAccessToken])
    {
        NSLog(@"Token is available : %@",[[FBSDKAccessToken currentAccessToken]tokenString]);
        
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, picture, email, birthday,gender"}]//picture.type(large)
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error)
         {
             if (!error)
             {
                 //NSLog(@"result is:%@",result);
                 NSString *tokenString=[NSString stringWithFormat:@"%@",[[FBSDKAccessToken currentAccessToken]tokenString]];
                 [self postSocialMediaDataToServer:result withaccessToken:tokenString];
             }
             else
             {
                 NSLog(@"Error from FB %@",error);
                 
                 [Utility alertWithTitle:@"Alert!" withMessage:@"Some thing went wrong. Please try again." onVC:self];
                 
             }
         }];
        
    }
}

#pragma mark - Posting social media data on server

- (void) postSocialMediaDataToServer:(NSDictionary*)dict withaccessToken:(NSString *)accessToken
{
    NSLog(@"fb return=%@ %@",dict[@"email"],dict[@"name"]);
    NSString *userName = dict[@"name"];
    NSArray *nameArray = [userName componentsSeparatedByString:@" "];
    NSString* firstName,*lastName,*email,*strID;
    
    if (nameArray.count)
        firstName = nameArray[0];
    
    if (nameArray.count > 1)
        lastName = nameArray[1];
    else
        lastName = @"";
    
    if (dict[@"email"])
        email = dict[@"email"];
    else
        email = @"";
    
    strID =dict[@"id"];
    
    
    NSLog(@"email=%@ %@",email,dict[@"name"]);
    
    [MyLoader showLoadingView];
  
    NSString *postData=[NSString stringWithFormat:@"facebook_id=%@&role=%d&email=iwsdev@gmail.com&fullname=%@",strID,doctorFlag,dict[@"name"]];
    
    NSLog(@"post data=%@",postData);
    
    [MyLoader showLoadingView];
  
    
    [DataParser facebookAPIHit:postData WithCompletionHandler:^(NSDictionary *dict, BOOL isSuccessful)
     {
         if (isSuccessful)
         {
             [MyLoader hideLoadingView];
             
             if([dict[@"status"] isEqualToString:@"success"])
             {
                 NSMutableArray *data=dict[@"message"];
                 
                 NSDictionary *dict1=[data objectAtIndex:0];
                 
                 if([dict1[@"role"] isEqualToString:[NSString stringWithFormat:@"%d",doctorFlag]])
                 {
                     if([dict1[@"user_status"] isEqualToString:@"Inactive"])
                     {
                            [Utility alertWithTitle:kAlertTitle withMessage:@"This account is not active.Please contact to admin." onVC:self];

                     }
                     else
                     {
                         NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
                         [defaults setObject:postData forKey:@"userLoginInfo"];
                         
                         [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"KHome"];
                         [[NSUserDefaults standardUserDefaults]synchronize];
                         
                         [defaults setObject:@"1" forKey:@"socialLogin"];
                         [defaults synchronize];
                         
                         if(doctorFlag==2 )
                         {
                             NSLog(@"Enter in doctor");
                             [[UINavigationBar appearance] setTranslucent:NO];
                             
                             [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:215.0f/255.0f green:29.0f/255.0f blue:32.0f/255.0f alpha:1.0]];
                             
                             UIStoryboard *storyboard1 = [UIStoryboard storyboardWithName:@"Main"
                                                                                   bundle: nil];
                             
                             UINavigationController *loginNavigationController = (UINavigationController*)[storyboard1 instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
                             [self presentViewController:loginNavigationController animated:NO completion:nil];
                             
                             
                         }
                         else
                         {
                             NSLog(@"Enter in Patient");
                             [[UINavigationBar appearance] setTranslucent:NO];
                             
                             [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:5.0f/255.0f green:52.0f/255.0f blue:222.0f/255.0f alpha:1.0]];
                             
                             UIStoryboard *storyboard1 = [UIStoryboard storyboardWithName:@"Patient"
                                                                                   bundle: nil];
                             
                             UINavigationController *loginNavigationController = (UINavigationController*)[storyboard1 instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
                             [self presentViewController:loginNavigationController animated:NO completion:nil];
                             
                             
                             
                         }
                     }
                }
                 else
                 {
                       [Utility alertWithTitle:kAlertTitle withMessage:@"This user already register." onVC:self];
                 }


             }
             else
             {
                 

             }
         }
         else
         {
             [MyLoader hideLoadingView];
             [Utility alertWithTitle:@"" withMessage:kNetworkErrorTitle onVC:self];
         }
     }];
    

}

- (IBAction)twitter:(id)sender {
    
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:@"Choose Account type" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                            {
                                // Cancel button tappped do nothing.
                                
                            }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Login as Doctor" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                            {
                                doctorFlag=2;
                                [self twitterLogin];
                            }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Login as Patient" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                            {
                                doctorFlag=3;
                                [self twitterLogin];
                            }]];
    [self presentViewController:actionSheet animated:YES completion:nil];

    
}

- (IBAction)check_Touch:(id)sender {
    
    
    btn.selected = !btn.selected;
    if(btn.selected)
    {
        autologin=0;
        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"KHome"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [btn setImage:[UIImage imageNamed:@"untick"] forState:UIControlStateNormal];
    }
    else
    {
        autologin=1;
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"KHome"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [btn setImage:[UIImage imageNamed:@"tick"] forState:UIControlStateNormal];
        
    }

}
-(void)twitterLogin{
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    
    //Get Twitter account, stored in on the device, for the first time.
    [appDelegate getTwitterAccountOnCompletion:^(ACAccount *twitterAccount){
        //If we successfully retrieved a Twitter account
        if(twitterAccount) {
            //Make sure anything UI related happens on the main queue
            dispatch_async(dispatch_get_main_queue(), ^{
                NSDictionary *tempDict = [[NSMutableDictionary alloc] initWithDictionary:
                                          [twitterAccount dictionaryWithValuesForKeys:[NSArray arrayWithObject:@"properties"]]];
                NSString *name = [[tempDict objectForKey:@"properties"] objectForKey:@"fullName"];
                
                NSString *userid = [[tempDict objectForKey:@"properties"] objectForKey:@"user_id"];
                
                
                
                
                
                NSLog(@"name=%@",name);
                NSLog(@"user id=%@",userid);
                
                
                NSString *postData=[NSString stringWithFormat:@"facebook_id=%@&role=%d&email=%@@gmail.com&fullname=%@",userid,doctorFlag,name,name];
                
                
                
                NSLog(@"post data=%@",postData);
                
                [MyLoader showLoadingView];
                
                
                [DataParser facebookAPIHit:postData WithCompletionHandler:^(NSDictionary *dict, BOOL isSuccessful)
                 {
                     if (isSuccessful)
                     {
                         [MyLoader hideLoadingView];
                         
                         if([dict[@"status"] isEqualToString:@"success"])
                         {
                             NSMutableArray *data=dict[@"message"];
                             
                             NSDictionary *dict1=[data objectAtIndex:0];
                             
                             if([dict1[@"role"] isEqualToString:[NSString stringWithFormat:@"%d",doctorFlag]])
                             {
                                 if([dict1[@"user_status"] isEqualToString:@"Inactive"])
                                 {
                                     [Utility alertWithTitle:kAlertTitle withMessage:@"This account is not active.Please contact to admin." onVC:self];
                                     
                                 }
                                 else
                                 {
                                     NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
                                     [defaults setObject:postData forKey:@"userLoginInfo"];
                                     
                                     [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"KHome"];
                                     [[NSUserDefaults standardUserDefaults]synchronize];
                                     
                                     [defaults setObject:@"1" forKey:@"socialLogin"];
                                     [defaults synchronize];
                                     
                                     if(doctorFlag==2 )
                                     {
                                         NSLog(@"Enter in doctor");
                                         [[UINavigationBar appearance] setTranslucent:NO];
                                         
                                         [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:215.0f/255.0f green:29.0f/255.0f blue:32.0f/255.0f alpha:1.0]];
                                         
                                         UIStoryboard *storyboard1 = [UIStoryboard storyboardWithName:@"Main"
                                                                                               bundle: nil];
                                         
                                         UINavigationController *loginNavigationController = (UINavigationController*)[storyboard1 instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
                                         [self presentViewController:loginNavigationController animated:NO completion:nil];
                                         
                                         
                                     }
                                     else
                                     {
                                         NSLog(@"Enter in Patient");
                                         [[UINavigationBar appearance] setTranslucent:NO];
                                         
                                         [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:5.0f/255.0f green:52.0f/255.0f blue:222.0f/255.0f alpha:1.0]];
                                         
                                         UIStoryboard *storyboard1 = [UIStoryboard storyboardWithName:@"Patient"
                                                                                               bundle: nil];
                                         
                                         UINavigationController *loginNavigationController = (UINavigationController*)[storyboard1 instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
                                         [self presentViewController:loginNavigationController animated:NO completion:nil];
                                         
                                         
                                         
                                     }
                                 }
                             }
                             else
                             {
                                 [Utility alertWithTitle:kAlertTitle withMessage:@"This user already register." onVC:self];
                             }
                             
                             
                         }
                         else
                         {
                             
                             
                         }
                     }
                     else
                     {
                         [MyLoader hideLoadingView];
                         [Utility alertWithTitle:@"" withMessage:kNetworkErrorTitle onVC:self];
                     }
                 }];

                
                
                
                
                
                
                
                
                
            });
        }
    }];
    
}
-(void)logiAPI
{
    NSString *postData=[NSString stringWithFormat:@"username=%@&password=%@",txt_email.text,txt_password.text];
    
    [MyLoader showLoadingView];
    [DataParser loginAPIHit:postData WithCompletionHandler:^(NSDictionary *dict, BOOL isSuccessful)
     {
         if (isSuccessful)
         {
             [MyLoader hideLoadingView];
             
             if([dict[@"status"] isEqualToString:@"false"])
             {
                 [Utility alertWithTitle:kAlertTitle withMessage:dict[@"user"] onVC:self];
                 
                 //Ankur
             }
             else
             {
//                 NSLog(@"user id=%@",dict[@"user"][@"user_id"]);
                 NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
                 [defaults setObject:postData forKey:@"userLoginInfo"];
                 
                 
                 [defaults setObject:@"0" forKey:@"socialLogin"];
                 
                 
                 
                 [defaults synchronize];
                 
                 
                 
                 if([dict[@"user"][@"role"]isEqualToString:@"2"] )
                 {
                     if(autologin==1)
                     {
                         [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"KHome"];
                         [[NSUserDefaults standardUserDefaults]synchronize];
                     }
                     NSLog(@"Enter in doctor");
                     [defaults setObject:@"1" forKey:@"doctorlogin"];
                     [defaults synchronize];

                     [[UINavigationBar appearance] setTranslucent:NO];
                     
                     [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:215.0f/255.0f green:29.0f/255.0f blue:32.0f/255.0f alpha:1.0]];
                     
                     UIStoryboard *storyboard1 = [UIStoryboard storyboardWithName:@"Main"
                                                                           bundle: nil];
                     
                     UINavigationController *loginNavigationController = (UINavigationController*)[storyboard1 instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
                     [self presentViewController:loginNavigationController animated:NO completion:nil];
                     

                 }
                 else if([dict[@"user"][@"role"]isEqualToString:@"3"])
                 {
                     if(autologin==1)
                     {
                         [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"KHome"];
                         [[NSUserDefaults standardUserDefaults]synchronize];
                     }

                     NSLog(@"Enter in Patient");
                     [defaults setObject:@"0" forKey:@"doctorlogin"];
                     [defaults synchronize];
                     [[UINavigationBar appearance] setTranslucent:NO];
                     
                     [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:5.0f/255.0f green:52.0f/255.0f blue:222.0f/255.0f alpha:1.0]];
                     
                     UIStoryboard *storyboard1 = [UIStoryboard storyboardWithName:@"Patient"
                                                                           bundle: nil];
                     
                     UINavigationController *loginNavigationController = (UINavigationController*)[storyboard1 instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
                     [self presentViewController:loginNavigationController animated:NO completion:nil];
                     


                 }
                 else{
                     [Utility alertWithTitle:kAlertTitle withMessage:@"Invalid User" onVC:self];

                 }
                
             }
         }
         else
         {
             [MyLoader hideLoadingView];
             [Utility alertWithTitle:@"" withMessage:kNetworkErrorTitle onVC:self];
         }
     }];
    
    
    
}
- (BOOL) validateUserData
{
    
    NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    
    if ([txt_email.text isEqualToString:@" "]|| [[txt_email.text stringByTrimmingCharactersInSet:whitespace] length] == 0  || [txt_password.text isEqualToString:@""] || [[txt_password.text stringByTrimmingCharactersInSet:whitespace] length] == 0)
    {
        [Utility alertWithTitle:@"" withMessage:@"All fields are mandatory." onVC:self];
        return NO;
    }
    if([Utility emailValidation:txt_email.text])
    {
        
    }
    else
    {
        [Utility alertWithTitle:@"" withMessage:@"Please enter valid email address" onVC:self];
        return NO;
    }
    
    
    
    
    return YES;
}



@end
