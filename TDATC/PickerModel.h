//
//  PickerModel.h
//  TDATC
//
//  Created by iWeb on 10/26/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PickerModel : NSObject

@property (nonatomic, strong) NSString* dataId;
@property (nonatomic, strong) NSString* dataCode;
@property (nonatomic, strong) NSString* dataName;

@property (nonatomic, strong) NSString* price_monthly;
@property (nonatomic, strong) NSString* price_quaterly;
@property (nonatomic, strong) NSString* price_halfyearly;
@property (nonatomic, strong) NSString* price_yearly;



- (id) initWithCountriesDictionary:(NSDictionary*)dictionary;
- (id) initWithStatesDictionary:(NSDictionary *)dictionary;

- (id) initWithCitiesDictionary:(NSDictionary *)dictionary;
- (id) initWithMemberShipPlans:(NSDictionary*)dictionary;
- (id) initWithBloodGroupsDictionary:(NSDictionary*)dictionary;
- (id) initWithServiceDictionary:(NSDictionary*)dictionary;
- (id) initWithClinicDictionary:(NSDictionary*)dictionary;
@end
