//
//  PatientFeedback_TVC.h
//  TDATC
//
//  Created by iWeb on 9/23/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASStarRatingView.h"
#import "PatientFeedbackModel.h"

@interface PatientFeedback_TVC : UITableViewCell
@property (weak, nonatomic) IBOutlet ASStarRatingView *view_star;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@property (weak, nonatomic) IBOutlet UITextView *messageTextView;


- (void) setupData:(PatientFeedbackModel*)data;

@end
