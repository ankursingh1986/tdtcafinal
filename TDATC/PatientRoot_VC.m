//
//  RootViewController.m
//  NicoBeacon
//
//  Created by iWeb on 9/2/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "PatientRoot_VC.h"
//#import "HelpViewController.h"
//#import "MyProfileViewController.h"
//#import "MyOrdersViewController.h"
//#import "LocationViewController.h"
//#import "InviteFriendsViewController.h"
//#import "SettingsViewController.h"
#import "SWRevealViewController.h"
#import "HomeViewController.h"
#import "PatientMessage_VC.h"
//#import "LocationViewController.h"
//#import <SDWebImage/UIImageView+WebCache.h>
//#import <SDWebImage/UIView+WebCache.h>

#import "DataManager.h"


@interface PatientRoot_VC ()
{
    NSMutableArray *sideMenuContentArray;
    NSMutableArray *sideMenuImageaArray;
    
}

@end

@implementation PatientRoot_VC

- (void)viewDidLoad {
    sideMenuContentArray = [[NSMutableArray alloc] initWithObjects:@"MY PROFILE",@"MESSAGES",@"FEEDBACK",@"MY APPOINTMENTS", @"FIND A DOCTOR", nil];
    sideMenuImageaArray = [[NSMutableArray alloc] initWithObjects:@"my_profile",@"messages",@"feedback",@"my_appointments",@"find_a_doctor", nil];
    
    
    
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.frame = _btn_logout.layer.bounds;
    gradientLayer.colors = [NSArray arrayWithObjects:
                            (id)[UIColor colorWithRed:27.0/255.0 green:74.0/255.0 blue:245.0/255.0 alpha:1.0].CGColor,
                            (id)[UIColor colorWithRed:3.0/255.0 green:39.0/255.0 blue:169.0/255.0 alpha:1.0].CGColor,
                            nil];
    gradientLayer.startPoint = CGPointMake(0, 0.5);
    gradientLayer.endPoint = CGPointMake(1, 0.5);
    gradientLayer.cornerRadius = _btn_logout.layer.cornerRadius;
    
    [_btn_logout.layer addSublayer:gradientLayer];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
#pragma mark - TableView Delegate and data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    if(tableView.tag==1)
//    {
//        return 40;
//    }
//    return 0;
//}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // if(section==0)
    {
        return sideMenuContentArray.count+1;
    }
    // return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // if(indexPath.section==0)
    {
        if(indexPath.row==0)
        {
            return 206;
        }
        return 50;
        
    }
    // return 60;
    
}




-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // if(indexPath.section==0)
    // {
    static NSString *identifier1=@"Cell1";
    static NSString *identifier2=@"Cell";
    
    
    if (indexPath.row==0)
    {
        cellFirst =[self.tbl_home dequeueReusableCellWithIdentifier:identifier1 forIndexPath:indexPath];
        
        cellFirst.selectionStyle = UITableViewCellSelectionStyleNone;
        NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
        
        
        //  cellFirst.profileName=@"Ankur sigh";
        //  cellFirst.profileEmail=[defaults objectForKey:@"user_email"];
        //        [cellFirst.img_side sd_setShowActivityIndicatorView:YES];
        //        [cellFirst.img_side sd_setIndicatorStyle:UIActivityIndicatorViewStyleGray];
        //        [cellFirst.img_side sd_setImageWithURL:[NSURL URLWithString:[defaults objectForKey:@"img"]]
        //                     placeholderImage:[UIImage imageNamed:@"logo.png"]
        //                              options:SDWebImageRefreshCached];
        
        //    cellFirst.profileName.text=[defaults objectForKey:@"full_name"];
        //    cellFirst.profileEmail.text=[defaults objectForKey:@"user_email"];
        
        
        cellFirst.img_side.layer.cornerRadius = cellFirst.img_side.frame.size.width / 2;
        cellFirst.img_side.clipsToBounds = YES;
        
        cellFirst.img_side.layer.borderWidth = 6.0f;
        cellFirst.img_side.layer.borderColor = [UIColor whiteColor].CGColor;
        
        NSString *imagePath=[NSString stringWithFormat:@"%@%@",kPatientImageSubPath,_gDataManager.loginModel.image];
        if([imagePath isEqualToString:@"(null)(null)"])
        {
            
        }
        else
        {
            [self downloadImage:cellFirst imageUrl1:imagePath];
            
        }
        
        
        cellFirst.profileEmail.text = [NSString stringWithFormat:@"%@",_gDataManager.loginModel.email];
        cellFirst.profileName.text = [[NSString stringWithFormat:@"%@",_gDataManager.loginModel.full_name] uppercaseString];
        
        return cellFirst;
        
        
    }
    
    else
    {
        cellSecond =[self.tbl_home dequeueReusableCellWithIdentifier:identifier2 forIndexPath:indexPath];
        int a=indexPath.row;
        a=a-1;
        cellSecond.title_lbl.text=[sideMenuContentArray objectAtIndex:a];
        cellSecond.selectionStyle = UITableViewCellSelectionStyleNone;
        
        UIImage *image = [UIImage imageNamed:[sideMenuImageaArray objectAtIndex:a]];
        [cellSecond.sideImage setImage:image];
        
        
        return cellSecond;
    }
    
    
    
    return nil;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row==1) {
        UIStoryboard *storyBoard12 = [UIStoryboard storyboardWithName:@"Patient" bundle:nil];
        
        
        
        UIViewController *vc = [storyBoard12 instantiateViewControllerWithIdentifier:@"PatientProfile_VC_id"];
        
        UINavigationController *nav =[[UINavigationController alloc]initWithRootViewController:vc];
        
        [self.revealViewController setFrontViewController:nav];
        [self.revealViewController setFrontViewPosition:FrontViewPositionLeft animated:YES];
        
    }
    else if (indexPath.row==2) {
        UIStoryboard *storyBoard12 = [UIStoryboard storyboardWithName:@"Patient" bundle:nil];
        
        
        
        UIViewController *vc = [storyBoard12 instantiateViewControllerWithIdentifier:@"PatientMessage_VC_id"];
        
        UINavigationController *nav =[[UINavigationController alloc]initWithRootViewController:vc];
        
        [self.revealViewController setFrontViewController:nav];
        [self.revealViewController setFrontViewPosition:FrontViewPositionLeft animated:YES];
        
        
        
        
    }
    else if (indexPath.row==3) {
        UIStoryboard *storyBoard12 = [UIStoryboard storyboardWithName:@"Patient" bundle:nil];
        
        
        
        UIViewController *vc = [storyBoard12 instantiateViewControllerWithIdentifier:@"PatientFeedback_VC_id"];
        
        UINavigationController *nav =[[UINavigationController alloc]initWithRootViewController:vc];
        
        [self.revealViewController setFrontViewController:nav];
        [self.revealViewController setFrontViewPosition:FrontViewPositionLeft animated:YES];
        
    }
    else if (indexPath.row==4) {
        
        
        UIStoryboard *storyBoard12 = [UIStoryboard storyboardWithName:@"Patient" bundle:nil];
        
        
        
        UIViewController *vc = [storyBoard12 instantiateViewControllerWithIdentifier:@"PatientAppointment_VC_id"];
        
        UINavigationController *nav =[[UINavigationController alloc]initWithRootViewController:vc];
        
        [self.revealViewController setFrontViewController:nav];
        [self.revealViewController setFrontViewPosition:FrontViewPositionLeft animated:YES];
        
    }
    else if (indexPath.row==5) {
        UIStoryboard *storyBoard12 = [UIStoryboard storyboardWithName:@"Patient" bundle:nil];
        
        
        
        UIViewController *vc = [storyBoard12 instantiateViewControllerWithIdentifier:@"PatientFindDoctor_VC_id"];
        
        UINavigationController *nav =[[UINavigationController alloc]initWithRootViewController:vc];
        
        [self.revealViewController setFrontViewController:nav];
        [self.revealViewController setFrontViewPosition:FrontViewPositionLeft animated:YES];
        
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)touch_logout:(id)sender {
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"KHome"];
    [[NSUserDefaults standardUserDefaults]synchronize];

    UIStoryboard *storyboard1 = [UIStoryboard storyboardWithName:@"Main"
                                                          bundle: nil];
    UINavigationController *loginNavigationController = (UINavigationController*)[storyboard1 instantiateViewControllerWithIdentifier:@"LoginViewController_id"];
    [self presentViewController:loginNavigationController animated:NO completion:nil];
}

- (void) downloadImage:(SideMenuProfileTableViewCell*)cell imageUrl1:(NSString *)imgURL1
{
    NSLog(@"Image URL in method=%@",imgURL1);
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       image_manager = [SDWebImageManager sharedManager];
                       [image_manager downloadWithURL:[NSURL URLWithString:imgURL1] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize, NSInteger feedID, NSString *feedString)
                        {
                        } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished)
                        {
                            if (image)
                            {
                                cell.img_side.image = image;
                            }
                        }];
                   });
    
}
@end
