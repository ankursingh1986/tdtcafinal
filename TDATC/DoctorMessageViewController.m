//
//  DoctorMessageViewController.m
//  TDATC
//
//  Created by iWeb on 9/18/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "DoctorMessageViewController.h"
#import "DoctorMessageTableViewCell.h"
#import "DoctoreMessageDetailViewController.h"
#import "DataManager.h"
#import "DataParser.h"
#import "Utility.h"
#import "MyLoader.h"
#import "DoctorMessage.h"

@interface DoctorMessageViewController ()
{
    NSMutableArray* _messageArray;
}

@end

@implementation DoctorMessageViewController 

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self leftButton];
    [tblView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    self->tblView.tableFooterView = [[UIView alloc] init];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationItem.title = @"MESSAGES";
    
    [super viewWillAppear:animated];
    
    [self getDoctorMesssages];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) getDoctorMesssages
{
    
    [MyLoader showLoadingView];
    //[DataParser doctorMessageListing:[NSString stringWithFormat:@"id=%@",@"159"] WithCompletionHandler:^(NSArray *array, BOOL isSuccessful)
     [DataParser doctorMessageListing:[NSString stringWithFormat:@"id=%@",_gDataManager.loginModel.user_id] WithCompletionHandler:^(NSArray *array, BOOL isSuccessful)
     {
         [MyLoader hideLoadingView];
         if (isSuccessful)
         {
             _messageArray = [array copy];
             [tblView reloadData];
         }
         else
         {
             [Utility alertWithTitle:@"Alert!" withMessage:@"Please check your internet connection" onVC:self];
         }
         
    }];
}

#pragma mark - table view delegates and data socurce


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _messageArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    DoctorMessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Message_Cell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell isKindOfClass:[DoctorMessageTableViewCell class]])
    {
        DoctorMessageTableViewCell *cell1 = (DoctorMessageTableViewCell*)cell;
        DoctorMessage* data = [_messageArray objectAtIndex:indexPath.row];
        [cell1 setUpData:data];
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DoctorMessage* data = [_messageArray objectAtIndex:indexPath.row];
    DoctoreMessageDetailViewController* destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"DoctoreMessageDetailViewController_id"];
   
    destVC.patientId = data.patient_id;
    destVC.messageData=[_messageArray objectAtIndex:indexPath.row];
    destVC.inboxArray=_messageArray;
  
   
    destVC.doctorMessage = data.doctor_msg;
    
    [self.navigationController pushViewController:destVC animated:YES];
}


-(void)leftButton
{
    UIButton *button =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(backButtonAction:)forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0, 20, 20)];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = barButton;
    
}

- (IBAction) backButtonAction:(id)sender
{
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
    [self presentViewController:vc animated:NO completion:nil];
    
}

@end
