//
//  PatientAppointment_VC.m
//  TDATC
//
//  Created by iWeb on 9/25/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "PatientAppointment_VC.h"
#import "PatientAppointment_CVC.h"
#import "PatientWriteReview_VC.h"
#import "PatientFindDoctor_VC.h"
#import "PatientClinicInfo_VC.h"
#import "PatientDataParser.h"
#import "DataParser.h"
#import "DataManager.h"
#import "Defines.h"
#import "Utility.h"
#import "MyLoader.h"

@interface PatientAppointment_VC ()
{
    NSMutableArray* _appointmentArray;
    NSIndexPath* _selectedIndexPath;
    
    NSMutableArray* _selectedDateArray;
    
    NSMutableArray* _mainArray;
    NSMutableArray *_feedbcakArray;
}

@end

@implementation PatientAppointment_VC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self leftButton];
    [self rightButton];
    _calender1.delegate = self;
    _calender1.dataSource = self;
    
    self.gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    _appointmentArray = [[NSMutableArray alloc] init];
    _selectedDateArray = [[NSMutableArray alloc] init];
    _mainArray = [[NSMutableArray alloc] init];
    
    _feedbcakArray = [[NSMutableArray alloc]init];
    self.view_1.hidden=YES;
    [self setDate];
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    
    singleFingerTap.delegate = self;
    [self.view_1 addGestureRecognizer:singleFingerTap];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated
{
    _calender1.appearance.eventDefaultColor = [UIColor greenColor];

    [super viewWillAppear:animated];
    [self setDate];

    
    [self getAppointmentList];
}

- (void) getAppointmentList
{
    [MyLoader showLoadingView];
    [PatientDataParser patientAppointmentList:[NSString stringWithFormat:@"id=%@",_gDataManager.loginModel.user_id] WithCompletionHandler:^(NSArray *array, BOOL isSuccessful)
    {
        [MyLoader hideLoadingView];
        if (isSuccessful)
        {
            [_appointmentArray removeAllObjects];
            [_mainArray removeAllObjects];
            _appointmentArray = [array mutableCopy];
            
            NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd"];
            [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
            // or @"yyyy-MM-dd hh:mm:ss a" if you prefer the time with AM/PM
            NSString *date1 =  [dateFormatter stringFromDate:[NSDate date]];
            
           
            
            for (PatientAppointmentModel* obj in _appointmentArray)
            {
                NSLog(@"%@", obj.availabledate);
                NSString *dateStr = [Utility changeDateCalenderFormat:obj.availabledate];
                
                // Convert string to date object
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat:@"yyyy-MM-dd"];
                [dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
                NSDate *appointmentDate = [dateFormat dateFromString:dateStr];
                NSDate *date = [dateFormat dateFromString:date1];
                
                if ([date isEqualToDate:appointmentDate])
                {
                    [_mainArray addObject:obj];
                    ///_mainArray = [_selectedDateArray mutableCopy];
                }
            }

            
//            _mainArray = [array mutableCopy];
            [self.coll_view reloadData];
            NSLog(@"data get");
            [self.calender1 reloadData];
        }
        else
        {
            [Utility alertWithTitle:kAlertTitle withMessage:kNetworkErrorTitle onVC:self];
        }
    }];
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
   self.view_1.hidden=YES;
   
}
-(void)setDate{
    NSDate *dateGloble = [NSDate date];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    
    [df setDateFormat:@"MMMM dd, YYYY"];
    
    titleLabelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [titleLabelButton setTitle:[[df stringFromDate:dateGloble]uppercaseString] forState:UIControlStateNormal];
    titleLabelButton.frame = CGRectMake(0, 0, 70, 44);
    titleLabelButton.font = [UIFont fontWithName:@"Montserrat-Bold" size:20.0];
    [titleLabelButton addTarget:self action:@selector(didTapTitleView:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.titleView = titleLabelButton;
}
- (IBAction)didTapTitleView:(id) sender
{
    
    
    titleLabelButton.selected = !titleLabelButton.selected;
    if(titleLabelButton.selected)
    {
        self.view_1.hidden=NO;
       // _coll_view.hidden=YES;
    }
    else
    {
        self.view_1.hidden=YES;
      //  _coll_view.hidden=NO;
        
    }
    
    NSLog(@"Title tap");
}


- (IBAction)previous:(id)sender
{
    NSDate *currentMonth = self.calender1.currentPage;
    NSDate *previousMonth = [self.gregorian dateByAddingUnit:NSCalendarUnitMonth value:-1 toDate:currentMonth options:0];
    [self.calender1 setCurrentPage:previousMonth animated:YES];
}

- (IBAction)next:(id)sender
{
    NSDate *currentMonth = self.calender1.currentPage;
    NSDate *nextMonth = [self.gregorian dateByAddingUnit:NSCalendarUnitMonth value:1 toDate:currentMonth options:0];
    [self.calender1 setCurrentPage:nextMonth animated:YES];
}


#pragma mark - Calnder delegates
- (BOOL)calendar:(FSCalendar *)calendar hasEventForDate:(NSDate *)date
{
    if(_appointmentArray.count!=0)
    {
        for (int k=0; k<_appointmentArray.count; k++)
        {
            PatientAppointmentModel* data = [_appointmentArray objectAtIndex:k];
            NSString *dateStr = [Utility changeDateCalenderFormat:data.availabledate];
            
            // Convert string to date object
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"yyyy-MM-dd"];
            NSDate *datenew = [dateFormat dateFromString:dateStr];
            if ([date isEqualToDate:datenew])
            {
                return YES;
            }
        }
    }
    
    return NO;
}

///FSCalendarDelegate


- (void)calendar:(FSCalendar *)calendar didSelectDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition
{
    self.view_1.hidden=YES;
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"MMMM dd, YYYY"];
    NSLog(@"did select date %@",[df stringFromDate:date]);
    //NSDate *selectedDate = [df dateFromString:date];
    
    titleLabelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [titleLabelButton setTitle:[[df stringFromDate:date]uppercaseString] forState:UIControlStateNormal];
    titleLabelButton.frame = CGRectMake(0, 0, 70, 44);
    titleLabelButton.font = [UIFont fontWithName:@"Montserrat-Bold" size:20.0];
    [titleLabelButton addTarget:self action:@selector(didTapTitleView:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.titleView = titleLabelButton;
    
    [_mainArray removeAllObjects];
    [_selectedDateArray removeAllObjects];
    for (PatientAppointmentModel* obj in _appointmentArray)
    {
        NSLog(@"%@", obj.availabledate);
        NSString *dateStr = [Utility changeDateCalenderFormat:obj.availabledate];
        
        // Convert string to date object
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd"];
        NSDate *appointmentDate = [dateFormat dateFromString:dateStr];
        
        if ([date isEqualToDate:appointmentDate])
        {
            [_selectedDateArray addObject:obj];
            _mainArray = [_selectedDateArray mutableCopy];
        }
    }
    [self.coll_view reloadData];
    
    // [self configureVisibleCells];
}
- (void)calendar:(FSCalendar *)calendar didSelectDate:(NSDate *)dateGloble1
{
    self.view_1.hidden=YES;
    //  NSDate *dateGloble = [NSDate date];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    
    [df setDateFormat:@"MMMM dd, YYYY"];
    
    titleLabelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [titleLabelButton setTitle:[[df stringFromDate:dateGloble1]uppercaseString] forState:UIControlStateNormal];
    titleLabelButton.frame = CGRectMake(0, 0, 70, 44);
    titleLabelButton.font = [UIFont fontWithName:@"Montserrat-Bold" size:20.0];
    [titleLabelButton addTarget:self action:@selector(didTapTitleView:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.titleView = titleLabelButton;
    NSLog(@"date selected");
    //07839070871
}



#pragma mark- Collection View delegates
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _mainArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PatientAppointment_CVC *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Collection_cell" forIndexPath:indexPath];
    cell.view_star.selectedStar = [UIImage imageNamed:@"redstar"];
    
    
    cell.btn_cancel.tag = indexPath.row;
    cell.btn_resudule.tag = indexPath.row;

    cell.view_star.canEdit = NO;
    cell.view_star.maxRating = 5;
    PatientAppointmentModel* data = [_mainArray objectAtIndex:indexPath.item];
    doctorID = data.doctor_id;
    if([data.status isEqualToString:@"0"])
    {
        cell.lbl_approved.hidden = YES;
        cell.btn_cancel.hidden =NO;
        cell.btn_resudule.hidden =NO;
        cell.view_1.hidden =NO;

        [cell.btn_resudule addTarget:self action:@selector(btnReseduleAction:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.btn_cancel addTarget:self action:@selector(btnCancelAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
        cell.lbl_approved.hidden = NO;
        cell.btn_resudule.hidden =YES;
        cell.btn_cancel.hidden =YES;
        cell.view_1.hidden =YES;


    }
    
    
    _selectedIndexPath = indexPath;
    
    [cell setupUI];
    cell.view_star.rating = [data.rating intValue];
    [cell setUpData:data];
    
    NSString *imagePath=[NSString stringWithFormat:@"%@%@",kDoctorImagePath,data.image];
    if([imagePath isEqualToString:@"(null)(null)"])
    {
        
    }
    else
    {
        [self downloadImage:cell imageUrl1:imagePath];
        
    }
    
     return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    _selectedIndexPath = indexPath;
}



-(CGSize )collectionView:(UICollectionView* )collectionView layout:(nonnull UICollectionViewLayout* )collectionViewLayout sizeForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
      return  CGSizeMake(self.coll_view.frame.size.width,self.coll_view.frame.size.height);
   
}


- (IBAction)viewDeatail:(id)sender
{
    
}
- (IBAction)writeReview:(id)sender
{
    if(_mainArray.count>0)
    {
        NSLog(@"doctorID=%@",doctorID);
        [self getFeedbackForPatient];
    }
   
}

- (void) getFeedbackForPatient
{
    NSString* postData = [NSString stringWithFormat:@"id=%@",_gDataManager.loginModel.user_id];
    [MyLoader showLoadingView];

    [PatientDataParser patientFeedback:postData WithCompletionHandler:^(NSArray *array, BOOL isSuccessful)
     {
         if (isSuccessful)
         {
             [MyLoader hideLoadingView];

             _feedbcakArray = [array mutableCopy];
             int flag=0;
             
             for (int i = 0; i<_feedbcakArray.count; i++) {
                 PatientFeedbackModel *obj=[_feedbcakArray objectAtIndex:i];
                 NSString *doctIDLocat=[NSString stringWithFormat:@"%@", doctorID];
                 
                 if([doctIDLocat isEqualToString:obj.doctorId])
                 {
                     flag=1;
                     break;
                 }
                 
             }
             if(flag==1)
             {
                 [Utility alertWithTitle:nil withMessage:@"You have already given review." onVC:self];
             }
             else
             {
                 PatientAppointmentModel* data = [_mainArray objectAtIndex:_selectedIndexPath.row];
                 PatientWriteReview_VC* destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PatientWriteReview_VC_id"];
                 destVC.patientAppointmentData = data;
                 [self.navigationController pushViewController:destVC animated:YES];
             }
             

         }
         else
         {
             [Utility alertWithTitle:kAlertTitle withMessage:kNetworkErrorTitle onVC:self];
         }
     }];
}

-(void)rightButton
{
    button =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"add"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(addButtonAction:)forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0, 20, 20)];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.rightBarButtonItem = barButton;
}

- (IBAction) addButtonAction:(id)sender
{
    PatientFindDoctor_VC* destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PatientFindDoctor_VC_id"];
    [self.navigationController pushViewController:destVC animated:YES];
    UIStoryboard *storyBoard12 = [UIStoryboard storyboardWithName:@"Patient" bundle:nil];
}

- (void) btnCancelAction:(id)sender
{
    UIButton* btn = (UIButton*)sender;
    
    PatientAppointmentModel* data = [_mainArray objectAtIndex:btn.tag];
    
    NSString* payLoad = [NSString stringWithFormat:@"id=%@&booking_id=%@&status=2",_gDataManager.loginModel.user_id, data.booking_id];
    [MyLoader showLoadingView];
    [PatientDataParser patientCancelAppointment:payLoad WithCompletionHandler:^(NSDictionary *dict, BOOL isSuccessful)
     {
         if (isSuccessful)
         {
             NSLog(@"Appintment cancelled");
             [Utility alertWithTitle:kAlertTitle withMessage:@"Appointment cancelled" onVC:self];
             [self getAppointmentList];
             [self.coll_view reloadData];
         }
         else
         {
             [Utility alertWithTitle:kAlertTitle withMessage:kNetworkErrorTitle onVC:self];
         }
     }];
    
}
- (void) btnReseduleAction:(id)sender
{
    UIButton* btn = (UIButton*)sender;
    
    PatientAppointmentModel* data = [_mainArray objectAtIndex:btn.tag];
    
    PatientClinicInfo_VC* destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PatientClinicInfo_VC_id"];
    _gDataManager.fromAppointment=YES;
    destVC.patientAppointmentData=data;

    [self.navigationController pushViewController:destVC animated:YES];
    
    NSLog(@"resule click click");
    
    
}


- (IBAction) backButtonAction:(id)sender
{
    UIStoryboard *storyBoard12 = [UIStoryboard storyboardWithName:@"Patient" bundle:nil];
    
    UIViewController *vc = [storyBoard12 instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
    [self presentViewController:vc animated:NO completion:nil];
}

-(void)leftButton
{
    UIButton *button2 =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button2 setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [button2 addTarget:self action:@selector(backButtonAction:)forControlEvents:UIControlEventTouchUpInside];
    [button2 setFrame:CGRectMake(0, 0, 20, 20)];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button2];
    self.navigationItem.leftBarButtonItem = barButton;
    self.navigationItem.hidesBackButton=TRUE;
    
}
- (void) downloadImage:(PatientAppointment_CVC*)cell imageUrl1:(NSString *)imgURL1
{
    NSLog(@"Image URL in method=%@",imgURL1);
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       image_manager = [SDWebImageManager sharedManager];
                       [image_manager downloadWithURL:[NSURL URLWithString:imgURL1] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize, NSInteger feedID, NSString *feedString)
                        {
                        } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished)
                        {
                            if (image)
                            {
                                cell.profileImageView.image = image;
                            }
                        }];
                   });
    
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isEqual:self.view_1])
    {
        return YES;
    }
    else
    {
        return NO;
    }
    
    //[touch.view isKindOfClass:[FSCalendar class]]
}

@end
