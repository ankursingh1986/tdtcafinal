//
//  DoctorChangePlan_VC.h
//  TDATC
//
//  Created by iWeb on 9/19/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PayPalMobile.h"
#import "Global.pch"

@interface DoctorChangePlan_VC : UIViewController <UITextFieldDelegate,PayPalPaymentDelegate>
{
    PayPalConfiguration *payPalConfig;
    
    NSString *strC1,*strC2,*strC3;

}
// paypal start
- (void)setPayPalEnvironment:(NSString *)environment;

@property (strong, nonatomic) IBOutlet UIView *headerView;
@property(nonatomic, strong, readwrite) NSString *environment;
@property(nonatomic, assign, readwrite) BOOL acceptCreditCards;
@property (strong, nonatomic) NSString *productID;
@property(nonatomic, strong, readwrite) NSString *resultText;
@property(nonatomic, strong, readwrite) IBOutlet UIView *successView;

- (IBAction)touch_proceedChange:(id)sender;


@property (weak, nonatomic) IBOutlet UITextField *memberShipTypeTextField;

@property (weak, nonatomic) IBOutlet UITextField *numberOfYearsTextField;
@property (weak, nonatomic) IBOutlet UITextField *currentPriceTextField;
@property (nonatomic, strong) NSDictionary *dataDic;


@end
