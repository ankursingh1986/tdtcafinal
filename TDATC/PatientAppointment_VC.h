//
//  PatientAppointment_VC.h
//  TDATC
//
//  Created by iWeb on 9/25/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FSCalendar.h>
#import "Global.pch"



@interface PatientAppointment_VC : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate,FSCalendarDelegate,FSCalendarDataSource, UIGestureRecognizerDelegate>
{
    UIButton *button;
    UIButton *titleLabelButton;
    SDWebImageManager *image_manager;
    
    NSString *doctorID;

}

@property (weak, nonatomic) IBOutlet UIImageView *img_bg;
@property (weak, nonatomic) IBOutlet UIView *view_1;
@property (weak, nonatomic) IBOutlet UIView *view_sub_1;

@property (weak, nonatomic) IBOutlet UICollectionView *coll_view;
@property (weak, nonatomic) FSCalendar *calendar;


@property (weak, nonatomic) IBOutlet FSCalendar *calender1;
@property (strong, nonatomic) NSCalendar *gregorian;
- (IBAction)previous:(id)sender;

- (IBAction)next:(id)sender;

- (IBAction)viewDeatail:(id)sender;
- (IBAction)writeReview:(id)sender;



@end
