//
//  DoctorAppointment_TVC.h
//  TDATC
//
//  Created by iWeb on 9/18/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DoctorAppointment_TVC : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbl_name;
@property (weak, nonatomic) IBOutlet UILabel *lbl_phone;
@property (weak, nonatomic) IBOutlet UIButton *btn_status;
@property (weak, nonatomic) IBOutlet UILabel *lbl_desc;
@property (weak, nonatomic) IBOutlet UIImageView *img_profile;
@property (weak, nonatomic) IBOutlet UILabel *lbl_date;
@property (weak, nonatomic) IBOutlet UILabel *lbl_time;
@property (weak, nonatomic) IBOutlet UILabel *lbl_status;

@end
