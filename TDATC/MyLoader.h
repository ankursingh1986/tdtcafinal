//
//  MyLoader.h
//  PocketIn
//
//  Created by vinay on 08/08/16.
//  Copyright © 2016 CodeYeti. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyLoader : UIView

+ (void)showLoadingView;
+ (void)hideLoadingView;
+ (UIImage *) imageWithView:(UIView *)view;

@end
