//
//  DoctorPatientDetail_VC.h
//  TDATC
//
//  Created by iWeb on 9/19/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DoctorPatientModel.h"
#import "SDWebImageManager.h"
#import "Global.pch"


@interface DoctorPatientDetail_VC : UIViewController
{
    IBOutlet UITableView *tbl_View;
    NSMutableArray *arrayTitle,*arrayDesc;
    SDWebImageManager *image_manager;


}
@property(nonatomic,strong)DoctorPatientModel *patientInfo;

@end
