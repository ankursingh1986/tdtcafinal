//
//  DoctorMessageTableViewCell.m
//  TDATC
//
//  Created by iWeb on 9/18/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "DoctorMessageTableViewCell.h"
#import "DataManager.h"
#import "Defines.h"
#import "Global.pch"


@implementation DoctorMessageTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void) setUpData:(DoctorMessage*)data
{
    self.txtview_head.text = data.subject;
    
    
    
    if([data.attchment isEqualToString:@""])
    {
        NSString* result = [data.doctor_msg stringByReplacingOccurrencesOfString:@"<p>" withString:@""];
        result = [result stringByReplacingOccurrencesOfString:@"</p>" withString:@""];
        
        self.txtView_desc.text = result;
    }
    else
    {
       
        NSString* result = [data.doctor_msg stringByReplacingOccurrencesOfString:@"<p>" withString:@""];
        result = [result stringByReplacingOccurrencesOfString:@"</p>" withString:@""];
        
         NSString *strData=[NSString stringWithFormat:@"%@ \n %@\%@", result,_gDataManager.imgmsgpath, data.attchment];
        
        self.txtView_desc.text = strData;
    }

    
    
    
  
    self.lbl_Date.text = [Utility changeDateTimeFormat:data.date];
    NSString *imagePath=[NSString stringWithFormat:@"%@%@",_gDataManager.imgpatientpath,data.patientimag_path];
    if([imagePath isEqualToString:@"(null)(null)"])
    {
        
    }
    else
    {
        [self downloadImage:imagePath];
        
    }

}
- (void) downloadImage:(NSString *)imgURL1
{
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       image_manager = [SDWebImageManager sharedManager];
                       [image_manager downloadWithURL:[NSURL URLWithString:imgURL1] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize, NSInteger feedID, NSString *feedString)
                        {
                        } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished)
                        {
                            if (image)
                            {
                                self.img_profile.image = image;
                            }
                        }];
                   });
    
}





- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
