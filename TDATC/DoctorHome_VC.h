//
//  DoctorHome_VC.h
//  TDATC
//
//  Created by iWeb on 9/18/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDWebImageManager.h"
#import "DoctorHome_TVC.h"
#import "Global.pch"


@interface DoctorHome_VC : UIViewController
{
    IBOutlet UITableView *tbl_Home;
    
    IBOutlet UIImageView *imagHead;
    DoctorHome_TVC *cell;
    
    SDWebImageManager *image_manager;

    NSMutableArray *arrayImage,*arrayTitle,*arrayDesc;
    
    NSMutableArray *servicesArray,*specialitiesArray,*educationsArray,*experiencesArray,*awardsArray,*membershipsArray,*registrationArray;
    
}
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sideBarbtn;


@end
