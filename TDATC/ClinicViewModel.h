//
//  ClinicViewModel.h
//  TDATC
//
//  Created by iWeb on 11/2/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ClinicViewModel : NSObject

@property (nonatomic, strong) NSString* da_id;
@property (nonatomic, strong) NSString* availabledate;

@property (nonatomic, strong) NSMutableArray* doctor_avail_timesArray;

- (id) initWithDictionary:(NSDictionary*)dictionary;


@end
