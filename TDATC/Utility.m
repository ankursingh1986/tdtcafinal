//
//  Utility.h
//  DawaiBox
//
//  Created by Vinay Kumar on 21/11/16.
//  Copyright © 2016 com.garry. All rights reserved.
//
#import "Utility.h"

@implementation Utility

#pragma mark - alert message method
+(void)alertWithTitle: (NSString *) title withMessage: (NSString*) message onVC:(UIViewController*)VC
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    
    [VC presentViewController:alertController animated:YES completion:nil];
}
//-(NSString *)timeFormateChangeAmPm:(NSString *)dateData
//{
//    NSDateFormatter *dateFormatter3 = [[NSDateFormatter alloc] init];
//    [dateFormatter3 setDateStyle:NSDateFormatterMediumStyle];
//    [dateFormatter3 setDateFormat:@"HH:mm:ss"];
//    NSDate *date1 = [dateFormatter3 dateFromString:dateData];
//    
//    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//    [formatter setDateFormat:@"hh:mm a"];
//    return [formatter stringFromDate:date1];
//}
+ (NSString*) getAgeFromDOBFormat:(NSString*)date
{
    NSString *birthDate = [self changeDateFormat:date];
    NSDate *todayDate = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd"];
    int time = [todayDate timeIntervalSinceDate:[dateFormatter dateFromString:birthDate]];
    int allDays = (((time/60)/60)/24);
    int days = allDays%365;
    int years = (allDays-days)/365;
    return [NSString stringWithFormat:@"%d",years];
    
}

#pragma mark- Change dateFormat method
+ (NSString*) changeDateFormat:(NSString*)date
{
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    
    NSDate* fromDate = [formatter dateFromString:date];
    [formatter setDateFormat:@"YYYY-MM-dd"];
    
    NSString* formatedDate = [formatter stringFromDate:fromDate];
    
    if (formatedDate == nil)
    {
        [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
        
        fromDate = [formatter dateFromString:date];
        [formatter setDateFormat:@"YYYY-MM-dd"];
        
        formatedDate = [formatter stringFromDate:fromDate];
        
    }
    if (formatedDate == nil)
    {
        [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
        
        fromDate = [formatter dateFromString:date];
        [formatter setDateFormat:@"YYYY-MM-dd"];
        
        formatedDate = [formatter stringFromDate:fromDate];
        
    }
    
    return formatedDate;
    
}

+ (NSString*) changeDateFormat1:(NSString*)date{
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    
    NSDate* fromDate = [formatter dateFromString:date];
    [formatter setDateFormat:@"MMM dd, YYYY"];
    
    NSString* formatedDate = [formatter stringFromDate:fromDate];
    
    if (formatedDate == nil)
    {
        [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
        
        fromDate = [formatter dateFromString:date];
        [formatter setDateFormat:@"MMM dd, YYYY"];
        
        formatedDate = [formatter stringFromDate:fromDate];
        
    }
    if (formatedDate == nil)
    {
        [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
        
        fromDate = [formatter dateFromString:date];
        [formatter setDateFormat:@"MMM dd, YYYY"];
        
        
        formatedDate = [formatter stringFromDate:fromDate];
        
    }
    if (formatedDate == nil)
    {
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        fromDate = [formatter dateFromString:date];
        [formatter setDateFormat:@"MMM dd, YYYY"];
        
        
        formatedDate = [formatter stringFromDate:fromDate];
        
    }
    
    return formatedDate;
}


#pragma mark- Change dateFormat method
+ (NSString*) changeDateTimeFormat:(NSString*)date
{
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    
    NSDate* fromDate = [formatter dateFromString:date];
    [formatter setDateFormat:@"MMM dd, YYYY - hh:mm a"];
    
    NSString* formatedDate = [formatter stringFromDate:fromDate];
    
    if (formatedDate == nil)
    {
        [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
        
        fromDate = [formatter dateFromString:date];
        [formatter setDateFormat:@"MMM dd, YYYY - hh:mm a"];
        
        formatedDate = [formatter stringFromDate:fromDate];
        
    }
    if (formatedDate == nil)
    {
        [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
        
        fromDate = [formatter dateFromString:date];
        [formatter setDateFormat:@"MMM dd, YYYY - hh:mm a"];

        
        formatedDate = [formatter stringFromDate:fromDate];
        
    }
    if (formatedDate == nil)
    {
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        fromDate = [formatter dateFromString:date];
        [formatter setDateFormat:@"MMM dd, YYYY - hh:mm a"];
        
        
        formatedDate = [formatter stringFromDate:fromDate];
        
    }
    
    return formatedDate;
    
}




#pragma mark- Change dateFormat method
+ (NSString*) changeDateCalenderFormat:(NSString*)date
{
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    
    NSDate* fromDate = [formatter dateFromString:date];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    NSString* formatedDate = [formatter stringFromDate:fromDate];
    
    if (formatedDate == nil)
    {
        [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
        
        fromDate = [formatter dateFromString:date];
        [formatter setDateFormat:@"yyyy-MM-dd"];
        
        formatedDate = [formatter stringFromDate:fromDate];
        
    }
    if (formatedDate == nil)
    {
        [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
        
        fromDate = [formatter dateFromString:date];
        [formatter setDateFormat:@"yyyy-MM-dd"];
        
        
        formatedDate = [formatter stringFromDate:fromDate];
        
    }
    if (formatedDate == nil)
    {
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        fromDate = [formatter dateFromString:date];
        [formatter setDateFormat:@"yyyy-MM-dd"];
        
        
        formatedDate = [formatter stringFromDate:fromDate];
        
    }
    
    return formatedDate;
    
}

+ (NSString*) changeTimeFormat:(NSString*)date{
    NSLog(@"Time=%@",date);
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    
    NSDate* fromDate = [formatter dateFromString:date];
    [formatter setDateFormat:@"hh:mm a"];
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];

    
    NSString* formatedDate = [formatter stringFromDate:fromDate];
    
    if (formatedDate == nil)
    {
        [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
        
        fromDate = [formatter dateFromString:date];
        [formatter setDateFormat:@"hh:mm a"];
        [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
        
        formatedDate = [formatter stringFromDate:fromDate];
        
    }
    if (formatedDate == nil)
    {
        [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
        
        fromDate = [formatter dateFromString:date];
        [formatter setDateFormat:@"hh:mm a"];
        [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];

        
        
        formatedDate = [formatter stringFromDate:fromDate];
        
    }
    if (formatedDate == nil)
    {
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        fromDate = [formatter dateFromString:date];
        [formatter setDateFormat:@"hh:mm a"];
        [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];

        
        
        formatedDate = [formatter stringFromDate:fromDate];
        
    }
    
    return formatedDate;
    
}

//Email Validation

+ (BOOL) emailValidation:(NSString*)emailId
{
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,10}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    return [emailTest evaluateWithObject:emailId];
}

// Password Validation

+ (BOOL) passwordValidation:(NSString *)pwd
{
    NSCharacterSet *upperCaseChars = [NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLKMNOPQRSTUVWXYZ"];
    NSCharacterSet *lowerCaseChars = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyz"];
    
    NSCharacterSet *numbers = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    
    BOOL passwordValidate = YES;
    if ( [pwd length] < 6 || [pwd length] > 20 )
        passwordValidate = NO;  // too long or too short
    NSRange rang;
    rang = [pwd rangeOfCharacterFromSet:[NSCharacterSet letterCharacterSet]];
    if ( !rang.length )
        passwordValidate = NO;  // no letter
    rang = [pwd rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet]];
    if ( !rang.length )
        passwordValidate = NO;  // no number;
    rang = [pwd rangeOfCharacterFromSet:upperCaseChars];
    if ( !rang.length )
        passwordValidate = NO;  // no uppercase letter;
    rang = [pwd rangeOfCharacterFromSet:lowerCaseChars];
    if ( !rang.length )
        passwordValidate = NO;  // no lowerCase Chars;
    rang = [pwd rangeOfCharacterFromSet:numbers];
    if ( !rang.length )
        passwordValidate = NO;  // no Number;
    return passwordValidate;
}

// Mobile Number Validation

+ (BOOL) mobileNumberValidity:(NSString*)mobileNumber
{
    NSString *numberRegEx = @"[0-9]{10}";
    NSPredicate *numberTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", numberRegEx];
    return ([numberTest evaluateWithObject:mobileNumber] == YES);
}

+ (BOOL) pinNumberValidity:(NSString*)pin
{
    NSString *numberRegEx = @"[0-9]";
    NSPredicate *numberTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", numberRegEx];
    return ([numberTest evaluateWithObject:pin] == YES);
}

+ (BOOL) cardNumberValidity:(NSString*)pin
{
    NSString *numberRegEx = @"[0-9]{16}";
    NSPredicate *numberTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", numberRegEx];
    return ([numberTest evaluateWithObject:pin] == YES);
}

+ (BOOL) cvvNumberValidity:(NSString*)pin
{
    NSString *numberRegEx = @"[0-9]{3}";
    NSPredicate *numberTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", numberRegEx];
    return ([numberTest evaluateWithObject:pin] == YES);
}

//Vinay 02-05-2017

// AmexCard
+ (BOOL) amexCardValidation:(NSString*)cardNumber
{
    NSString *cardRegEx = @"^3[47][0-9]{13}$";
    NSPredicate *cardTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", cardRegEx];
    return [cardTest evaluateWithObject:cardNumber];
}

// DinersClubCard
+ (BOOL) dinersClubCardValidation:(NSString*)cardNumber
{
    NSString *cardRegEx = @"^3(?:0[0-5]|[68][0-9])[0-9]{11}$";
    NSPredicate *cardTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", cardRegEx];
    return [cardTest evaluateWithObject:cardNumber];
}

// DiscoverCard
+ (BOOL) discoverCardValidation:(NSString*)cardNumber
{
    NSString *cardRegEx = @"^65[4-9][0-9]{13}|64[4-9][0-9]{13}|6011[0-9]{12}|(622(?:12[6-9]|1[3-9][0-9]|[2-8][0-9][0-9]|9[01][0-9]|92[0-5])[0-9]{10})$";
    NSPredicate *cardTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", cardRegEx];
    return [cardTest evaluateWithObject:cardNumber];
}

// JCB CARD
+ (BOOL) JCBCardValidation:(NSString*)cardNumber
{
    NSString *cardRegEx = @"^(?:2131|1800|35\\d{3})\\d{11}$";
    NSPredicate *cardTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", cardRegEx];
    return [cardTest evaluateWithObject:cardNumber];
}

// Maestro Card
+ (BOOL) maestroCardValidation:(NSString*)cardNumber
{
    NSString *cardRegEx = @"^(5018|5020|5038|6304|6759|6761|6763)[0-9]{8,15}$";
    NSPredicate *cardTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", cardRegEx];
    return [cardTest evaluateWithObject:cardNumber];
}

// Master Card
+ (BOOL) mastercardValidation:(NSString*)cardNumber
{
    NSString *cardRegEx = @"^5[1-5][0-9]{14}$";
    NSPredicate *cardTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", cardRegEx];
    return [cardTest evaluateWithObject:cardNumber];
}

// Visa Card
+ (BOOL) visaCardValidation:(NSString*)cardNumber
{
    NSString *cardRegEx = @"^4[0-9]{12}(?:[0-9]{3})?$";
    NSPredicate *cardTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", cardRegEx];
    return [cardTest evaluateWithObject:cardNumber];
}

// AmericanExpress Card
+ (BOOL) americanExpressValidation:(NSString*)cardNumber
{
    NSString *cardRegEx = @"^3[47][0-9]{13}$";
    NSPredicate *cardTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", cardRegEx];
    return [cardTest evaluateWithObject:cardNumber];
}

// RupayDebitCard
+ (BOOL) rupayDebitCardValidation:(NSString*)cardNumber
{
    NSString *cardRegEx = @"^6[0-9]{15}$";
    NSPredicate *cardTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", cardRegEx];
    return [cardTest evaluateWithObject:cardNumber];
}
//Vinay 02-05-2017

//Name Validation

+ (BOOL) nameValidity:(NSString*)lastName
{
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:
                                  @"[a-zA-Z ]" options:0 error:&error];
    
    NSUInteger numberOfMatches = [regex numberOfMatchesInString:lastName options:0 range:NSMakeRange(0, [lastName length])];
    
    return numberOfMatches == lastName.length;
}

+ (int) calculateDiscount:(float)originalPrice salePrice:(float)salesPrice
{
    float salesPriceNew = [[NSString stringWithFormat:@"%.2f",salesPrice]floatValue];
    float originalPriceNew = [[NSString stringWithFormat:@"%.2f",originalPrice]floatValue];
    
    float percentOff = (originalPriceNew - salesPriceNew)/originalPriceNew;
    return percentOff*100;
    //NSLog(@"percentOff = %d",percentOff);
}
@end
