//
//  SignupViewController.m
//  TDATC
//
//  Created by iWeb on 9/15/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "SignupViewController.h"
#import "PatientProfile_VC.h"
#import "DataManager.h"
#import "Utility.h"
#import "MyLoader.h"
#import "DataParser.h"
#import "Defines.h"

@interface SignupViewController ()

@end

@implementation SignupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    check=0;
    
    _gDataManager.isDoctor=YES;
    
    UITapGestureRecognizer* gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userTappedOnTerm:)];
    // if labelView is not set userInteractionEnabled, you must do so
    [_lbl_term setUserInteractionEnabled:YES];
    [_lbl_term addGestureRecognizer:gesture];
    // Do any additional setup after loading the view.
}
- (void)userTappedOnTerm:(UIGestureRecognizer*)gestureRecognizer{
    NSLog(@"touch on");
    
}

- (IBAction)touch_Signup:(id)sender{
    if([self validateUserData])
    {
        if(check==1)
        {
            [self signupAPI];
        }
        else
        {

            [Utility alertWithTitle:nil withMessage:@"Please Agree Term and Conditions" onVC:self];

        }
    }
    
}

- (IBAction)touch_TermImage:(id)sender {
    _btn_Check.selected = !_btn_Check.selected;
    if(_btn_Check.selected)
    {
        check=0;
        [_btn_Check setImage:[UIImage imageNamed:@"untick"] forState:UIControlStateNormal];
    }
    else
    {
        check=1;
        [_btn_Check setImage:[UIImage imageNamed:@"tick"] forState:UIControlStateNormal];
        
    }
    
}
-(void)signupAPI
{
    NSString *postData;
    if(_gDataManager.isDoctor)
    {
        postData=[NSString stringWithFormat:@"fullname=%@&email=%@&password=%@&role=2",txt_fullname.text,txt_Email.text,txt_passowrd.text];
    }
    else
    {
        postData=[NSString stringWithFormat:@"fullname=%@&email=%@&password=%@&role=3",txt_fullname.text,txt_Email.text,txt_passowrd.text];
    }
    
    [MyLoader showLoadingView];
    [DataParser globalHitAPI:signUpAPI detaiInfi:postData WithCompletionHandler:^(NSDictionary *dict, BOOL isSuccessful)
     {
         if (isSuccessful)
         {
             [MyLoader hideLoadingView];
             
             if([dict[@"status"][@"status"] isEqualToString:@"success"])
             {
                 txt_fullname.text=@"";
                 txt_Email.text=@"";
                 txt_passowrd.text=@"";
                 
                 [Utility alertWithTitle:kAlertTitle withMessage:@"Registation Successfully. Please login." onVC:self];
                 
                 //Ankur
             }
             else
             {
                 [Utility alertWithTitle:kAlertTitle withMessage:@"Registation Un-Successfully" onVC:self];
                // [Utility alertWithTitle:kAlertTitle withMessage:dict[@"message"] onVC:self];
             }
         }
         else
         {
             [MyLoader hideLoadingView];
             [Utility alertWithTitle:@"" withMessage:kNetworkErrorTitle onVC:self];
         }
     }];
}
- (BOOL) validateUserData
{
    
    NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    
    if ([txt_Email.text isEqualToString:@" "]|| [[txt_Email.text stringByTrimmingCharactersInSet:whitespace] length] == 0 || [txt_fullname.text isEqualToString:@" "]|| [[txt_fullname.text stringByTrimmingCharactersInSet:whitespace] length] == 0 || [txt_passowrd.text isEqualToString:@" "]|| [[txt_passowrd.text stringByTrimmingCharactersInSet:whitespace] length] == 0)
    {
        [Utility alertWithTitle:@"" withMessage:@"All fields are mandatory." onVC:self];
        return NO;
    }
    if([Utility emailValidation:txt_Email.text])
    {
        
    }
    else
    {
        [Utility alertWithTitle:@"" withMessage:@"Please Enter valid email" onVC:self];
        return NO;
    }
    if(txt_passowrd.text.length>6)
    {
        
    }
    else
    {
        [Utility alertWithTitle:@"" withMessage:@"Password Must have more than six number" onVC:self];
        return NO;
    }

    return YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)touch_Doctor:(id)sender{
    btn_Doctor.selected = !btn_Doctor.selected;
    if(btn_Doctor.selected)
    {
        
        _gDataManager.isDoctor=YES;
       
        
        [btn_Patient setImage:[UIImage imageNamed:@"radio_btn_inactive"] forState:UIControlStateNormal];
        [btn_Doctor setImage:[UIImage imageNamed:@"radio_btn_active"] forState:UIControlStateNormal];
    }
    else
    {
        _gDataManager.isDoctor=NO;
        [btn_Doctor setImage:[UIImage imageNamed:@"radio_btn_inactive"] forState:UIControlStateNormal];
        [btn_Patient setImage:[UIImage imageNamed:@"radio_btn_active"] forState:UIControlStateNormal];
      
    }
    
}
- (IBAction)touch_Patient:(id)sender{
    
    btn_Patient.selected = !btn_Patient.selected;
    if(btn_Patient.selected)
    {
        _gDataManager.isDoctor=NO;
        [btn_Doctor setImage:[UIImage imageNamed:@"radio_btn_inactive"] forState:UIControlStateNormal];
        [btn_Patient setImage:[UIImage imageNamed:@"radio_btn_active"] forState:UIControlStateNormal];
        
        
    }
    else
    {
        _gDataManager.isDoctor=YES;
        [btn_Patient setImage:[UIImage imageNamed:@"radio_btn_inactive"] forState:UIControlStateNormal];
        [btn_Doctor setImage:[UIImage imageNamed:@"radio_btn_active"] forState:UIControlStateNormal];
       
    }
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
