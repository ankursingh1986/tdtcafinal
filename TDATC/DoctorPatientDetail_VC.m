//
//  DoctorPatientDetail_VC.m
//  TDATC
//
//  Created by iWeb on 9/19/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "DoctorPatientDetail_VC.h"
#import "DoctorPatientDetail_TVC.h"
#import "DoctorPatientProfile_TVC.h"
#import "DataManager.h"
#import "Utility.h"
#import "MyLoader.h"
#import "DataParser.h"
#import "Defines.h"

@interface DoctorPatientDetail_VC ()

@end

@implementation DoctorPatientDetail_VC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self leftButton];
    [tbl_View setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    self->tbl_View.tableFooterView = [[UIView alloc] init];


    arrayTitle=[[NSMutableArray alloc]initWithObjects:@"EMAIL ID",@"MOBILE NO",@"ALTERNATE MOBILE NUMBER",@"STATUS",@"REFFERD BY",@"GURANTOR",@"RELATIONS WITH GUARANTOR",@"GAURANTOR CONTACT", nil];
    
    arrayDesc=[[NSMutableArray alloc]initWithObjects:@"JOHNDEV@GMAIL.COM",@"+91-987867567",@"+91-987867566",@"XXX",@"XXXX",@"MISAN JONSAN",@"BROTHER",@"+91-9878674534", nil];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationItem.title = @"PATIENT DETAILS";
    
    [super viewWillAppear:animated];
    
}
#pragma mark - table view delegates and data socurce


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section==0)
    {
        return 150;
        
    }
    return 80;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section==0)
    {
        return  1;
        
    }
    
    return arrayTitle.count;
}
//

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section==0)
    {
        DoctorPatientProfile_TVC *cell = [tableView dequeueReusableCellWithIdentifier:@"DoctorPatientProfile_Cell" forIndexPath:indexPath];
        cell.lbl_name.text=[self.patientInfo.full_name uppercaseString];
        cell.lbl_address.text=self.patientInfo.locality;
        NSString *imagePath;
        
        
        if([self.patientInfo.patient_id isEqualToString:@"<null>"])
        {
            imagePath=[NSString stringWithFormat:@"%@%@",kPatientImagePath,self.patientInfo.patient_image];
            cell.lbl_age.text=[NSString stringWithFormat:@"%@ Yrs (%@)",self.patientInfo.age,[self.patientInfo.gender uppercaseString]];

            
        }
        else
        {
            imagePath=[NSString stringWithFormat:@"%@%@",kPatientImagePath,self.patientInfo.profile_image];
            cell.lbl_age.text=[NSString stringWithFormat:@"%@ Yrs (%@)",self.patientInfo.dob,[self.patientInfo.gender uppercaseString]];

            
        }

        
        
        if([imagePath isEqualToString:@"(null)(null)"])
        {
            
        }
        else
        {
            [self downloadImage:cell imageUrl1:imagePath];
            
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;

            return cell;
    }
    else
    {
        DoctorPatientDetail_TVC *cell = [tableView dequeueReusableCellWithIdentifier:@"DoctorPatientDetail_Cell" forIndexPath:indexPath];
    
    
        cell.lbl_subject.text=[arrayTitle objectAtIndex:indexPath.row];
      //  cell.lbl_desc.text=[arrayDesc objectAtIndex:indexPath.row];

        if(indexPath.row==0)
        {
            cell.lbl_desc.text=self.patientInfo.email;

        }
        else if (indexPath.row==1)
        {
            cell.lbl_desc.text=self.patientInfo.mobile;

        }
        else if (indexPath.row==2)
        {
            cell.lbl_desc.text=self.patientInfo.alt_mobile;
            
        }
        else if (indexPath.row==3)
        {
            cell.lbl_desc.text=[self.patientInfo.married_status capitalizedString];
            
        }
        else if (indexPath.row==4)
        {
            cell.lbl_desc.text=[self.patientInfo.referrd_by capitalizedString];
            
        }
        else if (indexPath.row==5)
        {
            cell.lbl_desc.text=[self.patientInfo.guarantor capitalizedString];
            
        }
        else if (indexPath.row==6)
        {
            cell.lbl_desc.text=[self.patientInfo.guarantor_relation capitalizedString];
            
        }
        else if (indexPath.row==7)
        {
            cell.lbl_desc.text=self.patientInfo.g_contact;
            
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;

        return cell;
    }
    return  nil;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    DoctoreMessageDetailViewController* destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"DoctoreMessageDetailViewController_id"];
    //
    //    [self.navigationController pushViewController:destVC animated:YES];
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction) backButtonAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
    
}
-(void)leftButton{
    UIButton *button =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(backButtonAction:)forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0, 20, 20)];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = barButton;
    self.navigationItem.hidesBackButton=TRUE;
    
}
- (void) downloadImage:(DoctorPatientProfile_TVC*)cell imageUrl1:(NSString *)imgURL1
{
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       image_manager = [SDWebImageManager sharedManager];
                       [image_manager downloadWithURL:[NSURL URLWithString:imgURL1] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize, NSInteger feedID, NSString *feedString)
                        {
                        } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished)
                        {
                            if (image)
                            {
                                cell.img_profile.image = image;
                            }
                        }];
                   });
    
}


@end
