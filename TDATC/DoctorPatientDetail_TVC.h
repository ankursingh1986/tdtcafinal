//
//  DoctorPatientDetail_TVC.h
//  TDATC
//
//  Created by iWeb on 9/19/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DoctorPatientDetail_TVC : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbl_subject;
@property (weak, nonatomic) IBOutlet UILabel *lbl_desc;

@end
