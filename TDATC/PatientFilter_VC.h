//
//  PatientFilter_VC.h
//  TDATC
//
//  Created by iWeb on 9/25/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Global.pch"


@interface PatientFilter_VC : UIViewController
{
    IBOutlet UITableView *tbl_Filter;
    BOOL tbl_bool1,tbl_bool2;
    NSMutableArray *location,*availble;
    UIButton *button;
    SDWebImageManager *image_manager;

}
- (IBAction)touch_Location:(id)sender;
- (IBAction)touch_Avalable:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *img_location;
@property (weak, nonatomic) IBOutlet UIButton *btn_avalble;
@property (weak, nonatomic) IBOutlet UIImageView *img_availble;

@property (nonatomic, strong) NSMutableArray* clinicArray;


@property (weak, nonatomic) IBOutlet UIButton *btn_location;

@end
