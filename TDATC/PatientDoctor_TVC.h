//
//  PatientDoctor_tVC.h
//  TDATC
//
//  Created by iWeb on 9/23/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASStarRatingView.h"
#import "FindDoctorModel.h"
#import "Global.pch"

@interface PatientDoctor_TVC : UITableViewCell
{
    SDWebImageManager *image_manager;

}
@property (weak, nonatomic) IBOutlet UILabel *lbl_price;
@property (weak, nonatomic) IBOutlet ASStarRatingView *view_star;
@property (weak, nonatomic) IBOutlet UIButton *btn_Book;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (weak, nonatomic) IBOutlet UILabel *educationLabel;
@property (weak, nonatomic) IBOutlet UILabel *localityLabel;
@property (weak, nonatomic) IBOutlet UILabel *exprianceLabel;
@property (weak, nonatomic) IBOutlet UIImageView *img_profile;


- (void) setUpData:(FindDoctorModel*)data;

@end
