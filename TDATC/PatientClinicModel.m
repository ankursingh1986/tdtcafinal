//
//  PatientClinicModel.m
//  TDATC
//
//  Created by iWeb on 11/9/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "PatientClinicModel.h"

@implementation PatientClinicModel
- (id) initWithDictionary:(NSDictionary*)dict
{
    self = [super init];
    if (self)
    {
        if (![dict[@"da_id"] isKindOfClass:[NSNull class]])
            self.da_id = dict[@"da_id"];
        else
            self.da_id = @"";
        if (![dict[@"availabledate"] isKindOfClass:[NSNull class]])
            self.availabledate = dict[@"availabledate"];
        else
            self.availabledate = @"";
        
        self.doctor_avail_timesArray = dict[@"doctor_avail_times"];
             
    }
    return self;
}

@end
