//
//  PatientInboxMessageTableViewCell.m
//  TDATC
//
//  Created by Vinay on 07/11/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "PatientInboxMessageTableViewCell.h"

@implementation PatientInboxMessageTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void) setUpData:(PatientMessageModel*)data
{
    self.txt_title.text = data.subject;
    
    
    if([data.attchment isEqualToString:@""])
    {
        self.txt_desc.text = data.doctor_msg;
        
    }
    else
    {
        NSString *strData=[NSString stringWithFormat:@"%@ \n http://thedocsaroundtheclock.com/img/msg/%@", data.doctor_msg, data.attchment];
        self.txt_desc.text = strData;

    }

    
    self.lbl_date.text = [Utility changeDateTimeFormat:data.date];
    [self downloadImage:data.doctorimage_path];

//    self.dateLabel.text = data.date;

}
- (void) downloadImage:(NSString *)imgURL1
{
    NSLog(@"Image URL in method=%@",imgURL1);
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       image_manager = [SDWebImageManager sharedManager];
                       [image_manager downloadWithURL:[NSURL URLWithString:imgURL1] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize, NSInteger feedID, NSString *feedString)
                        {
                        } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished)
                        {
                            if (image)
                            {
                                _img_profile.image = image;
                            }
                        }];
                   });
    
}

@end
