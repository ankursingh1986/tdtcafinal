//
//  PatientMessageModel.m
//  TDATC
//
//  Created by Vinay on 07/11/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "PatientMessageModel.h"

@implementation PatientMessageModel

- (id) initWithDictionary:(NSDictionary*)dictionary
{
    self = [super init];
    if (self)
    {
        if (![dictionary[@"subject"] isKindOfClass:[NSNull class]])
            self.subject = dictionary[@"subject"];
        else
            self.subject = @"";
        
        if (![dictionary[@"date"] isKindOfClass:[NSNull class]])
            self.date = dictionary[@"date"];
        else
            self.date = @"";
        
        self.msg_id = dictionary[@"msg_id"];
        
         if (![dictionary[@"doctor_msg"] isKindOfClass:[NSNull class]])
         {
        
            NSString* result = [dictionary[@"doctor_msg"] stringByReplacingOccurrencesOfString:@"<p>" withString:@""];
            result = [result stringByReplacingOccurrencesOfString:@"</p>" withString:@""];
            self.doctor_msg = result;
         }
         else
             self.doctor_msg = @"";
        
        self.doctorimage_path = dictionary[@"doctorimage_path"];
        self.patientimag_path = dictionary[@"patientimag_path"];
        
        if (![dictionary[@"attacmet"][@"filename"] isKindOfClass:[NSNull class]])
            self.attchment =[dictionary[@"attacmet"][@"filename"] stringByReplacingOccurrencesOfString:@" " withString:@"%20"] ;
        else
            self.attchment = @"";

    }
    return self;
}

@end
