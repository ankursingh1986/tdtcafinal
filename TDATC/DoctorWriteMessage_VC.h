//
//  DoctorWriteMessage_VC.h
//  TDATC
//
//  Created by iWeb on 9/18/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDWebImageManager.h"
#import "DoctorMessage.h"
#import "Global.pch"



@interface DoctorWriteMessage_VC : UIViewController<UITextFieldDelegate,UITextViewDelegate,UIGestureRecognizerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    int placeflag;
    IBOutlet UITableView *tbl_inbox,*tbl_sent;
    SDWebImageManager *image_manager;

}
@property (weak, nonatomic) IBOutlet UILabel *lbl_imageName;
@property (weak, nonatomic) IBOutlet UILabel *lbl_attachment;
@property (weak, nonatomic) IBOutlet UIImageView *img_profile;
@property (weak, nonatomic) IBOutlet UILabel *lbl_age;

@property (weak, nonatomic) IBOutlet UILabel *lbl_name;
@property (nonatomic, strong) NSString* patientId;
@property (weak, nonatomic) IBOutlet UITextView *textView_1;

@property (nonatomic,strong) DoctorMessage *patientMessageData;


@property (nonatomic,strong) NSMutableArray *inboxMessageArray;



- (IBAction)touch_Write:(id)sender;
- (IBAction)touch_Inbox:(id)sender;
- (IBAction)touch_Sent:(id)sender;
- (IBAction)sendBtnAction:(id)sender;
- (IBAction)attachmentBtnAction:(id)sender;


@property (weak, nonatomic) IBOutlet UIButton *btn_1;
@property (weak, nonatomic) IBOutlet UIButton *btn_2;
@property (weak, nonatomic) IBOutlet UIButton *btn_3;
@property (weak, nonatomic) IBOutlet UIView *view_1;
@property (weak, nonatomic) IBOutlet UITextField *txt_message;

@end
