//
//  FindDoctorModel.h
//  TDATC
//
//  Created by Vinay on 02/11/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FindDoctorModel : NSObject


@property (nonatomic, strong) NSString* user_id;
@property (nonatomic, strong) NSString* image;
@property (nonatomic, strong) NSString* full_name;
@property (nonatomic, strong) NSString* locality;
@property (nonatomic, strong) NSString* course;
@property (nonatomic, strong) NSString* institute;
@property (nonatomic, strong) NSString* city;
@property (nonatomic, strong) NSString* year;
@property (nonatomic, strong) NSString* rating;
@property (nonatomic, strong) NSString* exp;
@property (nonatomic, strong) NSString* award;
@property (nonatomic, strong) NSString* awardYear;







- (id) initWithDictionary:(NSDictionary*)dictionary;

@end
