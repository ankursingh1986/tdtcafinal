//
//  PatientFeedback_TVC.m
//  TDATC
//
//  Created by iWeb on 9/23/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "PatientFeedback_TVC.h"
#import "Global.pch"

@implementation PatientFeedback_TVC

- (void)awakeFromNib
{
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setupData:(PatientFeedbackModel*)data
{
    self.nameLabel.text = [data.full_name uppercaseString];
    self.dateLabel.text = [Utility changeDateTimeFormat:data.date];

   // self.dateLabel.text = data.date;
    self.messageTextView.text = data.pasent_msg;
}

@end
