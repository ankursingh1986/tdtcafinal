//
//  PatientFindDoctor_VC.m
//  TDATC
//
//  Created by iWeb on 9/23/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "PatientFindDoctor_VC.h"
#import "PatientDoctor_VC.h"
#import "DataManager.h"
#import "PatientDataParser.h"
#import "DataParser.h"
#import "Defines.h"
#import "Utility.h"
#import "MyLoader.h"
#import "DataParser.h"
#import "PickerModel.h"
#import "PickerTool.h"

@interface PatientFindDoctor_VC ()
{
    PickerTool* _pickerView;
    NSMutableArray* _countriesArray;
    NSMutableArray* _statesArray;
    NSMutableArray* _citiesArray;
    NSMutableArray* _servicesArray;

    NSString* _countryId;
    NSString* _stateId;
    NSString* _cityId;
    NSString* _serviceId;
}

@end

@implementation PatientFindDoctor_VC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self leftButton];
    [self rightButton];
    [self pickerViews];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationItem.title = @"FIND A DOCTOR";
    
    [super viewWillAppear:animated];
    
    _countriesArray = [[NSMutableArray alloc] init];
    _statesArray = [[NSMutableArray alloc] init];
    _citiesArray = [[NSMutableArray alloc] init];
    _servicesArray = [[NSMutableArray alloc] init];
    
    [self getCountryList];
//    [self getStateList];
//    [self getCityList];
    [self getServiceList];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) pickerViews
{
    _pickerView = [PickerTool loadClass];
    self.countryTextField.inputView = _pickerView;
    self.stateTextField.inputView = _pickerView;
    self.cityTextField.inputView = _pickerView;
    self.serviceTextField.inputView = _pickerView;
}

- (void) getCountryList
{
    [DataParser getCountryListWithCompletionHandler:^(NSArray *array, BOOL isSuccessful)
     {
         if (isSuccessful)
         {
             _countriesArray = [array mutableCopy];
             
             if (_countriesArray.count)
             {
                 PickerModel* data = [_countriesArray objectAtIndex:0];
                 _countryId = data.dataId;
                 self.countryTextField.text = data.dataName;
                 
                 [self getStateList:_countryId];
             }
         }
         else
         {
             [Utility alertWithTitle:kAlertTitle withMessage:kNetworkErrorTitle onVC:self];
         }
        
     }];
}

- (void) getStateList:(NSString*)data
{
    NSString* postData = [NSString stringWithFormat:@"country_id=%@",data];
    [DataParser getStateList:postData WithCompletionHandler:^(NSArray *array, BOOL isSuccessful)
     {
         if (isSuccessful)
         {
             [_statesArray removeAllObjects];
             _statesArray = [array mutableCopy];
             if (_statesArray.count)
             {
                 PickerModel* data = [_statesArray objectAtIndex:0];
                 _stateId = data.dataId;
                 self.stateTextField.text = data.dataName;
                [self getCityList:_stateId];
             }
         }
         else
         {
             [Utility alertWithTitle:kAlertTitle withMessage:kNetworkErrorTitle onVC:self];
         }
     }];
}

- (void) getCityList:(NSString*)data
{
    NSString* postData = [NSString stringWithFormat:@"state_id=%@",data];
    [DataParser getCityList:postData WithCompletionHandler:^(NSArray *array, BOOL isSuccessful)
     {
         if (isSuccessful)
         {
             [_citiesArray removeAllObjects];
             _citiesArray = [array mutableCopy];
             
             if (_citiesArray.count)
             {
                 PickerModel* data = [_citiesArray objectAtIndex:0];
                 _cityId = data.dataId;
                 self.cityTextField.text = data.dataName;
                 
                 // [self getStateList:_countryId];
             }

         }
         else
         {
             [Utility alertWithTitle:kAlertTitle withMessage:kNetworkErrorTitle onVC:self];
         }
     }];
}

- (void) getServiceList
{
    [PatientDataParser getServiceListWithCompletionHandler:^(NSArray *array, BOOL isSuccessful)
    {
        if (isSuccessful)
        {
            [_servicesArray removeAllObjects];
            _servicesArray = [array mutableCopy];
            
            if (_servicesArray.count)
            {
                PickerModel* data = [_servicesArray  objectAtIndex:0];
                _serviceId = data.dataId;
                self.serviceTextField.text = data.dataName;
            }

        }
        else
        {
            
        }
        
    }];
}

-(void)rightButton
{
    button =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"notification"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(nitificatioButtonAction:)forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0, 20, 20)];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.rightBarButtonItem = barButton;
}
- (IBAction) nitificatioButtonAction:(id)sender
{
    
    //    button.selected = !button.selected;
    //    if(button.selected)
    //    {
    //        self.view_1.hidden=NO;
    //        tbl_FeedBack.hidden=YES;
    //    }
    //    else
    //    {
    //        self.view_1.hidden=YES;
    //        tbl_FeedBack.hidden=NO;
    //    }
    
}
- (IBAction)touch_FindDoctor:(id)sender
{
    UIStoryboard *storyBoard12 = [UIStoryboard storyboardWithName:@"Patient" bundle:nil];
    PatientDoctor_VC* destVC = [storyBoard12 instantiateViewControllerWithIdentifier:@"PatientDoctor_VC_id"];
    destVC.countryId = _countryId;
    destVC.stateId = _stateId;
    destVC.cityId = _cityId;
    destVC.serviceId = _serviceId;
    [self.navigationController pushViewController:destVC animated:YES];
}

- (IBAction) backButtonAction:(id)sender
{
    UIStoryboard *storyBoard12 = [UIStoryboard storyboardWithName:@"Patient" bundle:nil];
    
    UIViewController *vc = [storyBoard12 instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
    [self presentViewController:vc animated:NO completion:nil];
    
    
}
-(void)leftButton
{
    UIButton *button2 =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button2 setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [button2 addTarget:self action:@selector(backButtonAction:)forControlEvents:UIControlEventTouchUpInside];
    [button2 setFrame:CGRectMake(0, 0, 20, 20)];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button2];
    self.navigationItem.leftBarButtonItem = barButton;
    self.navigationItem.hidesBackButton=TRUE;
    
}

//-------------------------------------------------------------------------------------------
#pragma mark- Text field delegate and data source
//-------------------------------------------------------------------------------------------

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField==self.countryTextField )
    {
        [self setPickerInfo:self.countryTextField withArray:_countriesArray];
    }
    else if(textField == self.stateTextField)
    {
        [MyLoader showLoadingView];
        [DataParser getStateList:[NSString stringWithFormat:@"country_id=%@",_countryId] WithCompletionHandler:^(NSArray *array, BOOL isSuccessful)
         {
             [MyLoader hideLoadingView];
             if (isSuccessful)
             {
                 if (array.count)
                 {
                     [_statesArray removeAllObjects];
                     _statesArray = [array mutableCopy];
                     [self setPickerInfo:self.stateTextField withArray:_statesArray];
                 }
                
             }
             else
             {
                 
             }
         }];
    }
    else if (textField == self.cityTextField)
    {
        [MyLoader showLoadingView];
        [DataParser getCityList:[NSString stringWithFormat:@"state_id=%@",_stateId] WithCompletionHandler:^(NSArray *array, BOOL isSuccessful)
         {
             [MyLoader hideLoadingView];
             if (isSuccessful)
             {
                 if (array.count)
                 {
                     [_citiesArray removeAllObjects];
                     _citiesArray = [array mutableCopy];
                     [self setPickerInfo:self.cityTextField withArray:_citiesArray];
                 }
             }
             else
             {
                 
             }
         }];
    }
    else if (textField == self.serviceTextField)
    {
        // [self getBloodGroupsList];
//        [PatientDataParser getServiceListWithCompletionHandler:^(NSArray *array, BOOL isSuccessful)
//         {
//             if (isSuccessful)
//             {
//                 [_servicesArray removeAllObjects];
//                 _servicesArray = [array mutableCopy];
                 [self setPickerInfo:self.serviceTextField withArray:_servicesArray];
//             }
//             else
//             {
//                 
//             }
//         }];
    }
}

#pragma mark- Picker Methods

- (void) setPickerInfo:(UITextField*)textfield  withArray:(NSArray*)array
{
    [_pickerView pickerViewMethod:textfield arr:array];
    _pickerView.completionHandler = ^(PickerModel* detail)
    {
        if(textfield == self.countryTextField)
            _countryId = detail.dataId;
        else if (textfield == self.stateTextField)
            _stateId = detail.dataId;
        else if (textfield == self.cityTextField)
            _cityId = detail.dataId;
        else if (textfield == self.serviceTextField)
        {
            _serviceId = detail.dataId;
        }
        //[textfield resignFirstResponder];
    };
}



@end
