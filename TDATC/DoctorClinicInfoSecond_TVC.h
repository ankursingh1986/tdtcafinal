//
//  DoctorClinicInfoSecond_TVC.h
//  TDATC
//
//  Created by iWeb on 11/2/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DoctorClinicInfoSecond_TVC : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lbl_1;
@property (weak, nonatomic) IBOutlet UILabel *lbl_2;
@property (weak, nonatomic) IBOutlet UILabel *lbl_3;
@property (weak, nonatomic) IBOutlet UILabel *lbl_4;
@property (weak, nonatomic) IBOutlet UILabel *lbl_5;
@property (weak, nonatomic) IBOutlet UILabel *lbl_6;
@property (weak, nonatomic) IBOutlet UILabel *lbl_7;
@property (weak, nonatomic) IBOutlet UILabel *lbl_8;
@property (weak, nonatomic) IBOutlet UILabel *lbl_9;
@property (weak, nonatomic) IBOutlet UILabel *lbl_10;
@property (weak, nonatomic) IBOutlet UILabel *lbl_11;
@property (weak, nonatomic) IBOutlet UILabel *lbl_12;
@property (weak, nonatomic) IBOutlet UILabel *lbl_13;
@property (weak, nonatomic) IBOutlet UILabel *lbl_14;
@property (weak, nonatomic) IBOutlet UILabel *lbl_15;
@property (weak, nonatomic) IBOutlet UILabel *lbl_16;
@property (weak, nonatomic) IBOutlet UILabel *lbl_17;
@property (weak, nonatomic) IBOutlet UILabel *lbl_18;
@property (weak, nonatomic) IBOutlet UILabel *lbl_19;
@property (weak, nonatomic) IBOutlet UILabel *lbl_20;
@property (weak, nonatomic) IBOutlet UILabel *lbl_21;
@property (weak, nonatomic) IBOutlet UILabel *lbl_22;
@property (weak, nonatomic) IBOutlet UILabel *lbl_23;
@property (weak, nonatomic) IBOutlet UILabel *lbl_24;

@end
