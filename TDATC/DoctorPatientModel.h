//
//  DoctorPatientModel.h
//  TDATC
//
//  Created by iWeb on 10/31/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DoctorPatientModel : NSObject
@property (nonatomic, strong) NSString* doctor_id;
@property (nonatomic, strong) NSString* patient_id;
@property (nonatomic, strong) NSString* full_name;

@property (nonatomic, strong) NSString* patient_image;
@property (nonatomic, strong) NSString* profile_image;

@property (nonatomic, strong) NSString* gender;
@property (nonatomic, strong) NSString* dob;


@property (nonatomic, strong) NSString* age;

@property (nonatomic, strong) NSString* locality;
@property (nonatomic, strong) NSString* email;
@property (nonatomic, strong) NSString* mobile;
@property (nonatomic, strong) NSString* alt_mobile;
@property (nonatomic, strong) NSString* married_status;
//
@property (nonatomic, strong) NSString* referrd_by;
@property (nonatomic, strong) NSString* guarantor;
@property (nonatomic, strong) NSString* guarantor_relation;
@property (nonatomic, strong) NSString* g_contact;

- (id) initWithDictionary:(NSDictionary*)dictionary;
@end
