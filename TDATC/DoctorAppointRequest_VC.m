//
//  DoctorAppointRequest_VC.m
//  TDATC
//
//  Created by iWeb on 9/19/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "DoctorAppointRequest_VC.h"
#import "DoctorAppointRequest_TVC.h"

#import "DataManager.h"
#import "Utility.h"
#import "MyLoader.h"
#import "DataParser.h"
#import "Defines.h"
#import "AppointRequestModel.h"

@interface DoctorAppointRequest_VC ()
{
    NSMutableArray* appointmentReqArray;
    
}

@end

@implementation DoctorAppointRequest_VC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self leftButton];
    [self getDataFromServer];
    
    appointmentReqArray = [[NSMutableArray alloc] init];

    [tbl_Appotment setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    self->tbl_Appotment.tableFooterView = [[UIView alloc] init];


    // Do any additional setup after loading the view.
}

-(void)getDataFromServer{
    
    
      NSString *postData=[NSString stringWithFormat:@"id=%@",_gDataManager.loginModel.user_id];
    //NSString *postData=[NSString stringWithFormat:@"id=210"];
    
    
    [MyLoader showLoadingView];
    [DataParser DappointmentRequestListHit:postData WithCompletionHandler:^(NSMutableArray *array, BOOL isSuccessful)
     {
         if (isSuccessful)
         {
             [MyLoader hideLoadingView];
             
             
             appointmentReqArray = [array mutableCopy];
             [tbl_Appotment reloadData];
             
             
             
         }
         else
         {
             [MyLoader hideLoadingView];
             [Utility alertWithTitle:@"" withMessage:kNetworkErrorTitle onVC:self];
         }
     }];
    
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationItem.title = @"APPOINTMENT REQUEST";
    
    [super viewWillAppear:animated];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - table view delegates and data socurce


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return appointmentReqArray.count;
}
//

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    DoctorAppointRequest_TVC *cell = [tableView dequeueReusableCellWithIdentifier:@"DoctorAppointRequest_Cell" forIndexPath:indexPath];
    
    AppointRequestModel*obj=[appointmentReqArray objectAtIndex:indexPath.row];
 
    cell.lbl_name.text=[obj.patientname uppercaseString];
    cell.lbl_mobile.text=obj.mobile;
    cell.lbl_desc.text=obj.sickness;
    NSArray* foo = [obj.createddate componentsSeparatedByString: @"T"];
    cell.lbl_date.text=[foo objectAtIndex: 0];
    cell.lbl_time.text=[Utility changeTimeFormat:obj.createddate];
    cell.lbl_facetoface.text=obj.consultingmode;
    cell.lbl_paymentMode.text=obj.payementway;
      [cell.btn_accept addTarget:self action:@selector(acceptAction:) forControlEvents:UIControlEventTouchUpInside];
      [cell.btn_reject addTarget:self action:@selector(rejectAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    NSString *imagePath=[NSString stringWithFormat:@"%@%@",kPatientImageSubPath,obj.image];
    if([imagePath isEqualToString:@"(null)(null)"])
    {
        
    }
    else
    {
        [self downloadImage:cell imageUrl1:imagePath];
        
    }

    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}
- (void) acceptAction:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero
                                           toView:tbl_Appotment];
    NSIndexPath *indexPath = [self->tbl_Appotment indexPathForRowAtPoint:buttonPosition];
    AppointRequestModel *obj=[appointmentReqArray objectAtIndex:indexPath.row];
    
    
    
    NSString *postData=[NSString stringWithFormat:@"booking_id=%@",obj.booking_id];
    
    [MyLoader showLoadingView];
    [DataParser globalHitAPI:doctorAppointmentACCEPTAPI detaiInfi:postData WithCompletionHandler:^(NSDictionary *dict, BOOL isSuccessful)
     {
         if (isSuccessful)
         {
             [MyLoader hideLoadingView];
             
             if([dict[@"status"] isEqualToString:@"false"])
             {

                 [Utility alertWithTitle:kAlertTitle withMessage:dict[@"message"] onVC:self];
                 
                 //Ankur
             }
             else
             {
                 [self getDataFromServer];

                 [Utility alertWithTitle:kAlertTitle withMessage:dict[@"message"] onVC:self];
             }
         }
         else
         {
             [MyLoader hideLoadingView];
             [Utility alertWithTitle:@"" withMessage:kNetworkErrorTitle onVC:self];
         }
     }];
   
    
}
- (void) rejectAction:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero
                                           toView:tbl_Appotment];
    NSIndexPath *indexPath = [self->tbl_Appotment indexPathForRowAtPoint:buttonPosition];
    AppointRequestModel *obj=[appointmentReqArray objectAtIndex:indexPath.row];
    
    NSString *postData=[NSString stringWithFormat:@"booking_id=%@",obj.booking_id];
    
    [MyLoader showLoadingView];
    [DataParser globalHitAPI:doctorAppointmentREJECTAPI detaiInfi:postData WithCompletionHandler:^(NSDictionary *dict, BOOL isSuccessful)
     {
         if (isSuccessful)
         {
             [MyLoader hideLoadingView];
             
             if([dict[@"status"] isEqualToString:@"false"])
             {
                 [Utility alertWithTitle:kAlertTitle withMessage:dict[@"message"] onVC:self];
                 //Ankur
             }
             else
             {
                 [self getDataFromServer];

                 [Utility alertWithTitle:kAlertTitle withMessage:dict[@"message"] onVC:self];
             }
         }
         else
         {
             [MyLoader hideLoadingView];
             [Utility alertWithTitle:@"" withMessage:kNetworkErrorTitle onVC:self];
         }
     }];
    
    
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    DoctoreMessageDetailViewController* destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"DoctoreMessageDetailViewController_id"];
    //
    //    [self.navigationController pushViewController:destVC animated:YES];
    
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction) backButtonAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
    
}
-(void)leftButton{
    UIButton *button =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(backButtonAction:)forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0, 20, 20)];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = barButton;
    self.navigationItem.hidesBackButton=TRUE;
    
}
- (void) downloadImage:(DoctorAppointRequest_TVC*)cell imageUrl1:(NSString *)imgURL1
{
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       image_manager = [SDWebImageManager sharedManager];
                       [image_manager downloadWithURL:[NSURL URLWithString:imgURL1] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize, NSInteger feedID, NSString *feedString)
                        {
                        } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished)
                        {
                            if (image)
                            {
                                cell.img_profile.image = image;
                            }
                        }];
                   });
    
}


@end
