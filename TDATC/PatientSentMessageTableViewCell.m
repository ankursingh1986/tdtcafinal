//
//  PatientSentMessageTableViewCell.m
//  TDATC
//
//  Created by Vinay on 07/11/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "PatientSentMessageTableViewCell.h"

@implementation PatientSentMessageTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setUpData:(PatientMessageModel*)data
{
    self.txt_title.text = data.subject;
    
    if([data.attchment isEqualToString:@""])
    {
        self.txt_desc.text = data.doctor_msg;
        
    }
    else
    {
        NSString *strData=[NSString stringWithFormat:@"%@ \n http://thedocsaroundtheclock.com/img/msg/%@", data.doctor_msg, data.attchment];
        self.txt_desc.text = strData;
        
    }
    
    
    self.lbl_date.text = [Utility changeDateTimeFormat:data.date];
    [self downloadImage:data.patientimag_path];

    
  //  [self downloadImage:]
}

- (void) downloadImage:(NSString *)imgURL1
{
    NSLog(@"Image URL in method=%@",imgURL1);
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       image_manager = [SDWebImageManager sharedManager];
                       [image_manager downloadWithURL:[NSURL URLWithString:imgURL1] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize, NSInteger feedID, NSString *feedString)
                        {
                        } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished)
                        {
                            if (image)
                            {
                                _img_profile.image = image;
                            }
                        }];
                   });
    
}


@end
