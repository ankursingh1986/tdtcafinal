//
//  DoctorMembership_VC.h
//  TDATC
//
//  Created by iWeb on 9/19/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Global.pch"

@interface DoctorMembership_VC : UIViewController
- (IBAction)touch_ViewPlan:(id)sender;
- (IBAction)touch_RenewPlan:(id)sender;
- (IBAction)touch_changeplan:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *view_head;
@property (weak, nonatomic) IBOutlet UILabel *currentMemberShipTypeLabel;
@property (weak, nonatomic) IBOutlet UITextField *memberShipYearType;
@property (weak, nonatomic) IBOutlet UITextField *memberShipPriceTextField;
@property (weak, nonatomic) IBOutlet UITextField *startDateTextField;
@property (weak, nonatomic) IBOutlet UITextField *endDateTextField;

@end
