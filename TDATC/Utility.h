//
//  Utility.h
//  DawaiBox
//
//  Created by Vinay Kumar on 21/11/16.
//  Copyright © 2016 com.garry. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Utility : NSObject

+ (void) alertWithTitle: (NSString *)title withMessage: (NSString*)message onVC:(UIViewController*)VC;

+ (BOOL) emailValidation:(NSString*)emailId;
+ (BOOL) passwordValidation:(NSString *)pwd;
+ (BOOL) cardNumberValidity:(NSString*)pin;
+ (BOOL) cvvNumberValidity:(NSString*)pin;
+ (BOOL) mobileNumberValidity:(NSString*)mobileNumber;
+ (BOOL) nameValidity:(NSString*)lastName;
+ (BOOL) pinNumberValidity:(NSString*)pin;
+ (NSString*) changeDateFormat:(NSString*)date;
+ (NSString*) changeDateTimeFormat:(NSString*)date;
+ (NSString*) changeTimeFormat:(NSString*)date;
+ (NSString*) changeDateCalenderFormat:(NSString*)date;

+ (NSString*) changeDateFormat1:(NSString*)date;


+ (NSString*) getAgeFromDOBFormat:(NSString*)date;




//Vinay 02-05-2017
+ (BOOL) amexCardValidation:(NSString*)cardNumber;
+ (BOOL) dinersClubCardValidation:(NSString*)cardNumber;
+ (BOOL) discoverCardValidation:(NSString*)cardNumber;
+ (BOOL) rupayDebitCardValidation:(NSString*)cardNumber;
+ (BOOL) americanExpressValidation:(NSString*)cardNumber;
+ (BOOL) visaCardValidation:(NSString*)cardNumber;
+ (BOOL) mastercardValidation:(NSString*)cardNumber;
+ (BOOL) maestroCardValidation:(NSString*)cardNumber;
+ (BOOL) JCBCardValidation:(NSString*)cardNumbe;
//Vinay 02-05-2017
+ (int) calculateDiscount:(float)originalPrice salePrice:(float)salesPrice;
@end
