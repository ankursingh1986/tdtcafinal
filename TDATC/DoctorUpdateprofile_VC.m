//
//  DoctorUpdateprofile_VC.m
//  TDATC
//
//  Created by iWeb on 9/19/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "DoctorUpdateprofile_VC.h"
#import "DataManager.h"
#import "Utility.h"
#import "MyLoader.h"
#import "DataParser.h"
#import "Defines.h"
#import "PickerModel.h"
#import "PickerTool.h"

@interface DoctorUpdateprofile_VC ()
{
    PickerTool* _pickerView;
    NSString* _genderString;
    NSMutableArray* _countriesArray;
    NSMutableArray* _statesArray;
    NSMutableArray* _citiesArray;
    
    BOOL _isImageSelected;

    NSString* _countryId;
    NSString* _stateId;
    NSString* _cityId;
    UIImagePickerController* _imagePicker;

}

@end

@implementation DoctorUpdateprofile_VC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self leftButton];
    
    
    _countriesArray = [[NSMutableArray alloc] init];
    _statesArray = [[NSMutableArray alloc] init];
    _citiesArray = [[NSMutableArray alloc] init];
    
    _countryId = _gDataManager.loginModel.country_id;
    _stateId = _gDataManager.loginModel.state_id;
    _cityId = _gDataManager.loginModel.city_id;
    
    [self getAllCountriesList];
    [self getStateList:_gDataManager.loginModel.country_id];
    [self getCitiesList:_gDataManager.loginModel.state_id];
    
    
    _img_profile.layer.cornerRadius = _img_profile.frame.size.width / 2;
    _img_profile.clipsToBounds = YES;
    
    _img_profile.layer.borderWidth = 6.0f;
    _img_profile.layer.borderColor = [UIColor whiteColor].CGColor;
    // Do any additional setup after loading the view.
    [self FirstData];
    [self pickerViews];
    [self dateOfBirthPicker];
    [self loadData];
}
-(void)loadData{
    NSString *imagePath=[NSString stringWithFormat:@"%@",_gDataManager.doctorProfileImagePath];
    if([imagePath isEqualToString:@"(null)(null)"])
    {
        
    }
    else
    {
        [self downloadImage:imagePath];
        
    }

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    
    self.navigationItem.title = [_gDataManager.loginModel.full_name uppercaseString];
   
}

- (void) pickerViews
{
    _pickerView = [PickerTool loadClass];
    self.txt_india.inputView = _pickerView;
    self.txt_state.inputView = _pickerView;
    self.txt_city.inputView = _pickerView;
    
    
    _img_profile.userInteractionEnabled=YES;
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(oneTap:)];
    singleTap.numberOfTapsRequired = 1;
    singleTap.numberOfTouchesRequired = 1;
    singleTap.delegate = self;
    [_img_profile addGestureRecognizer:singleTap];
}

-(void)FirstData
{
    
    _btn_update.hidden=YES;
    
    _txt_DOB.userInteractionEnabled=NO;
    _txt_pin.userInteractionEnabled=NO;
    _txt_city.userInteractionEnabled=NO;
    _txt_email.userInteractionEnabled=NO;
    _txt_india.userInteractionEnabled=NO;
    _txt_state.userInteractionEnabled=NO;
    _txt_mobile.userInteractionEnabled=NO;
    _txt_fullname.userInteractionEnabled=NO;
    _txt_locality.userInteractionEnabled=NO;
    _btn_female.userInteractionEnabled=NO;
    _btn_male.userInteractionEnabled=NO;
    
    _img_city.hidden=YES;
    _img_india.hidden=YES;
    _img_state.hidden=YES;
    
    
    if ([_gDataManager.loginModel.gender isEqualToString:@"female"])
    {
        _genderString = @"female";
        [self.btn_female setImage:[UIImage imageNamed:@"radiobtn_active"] forState:UIControlStateNormal];
        [self.btn_male setImage:[UIImage imageNamed:@"radiobtn_inactive"] forState:UIControlStateNormal];
        self.btn_female.selected = !self.btn_female.selected;
    }
    else
    {
        _genderString = @"male";
        [self.btn_male setImage:[UIImage imageNamed:@"radiobtn_active"] forState:UIControlStateNormal];
        [self.btn_female setImage:[UIImage imageNamed:@"radiobtn_inactive"] forState:UIControlStateNormal];
         self.btn_male.selected = !self.btn_male.selected;
    }
    
    self.txt_DOB.text = [Utility changeDateFormat:_gDataManager.loginModel.dob];

    self.txt_pin.text = _gDataManager.loginModel.pincode;
    //self.txt_city.text = _gDataManager.loginModel.
    self.txt_email.text = _gDataManager.loginModel.email;
    //self.txt_india.text = _gDataManager.loginModel.country_id;
    
    //self.txt_state.text = _gDataManager.loginModel.state_id;
    
    self.txt_mobile.text = [NSString stringWithFormat:@"%@",_gDataManager.loginModel.mobile_number];
    self.txt_fullname.text = _gDataManager.loginModel.full_name;
    
    self.txt_locality.text  = _gDataManager.loginModel.locality;
    
}


- (void) getAllCountriesList
{
    [MyLoader showLoadingView];
    [DataParser getCountryListWithCompletionHandler:^(NSArray *array, BOOL isSuccessful)
    {
        [MyLoader hideLoadingView];
        if (isSuccessful)
        {
            NSLog(@"country id=%@",_gDataManager.loginModel.country_id);
            _countriesArray = [array mutableCopy];
            for (int i=0; i<_countriesArray.count; i++) {
                PickerModel *obj=[_countriesArray objectAtIndex:i];
                NSLog(@"id=%@",obj.dataId);
                NSString *datId=[NSString stringWithFormat:@"%@",obj.dataId];
                NSString *coutryId=[NSString stringWithFormat:@"%@",_gDataManager.loginModel.country_id];

                if([datId isEqualToString:coutryId])
                {
                    _txt_india.text=obj.dataName;
                    break;
                }
            }
        }
        else
        {
            [Utility alertWithTitle:kAlertTitle withMessage:kNetworkErrorTitle onVC:self];
        }
        
    }];
}

- (void) getStateList:(NSString*)countyId
{
    [MyLoader showLoadingView];
    [DataParser getStateList:[NSString stringWithFormat:@"country_id=%@",countyId] WithCompletionHandler:^(NSArray *array, BOOL isSuccessful)
    {
        [MyLoader hideLoadingView];
        if (isSuccessful)
        {
            [_statesArray removeAllObjects];
            _statesArray = [array mutableCopy];
            for (int i=0; i<_statesArray.count; i++) {
                PickerModel *obj=[_statesArray objectAtIndex:i];
                
                NSString *datId=[NSString stringWithFormat:@"%@",obj.dataId];
                NSString *stateId=[NSString stringWithFormat:@"%@",_gDataManager.loginModel.state_id];
                if([datId isEqualToString:stateId])
                {
                    _txt_state.text=obj.dataName;
                    break;
                }
            }
        }
        else
        {
            [Utility alertWithTitle:kAlertTitle withMessage:kNetworkErrorTitle onVC:self];
        }
        
    }];
}

- (void) getCitiesList:(NSString*)stateId
{
    [MyLoader showLoadingView];
    [DataParser getCityList:[NSString stringWithFormat:@"state_id=%@",stateId] WithCompletionHandler:^(NSArray *array, BOOL isSuccessful)
     {
         [MyLoader hideLoadingView];
         if (isSuccessful)
         {
             [_citiesArray removeAllObjects];
             _citiesArray = [array mutableCopy];
             for (int i=0; i<_citiesArray.count; i++) {
                 PickerModel *obj=[_citiesArray objectAtIndex:i];
                 
                 NSString *datId=[NSString stringWithFormat:@"%@",obj.dataId];
                 NSString *cityId=[NSString stringWithFormat:@"%@",_cityId];
                 if([datId isEqualToString:cityId])
                 {
                     _txt_city.text=obj.dataName;
                     break;
                 }
             }
         }
         else
         {
             [Utility alertWithTitle:kAlertTitle withMessage:kNetworkErrorTitle onVC:self];
         }
         
     }];
}

- (IBAction) backButtonAction:(id)sender
{
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
    [self presentViewController:vc animated:NO completion:nil];
}


-(void)leftButton
{
    UIButton *button =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(backButtonAction:)forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0, 20, 20)];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = barButton;
    self.navigationItem.hidesBackButton=TRUE;
}


- (IBAction)touch_edit:(id)sender
{
    
    _btn_update.hidden=NO;
    _btn_edit.hidden=YES;
    
    _txt_DOB.userInteractionEnabled=YES;
    _txt_pin.userInteractionEnabled=YES;
    _txt_city.userInteractionEnabled=YES;
    _txt_email.userInteractionEnabled=NO;
    _txt_india.userInteractionEnabled=YES;
    _txt_state.userInteractionEnabled=YES;
    _txt_mobile.userInteractionEnabled=YES;
    _txt_fullname.userInteractionEnabled=YES;
    _txt_locality.userInteractionEnabled=YES;
    _btn_female.userInteractionEnabled=YES;
    _btn_male.userInteractionEnabled=YES;
    
    _img_city.hidden=NO;
    _img_india.hidden=NO;
    _img_state.hidden=NO;
    
}
- (IBAction)btn_MaleTouch:(id)sender
{
    _btn_male.selected = !_btn_male.selected;
    if(_btn_male.selected)
    {
        [_btn_female setImage:[UIImage imageNamed:@"radiobtn_inactive"] forState:UIControlStateNormal];
        [_btn_male setImage:[UIImage imageNamed:@"radiobtn_active"] forState:UIControlStateNormal];
      
    }
    else
    {
       
        [_btn_male setImage:[UIImage imageNamed:@"radiobtn_inactive"] forState:UIControlStateNormal];
        [_btn_female setImage:[UIImage imageNamed:@"radiobtn_active"] forState:UIControlStateNormal];
    }
    
}
- (IBAction)touch_femail:(id)sender
{
    
    _btn_female.selected = !_btn_female.selected;
    if(_btn_female.selected)
    {
        
        [_btn_male setImage:[UIImage imageNamed:@"radiobtn_inactive"] forState:UIControlStateNormal];
        [_btn_female setImage:[UIImage imageNamed:@"radiobtn_active"] forState:UIControlStateNormal];
    }
    else
    {
        [_btn_female setImage:[UIImage imageNamed:@"radiobtn_inactive"] forState:UIControlStateNormal];
        [_btn_male setImage:[UIImage imageNamed:@"radiobtn_active"] forState:UIControlStateNormal];
    }

    
}

- (IBAction)touch_update:(id)sender
{
    if ([self userInputValidations])
    {
        
        NSString* image64Data;
        
        if (!_isImageSelected)
        {
            image64Data = @"";
        }
        else
        {
            //image64Data = @"";
            
            NSData *imageData=UIImageJPEGRepresentation(_img_profile.image,.5);
            // NSString *base64 = [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
            image64Data = [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
        }
        

        
        NSString* postData = [NSString stringWithFormat:@"id=%@&full_name=%@&mobile_number=%@&locality=%@&pincode=%@&country_id=%@&state_id=%@& city_id=%@&insurance=%@&zoom_id=%@&zoom_vedio_url_id=%@&image=%@&gender=%@&dob=%@",_gDataManager.loginModel.user_id, self.txt_fullname.text, self.txt_mobile.text, self.txt_locality.text, self.txt_pin.text,_countryId,_stateId, _cityId,@"", _gDataManager.loginModel.zoom_id,_gDataManager.loginModel.zoom_vedio_url_id,image64Data, _gDataManager.loginModel.gender, self.txt_DOB.text];
        NSLog(@"send data=%@",postData);
        [MyLoader showLoadingView];

        [DataParser updateDoctorProfileAPI:postData WithCompletionHandler:^(NSDictionary *dict, BOOL isSuccessful)
         {
             [MyLoader hideLoadingView];

             if (isSuccessful)
             {
                 _btn_update.hidden=YES;
                 _btn_edit.hidden=NO;
                 
                 _txt_DOB.userInteractionEnabled=NO;
                 _txt_pin.userInteractionEnabled=NO;
                 _txt_city.userInteractionEnabled=NO;
                 _txt_email.userInteractionEnabled=NO;
                 _txt_india.userInteractionEnabled=NO;
                 _txt_state.userInteractionEnabled=NO;
                 _txt_mobile.userInteractionEnabled=NO;
                 _txt_fullname.userInteractionEnabled=NO;
                 _txt_locality.userInteractionEnabled=NO;
                 _btn_female.userInteractionEnabled=NO;
                 _btn_male.userInteractionEnabled=NO;
                 
                 _img_city.hidden=YES;
                 _img_india.hidden=YES;
                 _img_state.hidden=YES;
                 NSLog(@"response data=%@",dict);
                 if([dict[@"status"] isEqualToString:@"true"])
                 {
                      [Utility alertWithTitle:nil withMessage:dict[@"message"] onVC:self];
                 }
                 else
                 {
                     [Utility alertWithTitle:nil withMessage:dict[@"message"] onVC:self];

                 }
                 
             }
             else
             {
                 [Utility alertWithTitle:kAlertTitle withMessage:kNetworkErrorTitle onVC:self];
             }
             
         }];
    }
    
   }

- (BOOL) userInputValidations
{
    if ([self.txt_email.text isEqualToString:@""])
    {
        [Utility alertWithTitle:kAlertTitle withMessage:@"Please enter a valid email" onVC:self];
        return FALSE;
    }
    
    if ([self.txt_mobile.text isEqualToString:@""])
    {
        [Utility alertWithTitle:kAlertTitle withMessage:@"Please enter a valid mobile number" onVC:self];
        return FALSE;
    }
    
    if ([self.txt_fullname.text isEqualToString:@""])
    {
        [Utility alertWithTitle:kAlertTitle withMessage:@"Please enter your full name" onVC:self];
        return FALSE;
    }
    
//    if ([self.txt_DOB.text isEqualToString:@""])
//    {
//        [Utility alertWithTitle:kAlertTitle withMessage:@"Please enter your DOB" onVC:self];
//        return FALSE;
//    }
    
    if ([self.txt_india.text isEqualToString:@""])
    {
        [Utility alertWithTitle:kAlertTitle withMessage:@"Please enter your country" onVC:self];
        return FALSE;
    }
    
    if ([self.txt_state.text isEqualToString:@""])
    {
        [Utility alertWithTitle:kAlertTitle withMessage:@"Please enter your country" onVC:self];
        return FALSE;
    }
    
    if ([self.txt_state.text isEqualToString:@""])
    {
        [Utility alertWithTitle:kAlertTitle withMessage:@"Please enter your state" onVC:self];
        return FALSE;
    }

    if ([self.txt_city.text isEqualToString:@""])
    {
        [Utility alertWithTitle:kAlertTitle withMessage:@"Please enter your state" onVC:self];
        return FALSE;
    }
    
    if ([self.txt_locality.text isEqualToString:@""])
    {
        [Utility alertWithTitle:kAlertTitle withMessage:@"Please enter your locality" onVC:self];
        return FALSE;
    }
    
    if ([self.txt_pin.text isEqualToString:@""])
    {
        [Utility alertWithTitle:kAlertTitle withMessage:@"Please enter your pin" onVC:self];
        return FALSE;
    }
    return TRUE;
}

//-------------------------------------------------------------------------------------------
#pragma mark- Text field delegate and data source
//-------------------------------------------------------------------------------------------

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField==self.txt_india )
    {
        [self setPickerInfo:self.txt_india withArray:_countriesArray];
    }
    else if(textField == self.txt_state)
    {
        [MyLoader showLoadingView];
        [DataParser getStateList:[NSString stringWithFormat:@"country_id=%@",_countryId] WithCompletionHandler:^(NSArray *array, BOOL isSuccessful)
         {
             [MyLoader hideLoadingView];
             if (isSuccessful)
             {
                 [_statesArray removeAllObjects];
                 _statesArray = [array mutableCopy];
                 [self setPickerInfo:self.txt_state withArray:_statesArray];
             }
             else
             {
                 
             }
         }];
    }
    else if (textField == self.txt_city)
    {
        [MyLoader showLoadingView];
        [DataParser getCityList:[NSString stringWithFormat:@"state_id=%@",_stateId] WithCompletionHandler:^(NSArray *array, BOOL isSuccessful)
        {
            [MyLoader hideLoadingView];
            if (isSuccessful)
            {
                [_citiesArray removeAllObjects];
                _citiesArray = [array mutableCopy];
                [self setPickerInfo:self.txt_city withArray:_citiesArray];
            }
            else
            {
                
            }
        }];
    }
}

#pragma mark- Picker Methods

// DateOfBirth Picker
- (void) dateOfBirthPicker
{
    UIDatePicker *datePicker = [[UIDatePicker alloc]init];
    [datePicker setDate:[NSDate date]];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker addTarget:self action:@selector(dateTextField:) forControlEvents:UIControlEventValueChanged];
    [self.txt_DOB setInputView:datePicker];
}

-(void) dateTextField:(id)sender
{
    UIDatePicker* picker = (UIDatePicker*) _txt_DOB.inputView;
    [picker setMaximumDate:[NSDate date]];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    NSDate *eventDate = picker.date;
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSString *dateString = [dateFormat stringFromDate:eventDate];
    _txt_DOB.text = [NSString stringWithFormat:@"%@",dateString];
}

- (void) setPickerInfo:(UITextField*)textfield  withArray:(NSArray*)array
{
    [_pickerView pickerViewMethod:textfield arr:array];
    _pickerView.completionHandler = ^(PickerModel* detail)
    {
        if(textfield == self.txt_india)
            _countryId = detail.dataId;
        else if (textfield == self.txt_state)
            _stateId = detail.dataId;
        else if (textfield == self.txt_city)
            _cityId = detail.dataId;
        [textfield resignFirstResponder];
    };
}

- (void) downloadImage:(NSString *)imgURL1
{
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       image_manager = [SDWebImageManager sharedManager];
                       [image_manager downloadWithURL:[NSURL URLWithString:imgURL1] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize, NSInteger feedID, NSString *feedString)
                        {
                        } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished)
                        {
                            if (image)
                            {
                                _img_profile.image = image;
                            }
                        }];
                   });
    
}
- (void) oneTap:(UITapGestureRecognizer*)sender
{
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:@"Choose resume" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                            {
                                // Cancel button tappped do nothing.
                                
                            }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Capture From Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                            {
                                [self takePhotoFromCamra];
                                
                            }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"From Album" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                            {
                                [self choosePhotoFromAlbum];
                                
                            }]];
    [self presentViewController:actionSheet animated:YES completion:nil];
}

#pragma mark - Take Photo From Camera
- (void) takePhotoFromCamra
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        //_isCaemraPhoto = YES;
        _imagePicker=[[UIImagePickerController alloc] init];
        _imagePicker.delegate = self;
        _imagePicker.sourceType=UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:_imagePicker animated:YES completion:nil];
    }
    else
    {
        //[Utility alertWithTitle:@"Camera Unavailable" withMessage:@"Unable to find a camera on your device." onVC:self];
    }
    
}
#pragma mark - Take Photo From Album

- (void) choosePhotoFromAlbum
{
    _imagePicker=[[UIImagePickerController alloc] init];
    _imagePicker.delegate = self;
    //_imagePicker.sourceType=UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    _imagePicker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:_imagePicker animated:YES completion:nil];
}

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    UIImage *img = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    _isImageSelected = YES;
    
    _img_profile.image = img;
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}





@end
