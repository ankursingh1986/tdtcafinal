//
//  PatientProfile_VC.h
//  TDATC
//
//  Created by iWeb on 9/21/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Global.pch"


@interface PatientProfile_VC : UIViewController <UIGestureRecognizerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    SDWebImageManager *image_manager;

    __weak IBOutlet UIImageView *img_lang;
    __weak IBOutlet UIImageView *img_blood;
    IBOutlet UIImageView *img_profile,*img_india,*img_state,*img_city;
    IBOutlet UIButton *btn_edit,*btn_male,*btn_female,*btn_update,*btn_notification;
    IBOutlet UITextField *txt_email,*txt_mobile,*txt_fullname,*txt_DOB,*txt_india,*txt_state,*txt_city,*txt_locality,*txt_pin,*txt_blood,*txt_language,*txt_alternateNo,*txt_insorance;
}


- (IBAction)touch_edit:(id)sender;
- (IBAction)btn_MaleTouch:(id)sender;
- (IBAction)touch_femail:(id)sender;
- (IBAction)touch_update:(id)sender;
- (IBAction)touch_Notfiaction:(id)sender;




@property (weak, nonatomic) IBOutlet UIBarButtonItem *sideBarbtn;

@end
