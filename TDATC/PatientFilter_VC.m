//
//  PatientFilter_VC.m
//  TDATC
//
//  Created by iWeb on 9/25/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "PatientFilter_VC.h"
#import "PatientFilter_TVC1.h"
#import "Global.pch"
#import "PatientFilter_TVC2.h"



@interface PatientFilter_VC ()
{
    NSMutableArray* _filteredDataArray;
}

@end

@implementation PatientFilter_VC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self leftButton];
    [self rightButton];
    [tbl_Filter setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    self->tbl_Filter.tableFooterView = [[UIView alloc] init];

    _filteredDataArray = [[NSMutableArray alloc] init];
    
    _gDataManager.clinicArray = [_clinicArray mutableCopy];
    
    tbl_bool1=YES;
    tbl_bool2=NO;
    [tbl_Filter reloadData];


    location=[[NSMutableArray alloc]initWithObjects:@"Noida",@"Kanpur",@"New Delhi",@"Kolkata",@"Mumbai", nil];
    availble=[[NSMutableArray alloc]initWithObjects:@"Sunday",@"Monday",@"Tuesday",@"Wednesday",@"Thursday",@"Friday",@"Saturday",nil];
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationItem.title=@"FILTER";
    [super viewWillAppear:animated];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView DataSource and delegates

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tbl_bool1)
    {
        return _clinicArray.count;
       // return location.count;
    }
    else if (tbl_bool2)
    {
        return availble.count;
    }
    return 4 ;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tbl_bool1)
    {
        PatientFilter_TVC1 *cell = [tableView dequeueReusableCellWithIdentifier:@"PatientFilter_Cell1" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        ClinicInfoModel* data = [_clinicArray objectAtIndex:indexPath.row];
        cell.lbl_location.text=data.locality;

       // cell.lbl_location.text=[location objectAtIndex:indexPath.row];
        cell.btn_Check.tag=indexPath.row;
        [cell.btn_Check addTarget:self action:@selector(btnLocationCheckAction:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;


        return cell;

    }
   else
   {
       PatientFilter_TVC2 *cell = [tableView dequeueReusableCellWithIdentifier:@"PatientFilter_Cell2" forIndexPath:indexPath];
       cell.selectionStyle = UITableViewCellSelectionStyleNone;
       cell.lbl_Avalable.text=[availble objectAtIndex:indexPath.row];
       cell.btn_avalable.tag=indexPath.row;
       [cell.btn_avalable addTarget:self action:@selector(btnAvalableCheckAction:) forControlEvents:UIControlEventTouchUpInside];
       
       cell.selectionStyle = UITableViewCellSelectionStyleNone;

       return cell;
   }
    
    return nil;
}
- (void) btnLocationCheckAction:(UIControl*)sender
{
   
    NSIndexPath *myIP = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    //Type cast it to CustomCell
    PatientFilter_TVC1 *cell = (PatientFilter_TVC1*)[tbl_Filter cellForRowAtIndexPath:myIP];
    
    ClinicInfoModel* data = [_clinicArray objectAtIndex:sender.tag];

   
        if(cell.btn_Check.selected)
        {
            NSLog(@"count array add=%lu",(unsigned long)_gDataManager.clinicArray.count);

            [_gDataManager.clinicArray addObject:data];
            [cell.btn_Check setImage:[UIImage imageNamed:@"tick"] forState:UIControlStateNormal];
           
        }
        else
        {
            for(int k=0;k<_gDataManager.clinicArray.count;k++)
            {
                ClinicInfoModel* data1 = [_gDataManager.clinicArray objectAtIndex:k];
                if([data.locality isEqualToString:data1.locality])
                {
                    [_gDataManager.clinicArray removeObjectAtIndex:k];

                }

            }
            NSLog(@"count array=%lu",(unsigned long)_gDataManager.clinicArray.count);
            [cell.btn_Check setImage:[UIImage imageNamed:@"untick"] forState:UIControlStateNormal];
        }
    cell.btn_Check.selected = !cell.btn_Check.selected;

 
    NSLog(@"btnLocationCheckAction");
}
- (void) btnAvalableCheckAction:(UIControl*)sender
{
    NSIndexPath *myIP = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    //Type cast it to CustomCell
    PatientFilter_TVC2 *cell = (PatientFilter_TVC2*)[tbl_Filter cellForRowAtIndexPath:myIP];
   // PatientFilter_TVC2 *cell = (PatientFilter_TVC2*)[_clinicArray cellForRowAtIndexPath:myIP];

    
    cell.btn_avalable.selected = !cell.btn_avalable.selected;
    if(cell.btn_avalable.selected)
    {
        [cell.btn_avalable setImage:[UIImage imageNamed:@"tick"] forState:UIControlStateNormal];
        
    }
    else
    {
        [cell.btn_avalable setImage:[UIImage imageNamed:@"untick"] forState:UIControlStateNormal];
    }
    
    NSLog(@"btnAvalableCheckAction");
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (IBAction)touch_Location:(id)sender {
    
    tbl_bool1=YES;
    tbl_bool2=NO;
    [tbl_Filter reloadData];
    
}

- (IBAction)touch_Avalable:(id)sender {
    tbl_bool1=NO;
    tbl_bool2 = YES;
    [tbl_Filter reloadData];
}
-(void)rightButton{
    button =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"apply"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(applyButtonAction:)forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0, 20, 20)];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.rightBarButtonItem = barButton;
}
- (IBAction) applyButtonAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction) backButtonAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];

//    UIStoryboard *storyBoard12 = [UIStoryboard storyboardWithName:@"Patient" bundle:nil];
//    
//    UIViewController *vc = [storyBoard12 instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
//    [self presentViewController:vc animated:NO completion:nil];
    
    
}
-(void)leftButton{
    UIButton *button2 =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button2 setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [button2 addTarget:self action:@selector(backButtonAction:)forControlEvents:UIControlEventTouchUpInside];
    [button2 setFrame:CGRectMake(0, 0, 20, 20)];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button2];
    self.navigationItem.leftBarButtonItem = barButton;
    self.navigationItem.hidesBackButton=TRUE;
    
}

@end
