//
//  PatientDoctorDeatil_TVC2.h
//  TDATC
//
//  Created by iWeb on 9/24/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PatientDoctorDeatil_TVC2 : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *btn_View;
@property (weak, nonatomic) IBOutlet UILabel *exprienceLabel;
@property (weak, nonatomic) IBOutlet UILabel *awardLabel;
@property (weak, nonatomic) IBOutlet UILabel *yearLabel;

@end
