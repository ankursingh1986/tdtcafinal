//
//  DataManager.h
//  TDATC
//
//  Created by iWeb on 9/26/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DoctorProfileModel.h"
#import "LoginModel.h"

@interface DataManager : NSObject
@property (nonatomic,readwrite) BOOL isDoctor;

@property (nonatomic,readwrite) BOOL fromAppointment;

@property(nonatomic, strong) DoctorProfileModel* doctorModel;

@property(nonatomic, strong) LoginModel* loginModel;


@property (nonatomic, strong) NSMutableArray* clinicArray;
@property (nonatomic, strong) NSString* memberShip_id;
@property (nonatomic, strong) NSString* doctorProfileImagePath;
@property (nonatomic, strong) NSString* doctorProfileImage;

@property (nonatomic, strong) NSString* doctorMessageId, *patientMessageID;


@property (nonatomic, strong) NSString* patientImagePath;

@property (nonatomic, strong) NSString* smsImagePath;

@property (nonatomic, strong) NSString* imgmsgpath,*imgdoctorpath,*imgpatientpath;






+ (DataManager*) sharedManager;


@end
#define _gDataManager [DataManager sharedManager]
