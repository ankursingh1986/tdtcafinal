//
//  ViewController.h
//  TDATC
//
//  Created by iWeb on 9/13/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController
{
    IBOutlet UITextField *txt_email, *txt_password;
    int doctorFlag;
    IBOutlet UIButton *btn;
    int autologin;
}
- (IBAction)login_Touch:(id)sender;
- (IBAction)facebook:(id)sender;
- (IBAction)twitter:(id)sender;
- (IBAction)check_Touch:(id)sender;


@end

