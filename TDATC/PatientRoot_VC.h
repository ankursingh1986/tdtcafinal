//
//  PatientRoot_VC.h
//  TDATC
//
//  Created by iWeb on 9/21/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SideMenuProfileTableViewCell.h"
#import "SideMenuTableViewCell.h"
#import "SDWebImageManager.h"

#import "Global.pch"


@interface PatientRoot_VC : UIViewController
{
    SideMenuProfileTableViewCell *cellFirst;
    SideMenuTableViewCell *cellSecond;
    SDWebImageManager *image_manager;

}
@property (weak, nonatomic) IBOutlet UITableView *tbl_home;
@property (weak, nonatomic) IBOutlet UIButton *btn_logout;
- (IBAction)touch_logout:(id)sender;
@end
