//
//  DoctorFeedback_VC.h
//  TDATC
//
//  Created by iWeb on 9/18/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FSCalendar.h>
#import "SDWebImageManager.h"
#import "Global.pch"
#import <FSCalendar.h>


@interface DoctorFeedback_VC : UIViewController<FSCalendarDelegate,FSCalendarDataSource>
{
    IBOutlet UITableView *tbl_FeedBack;
    UIButton *button;
    SDWebImageManager *image_manager;

}
@property (weak, nonatomic) IBOutlet UIView *view_1;
@property (weak, nonatomic) IBOutlet UIButton *btn_next;
@property (weak, nonatomic) IBOutlet UIButton *btn_Back;
@property (weak, nonatomic) FSCalendar *calendar;
@property (weak, nonatomic) UIButton *previousButton;
@property (weak, nonatomic) UIButton *nextButton;

@property (weak, nonatomic) IBOutlet FSCalendar *calender1;
@property (strong, nonatomic) NSCalendar *gregorian;

- (void)previousClicked:(id)sender;
- (void)nextClicked:(id)sender;

- (IBAction)previous:(id)sender;
- (IBAction)next:(id)sender;

@end
