//
//  PatientProfile_VC.m
//  TDATC
//
//  Created by iWeb on 9/21/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "PatientProfile_VC.h"
#import "SWRevealViewController.h"
#import "PatientDataParser.h"
#import "Utility.h"
#import "Defines.h"
#import "DataManager.h"
#import "MyLoader.h"
#import "DataParser.h"
#import "PickerTool.h"


@interface PatientProfile_VC ()
{
    NSString* _genderString;
    NSString* _countryId;
    NSString* _stateId;
    NSString* _cityId;
    NSString* _booldGroupId;
    
    
    BOOL _isImageSelected;
    PickerTool* _pickerView;
    NSMutableArray* _countriesArray;
    NSMutableArray* _statesArray;
    NSMutableArray* _citiesArray;
    NSMutableArray* _bloodGroupsArray;
    UIImagePickerController* _imagePicker;
}

@end

@implementation PatientProfile_VC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    img_profile.layer.cornerRadius = img_profile.frame.size.width / 2;
    img_profile.clipsToBounds = YES;
    
    img_profile.layer.borderWidth = 6.0f;
    img_profile.layer.borderColor = [UIColor whiteColor].CGColor;
    
    
    
    SWRevealViewController *vc=self.revealViewController;
    if (vc)
    {
        _sideBarbtn.target=self.revealViewController;
        //[_sideBarbtn setAction: @selector(revalTogel: )];
        //   [self.sideBarbtn setAction:@selector(revealToggle:)];
        _sideBarbtn.action=@selector(revealToggle:);
        [self.navigationController.navigationBar addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    [self dateOfBirthPicker];
    [self FirstData];
    [self pickerViews];
       // [self getPatientProfile];
    [self setUpData];

    
    [img_profile setUserInteractionEnabled:NO];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    
     NSString *socialLogin = [[NSUserDefaults standardUserDefaults] objectForKey:@"socialLogin"];
    
    if([socialLogin isEqualToString:@"0"])
    {
        [self userAutoLoginKill];
    }
    else
    {
        [self userloginwithFB];
    }
}

-(void)userloginwithFB{
    
    NSString *postData = [[NSUserDefaults standardUserDefaults] objectForKey:@"userLoginInfo"];
    
    
    [MyLoader showLoadingView];
    [DataParser facebookAPIHit:postData WithCompletionHandler:^(NSDictionary *dict, BOOL isSuccessful)
     {
         if (isSuccessful)
         {
             [MyLoader hideLoadingView];
             
             if([dict[@"status"] isEqualToString:@"false"])
             {
                 
             }
             else
             {
                 [self setUpData];
                 
                 self.navigationItem.title = [_gDataManager.loginModel.full_name uppercaseString];
                 
                 _countriesArray = [[NSMutableArray alloc] init];
                 _statesArray = [[NSMutableArray alloc] init];
                 _citiesArray = [[NSMutableArray alloc] init];
                 _bloodGroupsArray = [[NSMutableArray alloc] init];
                 
                 _countryId = _gDataManager.loginModel.country_id;
                 _stateId = _gDataManager.loginModel.state_id;
                 _cityId = _gDataManager.loginModel.city_id;
                 
                 
                 [self getAllCountriesList];
                 [self getStateList:_gDataManager.loginModel.country_id];
                 [self getCitiesList:_gDataManager.loginModel.state_id];
                 [self getBloodGroupsList];
                 
             }
         }
         else
         {
             [MyLoader hideLoadingView];
             [Utility alertWithTitle:@"" withMessage:kNetworkErrorTitle onVC:self];
         }
     }];
    
    
    
}


- (void) userAutoLoginKill
{
    NSString *postData = [[NSUserDefaults standardUserDefaults] objectForKey:@"userLoginInfo"];
    
    [MyLoader showLoadingView];
    [DataParser loginAPIHit:postData WithCompletionHandler:^(NSDictionary *dict, BOOL isSuccessful)
     {
         if (isSuccessful)
         {
             [MyLoader hideLoadingView];
             
             if([dict[@"status"] isEqualToString:@"false"])
             {
                 
             }
             else
             {
                 [self setUpData];
                 
                 self.navigationItem.title = [_gDataManager.loginModel.full_name uppercaseString];
                 
                 _countriesArray = [[NSMutableArray alloc] init];
                 _statesArray = [[NSMutableArray alloc] init];
                 _citiesArray = [[NSMutableArray alloc] init];
                 _bloodGroupsArray = [[NSMutableArray alloc] init];
                 
                 _countryId = _gDataManager.loginModel.country_id;
                 _stateId = _gDataManager.loginModel.state_id;
                 _cityId = _gDataManager.loginModel.city_id;
                 
                 
                 [self getAllCountriesList];
                 [self getStateList:_gDataManager.loginModel.country_id];
                 [self getCitiesList:_gDataManager.loginModel.state_id];
                 [self getBloodGroupsList];
                 
                 
             }
         }
         else
         {
             [MyLoader hideLoadingView];
             [Utility alertWithTitle:@"" withMessage:kNetworkErrorTitle onVC:self];
         }
     }];
    
    
}

- (void) userAutoLogin
{
    NSString *postData = [[NSUserDefaults standardUserDefaults] objectForKey:@"userLoginInfo"];
    
    [MyLoader showLoadingView];
    [DataParser loginAPIHit:postData WithCompletionHandler:^(NSDictionary *dict, BOOL isSuccessful)
     {
         if (isSuccessful)
         {
             [MyLoader hideLoadingView];
             
             if([dict[@"status"] isEqualToString:@"false"])
             {
//                 [Utility alertWithTitle:kAlertTitle withMessage:dict[@"user"] onVC:self];
                 
                 //Ankur
             }
             else
             {
                 
                 self.navigationItem.title = [_gDataManager.loginModel.full_name uppercaseString];

                 //[self getDoctorProfile];
             }
         }
         else
         {
             [MyLoader hideLoadingView];
             [Utility alertWithTitle:@"" withMessage:kNetworkErrorTitle onVC:self];
         }
     }];
    
    
}


- (void) pickerViews
{
    _pickerView = [PickerTool loadClass];
    self->txt_india.inputView = _pickerView;
    self->txt_state.inputView = _pickerView;
    self->txt_city.inputView = _pickerView;
    self->txt_blood.inputView = _pickerView;
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(oneTap:)];
    singleTap.numberOfTapsRequired = 1;
    singleTap.numberOfTouchesRequired = 1;
    singleTap.delegate = self;
    [img_profile addGestureRecognizer:singleTap];
}

- (void) oneTap:(UITapGestureRecognizer*)sender
{
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:@"Choose Image" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                            {
                                // Cancel button tappped do nothing.
                                
                            }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Capture From Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                            {
                                [self takePhotoFromCamra];
                                
                            }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"From Album" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                            {
                                [self choosePhotoFromAlbum];
                                
                            }]];
    [self presentViewController:actionSheet animated:YES completion:nil];
}

#pragma mark - Take Photo From Camera
- (void) takePhotoFromCamra
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        //_isCaemraPhoto = YES;
        _imagePicker=[[UIImagePickerController alloc] init];
        _imagePicker.delegate = self;
        _imagePicker.sourceType=UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:_imagePicker animated:YES completion:nil];
    }
    else
    {
        //[Utility alertWithTitle:@"Camera Unavailable" withMessage:@"Unable to find a camera on your device." onVC:self];
    }
    
}

#pragma mark - Take Photo From Album

- (void) choosePhotoFromAlbum
{
    _imagePicker=[[UIImagePickerController alloc] init];
    _imagePicker.delegate = self;
    //_imagePicker.sourceType=UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    _imagePicker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:_imagePicker animated:YES completion:nil];
}



- (void) getPatientProfile
{
    NSString* postData = [NSString stringWithFormat:@"id=%@",_gDataManager.loginModel.user_id];
    [PatientDataParser getPatientProfile:postData WithCompletionHandler:^(NSDictionary *dict, BOOL isSuccessful)
    {
        if (isSuccessful)
        {
            
        }
        else
        {
            [Utility alertWithTitle:kAlertTitle withMessage:kNetworkErrorTitle onVC:self];
        }
    }];
}


-(void)FirstData
{
    btn_update.hidden=YES;
    txt_DOB.userInteractionEnabled=NO;
    txt_pin.userInteractionEnabled=NO;
    txt_city.userInteractionEnabled=NO;
    txt_email.userInteractionEnabled=NO;
    txt_india.userInteractionEnabled=NO;
    txt_state.userInteractionEnabled=NO;
    txt_mobile.userInteractionEnabled=NO;
    txt_fullname.userInteractionEnabled=NO;
    txt_locality.userInteractionEnabled=NO;
    txt_blood.userInteractionEnabled=NO;
    txt_language.userInteractionEnabled=NO;
    txt_insorance.userInteractionEnabled=NO;
    txt_alternateNo.userInteractionEnabled=NO;
    btn_female.userInteractionEnabled=NO;
    btn_male.userInteractionEnabled=NO;
    btn_notification.userInteractionEnabled=NO;
    img_city.hidden=YES;
    img_india.hidden=YES;
    img_state.hidden=YES;
    img_blood.hidden=YES;
    img_lang.hidden=YES;
}


- (void) setUpData
{
    txt_email.text = [NSString stringWithFormat:@"%@",_gDataManager.loginModel.email];
    txt_mobile.text = [NSString stringWithFormat:@"%@",_gDataManager.loginModel.mobile_number];
    txt_fullname.text = [[NSString stringWithFormat:@"%@",_gDataManager.loginModel.full_name] capitalizedString];
    txt_DOB.text = [Utility changeDateFormat: [NSString stringWithFormat:@"%@",_gDataManager.loginModel.dob]];

    txt_blood.text = @"";
    txt_language.text = @"";
   
    txt_pin.text = [NSString stringWithFormat:@"%@",_gDataManager.loginModel.pincode];
    txt_alternateNo.text = [NSString stringWithFormat:@"%@",_gDataManager.loginModel.alternate_mobile_number];
    txt_insorance.text = @"";
    
    if ([_gDataManager.loginModel.gender isEqualToString:@"female"])
    {
        _genderString = @"female";
        [self->btn_female setImage:[UIImage imageNamed:@"radiobtn_active"] forState:UIControlStateNormal];
        [self->btn_male setImage:[UIImage imageNamed:@"radiobtn_inactive"] forState:UIControlStateNormal];
        self->btn_female.selected = !self->btn_female.selected;
    }
    else
    {
        _genderString = @"male";
        [self->btn_male setImage:[UIImage imageNamed:@"radiobtn_active"] forState:UIControlStateNormal];
        [self->btn_female setImage:[UIImage imageNamed:@"radiobtn_inactive"] forState:UIControlStateNormal];
        self->btn_male.selected = !self->btn_male.selected;
    }
    
    NSString *imagePath=[NSString stringWithFormat:@"%@%@",kPatientImageSubPath,_gDataManager.loginModel.image];
    if([imagePath isEqualToString:@"(null)(null)"])
    {
        
    }
    else
    {
        [self downloadImage:imagePath];
        
    }


    
}

- (void) getAllCountriesList
{
    [MyLoader showLoadingView];
    [DataParser getCountryListWithCompletionHandler:^(NSArray *array, BOOL isSuccessful)
     {
         [MyLoader hideLoadingView];
         if (isSuccessful)
         {
             _countriesArray = [array mutableCopy];
             
             for (PickerModel* obj in _countriesArray)
             {
                 NSString* country = [NSString stringWithFormat:@"%@",_gDataManager.loginModel.country_id];
                 
                 if ([country isEqualToString:[NSString stringWithFormat:@"%@",obj.dataId]])
                 {
                     _countryId = obj.dataId;
                     txt_india.text = obj.dataName;
                     break;
                 }
                 
             }
             
         }
         else
         {
             [Utility alertWithTitle:kAlertTitle withMessage:kNetworkErrorTitle onVC:self];
         }
         
     }];
}

- (void) getStateList:(NSString*)countyId
{
    [MyLoader showLoadingView];
    [DataParser getStateList:[NSString stringWithFormat:@"country_id=%@",countyId] WithCompletionHandler:^(NSArray *array, BOOL isSuccessful)
     {
         [MyLoader hideLoadingView];
         if (isSuccessful)
         {
             [_statesArray removeAllObjects];
             _statesArray = [array mutableCopy];
             
             for (PickerModel* obj in _statesArray)
             {
                 NSString* state = [NSString stringWithFormat:@"%@",_gDataManager.loginModel.state_id];
                 
                 if ([state isEqualToString:[NSString stringWithFormat:@"%@",obj.dataId]])
                 {
                     _stateId = obj.dataId;
                     txt_state.text = obj.dataName;
                     break;
                 }
             }
         }
         else
         {
             [Utility alertWithTitle:kAlertTitle withMessage:kNetworkErrorTitle onVC:self];
         }
         
     }];
}

- (void) getCitiesList:(NSString*)stateId
{
    [MyLoader showLoadingView];
    [DataParser getCityList:[NSString stringWithFormat:@"state_id=%@",stateId] WithCompletionHandler:^(NSArray *array, BOOL isSuccessful)
     {
         [MyLoader hideLoadingView];
         if (isSuccessful)
         {
             [_citiesArray removeAllObjects];
             _citiesArray = [array mutableCopy];
             
             for (PickerModel* obj in _citiesArray)
             {
                 NSString* city = [NSString stringWithFormat:@"%@",_gDataManager.loginModel.city_id];
                 
                 if ([city isEqualToString:[NSString stringWithFormat:@"%@",obj.dataId]])
                 {
                     _cityId = obj.dataId;
                     txt_city.text = obj.dataName;
                     break;
                 }
             }
             
         }
         else
         {
             [Utility alertWithTitle:kAlertTitle withMessage:kNetworkErrorTitle onVC:self];
         }
         
     }];
}

- (void) getBloodGroupsList
{
    [MyLoader showLoadingView];
    [PatientDataParser getBloodGroupsListWithCompletionHandler:^(NSArray *array, BOOL isSuccessful)
     {
         [MyLoader hideLoadingView];
         if (isSuccessful)
         {
             [_bloodGroupsArray removeAllObjects];
             _bloodGroupsArray = [array mutableCopy];
             
             for (PickerModel* obj in _bloodGroupsArray)
             {
                 NSString* blood = [NSString stringWithFormat:@"%@",_gDataManager.loginModel.b_id];
                 
                 if ([blood isEqualToString:[NSString stringWithFormat:@"%@",obj.dataId]])
                 {
                     _booldGroupId = obj.dataId;
                     txt_blood.text = obj.dataName;
                     break;
                 }
             }

             
         }
         else
         {
             [Utility alertWithTitle:kAlertTitle withMessage:kNetworkErrorTitle onVC:self];
         }
     }];
}

- (IBAction)touch_edit:(id)sender
{
    btn_update.hidden=NO;
    btn_edit.hidden=YES;
    
    txt_DOB.userInteractionEnabled=YES;
    txt_pin.userInteractionEnabled=YES;
    txt_city.userInteractionEnabled=YES;
    txt_email.userInteractionEnabled=NO;
    txt_india.userInteractionEnabled=YES;
    txt_state.userInteractionEnabled=YES;
    txt_mobile.userInteractionEnabled=YES;
    txt_fullname.userInteractionEnabled=YES;
    txt_locality.userInteractionEnabled=YES;
    txt_blood.userInteractionEnabled=YES;
    txt_language.userInteractionEnabled=YES;
    txt_insorance.userInteractionEnabled=YES;
    txt_alternateNo.userInteractionEnabled=YES;
    
    btn_female.userInteractionEnabled=YES;
    btn_male.userInteractionEnabled=YES;
    btn_notification.userInteractionEnabled=YES;
    
    [img_profile setUserInteractionEnabled:YES];
    
    img_city.hidden=NO;
    img_india.hidden=NO;
    img_state.hidden=NO;
    img_blood.hidden=NO;
    img_lang.hidden=NO;
}


- (IBAction)btn_MaleTouch:(id)sender
{
    btn_male.selected = !btn_male.selected;
    if(btn_male.selected)
    {
        [btn_female setImage:[UIImage imageNamed:@"radio_btn_inactive"] forState:UIControlStateNormal];
        [btn_male setImage:[UIImage imageNamed:@"radio_btn_active"] forState:UIControlStateNormal];
        
    }
    else
    {
        [btn_male setImage:[UIImage imageNamed:@"radio_btn_inactive"] forState:UIControlStateNormal];
        [btn_female setImage:[UIImage imageNamed:@"radio_btn_active"] forState:UIControlStateNormal];
    }
}


- (IBAction)touch_femail:(id)sender
{
    
    btn_female.selected = !btn_female.selected;
    if(btn_female.selected)
    {
        
        [btn_male setImage:[UIImage imageNamed:@"radio_btn_inactive"] forState:UIControlStateNormal];
        [btn_female setImage:[UIImage imageNamed:@"radio_btn_active"] forState:UIControlStateNormal];
    }
    else
    {
        [btn_female setImage:[UIImage imageNamed:@"radio_btn_inactive"] forState:UIControlStateNormal];
        [btn_male setImage:[UIImage imageNamed:@"radio_btn_active"] forState:UIControlStateNormal];
    }
}


- (IBAction)touch_update:(id)sender
{
    btn_edit.hidden=NO;
    [self FirstData];
    
    NSString* image64Data;
    
    if (!_isImageSelected)
    {
        image64Data = @"";
    }
    else
    {
        //image64Data = @"";
        
        NSData *imageData=UIImageJPEGRepresentation(img_profile.image,.5);
       // NSString *base64 = [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
        image64Data = [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    }
    
    
   // NSString* postData = [NSString stringWithFormat:@"id=%@&full_name=%@&mobile_number=%@&locality=%@&pincode=%@&country_id=%@&state_id=%@&city_id=%@&insurance=%@&b_id=%@&alternate_mobile_number=%@&image=%@&dob=%@",_gDataManager.loginModel.user_id,txt_fullname.text,txt_mobile.text,@"",txt_pin.text,_countryId,_stateId,_cityId,txt_insorance.text,_booldGroupId,txt_alternateNo.text, image64Data,txt_DOB.text];
     NSString* postData = [NSString stringWithFormat:@"id=%@&full_name=%@&mobile_number=%@&locality=%@&pincode=%@&country_id=%@&state_id=%@&city_id=%@&insurance=%@&b_id=%@&alternate_mobile_number=%@&image=%@&dob=%@",_gDataManager.loginModel.user_id,txt_fullname.text,txt_mobile.text,@"",txt_pin.text,_countryId,_stateId,_cityId,txt_insorance.text,_booldGroupId,txt_alternateNo.text, image64Data,txt_DOB.text];
    
    [MyLoader showLoadingView];
    [PatientDataParser updatePatientProfile:postData WithCompletionHandler:^(NSDictionary *dict, BOOL isSuccessful)
     {
         [MyLoader hideLoadingView];
         if (isSuccessful)
         {
             [Utility alertWithTitle:@"Success" withMessage:dict[@"message"] onVC:self];
             NSString *socialLogin = [[NSUserDefaults standardUserDefaults] objectForKey:@"socialLogin"];
             
             if([socialLogin isEqualToString:@"0"])
             {
                 [self userAutoLogin];
             }
             else
             {
                 [self facebookLogin];
             }
            
         }
         else
         {
             [Utility alertWithTitle:kAlertTitle withMessage:kNetworkErrorTitle onVC:self];
         }
     }];
}

-(void)facebookLogin{
    
    
        NSString *postData = [[NSUserDefaults standardUserDefaults] objectForKey:@"userLoginInfo"];
        
        
        [MyLoader showLoadingView];
        [DataParser facebookAPIHit:postData WithCompletionHandler:^(NSDictionary *dict, BOOL isSuccessful)
         {
             if (isSuccessful)
             {
                 [MyLoader hideLoadingView];
                 
                 if([dict[@"status"] isEqualToString:@"false"])
                 {
                     
                 }
                 else
                 {
                     self.navigationItem.title = [_gDataManager.loginModel.full_name uppercaseString];
                 }
             }
             else
             {
                 [MyLoader hideLoadingView];
                 [Utility alertWithTitle:@"" withMessage:kNetworkErrorTitle onVC:self];
             }
         }];
        
        
        
    

    
}

- (IBAction)touch_Notfiaction:(id)sender
{
    
    btn_notification.selected = !btn_notification.selected;
    if(btn_notification.selected)
    {
        [btn_notification setImage:[UIImage imageNamed:@"off_btn"] forState:UIControlStateNormal];
    }
    else
    {
        [btn_notification setImage:[UIImage imageNamed:@"on_btn"] forState:UIControlStateNormal];
    }
    
}



-(void)leftButton
{
    UIButton *button =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(backButtonAction:)forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0, 20, 20)];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = barButton;
    
}
- (IBAction) backButtonAction:(id)sender
{
    UIStoryboard *storyBoard12 = [UIStoryboard storyboardWithName:@"Patient" bundle:nil];
    
    UIViewController *vc = [storyBoard12 instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
    [self presentViewController:vc animated:NO completion:nil];
}

//-------------------------------------------------------------------------------------------
#pragma mark- Text field delegate and data source
//-------------------------------------------------------------------------------------------

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField==self->txt_india )
    {
        [self setPickerInfo:self->txt_india withArray:_countriesArray];
    }
    else if(textField == self->txt_state)
    {
        [MyLoader showLoadingView];
        [DataParser getStateList:[NSString stringWithFormat:@"country_id=%@",_countryId] WithCompletionHandler:^(NSArray *array, BOOL isSuccessful)
         {
             [MyLoader hideLoadingView];
             if (isSuccessful)
             {
                 [_statesArray removeAllObjects];
                 _statesArray = [array mutableCopy];
                 [self setPickerInfo:self->txt_state withArray:_statesArray];
             }
             else
             {
                 
             }
         }];
    }
    else if (textField == self->txt_city)
    {
        [MyLoader showLoadingView];
        [DataParser getCityList:[NSString stringWithFormat:@"state_id=%@",_stateId] WithCompletionHandler:^(NSArray *array, BOOL isSuccessful)
         {
             [MyLoader hideLoadingView];
             if (isSuccessful)
             {
                 [_citiesArray removeAllObjects];
                 _citiesArray = [array mutableCopy];
                 [self setPickerInfo:self->txt_city withArray:_citiesArray];
             }
             else
             {
                 
             }
         }];
    }
    else if (textField == txt_blood)
    {
       // [self getBloodGroupsList];
        [PatientDataParser getBloodGroupsListWithCompletionHandler:^(NSArray *array, BOOL isSuccessful)
        {
            if (isSuccessful)
            {
                [_bloodGroupsArray removeAllObjects];
                _bloodGroupsArray = [array mutableCopy];
                [self setPickerInfo:self->txt_blood withArray:_bloodGroupsArray];
            }
            else
            {
                
            }
        }];
        
        
       
    }
}

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    UIImage *img = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    _isImageSelected = YES;
    
    img_profile.image = img;
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark- Picker Methods

// DateOfBirth Picker
- (void) dateOfBirthPicker
{
    UIDatePicker *datePicker = [[UIDatePicker alloc]init];
    [datePicker setDate:[NSDate date]];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker addTarget:self action:@selector(dateTextField:) forControlEvents:UIControlEventValueChanged];
    [self->txt_DOB setInputView:datePicker];
}

-(void) dateTextField:(id)sender
{
    UIDatePicker* picker = (UIDatePicker*) txt_DOB.inputView;
    [picker setMaximumDate:[NSDate date]];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    NSDate *eventDate = picker.date;
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSString *dateString = [dateFormat stringFromDate:eventDate];
    txt_DOB.text = [NSString stringWithFormat:@"%@",dateString];
}

- (void) setPickerInfo:(UITextField*)textfield  withArray:(NSArray*)array
{
    [_pickerView pickerViewMethod:textfield arr:array];
    _pickerView.completionHandler = ^(PickerModel* detail)
    {
        if(textfield == self->txt_india)
            _countryId = detail.dataId;
        else if (textfield == self->txt_state)
            _stateId = detail.dataId;
        else if (textfield == self->txt_city)
            _cityId = detail.dataId;
        else if (textfield == self->txt_blood)
        {
            _booldGroupId = detail.dataId;
        }
        [textfield resignFirstResponder];
    };
}
- (void) downloadImage:(NSString *)imgURL1
{
    NSLog(@"Image URL in method=%@",imgURL1);
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       image_manager = [SDWebImageManager sharedManager];
                       [image_manager downloadWithURL:[NSURL URLWithString:imgURL1] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize, NSInteger feedID, NSString *feedString)
                        {
                        } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished)
                        {
                            if (image)
                            {
                                img_profile.image = image;
                            }
                        }];
                   });
    
}

@end
