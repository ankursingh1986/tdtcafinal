//
//  DoctorInBox_TVC.h
//  TDATC
//
//  Created by iWeb on 9/18/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DoctorInBox_TVC : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbl_subject;
@property (weak, nonatomic) IBOutlet UILabel *lbl_detail;
@property (weak, nonatomic) IBOutlet UILabel *lbl_date;
@property (weak, nonatomic) IBOutlet UITextView *txtView_inbox;

@end
