//
//  PatientDoctorDeatil_VC.h
//  TDATC
//
//  Created by iWeb on 9/24/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FindDoctorModel.h"
#import "Global.pch"


@interface PatientDoctorDeatil_VC : UIViewController
{
    IBOutlet UITableView *tbl_doctor_detail;
    UIButton *button;
    SDWebImageManager *image_manager;

}

@property (nonatomic, strong) FindDoctorModel* doctorData;

@end
