//
//  DoctorISent_TVC.h
//  TDATC
//
//  Created by iWeb on 9/18/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DoctorISent_TVC.h"
#import "DoctorSentMessageModel.h"

@interface DoctorISent_TVC : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *subjectLabel;

@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UITextView *txtView_sent;

- (void) setUpData:(DoctorSentMessageModel*)data;

@end
