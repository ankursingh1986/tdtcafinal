//
//  DataParser.h
//  DawaiBox
//
//  Created by Vinay Kumar on 21/11/16.
//  Copyright © 2016 com.garry. All rights reserved.
#import <Foundation/Foundation.h>
#import "WebServiceManager.h"


@interface DataParser : NSObject
//Get New data


// Post Request.....

+ (void) loginAPIHit:(NSString*)details WithCompletionHandler:(void (^)(NSDictionary* dict , BOOL isSuccessful)) handler;

+ (void) signupAPI:(NSString*)details WithCompletionHandler:(void (^)(NSDictionary* dict , BOOL isSuccessful)) handler;

+ (void) facebookAPIHit:(NSString*)details WithCompletionHandler:(void (^)(NSDictionary* dict , BOOL isSuccessful)) handler;
+ (void) twitterAPIHit:(NSString*)details WithCompletionHandler:(void (^)(NSDictionary* dict , BOOL isSuccessful)) handler;

+ (void) globalHitAPI:(NSString*)apiname detaiInfi:(NSString*)detail WithCompletionHandler:(void (^)(NSDictionary* dict , BOOL isSuccessful)) handler;



+ (void) DappointmentListHit:(NSString*)details WithCompletionHandler:(void (^)(NSMutableArray* array , BOOL isSuccessful)) handler;

+ (void) DappointmentDeleteListHit:(NSString*)details WithCompletionHandler:(void (^)(NSMutableArray* array , BOOL isSuccessful)) handler;

+ (void) DappointmentRequestListHit:(NSString*)details WithCompletionHandler:(void (^)(NSMutableArray* array , BOOL isSuccessful)) handler;

+ (void) DappointmentClinictListHit:(NSString*)details WithCompletionHandler:(void (^)(NSMutableArray* array , BOOL isSuccessful)) handler;

+ (void) DappointmentClinictViewHit:(NSString*)details WithCompletionHandler:(void (^)(NSMutableArray* array , BOOL isSuccessful)) handler;

+ (void) DappointmentPatienttListHit:(NSString*)details WithCompletionHandler:(void (^)(NSMutableArray* array , BOOL isSuccessful)) handler;


// Doctor API(Vinay)
+ (void) doctorDashboardProfile:(NSString*)userId WithCompletionHandler:(void(^)(NSDictionary* dict, BOOL isSuccessful))handler;
+ (void) doctorMessageListing:(NSString*)userId WithCompletionHandler:(void(^)(NSArray* array, BOOL isSuccessful))handler;
+ (void) doctorFeedback:(NSString*)userId WithCompletionHandler:(void(^)(NSArray* array, BOOL isSuccessful))handler;
+ (void) doctorComposeMsg:(NSString*)postData WithCompletionHandler:(void(^)(NSDictionary* dict, BOOL isSuccessful))handler;
+ (void) doctorMsg:(NSString*)postData WithCompletionHandler:(void(^)(NSDictionary* dict, BOOL isSuccessful))handler;
+ (void) doctorSentMsg:(NSString*)postData WithCompletionHandler:(void(^)(NSArray* array, BOOL isSuccessful))handler;

+ (void) updateDoctorProfileAPI:(NSString*)postData WithCompletionHandler:(void(^)(NSDictionary* dict, BOOL isSuccessful))handler;

+ (void) getCountryListWithCompletionHandler:(void(^)(NSArray* array, BOOL isSuccessful))handler;
+ (void) getStateList:(NSString*)stateId WithCompletionHandler:(void(^)(NSArray* array, BOOL isSuccessful))handler;
+ (void) getCityList:(NSString*)cityId WithCompletionHandler:(void(^)(NSArray* array, BOOL isSuccessful))handler;
+ (void) getCurrnentMemberShip:(NSString*)doctorId WithCompletionHandler:(void(^)(NSDictionary* dict, BOOL isSuccessful))handler;

+ (void) getMemberShipTypeWithCompletionHandler:(void(^)(NSArray* array, BOOL isSuccessful))handler;
// Ankur
+ (void) getMemberShipFeatureWithCompletionHandler:(void(^)(NSArray* array, BOOL isSuccessful))handler;

//

+ (id) dataObject:(NSData*)data;





// data saving into sharepreference




@end
