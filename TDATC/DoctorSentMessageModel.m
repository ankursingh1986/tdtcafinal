//
//  DoctorSentMessageModel.m
//  TDATC
//
//  Created by Vinay on 18/10/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "DoctorSentMessageModel.h"

@implementation DoctorSentMessageModel

- (id) initWithDictionary:(NSDictionary*)dictionary
{
    self = [super init];
    if (self)
    {
        if (![dictionary[@"comments"] isKindOfClass:[NSNull class]])
            self.comments = dictionary[@"comments"];
        else
            self.comments = @"";
        
        if (![dictionary[@"date"] isKindOfClass:[NSNull class]])
            self.date = dictionary[@"date"];
        else
            self.date = @"";
        if (![dictionary[@"doctor_msg"] isKindOfClass:[NSNull class]])
        {
            NSString* result = [dictionary[@"doctor_msg"] stringByReplacingOccurrencesOfString:@"<p>" withString:@""];
            result = [result stringByReplacingOccurrencesOfString:@"</p>" withString:@""];
            self.doctor_msg = result;
        }
        else
        {
            self.doctor_msg = @"";
        }
        
        self.msg_id = dictionary[@"msg_id"];
        
        if (![dictionary[@"subject"] isKindOfClass:[NSNull class]])
            self.subject = dictionary[@"subject"];
        else
            self.subject = @"";
        
        if (![dictionary[@"dr"][@"full_name"] isKindOfClass:[NSNull class]])
            self.doctor_full_name = dictionary[@"dr"][@"full_name"];
        else
            self.doctor_full_name = @"";
        
        if (![dictionary[@"dr"][@"user_id"] isKindOfClass:[NSNull class]])
            self.patientID = dictionary[@"dr"][@"user_id"];
        else
            self.patientID = @"";
        
        if (![dictionary[@"attacmet"][@"filename"] isKindOfClass:[NSNull class]])
            self.attchment = dictionary[@"attacmet"][@"filename"];
        else
            self.attchment = @"";
        
        if (![dictionary[@"us"][@"full_name"] isKindOfClass:[NSNull class]])
               self.patient_full_name = dictionary[@"us"][@"full_name"];
        else
            self.patient_full_name = @"";

    }
    return self;
}

@end
