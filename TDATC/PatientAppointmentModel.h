//
//  PatientAppointmentModel.h
//  TDATC
//
//  Created by Vinay on 03/11/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PatientAppointmentModel : NSObject

@property (nonatomic, strong) NSString* booking_id;
@property (nonatomic, strong) NSString* patient_id;
@property (nonatomic, strong) NSString* doctor_id;
@property (nonatomic, strong) NSString* clinic_id;
@property (nonatomic, strong) NSString* patientname;
@property (nonatomic, strong) NSString* da_id;
@property (nonatomic, strong) NSString* dat_id;
@property (nonatomic, strong) NSString* sickness;
@property (nonatomic, strong) NSString* consultingmode;
@property (nonatomic, strong) NSString* doctorName;

@property (nonatomic, strong) NSString* avalableTime;

@property (nonatomic, strong) NSString* status;



@property (nonatomic, strong) NSString* locality;
@property (nonatomic, strong) NSString* availabledate;
@property (nonatomic, strong) NSString* image;
@property (nonatomic, strong) NSString* rating;
@property (nonatomic, strong) NSString* amount;



- (id) initWithDictionary:(NSDictionary*)dictionary;

@end
