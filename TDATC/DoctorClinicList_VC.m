//
//  DoctorClinicList_VC.m
//  TDATC
//
//  Created by iWeb on 9/18/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "DoctorClinicList_VC.h"
#import "DoctorClinicList_TVC.h"
#import "DoctorClinicInfo_VC.h"
#import "ClinicModel.h"
#import "DataManager.h"
#import "Utility.h"
#import "MyLoader.h"
#import "DataParser.h"
#import "Defines.h"


@interface DoctorClinicList_VC ()
{
    NSMutableArray *clinicArray;
}

@end

@implementation DoctorClinicList_VC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self leftButton];
    [self getDataFromServer];
    clinicArray=[[NSMutableArray alloc]init];
    [tbl_Clinic setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    self->tbl_Clinic.tableFooterView = [[UIView alloc] init];
    // Do any additional setup after loading the view.
}

-(void)getDataFromServer{
    
    
    NSString *postData=[NSString stringWithFormat:@"id=%@",_gDataManager.loginModel.user_id];
    
    
    [MyLoader showLoadingView];
    [DataParser DappointmentClinictListHit:postData WithCompletionHandler:^(NSMutableArray *array, BOOL isSuccessful)
     {
         if (isSuccessful)
         {
             [MyLoader hideLoadingView];
             
             
             clinicArray = [array mutableCopy];
             [tbl_Clinic reloadData];
             
             
             
         }
         else
         {
             [MyLoader hideLoadingView];
             [Utility alertWithTitle:@"" withMessage:kNetworkErrorTitle onVC:self];
         }
     }];
    
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationItem.title = @"CLINIC";
    
    [super viewWillAppear:animated];
    
}
#pragma mark - table view delegates and data socurce


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return clinicArray.count;
}
//

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    DoctorClinicList_TVC *cell = [tableView dequeueReusableCellWithIdentifier:@"DoctorClinicList_Cell" forIndexPath:indexPath];
    
    ClinicModel*obj=[clinicArray objectAtIndex:indexPath.row];
    cell.lbl_title.text=[obj.clicnic_name uppercaseString];
    cell.lbl_spot.text=obj.locality;
    
    cell.btn_view.tag=indexPath.row;
    
    [cell.btn_view addTarget:self action:@selector(viewBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    NSString *imagePath=[NSString stringWithFormat:@"%@",_gDataManager.doctorProfileImagePath];
    if([imagePath isEqualToString:@"(null)(null)"])
    {
        
    }
    else
    {
        [self downloadImage:cell imageUrl1:imagePath];
        
    }


    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}
- (void) viewBtnAction:(id)sender
{
    UIButton *button=(UIButton *)sender;
    NSInteger tag = [button tag];
    DoctorClinicInfo_VC* destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"DoctorClinicInfo_VC_id"];
    destVC.clinicData=[clinicArray objectAtIndex:tag];
    [self.navigationController pushViewController:destVC animated:YES];
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    DoctoreMessageDetailViewController* destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"DoctoreMessageDetailViewController_id"];
    //
    //    [self.navigationController pushViewController:destVC animated:YES];
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)leftButton{
    UIButton *button =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(backButtonAction:)forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0, 20, 20)];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = barButton;
    
}
- (IBAction) backButtonAction:(id)sender {
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
    [self presentViewController:vc animated:NO completion:nil];
    
}
- (void) downloadImage:(DoctorClinicList_TVC*)cell imageUrl1:(NSString *)imgURL1
{
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       image_manager = [SDWebImageManager sharedManager];
                       [image_manager downloadWithURL:[NSURL URLWithString:imgURL1] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize, NSInteger feedID, NSString *feedString)
                        {
                        } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished)
                        {
                            if (image)
                            {
                                cell.img_profile.image = image;
                            }
                        }];
                   });
    
}


@end
