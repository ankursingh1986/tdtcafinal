//
//  PatientAppointment_CVC.h
//  TDATC
//
//  Created by iWeb on 9/25/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASStarRatingView.h"
#import "PatientAppointmentModel.h"

@interface PatientAppointment_CVC : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *view_colection;
@property (weak, nonatomic) IBOutlet ASStarRatingView *view_star;
@property (weak, nonatomic) IBOutlet UIButton *btn_resudule;
@property (weak, nonatomic) IBOutlet UIButton *btn_cancel;

@property (weak, nonatomic) IBOutlet UILabel *lbl_approved;
@property (weak, nonatomic) IBOutlet UIView *view_1;

@property (weak, nonatomic) IBOutlet UITextField *sicknessLabel;
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;


@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UITextField *patientNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;


-(void)setupUI;

- (void) setUpData:(PatientAppointmentModel*)data;

@end
