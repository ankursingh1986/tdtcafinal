//
//  DoctorAppointment_VC.m
//  TDATC
//
//  Created by iWeb on 9/18/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "DoctorAppointCancel_VC.h"
#import "DoctorAppointmentCancel_TVC.h"

#import "DataManager.h"
#import "Utility.h"
#import "MyLoader.h"
#import "DataParser.h"
#import "Defines.h"
#import "AppointmentCancelModel.h"


@interface DoctorAppointCancel_VC ()
{
    NSMutableArray* appointmentArray;
    
}

@end

@implementation DoctorAppointCancel_VC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    appointmentArray = [[NSMutableArray alloc] init];
    
    
    [self leftButton];
    [tbl_AppointmentCancel setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    self->tbl_AppointmentCancel.tableFooterView = [[UIView alloc] init];
    
    
      
    // Do any additional setup after loading the view.
}
-(void)getDataFromServer{
    
    
      NSString *postData=[NSString stringWithFormat:@"id=%@",_gDataManager.loginModel.user_id];
    //NSString *postData=[NSString stringWithFormat:@"id=159"];
    
    
    [MyLoader showLoadingView];
    [DataParser DappointmentDeleteListHit:postData WithCompletionHandler:^(NSMutableArray *array, BOOL isSuccessful)
     {
         if (isSuccessful)
         {
             [MyLoader hideLoadingView];
             
             
             appointmentArray = [array mutableCopy];
             appointmentArray=[[[appointmentArray reverseObjectEnumerator] allObjects] mutableCopy];

             [tbl_AppointmentCancel reloadData];
             
             
             
         }
         else
         {
             [MyLoader hideLoadingView];
             [Utility alertWithTitle:@"" withMessage:kNetworkErrorTitle onVC:self];
         }
     }];
    
    
    
}
-(void)viewWillAppear:(BOOL)animated
{
     self.navigationItem.title = @"CANCEL APPOINTMENTS ";
    [self getDataFromServer];
    
    
    
    [super viewWillAppear:animated];
    
}

- (IBAction)didTapTitleView:(id) sender{
    //Perform your actions
    NSLog(@"Title tap");
}
-(void) titleTapped {
    NSLog(@"clcik here");
}

#pragma mark - table view delegates and data socurce


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return appointmentArray.count;
}
//

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    DoctorAppointmentCancel_TVC *cell = [tableView dequeueReusableCellWithIdentifier:@"DoctorAppointmentCancel_Cell" forIndexPath:indexPath];
    AppointmentCancelModel*obj=[appointmentArray objectAtIndex:indexPath.row];
    
    cell.lbl_name.text=[obj.patientname uppercaseString];
    cell.lbl_phone.text=obj.mobile;
    cell.lbl_desc.text=obj.sickness;
    NSArray* foo = [obj.createddate componentsSeparatedByString: @"T"];
    cell.lbl_date.text=[foo objectAtIndex: 0];
    cell.lbl_time.text=[Utility changeTimeFormat:obj.createddate];
    NSString *imagePath;
    
       int total=(int)appointmentArray.count;
//    total--;
//    if(indexPath.row==total)
//    {
        imagePath=[NSString stringWithFormat:@"http://thedocsaroundtheclock.com%@",obj.image];

//    }
//    else
//    {
//        int k= (int)indexPath.row;
//        k++;
//        AppointmentCancelModel*obj=[appointmentArray objectAtIndex:k];
//        
//        imagePath=[NSString stringWithFormat:@"http://thedocsaroundtheclock.com%@",obj.image];
//
//    }

    if([imagePath isEqualToString:@"(null)(null)"])
    {
        
    }
    else
    {
        [self downloadImage:cell imageUrl1:imagePath];
        
    }

    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    DoctoreMessageDetailViewController* destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"DoctoreMessageDetailViewController_id"];
    //
    //    [self.navigationController pushViewController:destVC animated:YES];
    
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(void)leftButton{
    UIButton *button =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(backButtonAction:)forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0, 20, 20)];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = barButton;
    
}
- (IBAction) backButtonAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
}
- (void) downloadImage:(DoctorAppointmentCancel_TVC*)cell imageUrl1:(NSString *)imgURL1
{
    NSLog(@"image url=%@",imgURL1);
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       image_manager = [SDWebImageManager sharedManager];
                       [image_manager downloadWithURL:[NSURL URLWithString:imgURL1] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize, NSInteger feedID, NSString *feedString)
                        {
                        } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished)
                        {
                            if (image)
                            {
                                cell.img_profile.image = image;
                            }
                        }];
                   });
    
}


@end
