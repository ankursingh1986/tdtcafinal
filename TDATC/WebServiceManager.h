//
//  WebServiceManager.h
//  NewsApp
//
//  Created by Vinay Kumar on 24/02/16.
//  Copyright © 2016 Vinay Kumar. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <sys/xattr.h>
#import <SystemConfiguration/SCNetworkReachability.h>
#import <netinet/in.h>
#import "AppDelegate.h"

#define kNetworkErrorMeassage @"internet not working"

@interface WebServiceManager : NSObject

// post value only for Push notification
+ (NSURLRequest*) requestWithServiceGET:(NSString*)service;
+ (NSMutableURLRequest*) requestWithUrlStringGET:(NSString*)urlString;

//+ (NSURLRequest*) postRequestWithServicePushNotification:(NSString*)service withPayload:(NSDictionary*)dict;

//+ (NSMutableURLRequest*) requestWithUrlStringForPushNotification:(NSString*)urlString;


#pragma mark - GET Requests
+ (NSURLRequest*) requestWithService:(NSString*)service;
+ (NSMutableURLRequest*) requestWithUrlString:(NSString*)urlString;
+ (NSURLRequest*) requestWithServiceAndWithSession:(NSString*)service;
#pragma mark - POST Requests
+ (NSURLRequest*) postRequestWithService:(NSString*)service withPayload:(NSString*)details;
+ (NSMutableURLRequest*) postRequestWithUrlString:(NSString*)urlString withPayload:(NSString*)details;

+ (NSURLRequest*) requestWithServiceAndWithSessionPOST:(NSString*)service;
+ (NSMutableURLRequest*) requestWithUrlStringAndWithSessionPOST:(NSString*)urlString;
+ (NSURLRequest*) deleteRequestWithService:(NSString*)service withDictionary:(NSDictionary*)dict;
+ (NSURLRequest*) requestWithServiceAndWithSessionForWishList:(NSString*)service;
+ (NSMutableURLRequest*) postRequestWithUrlDicwithSessionForWishList:(NSString*)urlString withDictionary:(NSDictionary*)dict;
+ (NSURLRequest*) deleteRequestWithServiceForWishList:(NSString*)service withDictionary:(NSDictionary*)dict;
+ (NSURLRequest*) putRequestWithService:(NSString*)service withDictionary:(NSDictionary*)dict;
+ (NSURLRequest*) postRequestWithServiceForWishList:(NSString*)service withPayload:(NSDictionary*)dict;
+ (NSMutableURLRequest*) requestWithUrlStringAndWithSessionForWishList:(NSString*)urlString;
+ (void) sendRequest:(NSURLRequest*)request completion: (void (^)(NSData* , NSError*)) callback;

//+ (NSURLRequest*) postRequestWithService1:(NSString*)service withDictionary:(NSDictionary*)dict;

+ (NSMutableURLRequest*) postRequestWithUrlString:(NSString*)urlString withDictionary:(NSString*)dict;
+ (NSMutableURLRequest*) deleteRequestWithUrlDicwithSessionForWishList:(NSString*)urlString withDictionary:(NSDictionary*)dict;
+ (NSMutableURLRequest*) postRequestWithServiceDic:(NSString*)service withDictionary:(NSDictionary*)dict;
+ (NSMutableURLRequest*) postRequestWithServiceDicwithSesion:(NSString*)service withDictionary:(NSDictionary*)dict;
+ (BOOL) isConnectedToNetwork;
@end
