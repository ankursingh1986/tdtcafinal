//
//  PatientConfirmAppoint_VC.m
//  TDATC
//
//  Created by iWeb on 9/23/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "PatientConfirmAppoint_VC.h"
#import "PayPalMobile.h"

#define kPayPalEnvironment PayPalEnvironmentProduction


//#define kPayPalEnvironment PayPalEnvironmentSandbox

@interface PatientConfirmAppoint_VC ()

@end

@implementation PatientConfirmAppoint_VC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self paypalinit];

    [self leftButton];
    [self setupdate];
    // Do any additional setup after loading the view.
}
-(void)setupdate{
    _txt_email.text = [NSString stringWithFormat:@"%@",_gDataManager.loginModel.email];
    _txt_mobile.text = [NSString stringWithFormat:@"%@",_gDataManager.loginModel.mobile_number];
    _txt_name.text = [[NSString stringWithFormat:@"%@",_gDataManager.loginModel.full_name] capitalizedString];
    _txt_mode.text = @"Zoom Video Confrencing";
    _txt_paymentMode.text = @"Online";
    
        _txt_fees.text=[NSString stringWithFormat:@"$ %@", self.clinicFeeData.fees];

        
    

    
//    [_txt_mode addTarget:delegate action:@selector(textField1Active:) forControlEvents:UIControlEventEditingDidBegin];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didRecognizeTapGesture:)];
    [self.txt_mode.superview addGestureRecognizer:tapGesture];
    
    
    UITapGestureRecognizer *tapGesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didRecognizeTapGesturePayment:)];
    [self.txt_paymentMode.superview addGestureRecognizer:tapGesture1];
    
}
- (void) didRecognizeTapGesture:(UITapGestureRecognizer*) gesture {
    CGPoint point = [gesture locationInView:gesture.view];
    
    if (gesture.state == UIGestureRecognizerStateEnded) {
        if (CGRectContainsPoint(self.txt_mode.frame, point)) {
            UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:@"Choose Consulting Mode" preferredStyle:UIAlertControllerStyleActionSheet];
            
            [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                                    {
                                        // Cancel button tappped do nothing.
                                        
                                    }]];
            
            [actionSheet addAction:[UIAlertAction actionWithTitle:@"Zoom Video Confrencing" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                    {
                                        _txt_mode.text = @"Zoom Video Confrencing";
                                        
                                    }]];
            
            [actionSheet addAction:[UIAlertAction actionWithTitle:@"Face to Face" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                    {
                                        _txt_mode.text = @"Face to Face";
                                        
                                    }]];
            [self presentViewController:actionSheet animated:YES completion:nil];
        }
    }
}


- (void) didRecognizeTapGesturePayment:(UITapGestureRecognizer*) gesture {
    CGPoint point = [gesture locationInView:gesture.view];
    
    if (gesture.state == UIGestureRecognizerStateEnded) {
        if (CGRectContainsPoint(self.txt_paymentMode.frame, point)) {
            UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:@"Choose Mode of Payment" preferredStyle:UIAlertControllerStyleActionSheet];
            
            [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                                    {
                                        // Cancel button tappped do nothing.
                                        
                                    }]];
            
            [actionSheet addAction:[UIAlertAction actionWithTitle:@"Online" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                    {
                                        _txt_paymentMode.text = @"Online";
                                        
                                    }]];
            
            [actionSheet addAction:[UIAlertAction actionWithTitle:@"Offline" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                    {
                                        _txt_paymentMode.text = @"Offline";
                                        
                                    }]];
            [self presentViewController:actionSheet animated:YES completion:nil];
        }
    }
}



-(void)viewWillAppear:(BOOL)animated
{
    self.navigationItem.title = @"CONFIRM APPOINTMENT";
    [self setPayPalEnvironment:self.environment];

    
    [super viewWillAppear:animated];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)lbl_patientName:(id)sender {
}

- (IBAction)touch_Confirm:(id)sender{
    if([_txt_sick.text isEqualToString:@""] || _txt_sick.text.length==0)
    {
        [Utility alertWithTitle:nil withMessage:@"Enter Sickness" onVC:self];
        
    }
    else
    {
        if([self.txt_paymentMode.text isEqualToString:@"Offline"])
        {
            if([self.txt_paymentMode.text isEqualToString:@"Offline"]&&[self.txt_mode.text isEqualToString:@"Zoom Video Confrencing"])
            {
                [Utility alertWithTitle:nil withMessage:@"Zoom Video Confrencing not possible for Offline mode" onVC:self];

            }
            else{
                [self paymentAPI];
            }

            
        }
        else
        {
            NSLog(@"Go to Paypal");
            [self paypalCallUI];
            
        }
    }

    
//    UIStoryboard *storyboard1 = [UIStoryboard storyboardWithName:@"Patient"
//                                                          bundle: nil];
//    UINavigationController *loginNavigationController = (UINavigationController*)[storyboard1 instantiateViewControllerWithIdentifier:@"PatientThankYou_VC_id"];
//    [self presentViewController:loginNavigationController animated:NO completion:nil];
//    

    
}
- (IBAction) backButtonAction:(id)sender {
     [self.navigationController popViewControllerAnimated:YES];
}
-(void)leftButton{
    UIButton *button =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(backButtonAction:)forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0, 20, 20)];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = barButton;
    self.navigationItem.hidesBackButton=TRUE;
    
}


-(void)paypalCallUI{
    PayPalPayment *payment = [[PayPalPayment alloc] init];
    // NSDecimal *a=[NSDecimal decimalNumberWithString:@"1"];
    //int value = [string intValue];
    // payment.amount = [string integerValue];
    NSDecimalNumber *price1;
    price1 = [NSDecimalNumber decimalNumberWithString:self.clinicFeeData.fees];
    
   
    payment.amount = price1;
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *currencyCode = [prefs stringForKey:@"currencyA"];
    // payment.currencyCode = currencyCode;
    payment.currencyCode = @"USD";
    payment.shortDescription = [NSString stringWithFormat:@"Booking Appointment"];
    
    
    payment.items = nil;
    payment.paymentDetails = nil;
    
    
    self->payPalConfig.payPalShippingAddressOption=PayPalShippingAddressOptionPayPal;
    
    if (!payment.processable) {
        // This particular payment will always be processable. If, for
        // example, the amount was negative or the shortDescription was
        // empty, this payment wouldn't be processable, and you'd want
        // to handle that here.
    }
    
    // Update payPalConfig re accepting credit cards.
    // self.payPalConfig.acceptCreditCards = self.acceptCreditCards;
    self->payPalConfig.acceptCreditCards = YES;
    
    
    PayPalPaymentViewController *paymentViewController = [[PayPalPaymentViewController alloc] initWithPayment:payment
                                                                                                configuration:self->payPalConfig
                                                                                                     delegate:self];
    [self presentViewController:paymentViewController animated:YES completion:nil];
}

#pragma mark PayPalPaymentDelegate methods

- (void)payPalPaymentViewController:(PayPalPaymentViewController *)paymentViewController didCompletePayment:(PayPalPayment *)completedPayment {
    NSLog(@"PayPal Payment Success!");
    self.resultText = [completedPayment description];
    [self showSuccess];
    [self paymentAPI];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)paymentAPI{
    NSString *postData;
    
           postData=[NSString stringWithFormat:@"id=%@&clinic_id=%@&dat_id=%@&payementway=%@&sickness=%@&doctor_id=%@&consultingmode=%@",_gDataManager.loginModel.user_id,_clinicFeeData.clinic_id,_str_dat_id,_txt_paymentMode.text,_txt_sick.text,_doctorProfileData.user_id,_txt_mode.text];

    
    NSLog(@"send data=%@",postData);
    
    [MyLoader showLoadingView];
    [DataParser globalHitAPI:patientConfirmAppointmentAPI detaiInfi:postData WithCompletionHandler:^(NSDictionary *dict, BOOL isSuccessful)
     {
         if (isSuccessful)
         {
             [MyLoader hideLoadingView];
             
             if([dict[@"status"] isEqualToString:@"false"])
             {
                 
                 [Utility alertWithTitle:kAlertTitle withMessage:dict[@"message"] onVC:self];
             }
             else
             {
                     UIStoryboard *storyboard1 = [UIStoryboard storyboardWithName:@"Patient"
                                                                           bundle: nil];
                     UINavigationController *loginNavigationController = (UINavigationController*)[storyboard1 instantiateViewControllerWithIdentifier:@"PatientThankYou_VC_id"];
                     [self presentViewController:loginNavigationController animated:NO completion:nil];
                 
//                 [self.navigationController popViewControllerAnimated:YES];
//                 
//                 [Utility alertWithTitle:kAlertTitle withMessage:dict[@"message"] onVC:self];
                 
             }
         }
         else
         {
             [MyLoader hideLoadingView];
             [Utility alertWithTitle:@"" withMessage:kNetworkErrorTitle onVC:self];
         }
     }];
    
}

- (void)payPalPaymentDidCancel:(PayPalPaymentViewController *)paymentViewController {
    NSLog(@"PayPal Payment Canceled");
    self.resultText = nil;
    self.successView.hidden = YES;
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)showSuccess {
    self.successView.hidden = NO;
    self.successView.alpha = 1.0f;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelay:2.0];
    self.successView.alpha = 0.0f;
    [UIView commitAnimations];
}

- (void)setPayPalEnvironment:(NSString *)environment {
    self.environment = environment;
    [PayPalMobile preconnectWithEnvironment:environment];
}
-(void)paypalinit{
    payPalConfig = [[PayPalConfiguration alloc] init];
    payPalConfig.acceptCreditCards = YES;
    
    payPalConfig.languageOrLocale = [NSLocale preferredLanguages][0];
    
    
    // Setting the payPalShippingAddressOption property is optional.
    //
    // See PayPalConfiguration.h for details.
    
    payPalConfig.payPalShippingAddressOption = PayPalShippingAddressOptionPayPal;
    
    // Do any additional setup after loading the view, typically from a nib.
    
    //self.successView.hidden = YES;
    
    // use default environment, should be Production in real life
    self.environment = kPayPalEnvironment;
    
    NSLog(@"PayPal iOS SDK version: %@", [PayPalMobile libraryVersion]);
    
    // Do any additional setup after loading the view from its nib.
    
}


@end
