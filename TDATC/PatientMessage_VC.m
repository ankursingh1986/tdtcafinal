//
//  DoctorWriteMessage_VC.m
//  TDATC
//
//  Created by iWeb on 9/18/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "PatientMessage_VC.h"
#import "PatientDataParser.h"
#import "Utility.h"
#import "Defines.h"
#import "DataManager.h"
#import "MyLoader.h"
#import "PatientSentMessageTableViewCell.h"
#import "PatientInboxMessageTableViewCell.h"
#import "PatientMessageModel.h"
#import "Utility.h"
#import "Global.pch"


@interface PatientMessage_VC ()
{
    NSMutableArray* _messageArray;
    NSMutableArray* _doctorArray;
    BOOL _isImageSelected;
    UIImagePickerController* _imagePicker;
    UIImage *imgData;
}

@end

@implementation PatientMessage_VC

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    [self leftButton];
    _textView_1.text = @"MESSAGE";
    placeflag=1;
    indexArrayId = 0;

    
    tbl_inbox.hidden=YES;
    tbl_sent.hidden=YES;
    _view_1.hidden=NO;
    
    [tbl_inbox setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    self->tbl_inbox.tableFooterView = [[UIView alloc] init];

    [tbl_sent setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    self->tbl_sent.tableFooterView = [[UIView alloc] init];

    
    _view_star.canEdit = NO;
    _view_star.maxRating = 5;
    
   // _view_star.rating = 3;
    _view_star.selectedStar = [UIImage imageNamed:@"redstar"];
    _view_star.notSelectedStar = [UIImage imageNamed:@"greystar"];
    
    _txt_message.delegate=self;
    _textView_1.delegate=self;
    
    
    _lbl_attachmenttext.userInteractionEnabled=YES;
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(attachmentTap:)];
    singleTap.numberOfTapsRequired = 1;
    singleTap.numberOfTouchesRequired = 1;
    singleTap.delegate = self;
    [_lbl_attachmenttext addGestureRecognizer:singleTap];


    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationItem.title = @"WRITE MESSAGE";
    _messageArray = [[NSMutableArray alloc] init];
    _doctorArray = [[NSMutableArray alloc] init];
    [self getDoctorList];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) getDoctorList
{
    [MyLoader showLoadingView];
    [PatientDataParser getDoctorList:[NSString stringWithFormat:@"id=%@",_gDataManager.loginModel.user_id] WithCompletionHandler:^(NSArray *array, BOOL isSuccessful)
     {
         [MyLoader hideLoadingView];
         if (isSuccessful)
         {
             _doctorArray = [array mutableCopy];
             if(_doctorArray.count != 0)
             {
                 DoctorList *data=[_doctorArray objectAtIndex:0];
                 _lbl_name.text=[data.full_name uppercaseString];
                 _lbl_address.text=data.locality;
                 _view_star.rating = [data.rating intValue];

                 NSString *imagePath=[NSString stringWithFormat:@"%@%@",kDoctorImagePath,data.image];
                 if([imagePath isEqualToString:@"(null)(null)"])
                 {
                     
                 }
                 else
                 {
                     [self downloadImage:imagePath];
                     
                 }

             }
         }
         else
         {
             [Utility alertWithTitle:kAlertTitle withMessage:kNetworkErrorTitle onVC:self];
         }
        
     }];
}

- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    if (placeflag==1) {
        _textView_1.text = @"";
        placeflag=0;
        
    }
    
    return YES;
}

-(void) textViewDidChange:(UITextView *)textView
{
    if(_textView_1.text.length == 0){
        
        _textView_1.text = @"MESSAGE";
        placeflag=1;
        [_textView_1 resignFirstResponder];
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        if(_textView_1.text.length == 0){
            
            _textView_1.text = @"MESSAGE";
            placeflag=1;
            [_textView_1 resignFirstResponder];
        }
        return NO;
    }
    
    return YES;
}



#pragma mark - table view delegates and data socurce


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _messageArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(tableView.tag==1)
    {
        PatientInboxMessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Inbox_Cell" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        PatientMessageModel* data = [_messageArray objectAtIndex:indexPath.row];
        [cell setUpData:data];
        
        return cell;
    }
    else if (tableView.tag==2)
    {
        PatientSentMessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Sent_Cell" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        PatientMessageModel* data = [_messageArray objectAtIndex:indexPath.row];
        [cell setUpData:data];
        return cell;
    }
    return nil;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    //    DoctoreMessageDetailViewController* destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"DoctoreMessageDetailViewController_id"];
//    //
//    //    [self.navigationController pushViewController:destVC animated:YES];
//    
//    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"DoctoreMessageDetailViewController_id"];
//    [self presentViewController:vc animated:NO completion:nil];
    
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)touch_Write:(id)sender
{
    tbl_inbox.hidden=YES;
    tbl_sent.hidden=YES;
    _view_1.hidden=NO;
    
    }

- (IBAction)touch_Inbox:(id)sender
{
    tbl_inbox.hidden=NO;
    tbl_sent.hidden=YES;
    _view_1.hidden=YES;
    
    [MyLoader showLoadingView];
    DoctorList *data=[_doctorArray objectAtIndex:indexArrayId];

   NSString* postData = [NSString stringWithFormat:@"id=%@",_gDataManager.loginModel.user_id];
   // NSString* postData = [NSString stringWithFormat:@"id=%@",data.doctor_id];
    
    _gDataManager.doctorMessageId=data.doctor_id;


    [PatientDataParser inboxPatientMessage:postData WithCompletionHandler:^(NSArray *array, BOOL isSuccessful)
     {
         [MyLoader hideLoadingView];
         if (isSuccessful)
         {
             [_messageArray removeAllObjects];
             _messageArray = [array mutableCopy];
             [tbl_inbox reloadData];
         }
         else
         {
             [Utility alertWithTitle:kAlertTitle withMessage:kNetworkErrorTitle onVC:self];
         }
     }];
}

- (IBAction)touch_Sent:(id)sender
{
    tbl_inbox.hidden=YES;
    tbl_sent.hidden=NO;
    _view_1.hidden=YES;
    [MyLoader showLoadingView];
    DoctorList *data=[_doctorArray objectAtIndex:indexArrayId];

    NSString* postData = [NSString stringWithFormat:@"id=%@",_gDataManager.loginModel.user_id];
   // NSString* postData = [NSString stringWithFormat:@"id=%@",data.doctor_id];
    _gDataManager.doctorMessageId=data.doctor_id;
    [PatientDataParser sentPatientMessage:postData WithCompletionHandler:^(NSArray* array , BOOL isSuccessful)
     {
         [MyLoader hideLoadingView];
         if (isSuccessful)
         {
             [_messageArray removeAllObjects];
             _messageArray = [array mutableCopy];
             [tbl_sent reloadData];

         }
         else
         {
              [Utility alertWithTitle:kAlertTitle withMessage:kNetworkErrorTitle onVC:self];
         }
        
     }];
    
}

- (IBAction)nextTouch:(id)sender {
    if(_doctorArray.count>0)
    {
        
        int valur=(int)_doctorArray.count;
        
        if(valur > indexArrayId)
        {
            indexArrayId++;
            
            if(valur > indexArrayId)
            {
                DoctorList *data=[_doctorArray objectAtIndex:indexArrayId];
                _lbl_name.text=[data.full_name uppercaseString];
                _lbl_address.text=data.locality;
                _view_star.rating = [data.rating intValue];
                
                _view_1.hidden=NO;
                tbl_sent.hidden=YES;
                tbl_inbox.hidden=YES;
                
                NSString *imagePath=[NSString stringWithFormat:@"%@%@",kDoctorImagePath,data.image];
                if([imagePath isEqualToString:@"(null)(null)"])
                {
                    
                }
                else
                {
                    [self downloadImage:imagePath];
                    
                }

                
            }
        }
        else
        {
            [Utility alertWithTitle:@"" withMessage:@"No right date available" onVC:self];
            
        }
    }

}

- (IBAction)backTouch:(id)sender {
    if(_doctorArray.count>0)
    {
        if(indexArrayId>0)
        {
            indexArrayId--;
            
            DoctorList *data=[_doctorArray objectAtIndex:indexArrayId];
            _lbl_name.text=[data.full_name uppercaseString];
            _lbl_address.text=data.locality;
            _view_star.rating = [data.rating intValue];
            
            _view_1.hidden=NO;
            tbl_sent.hidden=YES;
            tbl_inbox.hidden=YES;
            
            NSString *imagePath=[NSString stringWithFormat:@"%@%@",kDoctorImagePath,data.image];
            if([imagePath isEqualToString:@"(null)(null)"])
            {
                
            }
            else
            {
                [self downloadImage:imagePath];
                
            }

        }
        else
        {
            [Utility alertWithTitle:@"" withMessage:@"No less date available" onVC:self];
            
        }
    }

}

- (IBAction)sendMessageBtnAction:(id)sender
{
    if ([self userInputValidations])
    {
        NSString* image64Data;
        
        if (!_isImageSelected)
        {
            image64Data = @"";
        }
        else
        {
            //image64Data = @"";
            
            NSData *imageData=UIImageJPEGRepresentation(imgData,.5);
            // NSString *base64 = [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
            image64Data = [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
        }
        DoctorList *data=[_doctorArray objectAtIndex:indexArrayId];

        
        NSString* postData = [NSString stringWithFormat:@"id=%@&doctor_id=%@&subject=%@&doctor_msg=%@&image=%@",_gDataManager.loginModel.user_id,data.doctor_id,self.txt_message.text,self.textView_1.text,image64Data];
        [MyLoader showLoadingView];

        
        [PatientDataParser createPatientMessage:postData WithCompletionHandler:^(NSDictionary *dict, BOOL isSuccessful)
         {
             [MyLoader hideLoadingView];

             if (isSuccessful)
             {
                 NSLog(@"dict=%@",dict);
                 _txt_message.text=@"";
                 _textView_1.text=@"";
                 [Utility alertWithTitle:nil withMessage:dict[@"message"] onVC:self];
             }
             else
             {
                 [Utility alertWithTitle:kAlertTitle withMessage:kNetworkErrorTitle onVC:self];
             }
         }];
    }
  
}

- (IBAction) backButtonAction:(id)sender
{
    UIStoryboard *storyBoard12 = [UIStoryboard storyboardWithName:@"Patient" bundle:nil];
    
    UIViewController *vc = [storyBoard12 instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
    [self presentViewController:vc animated:NO completion:nil];
    
    
}

-(void)leftButton
{
    UIButton *button =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(backButtonAction:)forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0, 20, 20)];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = barButton;
    self.navigationItem.hidesBackButton=TRUE;
}
- (void) downloadImage:(NSString *)imgURL1
{
    NSLog(@"Image URL in method=%@",imgURL1);
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       image_manager = [SDWebImageManager sharedManager];
                       [image_manager downloadWithURL:[NSURL URLWithString:imgURL1] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize, NSInteger feedID, NSString *feedString)
                        {
                        } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished)
                        {
                            if (image)
                            {
                                _img_profile.image = image;
                            }
                        }];
                   });
    
}

- (void) attachmentTap:(UITapGestureRecognizer*)sender
{
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:@"Choose attachment" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                            {
                                // Cancel button tappped do nothing.
                                
                            }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Capture From Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                            {
                                [self takePhotoFromCamra];
                                
                            }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"From Album" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                            {
                                [self choosePhotoFromAlbum];
                                
                            }]];
    [self presentViewController:actionSheet animated:YES completion:nil];
}

#pragma mark - Take Photo From Camera
- (void) takePhotoFromCamra
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        //_isCaemraPhoto = YES;
        _imagePicker=[[UIImagePickerController alloc] init];
        _imagePicker.delegate = self;
        _imagePicker.sourceType=UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:_imagePicker animated:YES completion:nil];
    }
    else
    {
        //[Utility alertWithTitle:@"Camera Unavailable" withMessage:@"Unable to find a camera on your device." onVC:self];
    }
    
}

#pragma mark - Take Photo From Album

- (void) choosePhotoFromAlbum
{
    _imagePicker=[[UIImagePickerController alloc] init];
    _imagePicker.delegate = self;
    //_imagePicker.sourceType=UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    _imagePicker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:_imagePicker animated:YES completion:nil];
}
- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    imgData = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    _isImageSelected = YES;
    int randomID = arc4random() % 9000 + 1000;
    
    _lbl_attachmentTitle.text = [NSString stringWithFormat:@"Img%d.jpg",randomID];
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - User Input validations
- (BOOL) userInputValidations
{
    if ([self.txt_message.text isEqualToString:@""])
    {
        [Utility alertWithTitle:@"Alert!" withMessage:@"Please enter subject" onVC:self];
        return NO;
    }
    
    if ([self.textView_1.text isEqualToString:@""])
    {
        [Utility alertWithTitle:@"Alert!" withMessage:@"Please enter message" onVC:self];
        return NO;
    }
    
    return YES;
}


@end
