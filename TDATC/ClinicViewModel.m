//
//  ClinicViewModel.m
//  TDATC
//
//  Created by iWeb on 11/2/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "ClinicViewModel.h"

@implementation ClinicViewModel
- (id) initWithDictionary:(NSDictionary*)dict
{
    self = [super init];
    if (self)
    {
        self.da_id = [NSString stringWithFormat:@"%@",dict[@"da_id"]];
        self.availabledate =[NSString stringWithFormat:@"%@",dict[@"availabledate"]];
        self.doctor_avail_timesArray = dict[@"doctor_avail_times"];
        
    }
    return self;
}

@end
