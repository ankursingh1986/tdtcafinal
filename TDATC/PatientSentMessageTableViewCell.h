//
//  PatientSentMessageTableViewCell.h
//  TDATC
//
//  Created by Vinay on 07/11/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PatientMessageModel.h"
#import "Global.pch"

@interface PatientSentMessageTableViewCell : UITableViewCell
{
    SDWebImageManager *image_manager;

}

@property (weak, nonatomic) IBOutlet UITextView *txt_title;

@property (weak, nonatomic) IBOutlet UILabel *lbl_date;
@property (weak, nonatomic) IBOutlet UIImageView *img_profile;

@property (weak, nonatomic) IBOutlet UITextView *txt_desc;

- (void) setUpData:(PatientMessageModel*)data;

@end
