//
//  SideMenuTableViewCell.h
//  NicoBeacon
//
//  Created by iWeb on 9/2/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SideMenuTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *sideImage;
@property (weak, nonatomic) IBOutlet UILabel *title_lbl;

@end
