//
//  PatientDoctor_tVC.m
//  TDATC
//
//  Created by iWeb on 9/23/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "PatientDoctor_TVC.h"


@implementation PatientDoctor_TVC

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void) setUpData:(FindDoctorModel*)data
{
    self.nameLabel.text = [data.full_name uppercaseString];
    self.educationLabel.text = [NSString stringWithFormat:@"%@, %@, %@, %@",data.course,data.institute, data.city, data.year];
    self.localityLabel.text = data.locality;
    self.exprianceLabel.text =[NSString stringWithFormat:@"Exp: %@ Years",data.exp];
    self.lbl_price.hidden = YES;

    [self downloadImage:[NSString stringWithFormat:@"%@%@",kDoctorImagePath,data.image]];
}
- (void) downloadImage:(NSString *)imgURL1
{
    NSLog(@"Image URL in method=%@",imgURL1);
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       image_manager = [SDWebImageManager sharedManager];
                       [image_manager downloadWithURL:[NSURL URLWithString:imgURL1] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize, NSInteger feedID, NSString *feedString)
                        {
                        } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished)
                        {
                            if (image)
                            {
                                _img_profile.image = image;
                            }
                        }];
                   });
    
}

@end
