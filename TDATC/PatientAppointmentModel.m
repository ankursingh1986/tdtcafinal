//
//  PatientAppointmentModel.m
//  TDATC
//
//  Created by Vinay on 03/11/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "PatientAppointmentModel.h"

@implementation PatientAppointmentModel

- (id) initWithDictionary:(NSDictionary*)dictionary
{
    self = [super init];
    if (self)
    {
        self.booking_id = dictionary[@"booking_id"];
        self.patient_id = dictionary[@"patient_id"];
        self.doctor_id = dictionary[@"doctor_id"];
        self.clinic_id = dictionary[@"clinic_id"];
        
        if (![dictionary[@"patientname"] isKindOfClass:[NSNull class]])
            self.patientname = dictionary[@"patientname"];
        else
            self.patientname = @"";
        
        
        if (![dictionary[@"da_id"] isKindOfClass:[NSNull class]])
            self.da_id = dictionary[@"da_id"];
        else
            self.da_id = @"";
        
        
        if (![dictionary[@"dat_id"] isKindOfClass:[NSNull class]])
            self.dat_id = dictionary[@"dat_id"];
        else
            self.dat_id = @"";
        
        if (![dictionary[@"sickness"] isKindOfClass:[NSNull class]])
            self.sickness = dictionary[@"sickness"];
        else
            self.sickness = @"";

        if (![dictionary[@"consultingmode"] isKindOfClass:[NSNull class]])
            self.consultingmode = dictionary[@"consultingmode"];
        else
            self.consultingmode = @"";

        if (![dictionary[@"user"][@"full_name"] isKindOfClass:[NSNull class]])
            self.doctorName = dictionary[@"user"][@"full_name"];
        else
            self.doctorName = @"";

        if (![dictionary[@"user"][@"locality"] isKindOfClass:[NSNull class]])
            self.locality = dictionary[@"user"][@"locality"];
        else
            self.locality = @"";
        
        if (![dictionary[@"amount"] isKindOfClass:[NSNull class]])
            self.amount = [NSString stringWithFormat:@"%@",dictionary[@"amount"]];
        else
            self.amount = @"";
        
        self.status=[NSString stringWithFormat:@"%@",dictionary[@"status"]];
        
        if (![dictionary[@"doctor_avail_time"][@"availabletime"] isKindOfClass:[NSNull class]])
            self.avalableTime = dictionary[@"doctor_avail_time"][@"availabletime"];
        else
            self.avalableTime = @"";
        
        if (![dictionary[@"doctor_availability"][@"availabledate"] isKindOfClass:[NSNull class]])
            self.availabledate = dictionary[@"doctor_availability"][@"availabledate"];
        else
            self.availabledate = @"";
        
        if (![dictionary[@"user"][@"image"] isKindOfClass:[NSNull class]])
            self.image = dictionary[@"user"][@"image"];
        else
            self.image = @"";
        if (![dictionary[@"rating"] isKindOfClass:[NSNull class]])
            self.rating = [NSString stringWithFormat:@"%@",dictionary[@"rating"]];
        else
            self.rating = @"";
        
        
    }
    return self;
}

@end
