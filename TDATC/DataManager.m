//
//  DataManager.m
//  TDATC
//
//  Created by iWeb on 9/26/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "DataManager.h"

static DataManager* sharedData = nil;


@implementation DataManager
+ (DataManager*) sharedManager
{
    if (sharedData == nil)
    {
        static dispatch_once_t once;
        dispatch_once(&once, ^{
            sharedData = [[DataManager alloc] init];
            
        });
        
    }
    return sharedData;
}
@end
