//
//  DoctorMemberShipfeaturesModel.h
//  TDATC
//
//  Created by iWeb on 10/31/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DoctorMemberShipfeaturesModel : NSObject
@property (nonatomic, strong) NSString* feature;
@property (nonatomic, strong) NSString* status;
@property (nonatomic, strong) NSString* plan_id;


- (id) initWithDictionary:(NSDictionary*)dictionary;


@end
