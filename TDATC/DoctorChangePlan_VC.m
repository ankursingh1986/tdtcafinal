//
//  DoctorChangePlan_VC.m
//  TDATC
//
//  Created by iWeb on 9/19/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "DoctorChangePlan_VC.h"
#import "DataManager.h"
#import "Utility.h"
#import "MyLoader.h"
#import "DataParser.h"
#import "Defines.h"
#import "PickerModel.h"
#import "PickerTool.h"
//#import "PickerTool.h"
//#import "PickerTool.h"
#import "PayPalMobile.h"

#define kPayPalEnvironment PayPalEnvironmentProduction


//#define kPayPalEnvironment PayPalEnvironmentSandbox





@interface DoctorChangePlan_VC ()
{
    PickerTool* _pickerView;
    NSMutableArray* _planeListArray;
    NSString* _planeId;
    NSMutableArray* _planesTypeListArray;
}

@end

@implementation DoctorChangePlan_VC

- (void)viewDidLoad
{
    [self paypalinit];
    [super viewDidLoad];
    [self pickerViews];
    [self leftButton];
    [self setdata];
    [self getMemberShipPlansList];

    _planesTypeListArray = [[NSMutableArray alloc] initWithObjects:@"Monthly",@"Quarterly",@"Halfyearly",@"Yearly", nil];
}
-(void)setdata{
    
    if ([[NSString stringWithFormat:@"%@",_dataDic[@"plan_id"]] isEqualToString:@"1"])
    {
        strC1=@"BASIC";
        self.memberShipTypeTextField.text = @"BASIC";
    }
    else if([[NSString stringWithFormat:@"%@",_dataDic[@"plan_id"]] isEqualToString:@"2"])
    {
        strC1=@"GOLD";

        self.memberShipTypeTextField.text = @"GOLD";
    }
    else if([[NSString stringWithFormat:@"%@",_dataDic[@"plan_id"]] isEqualToString:@"3"])
    {
        strC1=@"DIAMOND";

        self.memberShipTypeTextField.text = @"DIAMOND";
    }
    strC2 = _dataDic[@"plan"];
    strC3 = [NSString stringWithFormat:@"$%@",_dataDic[@"price"]];

    self.numberOfYearsTextField.text =_dataDic[@"plan"];
    self.currentPriceTextField.text = [NSString stringWithFormat:@"$%@",_dataDic[@"price"]];

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationItem.title = @"CHANGE PLAN";
    _planeListArray = [[NSMutableArray alloc] init];
    self.memberShipTypeTextField.userInteractionEnabled = YES;
    self.numberOfYearsTextField.userInteractionEnabled = YES;
    [self pickerViews];
    [self setPayPalEnvironment:self.environment];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void) pickerViews
{
    _pickerView = [PickerTool loadClass];
    self.memberShipTypeTextField.delegate = self;
    self.memberShipTypeTextField.inputView = _pickerView;
    self.numberOfYearsTextField.delegate = self;
    self.numberOfYearsTextField.inputView = _pickerView;
}
- (IBAction)touch_proceedChange:(id)sender{
    
    if([strC1 isEqualToString:self.memberShipTypeTextField.text]&&[strC2 isEqualToString:self.numberOfYearsTextField.text]&&[strC3 isEqualToString:self.currentPriceTextField.text])
    {
        [Utility alertWithTitle:nil withMessage:@"Select another Plan. This already purchased." onVC:self];

    }
    else
    {
        if([self.currentPriceTextField.text isEqualToString:@"Free"]||[self.currentPriceTextField.text isEqualToString:@"FREE"] )
        {
            NSString *postData=[NSString stringWithFormat:@"id=%@&plan_id=%@&plan=%@&price=Free",_gDataManager.memberShip_id,_planeId,self.numberOfYearsTextField.text];
            
            [MyLoader showLoadingView];
            [DataParser globalHitAPI:doctorChangePlanAPI detaiInfi:postData WithCompletionHandler:^(NSDictionary *dict, BOOL isSuccessful)
             {
                 if (isSuccessful)
                 {
                     [MyLoader hideLoadingView];
                     
                     if([dict[@"status"] isEqualToString:@"false"])
                     {
                         
                         [Utility alertWithTitle:kAlertTitle withMessage:dict[@"message"] onVC:self];
                         
                        
                     }
                     else
                     {
                         [self.navigationController popViewControllerAnimated:YES];

                         [Utility alertWithTitle:kAlertTitle withMessage:dict[@"message"] onVC:self];

                     }
                 }
                 else
                 {
                     [MyLoader hideLoadingView];
                     [Utility alertWithTitle:@"" withMessage:kNetworkErrorTitle onVC:self];
                 }
             }];

        }
        else
        {
            NSLog(@"Go to Paypal");
            [self paypalCallUI];

        }
    }
    
}


- (IBAction) backButtonAction1:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) getMemberShipPlansList
{
    [MyLoader showLoadingView];
    [DataParser getMemberShipTypeWithCompletionHandler:^(NSArray *array, BOOL isSuccessful)
    {
        [MyLoader hideLoadingView];
        if (isSuccessful)
        {
            _planeListArray = [array mutableCopy];
            PickerModel* data;
            
            if ([[NSString stringWithFormat:@"%@",_dataDic[@"plan_id"]] isEqualToString:@"1"])
            {
                data = [_planeListArray objectAtIndex:0];
                self.memberShipTypeTextField.text = @"BASIC";
            }
            else if([[NSString stringWithFormat:@"%@",_dataDic[@"plan_id"]] isEqualToString:@"2"])
            {
                data = [_planeListArray objectAtIndex:1];
                self.memberShipTypeTextField.text = @"GOLD";
            }
            else if([[NSString stringWithFormat:@"%@",_dataDic[@"plan_id"]] isEqualToString:@"3"])
            {
                data = [_planeListArray objectAtIndex:2];
                self.memberShipTypeTextField.text = @"DIAMOND";
            }

           // self.memberShipTypeTextField.text = data.dataName;
            _planeId = data.dataId;
        }
        else
        {
            [Utility alertWithTitle:kAlertTitle withMessage:kNetworkErrorTitle onVC:self];
        }
    }];

}

-(void)leftButton
{
    UIButton *button =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(backButtonAction1:)forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0, 20, 20)];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = barButton;
    self.navigationItem.hidesBackButton=TRUE;
    
}

//-------------------------------------------------------------------------------------------
#pragma mark- Text field delegate and data source
//-------------------------------------------------------------------------------------------

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField==self.memberShipTypeTextField )
    {
        [self setPickerInfo:self.memberShipTypeTextField withArray:_planeListArray];
    }
    else if(textField == self.numberOfYearsTextField)
    {
       [self setPickerInfo:self.numberOfYearsTextField withArray:_planesTypeListArray];
    }
//    else if (textField == self.pi)
//    {
//        //[self setPickerInfo:self.memberShipTypeTextField withArray:_planeListArray];
//    }
}

- (void) setPickerInfo:(UITextField*)textfield  withArray:(NSArray*)array
{
   // if(textfield==self.memberShipTypeTextField)
    {
        [_pickerView pickerViewMethod:textfield arr:array];
        _pickerView.completionHandler = ^(PickerModel* detail)
        {
            if(textfield == self.memberShipTypeTextField)
            {
                _planeId = detail.dataId;
                if([self.numberOfYearsTextField.text isEqualToString:@"Monthly"])
                {
                    self.currentPriceTextField.text=detail.price_monthly;
                }
                else if ([self.numberOfYearsTextField.text isEqualToString:@"Quarterly"])
                {
                    self.currentPriceTextField.text=detail.price_quaterly;
                }
                else if ([self.numberOfYearsTextField.text isEqualToString:@"Halfyearly"])
                {
                    self.currentPriceTextField.text=detail.price_halfyearly;
                }
                else if ([self.numberOfYearsTextField.text isEqualToString:@"Yearly"])
                {
                    self.currentPriceTextField.text=detail.price_yearly;
                }
            }
            else if (textfield == self.numberOfYearsTextField)
            {
                if([_planeId isEqualToString:@"1"])
                {
                    self.currentPriceTextField.text=@"1";

                    PickerModel *obj= [_planeListArray objectAtIndex:0];
                    if([self.numberOfYearsTextField.text isEqualToString:@"Monthly"])
                    {
                        self.currentPriceTextField.text=obj.price_monthly;
                    }
                    else if ([self.numberOfYearsTextField.text isEqualToString:@"Quarterly"])
                    {
                        self.currentPriceTextField.text=obj.price_quaterly;
                    }
                    else if ([self.numberOfYearsTextField.text isEqualToString:@"Halfyearly"])
                    {
                        self.currentPriceTextField.text=obj.price_halfyearly;
                    }
                    else if ([self.numberOfYearsTextField.text isEqualToString:@"Yearly"])
                    {
                        self.currentPriceTextField.text=obj.price_yearly;
                    }
                }
                else if([_planeId isEqualToString:@"2"])
                {
                    self.currentPriceTextField.text=@"2";

                    PickerModel *obj= [_planeListArray objectAtIndex:1];
                    if([self.numberOfYearsTextField.text isEqualToString:@"Monthly"])
                    {
                        self.currentPriceTextField.text=obj.price_monthly;
                    }
                    else if ([self.numberOfYearsTextField.text isEqualToString:@"Quarterly"])
                    {
                        self.currentPriceTextField.text=obj.price_quaterly;
                    }
                    else if ([self.numberOfYearsTextField.text isEqualToString:@"Halfyearly"])
                    {
                        self.currentPriceTextField.text=obj.price_halfyearly;
                    }
                    else if ([self.numberOfYearsTextField.text isEqualToString:@"Yearly"])
                    {
                        self.currentPriceTextField.text=obj.price_yearly;
                    }
                }
                else if([_planeId isEqualToString:@"3"])
                {
                        self.currentPriceTextField.text=@"3";

                    PickerModel *obj= [_planeListArray objectAtIndex:2];
                    if([self.numberOfYearsTextField.text isEqualToString:@"Monthly"])
                    {
                        self.currentPriceTextField.text=obj.price_monthly;
                    }
                    else if ([self.numberOfYearsTextField.text isEqualToString:@"Quarterly"])
                    {
                        self.currentPriceTextField.text=obj.price_quaterly;
                    }
                    else if ([self.numberOfYearsTextField.text isEqualToString:@"Halfyearly"])
                    {
                        self.currentPriceTextField.text=obj.price_halfyearly;
                    }
                    else if ([self.numberOfYearsTextField.text isEqualToString:@"Yearly"])
                    {
                        self.currentPriceTextField.text=obj.price_yearly;
                    }
                }

            }
    //       else if (textfield == self.txt_city)
    //            _cityId = detail.dataId;
            [textfield resignFirstResponder];
        };
    }
//    else
//    {
//        [_pickerView pickerViewMethod:textfield arr:array];
//
//    }
}


-(void)paypalCallUI{
    PayPalPayment *payment = [[PayPalPayment alloc] init];
    // NSDecimal *a=[NSDecimal decimalNumberWithString:@"1"];
       //int value = [string intValue];
    // payment.amount = [string integerValue];
    NSDecimalNumber *price1 = [NSDecimalNumber decimalNumberWithString:self.currentPriceTextField.text];
    payment.amount = price1;
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *currencyCode = [prefs stringForKey:@"currencyA"];
    // payment.currencyCode = currencyCode;
    payment.currencyCode = @"USD";
    payment.shortDescription = [NSString stringWithFormat:@"%@(%@)",self.memberShipTypeTextField.text,self.numberOfYearsTextField.text];
    
    
    payment.items = nil;
    payment.paymentDetails = nil;
    
   
    self->payPalConfig.payPalShippingAddressOption=PayPalShippingAddressOptionPayPal;
    
    if (!payment.processable) {
        // This particular payment will always be processable. If, for
        // example, the amount was negative or the shortDescription was
        // empty, this payment wouldn't be processable, and you'd want
        // to handle that here.
    }
    
    // Update payPalConfig re accepting credit cards.
    // self.payPalConfig.acceptCreditCards = self.acceptCreditCards;
    self->payPalConfig.acceptCreditCards = YES;
    
    
    PayPalPaymentViewController *paymentViewController = [[PayPalPaymentViewController alloc] initWithPayment:payment
                                                                                                configuration:self->payPalConfig
                                                                                                     delegate:self];
    [self presentViewController:paymentViewController animated:YES completion:nil];
}

#pragma mark PayPalPaymentDelegate methods

- (void)payPalPaymentViewController:(PayPalPaymentViewController *)paymentViewController didCompletePayment:(PayPalPayment *)completedPayment {
    NSLog(@"PayPal Payment Success!");
    self.resultText = [completedPayment description];
    [self showSuccess];
    [self paymentAPI];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)paymentAPI{
    NSString *postData=[NSString stringWithFormat:@"id=%@&plan_id=%@&plan=%@&price=%@",_gDataManager.memberShip_id,_planeId,self.numberOfYearsTextField.text,self.currentPriceTextField.text];
    
    [MyLoader showLoadingView];
    [DataParser globalHitAPI:doctorChangePlanAPI detaiInfi:postData WithCompletionHandler:^(NSDictionary *dict, BOOL isSuccessful)
     {
         if (isSuccessful)
         {
             [MyLoader hideLoadingView];
             
             if([dict[@"status"] isEqualToString:@"false"])
             {
                 
                 [Utility alertWithTitle:kAlertTitle withMessage:dict[@"message"] onVC:self];
             }
             else
             {
                 [self.navigationController popViewControllerAnimated:YES];
                 
                 [Utility alertWithTitle:kAlertTitle withMessage:dict[@"message"] onVC:self];
                 
             }
         }
         else
         {
             [MyLoader hideLoadingView];
             [Utility alertWithTitle:@"" withMessage:kNetworkErrorTitle onVC:self];
         }
     }];

}

- (void)payPalPaymentDidCancel:(PayPalPaymentViewController *)paymentViewController {
    NSLog(@"PayPal Payment Canceled");
    self.resultText = nil;
    self.successView.hidden = YES;
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)showSuccess {
    self.successView.hidden = NO;
    self.successView.alpha = 1.0f;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelay:2.0];
    self.successView.alpha = 0.0f;
    [UIView commitAnimations];
}

- (void)setPayPalEnvironment:(NSString *)environment {
    self.environment = environment;
    [PayPalMobile preconnectWithEnvironment:environment];
}
-(void)paypalinit{
    payPalConfig = [[PayPalConfiguration alloc] init];
    payPalConfig.acceptCreditCards = YES;
    
    payPalConfig.languageOrLocale = [NSLocale preferredLanguages][0];
    
    
    // Setting the payPalShippingAddressOption property is optional.
    //
    // See PayPalConfiguration.h for details.
    
    payPalConfig.payPalShippingAddressOption = PayPalShippingAddressOptionPayPal;
    
    // Do any additional setup after loading the view, typically from a nib.
    
    //self.successView.hidden = YES;
    
    // use default environment, should be Production in real life
    self.environment = kPayPalEnvironment;
    
    NSLog(@"PayPal iOS SDK version: %@", [PayPalMobile libraryVersion]);
    
    // Do any additional setup after loading the view from its nib.
    
}



@end
