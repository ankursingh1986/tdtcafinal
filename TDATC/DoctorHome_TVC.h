//
//  DoctorHome_TVC.h
//  TDATC
//
//  Created by iWeb on 9/18/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DoctorHome_TVC : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *img_home;
@property (weak, nonatomic) IBOutlet UILabel *lbl_titile;
@property (weak, nonatomic) IBOutlet UILabel *lbl_detail;

@end
