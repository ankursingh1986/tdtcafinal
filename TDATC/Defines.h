//
//  Defines.h
//  Sports Simba
//
//  Created by Mahabir on 05/12/16.
//  Copyright © 2016 I-WebServices. All rights reserved.
//

#import <Foundation/Foundation.h>


#define kServerURL @"http://thedocsaroundtheclock.com/api/"
#define kPatientImagePath @"http://thedocsaroundtheclock.com/webroot/img/patient/"
#define kPatientImageSubPath @"http://thedocsaroundtheclock.com/webroot/"
#define kDoctorImagePath @"http://thedocsaroundtheclock.com/webroot/img/doctor/"


#define kKeyData @"X-Oc-Merchant-Id"
#define kValueData @"123456789"


#define loginAPI @"Userapi/login.json"  // DONE
#define korgotPasswordAPI @"Userapi/forgetpasword.json" //DONE
#define signUpAPI @"Userapi/register.json" //DONE
#define faceBookApi @"userapi/facebooklogin.json" // Working
#define twitterApi @"userapi/twitterlogin.json" // Working



// Patient
#define patientProfileInfoAPI @"patient/profile.json" // WORKING
#define patientProfileEditAPI @"patient/editprofile.json" // WORKING
#define patientMessageSentAPI @"patient/messagesent.json" // WORKING
#define patientMessageInboxAPI @"patient/messageinbox.json" // WORKING
#define patientComposeMessageAPI @"patient/composemessage.json" // WORKING
#define patientFeedbackListAPI @"patient/feedback.json" // WORKING
#define patientFeedbackAddAPI @"patient/feedbackadd.json" // WORKING
#define patientAppointmentlistAPI @"patient/appointmentlist.json" // WORKING
#define patientTermAndConditionAPI @"patient/tnc.json" // WORKING
#define patientFindDoctorAPI @"patient/finddoctor.json" // WORKING
#define patientDoctorSearchResultAPI @"patient/doctorserachresult.json" // WORKING
#define patientConfirmAppointmentAPI @"patient/confirmappointment.json" // DONE
#define patientResuduleAppointmentAPI @"patient/timeresudule.json" // DONE


#define patientCancelAppointmentAPI @"patient/cancelappintement.json" // WORKING
#define patientClinicInformationAPI @"patient/clinicinformation.json" // WORKING
#define serviceListAPI @"patient/services.json" //
#define bloodGroupListAPI @"patient/bloodgroups.json"
#define kGetDoctorListAPI @"patient/massagecommaddlist.json"


#define patientClinicViewAPI @"patient/doctorapointmetncalender.json" // DONE


// Doctor
#define kMemberShipDetailsAPI @"doctor/membershipdetail.json" //"DONE"
#define kDoctorCurrnentMemberShip @"doctor/membership.json" // "DONE"
#define kDoctorCurrnentMemberShipFeature @"doctor/membershipfeatures.json" // "DONE"
#define doctorChangePlanAPI @"doctor/changeplan.json" // DONE


#define kCitiesListAPI @"doctor/city.json" // DONE
#define kStateListAPI @"doctor/state.json" // DONE
#define kCountriesList @"doctor/countries.json" // DONE
#define doctorEditProfileAPI @"Doctor/editprofile.json" // DONE
#define doctorProfileAPI @"doctor/profile.json" // DONE
#define doctorMessageListingAPI @"doctor/messagelisting.json" // DONE
#define doctorComposemsgAPI @"doctor/composemsg.json" // DONE
#define doctorSentmsgAPI @"doctor/sentmsg.json" // DONE
#define doctorFeedbackAPI @"doctor/feedback.json" // DONE
#define doctorAppointmentrequestAPI @"doctor/appointmentrequest.json" // DONE
#define doctorAppointmentapprovedAPI @"doctor/appointmentapproved.json" // DONE
#define doctorAppointmentACCEPTAPI @"doctor/doctorappointmentaproved.json" // DONE
#define doctorAppointmentREJECTAPI @"doctor/docterappointmetncanceled.json" // DONE

#define doctorAppointmentCancelListAPI @"doctor/doctorappoinmetentcancellist.json" //DONE
#define doctorPatientListAPI @"doctor/patientlisting.json" // DONE


#define doctorClinicListAPI @"doctor/cliniclist.json" // DONE
#define doctorClinicViewAPI @"doctor/clinicview.json" // DONE
#define doctorChangeTimeAPI @"doctor/doctortime.json" // Working











#define kBarButtonSizes CGRectMake(0, 0, 30, 30)

#define kAlertTitle @"Alert!"
#define kDataNotFoundTitle @"Data not Found."

#define kNetworkErrorTitle @"Sorry! you required Internet Connection."

