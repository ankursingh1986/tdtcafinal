//
//  PatientDataParser.h
//  TDATC
//
//  Created by Vinay on 01/11/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebServiceManager.h"
#import "Defines.h"
#import "DataManager.h"
#import "Global.pch"

@interface PatientDataParser : NSObject

+ (void) loginAPIHit:(NSString*)details WithCompletionHandler:(void (^)(NSDictionary* dict , BOOL isSuccessful)) handler;
+ (void) getPatientProfile:(NSString*)details WithCompletionHandler:(void (^)(NSDictionary* dict , BOOL isSuccessful)) handler;
+ (void) updatePatientProfile:(NSString*)details WithCompletionHandler:(void (^)(NSDictionary* dict , BOOL isSuccessful)) handler;

+ (void) sentPatientMessage:(NSString*)details WithCompletionHandler:(void (^)(NSArray* array , BOOL isSuccessful)) handler;
+ (void) inboxPatientMessage:(NSString*)details WithCompletionHandler:(void (^)(NSArray* array , BOOL isSuccessful)) handler;

+ (void) createPatientMessage:(NSString*)details WithCompletionHandler:(void (^)(NSDictionary* dict , BOOL isSuccessful)) handler;

+ (void) patientFeedback:(NSString*)details WithCompletionHandler:(void (^)(NSArray *array , BOOL isSuccessful)) handler;
+ (void) patientAddFeedback:(NSString*)details WithCompletionHandler:(void (^)(NSDictionary* dict , BOOL isSuccessful)) handler;

+ (void) patientAppointmentList:(NSString*)details WithCompletionHandler:(void (^)(NSArray *array , BOOL isSuccessful)) handler;
+ (void) patientFindDoctorList:(NSString*)details WithCompletionHandler:(void (^)(NSDictionary* dict , BOOL isSuccessful)) handler;

+ (void) patientSearchDoctorList:(NSString*)details WithCompletionHandler:(void (^)(NSArray* array , BOOL isSuccessful)) handler;
+ (void) patientConfirmAppointment:(NSString*)details WithCompletionHandler:(void (^)(NSDictionary* dict , BOOL isSuccessful)) handler;

+ (void) doctorClinicList:(NSString*)details WithCompletionHandler:(void (^)(NSArray *array , BOOL isSuccessful)) handler;
+ (void) patientCancelAppointment:(NSString*)details WithCompletionHandler:(void (^)(NSDictionary* dict , BOOL isSuccessful)) handler;
+ (void) patientClinicInfo:(NSString*)details WithCompletionHandler:(void (^)(NSArray *array , BOOL isSuccessful)) handler;

+ (void) termsAndConditions:(NSString*)details WithCompletionHandler:(void (^)(NSDictionary* dict , BOOL isSuccessful)) handler;
+ (void) getServiceListWithCompletionHandler:(void (^)(NSArray* array , BOOL isSuccessful)) handler;
+ (void) getBloodGroupsListWithCompletionHandler:(void (^)(NSArray* array , BOOL isSuccessful)) handler;
+ (void) getDoctorList:(NSString*)details WithCompletionHandler:(void (^)(NSArray* array , BOOL isSuccessful)) handler;
+ (void) PatientClinictViewHit:(NSString*)details WithCompletionHandler:(void (^)(NSMutableArray* array , BOOL isSuccessful)) handler;

@end
