//
//  DoctorThankYou_VC.m
//  TDATC
//
//  Created by iWeb on 9/19/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "DoctorThankYou_VC.h"
#import "DataManager.h"
#import "Utility.h"
#import "MyLoader.h"
#import "DataParser.h"
#import "Defines.h"

@interface DoctorThankYou_VC ()

@end

@implementation DoctorThankYou_VC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
