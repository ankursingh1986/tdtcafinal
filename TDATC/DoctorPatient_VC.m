//
//  DoctorPatient_VC.m
//  TDATC
//
//  Created by iWeb on 9/18/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "DoctorPatient_VC.h"
#import "DoctorPatient_TVC.h"
#import "DoctorPatientDetail_VC.h"
#import "DoctorWriteMessage_VC.h"
#import "DataManager.h"
#import "Utility.h"
#import "MyLoader.h"
#import "DataParser.h"
#import "Defines.h"
#import "DoctorPatientModel.h"
#import "SWRevealViewController.h"

@interface DoctorPatient_VC ()
{
    NSMutableArray *patientArray;
}

@end

@implementation DoctorPatient_VC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self leftButton];
    patientArray=[[NSMutableArray alloc]init];

    [tbl_Patient setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    self->tbl_Patient.tableFooterView = [[UIView alloc] init];
    [self getDataFromServer];


    // Do any additional setup after loading the view.
}
-(void)getDataFromServer{
    
    
  //  NSString *postData=[NSString stringWithFormat:@"id=159"];
    NSString *postData=[NSString stringWithFormat:@"id=%@",_gDataManager.loginModel.user_id];

    
    
    [MyLoader showLoadingView];
    [DataParser DappointmentPatienttListHit:postData WithCompletionHandler:^(NSMutableArray *array, BOOL isSuccessful)
     {
         if (isSuccessful)
         {
             [MyLoader hideLoadingView];
             patientArray = [array mutableCopy];
             [tbl_Patient reloadData];
         }
         else
         {
             [MyLoader hideLoadingView];
             [Utility alertWithTitle:@"" withMessage:kNetworkErrorTitle onVC:self];
         }
     }];
    
}


#pragma mark - table view delegates and data socurce


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return patientArray.count;
}
//

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    DoctorPatient_TVC *cell = [tableView dequeueReusableCellWithIdentifier:@"Patient_Cell" forIndexPath:indexPath];
    
    DoctorPatientModel *obj = [patientArray objectAtIndex:indexPath.row];
    cell.lbl_Name.text=[obj.full_name uppercaseString];
    
   // cell.img_home.image=[UIImage imageNamed:[arrayImage objectAtIndex:indexPath.row]];
   // cell.lbl_titile.text=[arrayTitle objectAtIndex:indexPath.row];
   // cell.lbl_detail.text=[arrayDesc objectAtIndex:indexPath.row];
    
    cell.btn_View.tag=indexPath.row;
    cell.btn_message.tag=indexPath.row;

    [cell.btn_View addTarget:self action:@selector(viewBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btn_message addTarget:self action:@selector(messageBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    NSLog(@"id=%@",obj.patient_id);
    NSString *imagePath;
    if([obj.patient_id isEqualToString:@"<null>"])
    {
        imagePath=[NSString stringWithFormat:@"%@%@",kPatientImagePath,obj.patient_image];
        cell.lbl_age.text=[NSString stringWithFormat:@"%@ Yrs (%@)",obj.age,[obj.gender uppercaseString]];


    }
    else
    {
        imagePath=[NSString stringWithFormat:@"%@%@",kPatientImagePath,obj.profile_image];
        
        cell.lbl_age.text=[NSString stringWithFormat:@"%@ Yrs (%@)",obj.dob,[obj.gender uppercaseString]];


    }
    if([imagePath isEqualToString:@"(null)(null)"])
    {
        
    }
    else
    {
        [self downloadImage:cell imageUrl1:imagePath];
        
    }



    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    DoctoreMessageDetailViewController* destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"DoctoreMessageDetailViewController_id"];
    //
    //    [self.navigationController pushViewController:destVC animated:YES];
    
    
    
}

- (void) messageBtnAction:(id)sender
{
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"DoctorMessageViewController_id"];
    
    UINavigationController *nav =[[UINavigationController alloc]initWithRootViewController:vc];
    
    [self.revealViewController setFrontViewController:nav];
    [self.revealViewController setFrontViewPosition:FrontViewPositionLeft animated:YES];
//    DoctorWriteMessage_VC* destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"DoctorWriteMessage_VC_id"];
//    
//    [self.navigationController pushViewController:destVC animated:YES];
    
}

- (void) viewBtnAction:(id)sender
{
    UIButton *button=(UIButton *)sender;
    NSInteger tag = [button tag];
    DoctorPatientDetail_VC* destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"DoctorPatientDetail_VC_id"];
    destVC.patientInfo= [patientArray objectAtIndex:tag];
    [self.navigationController pushViewController:destVC animated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationItem.title = @"PATIENTS";
    
    [super viewWillAppear:animated];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)leftButton{
    UIButton *button =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(backButtonAction:)forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0, 20, 20)];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = barButton;
    
}
- (IBAction) backButtonAction:(id)sender {
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
    [self presentViewController:vc animated:NO completion:nil];
    
}
- (void) downloadImage:(DoctorPatient_TVC*)cell imageUrl1:(NSString *)imgURL1
{
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       image_manager = [SDWebImageManager sharedManager];
                       [image_manager downloadWithURL:[NSURL URLWithString:imgURL1] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize, NSInteger feedID, NSString *feedString)
                        {
                        } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished)
                        {
                            if (image)
                            {
                                cell.img_View.image = image;
                            }
                        }];
                   });
    
}


@end
