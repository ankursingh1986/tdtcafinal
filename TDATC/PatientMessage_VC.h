//
//  PatientMessage_VC.h
//  TDATC
//
//  Created by iWeb on 9/22/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASStarRatingView.h"
#import "Global.pch"

@interface PatientMessage_VC : UIViewController <UITextViewDelegate, UITextFieldDelegate,UIGestureRecognizerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    IBOutlet UITableView *tbl_inbox,*tbl_sent;
    int placeflag;
    SDWebImageManager *image_manager;
      int indexArrayId;

}
@property (weak, nonatomic) IBOutlet UILabel *lbl_attachmenttext;
@property (weak, nonatomic) IBOutlet UILabel *lbl_attachmentTitle;
- (IBAction)touch_Write:(id)sender;
- (IBAction)touch_Inbox:(id)sender;
- (IBAction)touch_Sent:(id)sender;
- (IBAction)nextTouch:(id)sender;
- (IBAction)backTouch:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btn_1;
@property (weak, nonatomic) IBOutlet UIButton *btn_2;
@property (weak, nonatomic) IBOutlet UIButton *btn_3;
@property (weak, nonatomic) IBOutlet UIView *view_1;
@property (weak, nonatomic) IBOutlet UIImageView *img_profile;
@property (weak, nonatomic) IBOutlet UILabel *lbl_name;
@property (weak, nonatomic) IBOutlet UILabel *lbl_address;

@property (weak, nonatomic) IBOutlet ASStarRatingView *view_star;

@property (weak, nonatomic) IBOutlet UITextView *textView_1;
@property (weak, nonatomic) IBOutlet UITextField *txt_message;


- (IBAction)sendMessageBtnAction:(id)sender;


@end
