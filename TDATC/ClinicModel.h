//
//  ClinicModel.h
//  TDATC
//
//  Created by iWeb on 10/14/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ClinicModel : NSObject

@property (nonatomic, strong) NSString* clinic_id;
@property (nonatomic, strong) NSString* doctor_id;
@property (nonatomic, strong) NSString* clicnic_name;

@property (nonatomic, strong) NSString* locality;
//@property (nonatomic, strong) NSString* patientname;
//
//@property (nonatomic, strong) NSString* mobile;
//
//@property (nonatomic, strong) NSString* createddate;
//@property (nonatomic, strong) NSString* sickness;
//@property (nonatomic, strong) NSString* status;
//@property (nonatomic, strong) NSString* consultingmode;
//@property (nonatomic, strong) NSString* payementway;
//

- (id) initWithDictionary:(NSDictionary*)dictionary;



@end
