//
//  LoginModel.h
//  TDATC
//
//  Created by iWeb on 10/24/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LoginModel : NSObject

@property (nonatomic, strong) NSString* user_id;
@property (nonatomic, strong) NSString* username;

@property (nonatomic, strong) NSString* full_name;
@property (nonatomic, strong) NSString* role;

@property (nonatomic, strong) NSString* image;
@property (nonatomic, strong) NSString* dob;

@property (nonatomic, strong) NSString* mobile_number;
@property (nonatomic, strong) NSString* alternate_mobile_number;


@property (nonatomic, strong) NSString* email;
@property (nonatomic, strong) NSString* zoom_id;

@property (nonatomic, strong) NSString* zoom_status;
@property (nonatomic, strong) NSString* zoom_vedio_url_id;

@property (nonatomic, strong) NSString* user_status;
@property (nonatomic, strong) NSString* gender;

@property (nonatomic, strong) NSString* country_id;
@property (nonatomic, strong) NSString* state_id;

@property (nonatomic, strong) NSString* city_id;
@property (nonatomic, strong) NSString* facebook_id;

@property (nonatomic, strong) NSString* twitter_id;
@property (nonatomic, strong) NSString* locality;
@property (nonatomic, strong) NSString* pincode;


@property (nonatomic, strong) NSString* b_id;


- (id) initWithDictionary:(NSDictionary*)dictionary;

@end
