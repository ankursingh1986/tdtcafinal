//
//  ClinicModel.m
//  TDATC
//
//  Created by iWeb on 10/14/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "ClinicModel.h"

@implementation ClinicModel
- (id) initWithDictionary:(NSDictionary*)dict
{
    self = [super init];
    if (self)
    {
        self.clinic_id = [NSString stringWithFormat:@"%@",dict[@"clinic_id"]];
        self.doctor_id =[NSString stringWithFormat:@"%@",dict[@"doctor_id"]];
        if (![dict[@"clicnic_name"] isKindOfClass:[NSNull class]])
            self.clicnic_name=[NSString stringWithFormat:@"%@",dict[@"clicnic_name"]];
        else
            self.clicnic_name = @"";
        
        if (![dict[@"locality"] isKindOfClass:[NSNull class]])
            self.locality= dict[@"locality"];
        else
            self.locality= @"";
        
    }
    return self;
}

@end
