//
//  DoctoreMessageDetailViewController.m
//  TDATC
//
//  Created by iWeb on 9/18/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "DoctoreMessageDetailViewController.h"
#import "DoctorWriteMessage_VC.h"
#import "Defines.h"


@interface DoctoreMessageDetailViewController ()

@end

@implementation DoctoreMessageDetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self leftButton];
    self.lbl_name.text=[self.messageData.full_name uppercaseString];
    self.lbl_age.text=[NSString stringWithFormat:@"24 Yrs(%@)",self.messageData.gender];
    self.lbl_date.text=[Utility changeDateTimeFormat:self.messageData.date];
    
    
    self.lbl_subject.text=self.messageData.subject;
    NSString *imagePath=[NSString stringWithFormat:@"%@%@",_gDataManager.imgpatientpath,self.messageData.patientimag_path];
    if([imagePath isEqualToString:@"(null)(null)"])
    {
        
    }
    else
    {
        [self downloadImage:imagePath];
        
    }


}


-(void)viewWillAppear:(BOOL)animated
{
    self.navigationItem.title = @"MESSAGES";
    [super viewWillAppear:animated];
        if([_messageData.attchment isEqualToString:@""])
        {
            self.doctorMessageTextView.text = _messageData.doctor_msg;
    
        }
        else
        {
            NSString *strData=[NSString stringWithFormat:@"%@ \n %@\%@", _messageData.doctor_msg,_gDataManager.imgmsgpath, _messageData.attchment];
            self.doctorMessageTextView.text = strData;
        }

    self.doctorMessageTextView.text = self.doctorMessage;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)touch_Reply:(id)sender
{
    DoctorWriteMessage_VC* destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"DoctorWriteMessage_VC_id"];
    
    destVC.patientId =self.patientId;
    destVC.inboxMessageArray=self.inboxArray;
    destVC.patientMessageData=self.messageData;
    
    [self.navigationController pushViewController:destVC animated:YES];

}

- (IBAction) backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    
    
}
-(void)leftButton
{
    UIButton *button =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(backButtonAction:)forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0, 20, 20)];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = barButton;
    self.navigationItem.hidesBackButton=TRUE;
    
}
- (void) downloadImage:(NSString *)imgURL1
{
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       image_manager = [SDWebImageManager sharedManager];
                       [image_manager downloadWithURL:[NSURL URLWithString:imgURL1] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize, NSInteger feedID, NSString *feedString)
                        {
                        } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished)
                        {
                            if (image)
                            {
                                self.img_profile.image = image;
                            }
                        }];
                   });
    
}

@end
