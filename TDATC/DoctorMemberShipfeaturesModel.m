//
//  DoctorMemberShipfeaturesModel.m
//  TDATC
//
//  Created by iWeb on 10/31/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "DoctorMemberShipfeaturesModel.h"

@implementation DoctorMemberShipfeaturesModel
- (id) initWithDictionary:(NSDictionary*)dict
{
    self = [super init];
    if (self)
    {
       
       
        if (![dict[@"feature"] isKindOfClass:[NSNull class]])
            self.feature = [NSString stringWithFormat:@"%@",dict[@"feature"]];
        else
            self.feature = @"";
        
        self.status =[NSString stringWithFormat:@"%@",dict[@"status"]];
            NSDictionary *locDic=dict[@"membership_right"];
            self.plan_id= [NSString stringWithFormat:@"%@",locDic[@"plan_id"]];
        
        
    }
    return self;
}

@end
