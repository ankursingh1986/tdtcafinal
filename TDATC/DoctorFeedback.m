//
//  DoctorFeedback.m
//  TDATC
//
//  Created by Vinay on 14/10/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "DoctorFeedback.h"

@implementation DoctorFeedback

- (id) initWithDictionary:(NSDictionary*)dictionary
{
    self = [super init];
    if (self)
    {
        if (![dictionary[@"patient_name"] isKindOfClass:[NSNull class]])
            self.patient_name = dictionary[@"patient_name"];
        else
            self.patient_name = @"";
        if (![dictionary[@"patient_name"] isKindOfClass:[NSNull class]])
            self.pasent_msg = dictionary[@"pasent_msg"];
        else
            self.pasent_msg = @"";
        
        //self.pasent_msg = dictionary[@"pasent_msg"];
        if (![dictionary[@"crdt"] isKindOfClass:[NSNull class]])
            self.crdt = dictionary[@"crdt"];
        else
            self.crdt = @"";
        //self.crdt = dictionary[@"crdt"];
        
        if (![dictionary[@"raiting_id"] isKindOfClass:[NSNull class]])
            self.raiting_id = [NSString stringWithFormat:@"%@",dictionary[@"raiting_id"]];
        else
            self.raiting_id = @"";
        //self.raiting_id = [NSString stringWithFormat:@"%@",dictionary[@"raiting_id"]];
        if (![dictionary[@"patient_image"] isKindOfClass:[NSNull class]])
            self.patient_image = [NSString stringWithFormat:@"%@",dictionary[@"patient_image"]];
        else
            self.patient_image = @"";
        //self.patient_image = [NSString stringWithFormat:@"%@",dictionary[@"patient_image"]];
        
        
    }
    return self;
}

@end
