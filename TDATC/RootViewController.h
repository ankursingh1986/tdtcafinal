//
//  RootViewController.h
//  NicoBeacon
//
//  Created by iWeb on 9/2/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SideMenuProfileTableViewCell.h"
#import "SideMenuTableViewCell.h"
//#import "LogOutTableViewCell.h"
//#import "SDWebImageManager.h"
#import "SDWebImageManager.h"


@interface RootViewController : UIViewController
{
    //SDWebImageManager *image_manager;
    SDWebImageManager *image_manager;


    SideMenuProfileTableViewCell *cellFirst;
    SideMenuTableViewCell *cellSecond;
}
@property (weak, nonatomic) IBOutlet UITableView *tbl_home;
@property (weak, nonatomic) IBOutlet UIButton *btn_logout;
- (IBAction)touch_logout:(id)sender;

@end
