//
//  DoctorFeedback.h
//  TDATC
//
//  Created by Vinay on 14/10/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DoctorFeedback : NSObject

@property(nonatomic, strong) NSString* patient_name;
@property(nonatomic, strong) NSString* pasent_msg;
@property(nonatomic, strong) NSString* crdt;
@property(nonatomic, strong) NSString* raiting_id;
@property(nonatomic, strong) NSString* patient_image;



- (id) initWithDictionary:(NSDictionary*)dictionary;

@end
