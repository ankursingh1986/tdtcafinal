//
//  DoctorActivePlan_VC.m
//  TDATC
//
//  Created by iWeb on 9/19/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "DoctorActivePlan_VC.h"
#import "DoctorActivePlan_TVC.h"
#import "DataManager.h"
#import "Utility.h"
#import "MyLoader.h"
#import "DataParser.h"
#import "Defines.h"
#import "DoctorMemberShipfeaturesModel.h"




@interface DoctorActivePlan_VC ()
{
    NSMutableArray* _planeFeatureListArray,*_localPlanArray;

}

@end

@implementation DoctorActivePlan_VC

- (void)viewDidLoad {
    [super viewDidLoad];
    [tbl_Data setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    self->tbl_Data.tableFooterView = [[UIView alloc] init];


    [self leftButton];
    _planeFeatureListArray = [[NSMutableArray alloc] init];
    _localPlanArray=[[NSMutableArray alloc]init];

    [self getMemberShipFeatureList];
    arrayTitle=[[NSMutableArray alloc]initWithObjects:@"Clinic Addition",@"Past Appointments",@"Reshedule Appointment",@"Patient Last visit",@"Clinic Addition",@"Past Appointments",@"Reshedule Appointment",@"Patient Last visit", nil];
    self.textSelectionList.delegate = self;
    self.textSelectionList.dataSource = self;
    
    self.textSelectionList.selectionIndicatorAnimationMode = HTHorizontalSelectionIndicatorAnimationModeNoBounce;
    self.textSelectionList.showsEdgeFadeEffect = NO;
    
    
    
//    self.textSelectionList.selectionIndicatorColor = [UIColor colorWithRed:82/255.0 green:0/255.0 blue:100/255.0 alpha:1];
    self.textSelectionList.selectionIndicatorColor = [UIColor whiteColor];
    self.textSelectionList.backgroundColor=[UIColor colorWithRed:215.0f/255.0f green:29.0f/255.0f blue:32.0f/255.0f alpha:1.0];
    [self.textSelectionList setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [self.textSelectionList setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

    [self.textSelectionList setTitleFont:[UIFont boldSystemFontOfSize:15] forState:UIControlStateNormal];
    [self.textSelectionList setTitleFont:[UIFont boldSystemFontOfSize:15] forState:UIControlStateSelected];
    [self.textSelectionList setTitleFont:[UIFont boldSystemFontOfSize:15] forState:UIControlStateHighlighted];
    self.carMakes = @[@"BASIC",
                      @"GOLD",
                      @"DIAMOND",
                      ];
    
    
    [self.view addSubview:self.textSelectionList];
    self.textSelectionList.snapToCenter = YES;
    self.textSelectionList.centerButtons=YES;
    [self.textSelectionList setSelectedButtonIndex:-1 animated:YES];
    // Do any additional setup after loading the view.
}

- (void) getMemberShipFeatureList
{
    [MyLoader showLoadingView];
    [DataParser getMemberShipFeatureWithCompletionHandler:^(NSArray *array, BOOL isSuccessful)
     {
         [MyLoader hideLoadingView];
         if (isSuccessful)
         {
            _localPlanArray=[array mutableCopy];
             
             _planeFeatureListArray = [[NSMutableArray alloc] init];
             for (int i=0;i<_localPlanArray.count;i++)
             {
                    DoctorMemberShipfeaturesModel* obj = [_localPlanArray objectAtIndex:i];
                 if([obj.plan_id isEqualToString:@"1"])
                 {
                     [_planeFeatureListArray addObject:obj];

                 }
                 
             }

             
             [tbl_Data reloadData];

         }
         else
         {
             [Utility alertWithTitle:kAlertTitle withMessage:kNetworkErrorTitle onVC:self];
         }
     }];
    
}


-(void)viewWillAppear:(BOOL)animated
{
    self.navigationItem.title = @"ACTIVE FEATURE";
    
    [super viewWillAppear:animated];
    
}

#pragma mark - HTHorizontalSelectionListDataSource Protocol Methods

- (NSInteger)numberOfItemsInSelectionList:(HTHorizontalSelectionList *)selectionList {
    return self.carMakes.count;
}

- (NSString *)selectionList:(HTHorizontalSelectionList *)selectionList titleForItemWithIndex:(NSInteger)index {
    //  return nil;
    return self.carMakes[index];
}

#pragma mark - HTHorizontalSelectionListDelegate Protocol Methods

- (void)selectionList:(HTHorizontalSelectionList *)selectionList didSelectButtonWithIndex:(NSInteger)index {
    
    NSLog(@"Click on Tab=%ld",(long)index);
    if(index==0)
    {
        _planeFeatureListArray = [[NSMutableArray alloc] init];
        for (int i=0;i<_localPlanArray.count;i++)
        {
            DoctorMemberShipfeaturesModel* obj = [_localPlanArray objectAtIndex:i];
            if([obj.plan_id isEqualToString:@"1"])
            {
                [_planeFeatureListArray addObject:obj];
                
            }
            
        }

    }
    else if (index==1)
    {
        _planeFeatureListArray = [[NSMutableArray alloc] init];
        for (int i=0;i<_localPlanArray.count;i++)
        {
            DoctorMemberShipfeaturesModel* obj = [_localPlanArray objectAtIndex:i];
            if([obj.plan_id isEqualToString:@"2"])
            {
                [_planeFeatureListArray addObject:obj];
                
            }
            
        }

    }
    else
    {
        _planeFeatureListArray = [[NSMutableArray alloc] init];
        for (int i=0;i<_localPlanArray.count;i++)
        {
            DoctorMemberShipfeaturesModel* obj = [_localPlanArray objectAtIndex:i];
            if([obj.plan_id isEqualToString:@"3"])
            {
                [_planeFeatureListArray addObject:obj];
                
            }
            
        }

    }
    [tbl_Data reloadData];
    
   // [self getProductsWithSpecificID:index];
}


#pragma mark - table view delegates and data socurce


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 55;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    NSLog(@"data");
 //   return arrayTitle.count;
    return  _planeFeatureListArray.count;
}
//

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DoctorActivePlan_TVC *cell = [tableView dequeueReusableCellWithIdentifier:@"DoctorActivePlan_Cell" forIndexPath:indexPath];
        DoctorMemberShipfeaturesModel *obj=[_planeFeatureListArray objectAtIndex:indexPath.row];
    
            cell.lbl_title.text=obj.feature;
  
      //  cell.lbl_title.text=[arrayTitle objectAtIndex:indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;

    
        return cell;
   
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    DoctoreMessageDetailViewController* destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"DoctoreMessageDetailViewController_id"];
    //
    //    [self.navigationController pushViewController:destVC animated:YES];
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction) backButtonAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
    
}
-(void)leftButton{
    UIButton *button =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(backButtonAction:)forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0, 20, 20)];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = barButton;
    self.navigationItem.hidesBackButton=TRUE;
    
}

@end
