//
//  PatientFeedbackModel.m
//  TDATC
//
//  Created by Vinay on 03/11/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "PatientFeedbackModel.h"

@implementation PatientFeedbackModel

- (id) initWithDictionary:(NSDictionary*)dictionary
{
    self = [super init];
    if (self)
    {
        
        if (![dictionary[@"full_name"] isKindOfClass:[NSNull class]])
            self.full_name = dictionary[@"full_name"];
        else
            self.full_name = @"";
        
        if (![dictionary[@"image"] isKindOfClass:[NSNull class]])
            self.image = dictionary[@"image"];
        else
            self.image = @"";
        if (![dictionary[@"image_path"] isKindOfClass:[NSNull class]])
            self.image_path = dictionary[@"image_path"];
        else
            self.image_path = @"";
        if (![dictionary[@"fb"][@"pasent_msg"] isKindOfClass:[NSNull class]])
            self.pasent_msg = dictionary[@"fb"][@"pasent_msg"];
        else
            self.pasent_msg = @"";
        if (![dictionary[@"fb"][@"raiting_id"] isKindOfClass:[NSNull class]])
            self.raiting_id = dictionary[@"fb"][@"raiting_id"];
        else
            self.raiting_id = @"";
        if (![dictionary[@"fb"][@"crdt"] isKindOfClass:[NSNull class]])
            self.date = dictionary[@"fb"][@"crdt"];
        else
            self.date = @"";
        if (![dictionary[@"fb"][@"id"] isKindOfClass:[NSNull class]])
            self.doctorId = [NSString stringWithFormat:@"%@",dictionary[@"fb"][@"user_id"]];
        else
            self.doctorId = @"";
    }
    return self;
}

@end
