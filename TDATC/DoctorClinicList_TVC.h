//
//  DoctorClinicList_TVC.h
//  TDATC
//
//  Created by iWeb on 9/18/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DoctorClinicList_TVC : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btn_view;
@property (weak, nonatomic) IBOutlet UIImageView *img_profile;
@property (weak, nonatomic) IBOutlet UILabel *lbl_title;
@property (weak, nonatomic) IBOutlet UILabel *lbl_spot;

@end
