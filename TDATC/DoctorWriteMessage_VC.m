//
//  DoctorWriteMessage_VC.m
//  TDATC
//
//  Created by iWeb on 9/18/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "DoctorWriteMessage_VC.h"
#import "Utility.h"
#import "DataParser.h"
#import "DataManager.h"
#import "MyLoader.h"
#import "DoctorSentMessageModel.h"
#import "DoctorISent_TVC.h"
#import "DoctorInBox_TVC.h"
#import "DoctorMessage.h"
#import "Defines.h"


@interface DoctorWriteMessage_VC ()
{
    NSMutableArray* _sentMessageArray;
    NSMutableArray* _sentLocalMessageArray;

    NSMutableArray* _inboxLocalArray;

     BOOL _isImageSelected;
    UIImagePickerController* _imagePicker;
    UIImage *imgData;
}

@end

@implementation DoctorWriteMessage_VC

#pragma mark - View Controllers life cycle methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    tbl_inbox.hidden=YES;
    tbl_sent.hidden=YES;
    [self leftButton];
    _textView_1.text = @"MESSAGE";
    placeflag=1;
    _inboxLocalArray = [[NSMutableArray alloc]init];
    [self loadInbox];
    [tbl_inbox setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    self->tbl_inbox.tableFooterView = [[UIView alloc] init];

    [tbl_sent setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    self->tbl_sent.tableFooterView = [[UIView alloc] init];
    _txt_message.delegate=self;
    _textView_1.delegate=self;
    
    _sentMessageArray = [[NSMutableArray alloc] init];
    self.lbl_name.text=[self.patientMessageData.full_name uppercaseString];
    self.lbl_age.text=[NSString stringWithFormat:@"24 Yrs(%@)",self.patientMessageData.gender];
    NSString *imagePath=[NSString stringWithFormat:@"%@%@",_gDataManager.imgpatientpath,self.patientMessageData.patientimag_path];
    if([imagePath isEqualToString:@"(null)(null)"])
    {
        
    }
    else
    {
        [self downloadImage:imagePath];
        
    }

    _lbl_attachment.userInteractionEnabled=YES;
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(attachmentTap:)];
    singleTap.numberOfTapsRequired = 1;
    singleTap.numberOfTouchesRequired = 1;
    singleTap.delegate = self;
    [_lbl_attachment addGestureRecognizer:singleTap];
    

}
-(void)loadInbox{
    for (int a=0; a<_inboxMessageArray.count; a++) {
        DoctorMessage *obj=[_inboxMessageArray objectAtIndex:a];
        if([obj.patient_id isEqualToString:_patientMessageData.patient_id])
        {
            [_inboxLocalArray addObject:obj];

        }
        [tbl_inbox reloadData];
        
    }
    if(_inboxLocalArray.count>0)
    {
        [_btn_2 setTitle:[NSString stringWithFormat:@"INBOX %lu",(unsigned long)_inboxLocalArray.count] forState:UIControlStateNormal];
        
    }
    

    
    
}
- (void) attachmentTap:(UITapGestureRecognizer*)sender
{
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:@"Choose attachment" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                            {
                                // Cancel button tappped do nothing.
                                
                            }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Capture From Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                            {
                                [self takePhotoFromCamra];
                                
                            }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"From Album" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                            {
                                [self choosePhotoFromAlbum];
                                
                            }]];
    [self presentViewController:actionSheet animated:YES completion:nil];
}

#pragma mark - Take Photo From Camera
- (void) takePhotoFromCamra
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        //_isCaemraPhoto = YES;
        _imagePicker=[[UIImagePickerController alloc] init];
        _imagePicker.delegate = self;
        _imagePicker.sourceType=UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:_imagePicker animated:YES completion:nil];
    }
    else
    {
        //[Utility alertWithTitle:@"Camera Unavailable" withMessage:@"Unable to find a camera on your device." onVC:self];
    }
    
}

#pragma mark - Take Photo From Album

- (void) choosePhotoFromAlbum
{
    _imagePicker=[[UIImagePickerController alloc] init];
    _imagePicker.delegate = self;
    //_imagePicker.sourceType=UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    _imagePicker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:_imagePicker animated:YES completion:nil];
}
- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    imgData = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    _isImageSelected = YES;
    int randomID = arc4random() % 9000 + 1000;

    _lbl_imageName.text = [NSString stringWithFormat:@"Img%d.jpg",randomID];
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}


-(void)viewWillAppear:(BOOL)animated
{
    self.navigationItem.title = @"WRITE MESSAGE";
    
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Send Message Btn Action
- (IBAction)sendBtnAction:(id)sender
{
    if ([self userInputValidations])
    {
        
        
        NSString* image64Data;
        
        if (!_isImageSelected)
        {
            image64Data = @"";
        }
        else
        {
            //image64Data = @"";
            
            NSData *imageData=UIImageJPEGRepresentation(imgData,.5);
            // NSString *base64 = [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
            image64Data = [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
        }

        
        NSString* payLoad = [NSString stringWithFormat:@"id=%@&pasent_id=%@&subject=%@&doctor_msg=%@&image=%@",_gDataManager.loginModel.user_id,self.patientId,self.txt_message.text,self.textView_1.text,image64Data];
        NSLog(@"send data=%@",payLoad);
        [MyLoader showLoadingView];
        
        [DataParser doctorComposeMsg:payLoad WithCompletionHandler:^(NSDictionary *dict, BOOL isSuccessful)
         {
             [MyLoader hideLoadingView];
             if (isSuccessful)
             {
                 if([dict[@"status"] isEqualToString:@"true"])
                 {
                     _txt_message.text = @"";
                     _textView_1.text = @"";

                     [Utility alertWithTitle:nil withMessage:dict[@"message"] onVC:self];

                 }
                 else
                 {
                     [Utility alertWithTitle:nil withMessage:dict[@"message"] onVC:self];

                 }
               
             }
             else
             {
                 [Utility alertWithTitle:@"Alert!" withMessage:@"Please check your internet connection" onVC:self];
             }
         }];
    }
}

#pragma mark - User Input validations
- (BOOL) userInputValidations
{
    if ([self.txt_message.text isEqualToString:@""])
    {
        [Utility alertWithTitle:@"Alert!" withMessage:@"Please enter subject" onVC:self];
        return NO;
    }
    
    if ([self.textView_1.text isEqualToString:@""])
    {
        [Utility alertWithTitle:@"Alert!" withMessage:@"Please enter message" onVC:self];
        return NO;
    }
    
    return YES;
}

#pragma mark - Attachment Btn Action
- (IBAction)attachmentBtnAction:(id)sender
{
    
}

#pragma mark - Text field delegates

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
   
}

- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    if (placeflag==1)
    {
        _textView_1.text = @"";
        placeflag=0;
    }
    return YES;
}

-(void) textViewDidChange:(UITextView *)textView
{
    if(_textView_1.text.length == 0)
    {
        _textView_1.text = @"MESSAGE";
        placeflag=1;
        [_textView_1 resignFirstResponder];
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    if([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
        if(_textView_1.text.length == 0)
        {
            _textView_1.text = @"MESSAGE";
            placeflag=1;
            [_textView_1 resignFirstResponder];
        }
        return NO;
    }
    
    return YES;
}


#pragma mark - table view delegates and data socurce


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView.tag==1)
        return _inboxLocalArray.count;
    else if(tableView.tag == 2)
        return _sentMessageArray.count;
    else
        return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(tableView.tag==1)
    {
        
        DoctorInBox_TVC *cell = [tableView dequeueReusableCellWithIdentifier:@"Inbox_Cell" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        DoctorMessage *obj=[_inboxLocalArray objectAtIndex:indexPath.row];
        cell.lbl_subject.text=obj.subject;
        cell.txtView_inbox.text=obj.doctor_msg;
        cell.lbl_date.text=[Utility changeDateTimeFormat:obj.date];
        
        return cell;
    }
    else if (tableView.tag==2)
    {

        DoctorISent_TVC *cell = [tableView dequeueReusableCellWithIdentifier:@"Sent_Cell" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        DoctorSentMessageModel* data = [_sentMessageArray objectAtIndex:indexPath.row];
        [cell setUpData:data];
        
        return cell;
    }
    return nil;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"DoctoreMessageDetailViewController_id"];
    //[self presentViewController:vc animated:NO completion:nil];
}

- (IBAction)touch_Write:(id)sender
{
    tbl_inbox.hidden=YES;
    tbl_sent.hidden=YES;
    _view_1.hidden=NO;
}

- (IBAction)touch_Inbox:(id)sender
{
    tbl_inbox.hidden=NO;
    tbl_sent.hidden=YES;
    _view_1.hidden=YES;
}

- (IBAction)touch_Sent:(id)sender
{
    tbl_inbox.hidden=YES;
    tbl_sent.hidden=NO;
    _view_1.hidden=YES;
    
    [MyLoader showLoadingView];
    [DataParser doctorSentMsg:[NSString stringWithFormat:@"id=%@",_gDataManager.loginModel.user_id] WithCompletionHandler:^(NSArray *array, BOOL isSuccessful)
     {
         [MyLoader hideLoadingView];
         if (isSuccessful)
         {
            // _sentMessageArray = [array copy];
             _sentLocalMessageArray =[array copy];
             for (int i=0; i<_sentLocalMessageArray.count; i++) {
                 DoctorSentMessageModel *obj=[_sentLocalMessageArray objectAtIndex:i];
                 if([obj.patientID isEqualToString:_patientMessageData.patient_id])
                 {
                     [_sentMessageArray addObject:obj];
                     
                 }

             }
             [tbl_sent reloadData];
         }
         else
         {
             [Utility alertWithTitle:@"Alert!" withMessage:@"Please check your internet connection" onVC:self];
         }
     }];
}


- (IBAction) backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)leftButton
{
    UIButton *button =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(backButtonAction:)forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0, 20, 20)];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = barButton;
    self.navigationItem.hidesBackButton=TRUE;
}
- (void) downloadImage:(NSString *)imgURL1
{
    NSLog(@"Image URL in method=%@",imgURL1);
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       image_manager = [SDWebImageManager sharedManager];
                       [image_manager downloadWithURL:[NSURL URLWithString:imgURL1] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize, NSInteger feedID, NSString *feedString)
                        {
                        } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished)
                        {
                            if (image)
                            {
                                self.img_profile.image = image;
                            }
                        }];
                   });
    
}

@end
