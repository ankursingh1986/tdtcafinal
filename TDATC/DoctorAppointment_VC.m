//
//  DoctorAppointment_VC.m
//  TDATC
//
//  Created by iWeb on 9/18/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "DoctorAppointment_VC.h"
#import "DoctorAppointment_TVC.h"
#import "DoctorAppointRequest_VC.h"

#import "DataManager.h"
#import "Utility.h"
#import "MyLoader.h"
#import "DataParser.h"
#import "Defines.h"
#import "AppointmentModel.h"
#import "DoctorAppointCancel_VC.h"


@interface DoctorAppointment_VC ()
{
    NSMutableArray* _appointmentArray;
    
    NSMutableArray* _selectedDateArray;
    
    NSMutableArray* _mainArray;

}

@end

@implementation DoctorAppointment_VC

- (void)viewDidLoad
{
    [super viewDidLoad];

    _appointmentArray = [[NSMutableArray alloc] init];
    _selectedDateArray = [[NSMutableArray alloc] init];
    _mainArray = [[NSMutableArray alloc] init];

    _calender1.delegate = self;
    _calender1.dataSource = self;
    
    [self leftButton];
    
    self.view_1.hidden=YES;
    [tbl_Appointment setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    self->tbl_Appointment.tableFooterView = [[UIView alloc] init];


    _lbl_request.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGesture =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(requestAppointmentTap)];
    [_lbl_request addGestureRecognizer:tapGesture];
    
    
    _lbl_cancel.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGesture1 =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(cancelAppintmentTap)];
    [_lbl_cancel addGestureRecognizer:tapGesture1];
     [self setDate];
    
    // Do any additional setup after loading the view.
}

-(void)cancelAppintmentTap{
    DoctorAppointCancel_VC* destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"DoctorAppointCancel_VC_id"];
    
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void)getDataFromServer
{
    
    
    NSString *postData=[NSString stringWithFormat:@"id=%@",_gDataManager.loginModel.user_id];
  //  NSString *postData=[NSString stringWithFormat:@"id=159"];

    
    [MyLoader showLoadingView];
    [DataParser DappointmentListHit:postData WithCompletionHandler:^(NSMutableArray *array, BOOL isSuccessful)
     {
         if (isSuccessful)
         {
             [MyLoader hideLoadingView];
             
            
                 _appointmentArray = [array mutableCopy];
             
             NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
             [dateFormatter setDateFormat:@"yyyy-MM-dd"];
             [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
             // or @"yyyy-MM-dd hh:mm:ss a" if you prefer the time with AM/PM
             NSString *date1 =  [dateFormatter stringFromDate:[NSDate date]];
             
             for (AppointmentModel* obj in _appointmentArray)
             {
                 NSLog(@"%@", obj.createddate);
                 NSString *dateStr = [Utility changeDateCalenderFormat:obj.createddate];
                 
                 // Convert string to date object
                 NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                 [dateFormat setDateFormat:@"yyyy-MM-dd"];
                 [dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
                 NSDate *appointmentDate = [dateFormat dateFromString:dateStr];
                 NSDate *date = [dateFormat dateFromString:date1];
                 
                 if ([date isEqualToDate:appointmentDate])
                 {
                     NSLog(@"datetime=%@",obj.createddate);
                     [_mainArray addObject:obj];
                     ///_mainArray = [_selectedDateArray mutableCopy];
                 }
             }
             
          //   [self.calender1 reloadData];
             
            [tbl_Appointment reloadData];
             [self.calender1 reloadData];


             
             
         }
         else
         {
             [MyLoader hideLoadingView];
             [Utility alertWithTitle:@"" withMessage:kNetworkErrorTitle onVC:self];
         }
     }];
    

    
}
-(void)requestAppointmentTap{
    DoctorAppointRequest_VC* destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"DoctorAppointRequest_VC_id"];
    
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void)viewWillAppear:(BOOL)animated
{
   // self.navigationItem.title = @"MY APPOINTMENTS";
    
    _calender1.appearance.eventDefaultColor = [UIColor greenColor];

    [super viewWillAppear:animated];
    [self getDataFromServer];


    
   
    
}


-(void)setDate
{
    NSDate *dateGloble = [NSDate date];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    
    [df setDateFormat:@"MMMM dd, YYYY"];
    
    titleLabelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [titleLabelButton setTitle:[[df stringFromDate:dateGloble]uppercaseString] forState:UIControlStateNormal];
    titleLabelButton.frame = CGRectMake(0, 0, 70, 44);
    titleLabelButton.font = [UIFont fontWithName:@"Montserrat-Bold" size:20.0];
    [titleLabelButton addTarget:self action:@selector(didTapTitleView:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.titleView = titleLabelButton;
}

- (IBAction)didTapTitleView:(id) sender
{
    //Perform your actions
    NSLog(@"Title tap");
    
    titleLabelButton.selected = !titleLabelButton.selected;
    if(titleLabelButton.selected)
    {
        
        self.view_1.hidden=NO;
        // _coll_view.hidden=YES;
    }
    else
    {
        self.view_1.hidden=YES;
        //  _coll_view.hidden=NO;
        
    }
    
    NSLog(@"Title tap");

}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    self.view_1.hidden=YES;
    
}

-(void) titleTapped
{
    NSLog(@"clcik here");
}

- (void) backToHOmePage
{
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
    [self presentViewController:vc animated:NO completion:nil];
    
}
#pragma mark - table view delegates and data socurce


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _mainArray.count;
}
//

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    DoctorAppointment_TVC *cell = [tableView dequeueReusableCellWithIdentifier:@"DoctorAppointment_Cell" forIndexPath:indexPath];
    AppointmentModel*obj=[_mainArray objectAtIndex:indexPath.row];
    
    cell.lbl_name.text=[obj.patientname uppercaseString];
    cell.lbl_phone.text=obj.mobile;
    cell.lbl_desc.text=obj.sickness;
    NSArray* foo = [obj.createddate componentsSeparatedByString: @"T"];
    cell.lbl_date.text=[foo objectAtIndex: 0];
    cell.lbl_time.text=[Utility changeTimeFormat:obj.createddate];
    NSLog(@"status=%@",obj.status);
    if([obj.status isEqualToString:@"3"])
    {
        cell.lbl_status.hidden=NO;
        cell.lbl_status.text=@"Visited";
        cell.lbl_status.backgroundColor=[UIColor blueColor];
//        [cell.btn_status setTitle:@"Visited" forState:UIControlStateNormal];
    }
    if([obj.status isEqualToString:@"0"])
    {
        cell.lbl_status.hidden=NO;
        cell.lbl_status.text=@"Not Approved";

        cell.lbl_status.backgroundColor=[UIColor colorWithRed:67.0f/255.0f green:160.0f/255.0f blue:71.0f/255.0f alpha:1.0];
        


    }


    NSString *imagePath=[NSString stringWithFormat:@"%@",obj.image];
    if([imagePath isEqualToString:@"(null)(null)"])
    {
        
    }
    else
    {
        [self downloadImage:cell imageUrl1:imagePath];
        
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;

}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    DoctoreMessageDetailViewController* destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"DoctoreMessageDetailViewController_id"];
    //
    //    [self.navigationController pushViewController:destVC animated:YES];
    
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)leftButton{
    UIButton *button =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(backButtonAction:)forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0, 20, 20)];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = barButton;
    
}
- (IBAction) backButtonAction:(id)sender {
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
    [self presentViewController:vc animated:NO completion:nil];
    
}
- (void) downloadImage:(DoctorAppointment_TVC*)cell imageUrl1:(NSString *)imgURL1
{
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       image_manager = [SDWebImageManager sharedManager];
                       [image_manager downloadWithURL:[NSURL URLWithString:imgURL1] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize, NSInteger feedID, NSString *feedString)
                        {
                        } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished)
                        {
                            if (image)
                            {
                                cell.img_profile.image = image;
                            }
                        }];
                   });
    
}

#pragma mark - Calnder delegates

- (BOOL)calendar:(FSCalendar *)calendar hasEventForDate:(NSDate *)date
{
    if(_appointmentArray.count!=0)
    {
        for (int k=0; k<_appointmentArray.count; k++) {
            AppointmentModel* data = [_appointmentArray objectAtIndex:k];
            NSString *dateStr = [Utility changeDateCalenderFormat:data.createddate];
            
            // Convert string to date object
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"yyyy-MM-dd"];
            NSDate *datenew = [dateFormat dateFromString:dateStr];
            if ([date isEqualToDate:datenew]) {
                return YES;
            }
            
        }
    }
    
    return NO;
}

///FSCalendarDelegate


- (void)calendar:(FSCalendar *)calendar didSelectDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition
{
    self.view_1.hidden=YES;
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"MMMM dd, YYYY"];
    NSLog(@"did select date %@",[df stringFromDate:date]);
    //NSDate *selectedDate = [df dateFromString:date];
    
    titleLabelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [titleLabelButton setTitle:[[df stringFromDate:date]uppercaseString] forState:UIControlStateNormal];
    titleLabelButton.frame = CGRectMake(0, 0, 70, 44);
    titleLabelButton.font = [UIFont fontWithName:@"Montserrat-Bold" size:20.0];
    [titleLabelButton addTarget:self action:@selector(didTapTitleView:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.titleView = titleLabelButton;
    
    [_mainArray removeAllObjects];
    [_selectedDateArray removeAllObjects];
    for (AppointmentModel* obj in _appointmentArray)
    {
        NSLog(@"%@", obj.createddate);
        NSString *dateStr = [Utility changeDateCalenderFormat:obj.createddate];
        
        // Convert string to date object
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd"];
        NSDate *appointmentDate = [dateFormat dateFromString:dateStr];
        
        if ([date isEqualToDate:appointmentDate])
        {
            [_selectedDateArray addObject:obj];
            _mainArray = [_selectedDateArray mutableCopy];
        }
    }
    [tbl_Appointment reloadData];
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isEqual:self.view_1])
    {
        return YES;
    }
    else
    {
        return NO;
    }
    
    //[touch.view isKindOfClass:[FSCalendar class]]
}



@end
