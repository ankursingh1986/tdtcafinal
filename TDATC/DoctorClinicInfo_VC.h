//
//  DoctorClinicInfo_VC.h
//  TDATC
//
//  Created by iWeb on 9/20/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ClinicModel.h"
#import "SDWebImageManager.h"
#import "Global.pch"


@interface DoctorClinicInfo_VC : UIViewController
{
    IBOutlet UITableView *tbl_View;
    NSDate *dateGloble;
    NSDateFormatter *df;
    NSCalendar *gregorian;
    NSDateComponents *components;
    int indexArrayId;
    int height_Cell1,height_Cell2,height_Cell3,height_Cell4;
    
    IBOutlet UIImageView *img_profile;
    SDWebImageManager *image_manager;

    
}
@property (weak, nonatomic) IBOutlet UILabel *lbl_address;
@property (weak, nonatomic) IBOutlet UILabel *lbl_clinicName;
@property (strong, nonatomic) NSIndexPath *expandedIndexPath;
@property (strong,nonatomic) ClinicModel *clinicData;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Date;
- (IBAction)touch_Right:(id)sender;
- (IBAction)touchLeft:(id)sender;

@end
