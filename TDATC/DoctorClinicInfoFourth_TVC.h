//
//  DoctorClinicInfoFourth_TVC.h
//  TDATC
//
//  Created by iWeb on 11/2/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DoctorClinicInfoFourth_TVC : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbl_1;
@property (weak, nonatomic) IBOutlet UILabel *lbl_2;
@property (weak, nonatomic) IBOutlet UILabel *lbl_3;
@property (weak, nonatomic) IBOutlet UILabel *lbl_4;
@property (weak, nonatomic) IBOutlet UILabel *lbl_5;
@property (weak, nonatomic) IBOutlet UILabel *lbl_6;
@property (weak, nonatomic) IBOutlet UILabel *lbl_7;
@property (weak, nonatomic) IBOutlet UILabel *lbl_8;
@property (weak, nonatomic) IBOutlet UILabel *lbl_9;
@property (weak, nonatomic) IBOutlet UILabel *lbl_10;
@property (weak, nonatomic) IBOutlet UILabel *lbl_11;
@property (weak, nonatomic) IBOutlet UILabel *lbl_12;

@end
