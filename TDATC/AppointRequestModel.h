//
//  AppointRequestModel.h
//  TDATC
//
//  Created by iWeb on 10/14/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppointRequestModel : NSObject
@property (nonatomic, strong) NSString* patient_id;
@property (nonatomic, strong) NSString* clinic_id;
@property (nonatomic, strong) NSString* booking_id;

@property (nonatomic, strong) NSString* visit;
@property (nonatomic, strong) NSString* patientname;

@property (nonatomic, strong) NSString* mobile;

@property (nonatomic, strong) NSString* createddate;
@property (nonatomic, strong) NSString* sickness;
@property (nonatomic, strong) NSString* status;
@property (nonatomic, strong) NSString* consultingmode;
@property (nonatomic, strong) NSString* payementway;
@property (nonatomic, strong) NSString* image;



- (id) initWithDictionary:(NSDictionary*)dictionary;


@end
