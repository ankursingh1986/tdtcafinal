//
//  DoctorActivePlan_VC.h
//  TDATC
//
//  Created by iWeb on 9/19/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <HTHorizontalSelectionList/HTHorizontalSelectionList.h>
#import "Global.pch"


@interface DoctorActivePlan_VC : UIViewController<HTHorizontalSelectionListDataSource, HTHorizontalSelectionListDelegate>
{
    IBOutlet UITableView *tbl_Data;
    NSMutableArray *arrayTitle;
    int flag_id;

}
@property (nonatomic, strong) NSArray *carMakes;

@property (weak, nonatomic) IBOutlet HTHorizontalSelectionList *textSelectionList;

@end
