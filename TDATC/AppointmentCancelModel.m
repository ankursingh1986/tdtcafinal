//
//  AppointmentCancelModel.m
//  TDATC
//
//  Created by iWeb on 10/18/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "AppointmentCancelModel.h"

@implementation AppointmentCancelModel
- (id) initWithDictionary:(NSDictionary*)dict
{
    self = [super init];
    if (self)
    {
        self.patient_id = [NSString stringWithFormat:@"%@",dict[@"patient_id"]];
        self.clinic_id =[NSString stringWithFormat:@"%@",dict[@"clinic_id"]];
        if (![dict[@"visit"] isKindOfClass:[NSNull class]])
            self.visit = dict[@"visit"];
        else
            self.visit = @"";
        if (![dict[@"patientname"] isKindOfClass:[NSNull class]])
            self.patientname = dict[@"patientname"];
        else
            self.patientname = @"";
        
        if (![dict[@"mobile"] isKindOfClass:[NSNull class]])
            self.mobile = dict[@"mobile"];
        else
            self.mobile = @"";
        self.createddate = dict[@"createddate"];
        if (![dict[@"sickness"] isKindOfClass:[NSNull class]])
            self.sickness=dict[@"sickness"];
        else
            self.sickness = @"";
        if (![dict[@"status"] isKindOfClass:[NSNull class]])
            self.status=[NSString stringWithFormat:@"%@",dict[@"status"]];
        else
            self.status = @"";
        
        if (![dict[@"image"] isKindOfClass:[NSNull class]])
            self.image=[NSString stringWithFormat:@"%@",dict[@"image"]];
        else
            self.image = @"";

        
    }
    return self;
}

@end
