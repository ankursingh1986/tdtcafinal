//
//  PatientDoctorDeatil_TVC1.h
//  TDATC
//
//  Created by iWeb on 9/24/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASStarRatingView.h"

@interface PatientDoctorDeatil_TVC1 : UITableViewCell

@property (weak, nonatomic) IBOutlet ASStarRatingView *view_star;

@property (weak, nonatomic) IBOutlet UIImageView *docrorImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *localityLabel;

@end
