//
//  DoctorClinicInfo_VC.m
//  TDATC
//
//  Created by iWeb on 9/20/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "PatientClinicInfo_VC.h"
#import "PatientClinicInfo_TVC.h"
#import "PatientConfirmAppoint_VC.h"

@interface PatientClinicInfo_VC ()
{
    NSMutableArray *clinicViewArray;

}

@end

@implementation PatientClinicInfo_VC

- (void)viewDidLoad {
    [super viewDidLoad];
   // [self loadData];
    [self leftButton];
    [self getDataFromServer];
    
    indexArrayId = 0;
    height_Cell1=0;
    height_Cell2=0;
    height_Cell3=0;
    height_Cell4=0;
    
    clinicViewArray= [[NSMutableArray alloc]init];
    if(_gDataManager.fromAppointment)
    {
        [self downloadImage:[NSString stringWithFormat:@"%@%@",kDoctorImagePath,self.patientAppointmentData.image]];
        _lbl_clinicName.text = [self.patientAppointmentData.doctorName uppercaseString];

        __lbl_hospitalname.hidden=YES;
        _lbl_address.hidden=YES;
    }
    else
    {
        [self downloadImage:[NSString stringWithFormat:@"%@%@",kDoctorImagePath,self.doctorProfileData.image]];
        _lbl_clinicName.text = [self.doctorProfileData.full_name uppercaseString];
        __lbl_hospitalname.text = self.clinicData.clicnic_name;
        _lbl_address.text = self.clinicData.locality;

    }
   
    //  [tbl_View setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    
    // Do any additional setup after loading the view.
}
-(void)getDataFromServer{
    NSString *postData;
    if(_gDataManager.fromAppointment)
    {
        postData=[NSString stringWithFormat:@"clinic_id=%@",self.patientAppointmentData.clinic_id];
    }
    else
    {
        postData=[NSString stringWithFormat:@"clinic_id=%@",self.clinicData.clinic_id];
    }
    
    
    [MyLoader showLoadingView];
    [PatientDataParser PatientClinictViewHit:postData WithCompletionHandler:^(NSMutableArray *array, BOOL isSuccessful)
     {
         if (isSuccessful)
         {
             [MyLoader hideLoadingView];
             clinicViewArray = [array mutableCopy];
             if(clinicViewArray.count==0)
             {
                 _lbl_Date.text=@"No time slot alloted";
                 tbl_View.userInteractionEnabled=NO;
                 
             }
             else
             {
                 PatientClinicModel *objData= [clinicViewArray objectAtIndex:indexArrayId];
                 
                 df = [[NSDateFormatter alloc] init];
                 gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
                 components = [[NSDateComponents alloc] init];
                 
                 
                 
                _lbl_Date.text=[self dateFomateString:objData.availabledate];
                 
                 
                 
                 tbl_View.userInteractionEnabled=YES;
                 
                 [tbl_View reloadData];
             }
         }
         else
         {
             [MyLoader hideLoadingView];
             [Utility alertWithTitle:@"" withMessage:kNetworkErrorTitle onVC:self];
         }
     }];
}

-(void)viewWillAppear:(BOOL)animated
{
    if(_gDataManager.fromAppointment)
    {
        self.navigationItem.title = @"RESCHEDULE";

    }
    else
    {
        self.navigationItem.title = @"APPOINTMENT CALENDER";

    }
    
    [super viewWillAppear:animated];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView DataSource and delegates

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4 ;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PatientClinicInfo_TVC *cell1,*cell2,*cell3,*cell4;
    if(indexPath.row==0)
    {
     
        cell1 = [tableView dequeueReusableCellWithIdentifier:@"PatientClinicInfo_Cell1" forIndexPath:indexPath];
        cell1.selectionStyle = UITableViewCellSelectionStyleNone;
        
        if(clinicViewArray.count>0)
        {
            int countdata=0;

            PatientClinicModel *objData= [clinicViewArray objectAtIndex:indexArrayId];
            NSMutableArray *arrayData=objData.doctor_avail_timesArray;
            int x=53,y=51;
            
            for (int i=0; i< arrayData.count; i++) {
                //label for message
                NSDictionary *dicData=[arrayData objectAtIndex:i];
                NSString *time1 = dicData[@"time"];
                NSString *time2 = @"12:00:00";
                
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"HH:mm:ss"];
                
                NSDate *date1= [formatter dateFromString:time1];
                NSDate *date2 = [formatter dateFromString:time2];
                
                NSComparisonResult result = [date1 compare:date2];
                if(result == NSOrderedAscending)
                {
                    NSLog(@"date2 is later than date1");
                    UILabel *lblMsg = [[UILabel alloc] initWithFrame:CGRectZero];
                    lblMsg.tag = i;
                    lblMsg.backgroundColor = [UIColor clearColor];
                    [lblMsg setFont:[UIFont fontWithName:@"Verdana" size:12]];
                    [lblMsg setLineBreakMode:UILineBreakModeWordWrap];
                    
                    if([dicData[@"status"] isEqualToString:@"1"])
                    {
                        //  lblMsg.textColor = [UIColor colorWithRed:(215/255.f) green:(29/255.f) blue:(32/255.f) alpha:.5];
                        lblMsg.textColor = [UIColor blueColor];
                        
                        UITapGestureRecognizer *lblAction = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(lblClickFromCell1:)];
                        lblAction.numberOfTapsRequired = 1;
                        lblMsg.userInteractionEnabled = YES;
                        [lblMsg addGestureRecognizer:lblAction];

                        
                        
                    }
                    else
                    {
                      //  lblMsg.textColor = [UIColor colorWithRed:(29/255.f) green:(29/255.f) blue:(38/255.f) alpha:1.0];
                        lblMsg.textColor = [UIColor blackColor];
                        
                    }
                    
                    
                    lblMsg.text=[self timeFormateChangeAmPm:dicData[@"time"]];
                    lblMsg.frame = CGRectMake(x,y, 66, 18);
                    x=x+65;
                    int k=i+1;
                    if(k % 4==0)
                    {
                        x=53;
                        y=y+35;
                    }
                    
                    countdata++;
                    
                    
                    [cell1 addSubview:lblMsg];
                }
                
            }
            
            
            if(countdata<5)
            {
                
                height_Cell1=y+2+35;
                
                UIView *paintView=[[UIView alloc]initWithFrame:CGRectMake(10, y+35, 300, 1)];
                [paintView setBackgroundColor:[UIColor lightGrayColor]];
                [cell1 addSubview:paintView];
            }
            else
            {
                height_Cell1=y+2;
                
                UIView *paintView=[[UIView alloc]initWithFrame:CGRectMake(10, y, 300, 1)];
                [paintView setBackgroundColor:[UIColor lightGrayColor]];
                [cell1 addSubview:paintView];
            }

            
           // height_Cell1=y+2;
            
           // UIView *paintView=[[UIView alloc]initWithFrame:CGRectMake(10, y, 300, 1)];
           // [paintView setBackgroundColor:[UIColor lightGrayColor]];
           // [cell1 addSubview:paintView];
            
        }
        
        return cell1;

        
        
        
    }
    else if (indexPath.row==1)
    {
       // cell = [tableView dequeueReusableCellWithIdentifier:@"PatientClinicInfo_Cell2" forIndexPath:indexPath];
        
        cell2 = [tableView dequeueReusableCellWithIdentifier:@"PatientClinicInfo_Cell2" forIndexPath:indexPath];
        cell2.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
        if(clinicViewArray.count>0)
        {
            PatientClinicModel *objData= [clinicViewArray objectAtIndex:indexArrayId];
            NSMutableArray *arrayData=objData.doctor_avail_timesArray;
            int x=53,y=51;
            
            for (int i=0; i< arrayData.count; i++) {
                NSDictionary *dicData=[arrayData objectAtIndex:i];
                NSString *time1 = dicData[@"time"];
                NSString *time2 = @"11:59:59";
                NSString *time3 = @"18:00:00";
                
                
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"HH:mm:ss"];
                
                NSDate *date1= [formatter dateFromString:time1];
                NSDate *date2 = [formatter dateFromString:time2];
                NSDate *date3 = [formatter dateFromString:time3];
                
                
                NSComparisonResult result1 = [date1 compare:date2];
                NSComparisonResult result2 = [date1 compare:date3];
                if(result1 == NSOrderedDescending)
                {
                    if(result2 == NSOrderedAscending)
                    {
                        NSLog(@"date2 is later than date1");
                        UILabel *lblMsg = [[UILabel alloc] initWithFrame:CGRectZero];
                        lblMsg.tag = i;
                        lblMsg.backgroundColor = [UIColor clearColor];
                        [lblMsg setFont:[UIFont fontWithName:@"Verdana" size:12]];
                        [lblMsg setLineBreakMode:UILineBreakModeWordWrap];
                        
                        
                        if([dicData[@"status"] isEqualToString:@"1"])
                        {
                            //lblMsg.textColor = [UIColor colorWithRed:(215/255.f) green:(29/255.f) blue:(32/255.f) alpha:0.5];
                            lblMsg.textColor = [UIColor blueColor];
                            
                           
                            UITapGestureRecognizer *lblAction = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(lblClickFromCell2:)];
                            lblAction.numberOfTapsRequired = 1;
                            lblMsg.userInteractionEnabled = YES;
                            [lblMsg addGestureRecognizer:lblAction];


                            
                            
                        }
                        else
                        {
                           // lblMsg.textColor = [UIColor colorWithRed:(29/255.f) green:(29/255.f) blue:(38/255.f) alpha:1.0];
                            lblMsg.textColor = [UIColor blackColor];

                            
                        }
                        
                        lblMsg.text=[self timeFormateChangeAmPm:dicData[@"time"]];
                        lblMsg.frame = CGRectMake(x,y, 66, 18);
                        x=x+65;
                        int k=i+1;
                        if(k % 4==0)
                        {
                            x=53;
                            y=y+35;
                        }
                        
                        
                        
                        [cell2 addSubview:lblMsg];
                    }
                }
                
            }
            // y=y+35;
            height_Cell2=y+2;
            
            
            UIView *paintView=[[UIView alloc]initWithFrame:CGRectMake(10, y, 300, 1)];
            [paintView setBackgroundColor:[UIColor lightGrayColor]];
            [cell2 addSubview:paintView];
        }
        
        
        
        return cell2;

        
    }
    else if (indexPath.row==2)
    {
      //  cell = [tableView dequeueReusableCellWithIdentifier:@"PatientClinicInfo_Cell3" forIndexPath:indexPath];
        
        cell3 = [tableView dequeueReusableCellWithIdentifier:@"PatientClinicInfo_Cell3" forIndexPath:indexPath];
        cell3.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
        if(clinicViewArray.count>0)
        {
            PatientClinicModel *objData= [clinicViewArray objectAtIndex:indexArrayId];
            NSMutableArray *arrayData=objData.doctor_avail_timesArray;
            int x=53,y=51;
            
            for (int i=0; i< arrayData.count; i++) {
                NSDictionary *dicData=[arrayData objectAtIndex:i];
                NSString *time1 = dicData[@"time"];
                NSString *time2 = @"17:59:59";
                NSString *time3 = @"21:00:00";
                
                
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"HH:mm:ss"];
                
                NSDate *date1= [formatter dateFromString:time1];
                NSDate *date2 = [formatter dateFromString:time2];
                NSDate *date3 = [formatter dateFromString:time3];
                
                
                NSComparisonResult result1 = [date1 compare:date2];
                NSComparisonResult result2 = [date1 compare:date3];
                if(result1 == NSOrderedDescending)
                {
                    if(result2 == NSOrderedAscending)
                    {
                        NSLog(@"date2 is later than date1");
                        UILabel *lblMsg = [[UILabel alloc] initWithFrame:CGRectZero];
                        lblMsg.tag = i;
                        lblMsg.backgroundColor = [UIColor clearColor];
                        [lblMsg setFont:[UIFont fontWithName:@"Verdana" size:12]];
                        [lblMsg setLineBreakMode:UILineBreakModeWordWrap];
                        lblMsg.text=[self timeFormateChangeAmPm:dicData[@"time"]];
                        
                        
                        if([dicData[@"status"] isEqualToString:@"1"])
                        {
                            //  lblMsg.textColor = [UIColor colorWithRed:(215/255.f) green:(29/255.f) blue:(32/255.f) alpha:.5];
                            lblMsg.textColor = [UIColor blueColor];
                            
                            UITapGestureRecognizer *lblAction = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(lblClickFromCell3:)];
                            lblAction.numberOfTapsRequired = 1;
                            lblMsg.userInteractionEnabled = YES;
                            [lblMsg addGestureRecognizer:lblAction];
                            
                        }
                        else
                        {
                           // lblMsg.textColor = [UIColor colorWithRed:(29/255.f) green:(29/255.f) blue:(38/255.f) alpha:1.0];
                            lblMsg.textColor = [UIColor blackColor];

                            
                        }
                        
                        
                        lblMsg.frame = CGRectMake(x,y, 66, 18);
                        x=x+65;
                        int k=i+1;
                        if(k%4==0)
                        {
                            x=53;
                            y=y+35;
                        }
                        
                        
                     
                        
                        [cell3 addSubview:lblMsg];
                    }
                }
                
            }
            
            //  y=y+35;
            height_Cell3=y+2;
            
            
            UIView *paintView=[[UIView alloc]initWithFrame:CGRectMake(10, y, 300, 1)];
            [paintView setBackgroundColor:[UIColor lightGrayColor]];
            [cell3 addSubview:paintView];
        }
        
        
        return cell3;
        
    }
    else if (indexPath.row==3)
    {
       // cell = [tableView dequeueReusableCellWithIdentifier:@"PatientClinicInfo_Cell4" forIndexPath:indexPath];
        
        cell4 = [tableView dequeueReusableCellWithIdentifier:@"PatientClinicInfo_Cell4" forIndexPath:indexPath];
        cell4.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
        if(clinicViewArray.count>0)
        {
            int countdata=0;

            PatientClinicModel *objData= [clinicViewArray objectAtIndex:indexArrayId];
            NSMutableArray *arrayData=objData.doctor_avail_timesArray;
            int x=53,y=51;
            
            for (int i=0; i< arrayData.count; i++) {
                NSDictionary *dicData=[arrayData objectAtIndex:i];
                NSString *time1 = dicData[@"time"];
                NSString *time2 = @"20:59:59";
                
                
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"HH:mm:ss"];
                
                NSDate *date1= [formatter dateFromString:time1];
                NSDate *date2 = [formatter dateFromString:time2];
                
                
                NSComparisonResult result1 = [date1 compare:date2];
                if(result1 == NSOrderedDescending)
                {
                    
                    NSLog(@"date2 is later than date1");
                    UILabel *lblMsg = [[UILabel alloc] initWithFrame:CGRectZero];
                    lblMsg.tag = i;
                    lblMsg.backgroundColor = [UIColor clearColor];
                    [lblMsg setFont:[UIFont fontWithName:@"Verdana" size:12]];
                    [lblMsg setLineBreakMode:UILineBreakModeWordWrap];
                    lblMsg.text=[self timeFormateChangeAmPm:dicData[@"time"]];
                    
                    
                    if([dicData[@"status"] isEqualToString:@"1"])
                    {
                        // lblMsg.textColor = [UIColor colorWithRed:(215/255.f) green:(29/255.f) blue:(32/255.f) alpha:.5];
                        lblMsg.textColor = [UIColor blueColor];
                        
                        UITapGestureRecognizer *lblAction = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(lblClickFromCell4:)];
                        lblAction.numberOfTapsRequired = 1;
                        lblMsg.userInteractionEnabled = YES;
                        [lblMsg addGestureRecognizer:lblAction];
                        
                    }
                    else
                    {
                       // lblMsg.textColor = [UIColor colorWithRed:(29/255.f) green:(29/255.f) blue:(38/255.f) alpha:1.0];
                        lblMsg.textColor = [UIColor blackColor];

                        
                    }
                    
                    
                    lblMsg.frame = CGRectMake(x,y, 66, 18);
                    x=x+65;
                    int k=i+1;
                    if(k % 4==0)
                    {
                        x=53;
                        y=y+35;
                    }
                    countdata++;
             
                    [cell4 addSubview:lblMsg];
                }
                
            }
            if(countdata<5)
            {
                
                height_Cell4=y+2+35;
                
                UIView *paintView=[[UIView alloc]initWithFrame:CGRectMake(10, y+35, 300, 1)];
                [paintView setBackgroundColor:[UIColor lightGrayColor]];
                [cell4 addSubview:paintView];
            }
            else
            {
                height_Cell4=y+2;
                
                UIView *paintView=[[UIView alloc]initWithFrame:CGRectMake(10, y, 300, 1)];
                [paintView setBackgroundColor:[UIColor lightGrayColor]];
                [cell4 addSubview:paintView];
            }
//            height_Cell4=y+2;
//            
//            UIView *paintView=[[UIView alloc]initWithFrame:CGRectMake(10, y, 300, 1)];
//            [paintView setBackgroundColor:[UIColor lightGrayColor]];
//            [cell4 addSubview:paintView];
            
            
        }
        
        return cell4;
        
    }
    
    
   
    return nil;
}

- (void)lblClickFromCell1:(UITapGestureRecognizer *)tapGesture {
    UILabel *label = (UILabel *)tapGesture.view;
    int index=(int)label.tag;
    if(_gDataManager.fromAppointment)
    {
        [self resheduleAPI:index];
    }
    else
    {

        PatientConfirmAppoint_VC* destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PatientConfirmAppoint_VC_id"];
        destVC.clinicFeeData=_clinicData;
        
        
        PatientClinicModel *objData= [clinicViewArray objectAtIndex:indexArrayId];
        NSMutableArray *arrayData=objData.doctor_avail_timesArray;
        
        NSDictionary *dicData=[arrayData objectAtIndex:index];

        
        destVC.str_dat_id=dicData[@"dat_id"];
        destVC.doctorProfileData=_doctorProfileData;

      
        [self.navigationController pushViewController:destVC animated:YES];
    }
    NSLog(@"Lable tag is ::%ld",(long)label.tag);
}
- (void)lblClickFromCell2:(UITapGestureRecognizer *)tapGesture {
    UILabel *label = (UILabel *)tapGesture.view;
    int index=(int)label.tag;
    if(_gDataManager.fromAppointment)
    {
        [self resheduleAPI:index];

    }
    else
    {

        PatientConfirmAppoint_VC* destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PatientConfirmAppoint_VC_id"];
        destVC.clinicFeeData=_clinicData;
        
        PatientClinicModel *objData= [clinicViewArray objectAtIndex:indexArrayId];
        NSMutableArray *arrayData=objData.doctor_avail_timesArray;
        
        NSDictionary *dicData=[arrayData objectAtIndex:index];
        
        
        destVC.str_dat_id=dicData[@"dat_id"];
        destVC.doctorProfileData=_doctorProfileData;
       

        [self.navigationController pushViewController:destVC animated:YES];
    }
    NSLog(@"Lable tag is ::%ld",(long)label.tag);
}
- (void)lblClickFromCell3:(UITapGestureRecognizer *)tapGesture {
    UILabel *label = (UILabel *)tapGesture.view;
    int index=(int)label.tag;
    
    if(_gDataManager.fromAppointment)
    {
        [self resheduleAPI:index];

    }
    else
    {

        PatientConfirmAppoint_VC* destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PatientConfirmAppoint_VC_id"];
        destVC.clinicFeeData=_clinicData;
        
        PatientClinicModel *objData= [clinicViewArray objectAtIndex:indexArrayId];
        NSMutableArray *arrayData=objData.doctor_avail_timesArray;
        
        NSDictionary *dicData=[arrayData objectAtIndex:index];
        
        
        destVC.str_dat_id=dicData[@"dat_id"];
        destVC.doctorProfileData=_doctorProfileData;
        
        [self.navigationController pushViewController:destVC animated:YES];
    }
    NSLog(@"Lable tag is ::%ld",(long)label.tag);
}
- (void)lblClickFromCell4:(UITapGestureRecognizer *)tapGesture {
    UILabel *label = (UILabel *)tapGesture.view;
    int index=(int)label.tag;
    if(_gDataManager.fromAppointment)
    {
       [self resheduleAPI:index];

    }
    else
    {
        PatientConfirmAppoint_VC* destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PatientConfirmAppoint_VC_id"];
        destVC.clinicFeeData=_clinicData;
        destVC.doctorProfileData=_doctorProfileData;
        
        PatientClinicModel *objData= [clinicViewArray objectAtIndex:indexArrayId];
        NSMutableArray *arrayData=objData.doctor_avail_timesArray;
        
        NSDictionary *dicData=[arrayData objectAtIndex:index];
        
        
        destVC.str_dat_id=dicData[@"dat_id"];
        
        [self.navigationController pushViewController:destVC animated:YES];
    }
    NSLog(@"Lable tag is ::%ld",(long)label.tag);
}

-(void)labelTap{
    PatientConfirmAppoint_VC* destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PatientConfirmAppoint_VC_id"];
    
    [self.navigationController pushViewController:destVC animated:YES];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView beginUpdates]; // tell the table you're about to start making changes
    
    if ([indexPath compare:self.expandedIndexPath] == NSOrderedSame)
    {
        self.expandedIndexPath = nil;
    }
    else
    {
        self.expandedIndexPath = indexPath;
    }
    
    [tableView endUpdates];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([indexPath compare:self.expandedIndexPath] == NSOrderedSame)
    {
        if(indexPath.row==0)
        {
            return height_Cell1;
        }
        if(indexPath.row==1)
        {
            return height_Cell2;
        }
        if(indexPath.row==2)
        {
            return height_Cell3;
        }
        if(indexPath.row==3)
        {
            return height_Cell4;
        }
        return 130; // Expanded height
    }
    return 50; // Normal height
}


-(void)resheduleAPI:(int)index{
    NSString *postData;
    
    PatientClinicModel *objData= [clinicViewArray objectAtIndex:indexArrayId];
    NSMutableArray *arrayData=objData.doctor_avail_timesArray;
    
    NSDictionary *dicData=[arrayData objectAtIndex:index];
    
    
    
    postData=[NSString stringWithFormat:@"da_id=%@&dat_id=%@&bookinid=%@",dicData[@"da_id"],dicData[@"dat_id"],_patientAppointmentData.booking_id];
    
    
    NSLog(@"resedule data=%@",postData);
    
    [MyLoader showLoadingView];
    [DataParser globalHitAPI:patientResuduleAppointmentAPI detaiInfi:postData WithCompletionHandler:^(NSDictionary *dict, BOOL isSuccessful)
     {
         if (isSuccessful)
         {
             [MyLoader hideLoadingView];
             
             if([dict[@"status"] isEqualToString:@"false"])
             {
                 
                 [Utility alertWithTitle:kAlertTitle withMessage:dict[@"message"] onVC:self];
             }
             else
             {
                
                [self.navigationController popViewControllerAnimated:YES];
             }
         }
         else
         {
             [MyLoader hideLoadingView];
             [Utility alertWithTitle:@"" withMessage:kNetworkErrorTitle onVC:self];
         }
     }];
    
}
-(void)loadData{
    
    dateGloble = [NSDate date];
    df = [[NSDateFormatter alloc] init];
    gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    components = [[NSDateComponents alloc] init];
    
    [df setDateFormat:@"EE, dd MMM"];
    _lbl_Date.text=[[df stringFromDate:dateGloble] uppercaseString];
}

- (IBAction)touch_Right:(id)sender {
    
    if(clinicViewArray.count>0)
    {
        
        int valur=(int)clinicViewArray.count;
        
        if(valur > indexArrayId)
        {
            indexArrayId++;
            
            if(valur > indexArrayId)
            {
                PatientClinicModel *objData= [clinicViewArray objectAtIndex:indexArrayId];
                _lbl_Date.text=[self dateFomateString:objData.availabledate];
                [tbl_View reloadData];
                
            }
        }
        else
        {
            [Utility alertWithTitle:@"" withMessage:@"No right date available" onVC:self];
            
        }
    }
    
}

- (IBAction)touchLeft:(id)sender {
    
    
    if(clinicViewArray.count>0)
    {
        if(indexArrayId>0)
        {
            indexArrayId--;
            
            PatientClinicModel *objData= [clinicViewArray objectAtIndex:indexArrayId];
            _lbl_Date.text=[self dateFomateString:objData.availabledate];
            [tbl_View reloadData];
        }
        else
        {
            [Utility alertWithTitle:@"" withMessage:@"No less date available" onVC:self];
            
        }
    }

    
}

- (IBAction) backButtonAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];

 
}
-(void)leftButton{
    UIButton *button =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(backButtonAction:)forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0, 20, 20)];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = barButton;
    self.navigationItem.hidesBackButton=TRUE;
    
}
-(NSString *)dateFomateString:(NSString *)dataString
{
    NSArray* foo = [dataString componentsSeparatedByString: @"T"];
    NSString *str = [foo objectAtIndex: 0];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormatter dateFromString: str];
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM dd, YYYY"];
    return [dateFormatter stringFromDate:date];
    
}
-(NSString *)timeFormateChangeAmPm:(NSString *)dateData
{
    NSDateFormatter *dateFormatter3 = [[NSDateFormatter alloc] init];
    [dateFormatter3 setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter3 setDateFormat:@"HH:mm:ss"];
    NSDate *date1 = [dateFormatter3 dateFromString:dateData];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"hh:mm a"];
    return [formatter stringFromDate:date1];
}
- (void) downloadImage:(NSString *)imgURL1
{
    NSLog(@"Image URL in method=%@",imgURL1);
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       image_manager = [SDWebImageManager sharedManager];
                       [image_manager downloadWithURL:[NSURL URLWithString:imgURL1] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize, NSInteger feedID, NSString *feedString)
                        {
                        } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished)
                        {
                            if (image)
                            {
                                _img_profile.image = image;
                            }
                        }];
                   });
    
}

@end
