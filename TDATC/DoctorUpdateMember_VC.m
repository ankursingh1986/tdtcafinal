//
//  DoctorUpdateMember_VC.m
//  TDATC
//
//  Created by iWeb on 9/19/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "DoctorUpdateMember_VC.h"
#import "DataManager.h"
#import "Utility.h"
#import "MyLoader.h"
#import "DataParser.h"
#import "Defines.h"

@interface DoctorUpdateMember_VC ()

@end

@implementation DoctorUpdateMember_VC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self leftButton];
    
    [self.view_uper.layer setCornerRadius:20.0f];
    
    // border
    [self.view_uper.layer setBorderColor:[UIColor whiteColor].CGColor];
    [self.view_uper.layer setBorderWidth:2.5f];
    
    // drop shadow
    [self.view_uper.layer setShadowColor:[UIColor colorWithRed:27/255.0 green:74/255.0 blue:245/255.0 alpha:1.0].CGColor];
    [self.view_uper.layer setShadowOpacity:0.4];
    [self.view_uper.layer setShadowRadius:3.0];
    self.view_uper.layer.masksToBounds = YES;
    //[self.view_head.layer setShadowOffset:CGSizeMake(2.0, 2.0)];

    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationItem.title = @"UPDATE PLAN";
    
    [super viewWillAppear:animated];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)leftButton{
    UIButton *button =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(backButtonAction:)forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0, 20, 20)];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = barButton;
    
}
- (IBAction) backButtonAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
}
@end
