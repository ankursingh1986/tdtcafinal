//
//  PickerModel.m
//  TDATC
//
//  Created by iWeb on 10/26/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "PickerModel.h"

@implementation PickerModel

- (id) initWithCountriesDictionary:(NSDictionary*)dictionary
{
    self = [super init];
    if (self)
    {
        self.dataId = [NSString stringWithFormat:@"%@",dictionary[@"country_id"]];

        self.dataCode = dictionary[@"country_code"];
        self.dataName = dictionary[@"country_name"];
    }
    return self;
}

- (id) initWithStatesDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if (self)
    {
        self.dataId = [NSString stringWithFormat:@"%@",dictionary[@"state_id"]];
        self.dataName = dictionary[@"state_name"];
    }
    return self;
}

- (id) initWithCitiesDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if (self)
    {
        self.dataId = [NSString stringWithFormat:@"%@",dictionary[@"city_id"]];

        self.dataName = dictionary[@"city_name"];
    }
    return self;
}

- (id) initWithMemberShipPlans:(NSDictionary*)dictionary
{
    self = [super init];
    if (self)
    {
        self.dataId = [NSString stringWithFormat:@"%@",dictionary[@"plan_id"]];
        self.dataName = dictionary[@"membership"];
        self.price_monthly=dictionary[@"price_monthly"];
        self.price_quaterly=dictionary[@"price_quaterly"];
        self.price_halfyearly=dictionary[@"price_halfyearly"];
        self.price_yearly=dictionary[@"price_yearly"];

    }
    return self;
}

- (id) initWithBloodGroupsDictionary:(NSDictionary*)dictionary;
{
    self = [super init];
    if (self)
    {
        self.dataId = dictionary[@"b_id"];
        self.dataName = dictionary[@"b_name"];
    }
    return self;
}

- (id) initWithServiceDictionary:(NSDictionary*)dictionary
{
    self = [super init];
    if (self)
    {
        self.dataId = dictionary[@"service_id"];
        self.dataName = dictionary[@"service_name"];
    }
    return self;
}

- (id) initWithClinicDictionary:(NSDictionary*)dictionary
{
    self = [super init];
    if (self)
    {
        self.dataId = dictionary[@"clinic_id"];
        self.dataName = dictionary[@"clicnic_name"];
    }
    return self;
}


@end
