//
//  PatientMessageModel.h
//  TDATC
//
//  Created by Vinay on 07/11/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PatientMessageModel : NSObject


@property (nonatomic, strong) NSString* subject;
@property (nonatomic, strong) NSString* date;
@property (nonatomic, strong) NSString* msg_id;
@property (nonatomic, strong) NSString* doctor_msg;
@property (nonatomic, strong) NSString* doctorimage_path;
@property (nonatomic, strong) NSString* patientimag_path;

@property(nonatomic, strong) NSString* attchment;



- (id) initWithDictionary:(NSDictionary*)dictionary;

@end
