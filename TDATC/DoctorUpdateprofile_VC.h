//
//  DoctorUpdateprofile_VC.h
//  TDATC
//
//  Created by iWeb on 9/19/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDWebImageManager.h"
#import "Global.pch"


@interface DoctorUpdateprofile_VC : UIViewController<UIGestureRecognizerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    SDWebImageManager *image_manager;

}
@property (weak, nonatomic) IBOutlet UIImageView *img_profile;
@property (weak, nonatomic) IBOutlet UIButton *btn_edit;
- (IBAction)touch_edit:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txt_email;
@property (weak, nonatomic) IBOutlet UITextField *txt_mobile;
@property (weak, nonatomic) IBOutlet UITextField *txt_fullname;
@property (weak, nonatomic) IBOutlet UIButton *btn_male;
- (IBAction)btn_MaleTouch:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btn_female;
- (IBAction)touch_femail:(id)sender;
- (IBAction)touch_update:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txt_DOB;
@property (weak, nonatomic) IBOutlet UITextField *txt_india;
@property (weak, nonatomic) IBOutlet UIImageView *img_india;
@property (weak, nonatomic) IBOutlet UIImageView *img_state;
@property (weak, nonatomic) IBOutlet UIImageView *img_city;
@property (weak, nonatomic) IBOutlet UITextField *txt_state;
@property (weak, nonatomic) IBOutlet UITextField *txt_city;
@property (weak, nonatomic) IBOutlet UITextField *txt_locality;
@property (weak, nonatomic) IBOutlet UITextField *txt_pin;
@property (weak, nonatomic) IBOutlet UIButton *btn_update;

@end
