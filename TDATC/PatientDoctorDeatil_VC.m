//
//  PatientDoctorDeatil_VC.m
//  TDATC
//
//  Created by iWeb on 9/24/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "PatientDoctorDeatil_VC.h"
#import "PatientDoctorDeatil_TVC1.h"
#import "PatientDoctorDeatil_TVC2.h"
#import "PatientClinicInfo_VC.h"
#import "PatientFilter_VC.h"
#import "DataParser.h"
#import "PatientDataParser.h"
#import "Utility.h"
#import "DataManager.h"
#import "Defines.h"
#import "MyLoader.h"
#import "ClinicTableViewCell.h"
#import "ClinicInfoModel.h"

@interface PatientDoctorDeatil_VC ()
{
    NSMutableArray* _clinicArray;
}

@end

@implementation PatientDoctorDeatil_VC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self leftButton];
    [self rightButton];
    [tbl_doctor_detail setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    self->tbl_doctor_detail.tableFooterView = [[UIView alloc] init];
    
    [self getClinicList];
    _clinicArray = [[NSMutableArray alloc] init];



    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated
{
    [tbl_doctor_detail reloadData];

    [super viewWillAppear:animated];
    self.navigationItem.title = @"DOCTOR DETAILS";
  //
}

- (void) getClinicList
{
    [MyLoader showLoadingView];
    [PatientDataParser patientClinicInfo:[NSString stringWithFormat:@"doctor_id=%@",self.doctorData.user_id] WithCompletionHandler:^(NSArray *array, BOOL isSuccessful)
    {
        [MyLoader hideLoadingView];
        if (isSuccessful)
        {
            _clinicArray = [array mutableCopy];
            _gDataManager.clinicArray = [array mutableCopy];
            [tbl_doctor_detail reloadData];
            
        }
        else
        {
            [Utility alertWithTitle:kAlertTitle withMessage:kNetworkErrorTitle onVC:self];
        }
        
    }];
}

#pragma mark - UITableView DataSource and delegates

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section==0)
    {
        return 1;
    }
    if(section==1)
    {
        return 2;
    }
    if(section==2)
    {
        return _gDataManager.clinicArray.count;
    }
    return 4 ;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if (indexPath.section==1)
    {
        return 65;
    }
    else if (indexPath.section==2)
    {
        return 90;
    }
    return 140;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section==0)
    {
        PatientDoctorDeatil_TVC1 *cell;
           cell = [tableView dequeueReusableCellWithIdentifier:@"PatientDoctorDeatil_Cell1" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.view_star.selectedStar = [UIImage imageNamed:@"redstar"];
        cell.view_star.canEdit = NO;
        cell.view_star.maxRating = 5;
        cell.view_star.rating = [self.doctorData.rating intValue];
        [self downloadImage:cell imageUrl1:[NSString stringWithFormat:@"%@%@",kDoctorImagePath,self.doctorData.image]];
        
        cell.nameLabel.text = [self.doctorData.full_name uppercaseString];
        cell.addressLabel.text = [NSString stringWithFormat:@"%@, %@, %@, %@",self.doctorData.course,self.doctorData.institute, self.doctorData.city, self.doctorData.year];;
        
        
        cell.localityLabel.text = self.doctorData.locality;

        
        return cell;
    
    }
    else if (indexPath.section==1)
    {
        PatientDoctorDeatil_TVC2 *cell;
        if(indexPath.row==0)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"PatientDoctorDeatil_Cell2" forIndexPath:indexPath];
            cell.exprienceLabel.text=[NSString stringWithFormat:@"%@ Yrs",self.doctorData.exp];
            
        }
        else if (indexPath.row==1)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"PatientDoctorDeatil_Cell3" forIndexPath:indexPath];
            cell.awardLabel.text=self.doctorData.award;
            cell.yearLabel.text=self.doctorData.awardYear;
            
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
    else
    {
        ClinicTableViewCell *cell;
       
       
            cell = [tableView dequeueReusableCellWithIdentifier:@"PatientDoctorDeatil_Cell4" forIndexPath:indexPath];
        cell.btn_View.tag=indexPath.row;

        
       [cell.btn_View addTarget:self action:@selector(viewBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        ClinicInfoModel* data = [_gDataManager.clinicArray objectAtIndex:indexPath.row];
        
        if (data.status)
        {
            [cell setUpData:data];
        }
        
        
        return cell;
    }
}
- (void) viewBtnAction:(id)sender
{
    UIButton *button1=(UIButton *)sender;
    NSInteger tag = [button1 tag];
    PatientClinicInfo_VC* destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PatientClinicInfo_VC_id"];
    destVC.clinicData=[_clinicArray objectAtIndex:tag];
    destVC.doctorProfileData=self.doctorData;
    
    _gDataManager.fromAppointment=NO;
    
    [self.navigationController pushViewController:destVC animated:YES];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)rightButton{
    button =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"filter"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(filtorButtonAction:)forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0, 20, 20)];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.rightBarButtonItem = barButton;
}
- (IBAction) filtorButtonAction:(id)sender {

    PatientFilter_VC* destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PatientFilter_VC_id"];
    destVC.clinicArray=_clinicArray;
    
    
    
    [self.navigationController pushViewController:destVC animated:YES];

}

- (IBAction) backButtonAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];

//    UIStoryboard *storyBoard12 = [UIStoryboard storyboardWithName:@"Patient" bundle:nil];
//    
//    UIViewController *vc = [storyBoard12 instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
//    [self presentViewController:vc animated:NO completion:nil];
//    
    
}
-(void)leftButton{
    UIButton *button2 =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button2 setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [button2 addTarget:self action:@selector(backButtonAction:)forControlEvents:UIControlEventTouchUpInside];
    [button2 setFrame:CGRectMake(0, 0, 20, 20)];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button2];
    self.navigationItem.leftBarButtonItem = barButton;
    self.navigationItem.hidesBackButton=TRUE;
    
}

- (void) downloadImage:(PatientDoctorDeatil_TVC1*)cell imageUrl1:(NSString *)imgURL1
{
    NSLog(@"Image URL in method=%@",imgURL1);
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       image_manager = [SDWebImageManager sharedManager];
                       [image_manager downloadWithURL:[NSURL URLWithString:imgURL1] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize, NSInteger feedID, NSString *feedString)
                        {
                        } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished)
                        {
                            if (image)
                            {
                                cell.docrorImageView.image = image;
                            }
                        }];
                   });
    
}

@end
