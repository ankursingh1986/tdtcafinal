//
//  ClinicTableViewCell.m
//  TDATC
//
//  Created by Vinay on 03/11/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "ClinicTableViewCell.h"

@implementation ClinicTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setUpData:(ClinicInfoModel*)data
{
    self.clinicNameLabel.text = [data.clicnic_name uppercaseString];
    self.addressLabel.text = [NSString stringWithFormat:@"%@ %@",data.locality,data.pincode];
}

@end
