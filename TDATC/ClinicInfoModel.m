//
//  ClinicInfoModel.m
//  TDATC
//
//  Created by Vinay on 02/11/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "ClinicInfoModel.h"

@implementation ClinicInfoModel

- (id) initWithDictionary:(NSDictionary*)dictionary
{
    self = [super init];
    if (self)
    {
        self.clinic_id = dictionary[@"clinic_id"];
        
        if (![dictionary[@"clicnic_name"] isKindOfClass:[NSNull class]])
            self.clicnic_name = dictionary[@"clicnic_name"];
        else
            self.clicnic_name = @"";
        
        
        if (![dictionary[@"locality"] isKindOfClass:[NSNull class]])
            self.locality = dictionary[@"locality"];
        else
            self.locality = @"";
        
        if (![dictionary[@"pincode"] isKindOfClass:[NSNull class]])
            self.pincode = dictionary[@"pincode"];
        else
            self.pincode = @"";
        
        if (![dictionary[@"fees"] isKindOfClass:[NSNull class]])
            self.fees = [NSString stringWithFormat:@"%@",dictionary[@"fee"]];
        else
            self.fees = @"";
        self.status = YES;
    
        self.avaliabeDayArray = dictionary[@"day"];
       
    }
    return self;
}

@end
