//
//  PatientFeedback_VC.m
//  TDATC
//
//  Created by iWeb on 9/23/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "PatientFeedback_VC.h"
#import "PatientFeedback_TVC.h"
#import "PatientWriteReview_VC.h"
#import "PatientDataParser.h"
#import "Defines.h"
#import "DataManager.h"
#import "Utility.h"

@interface PatientFeedback_VC ()
{
    NSMutableArray* _feedbcakArray;
}

@end

@implementation PatientFeedback_VC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self leftButton];
    [tbl_FeedBack setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    self->tbl_FeedBack.tableFooterView = [[UIView alloc] init];


    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
     self.navigationItem.title = @"FEEDBACK HISTORY";
    
    _feedbcakArray = [[NSMutableArray alloc] init];
    
    [self getFeedbackForPatient];
}

- (void) getFeedbackForPatient
{
    NSString* postData = [NSString stringWithFormat:@"id=%@",_gDataManager.loginModel.user_id];
    [PatientDataParser patientFeedback:postData WithCompletionHandler:^(NSArray *array, BOOL isSuccessful)
    {
        if (isSuccessful)
        {
            _feedbcakArray = [array mutableCopy];
            [tbl_FeedBack reloadData];
        }
        else
        {
            [Utility alertWithTitle:kAlertTitle withMessage:kNetworkErrorTitle onVC:self];
        }
    }];
}


#pragma mark - table view delegates and data socurce


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _feedbcakArray.count;
}
//

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    PatientFeedback_TVC *cell = [tableView dequeueReusableCellWithIdentifier:@"PatientFeedback_Cell" forIndexPath:indexPath];
    
    
    cell.view_star.canEdit = NO;
    cell.view_star.maxRating = 5;
    
    
    
    
    PatientFeedbackModel* data = [_feedbcakArray objectAtIndex:indexPath.row];
    
    cell.view_star.rating = [data.raiting_id intValue];
    
    [cell setupData:data];
    NSString *imagePath=[NSString stringWithFormat:@"%@",data.image_path];
    if([imagePath isEqualToString:@"(null)(null)"])
    {
        
    }
    else
    {
        [self downloadImage:cell imageUrl1:imagePath];
        
    }
    
    // cell.img_home.image=[UIImage imageNamed:[arrayImage objectAtIndex:indexPath.row]];
    // cell.lbl_titile.text=[arrayTitle objectAtIndex:indexPath.row];
    // cell.lbl_detail.text=[arrayDesc objectAtIndex:indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)leftButton
{
    UIButton *button11 =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button11 setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [button11 addTarget:self action:@selector(backButtonAction:)forControlEvents:UIControlEventTouchUpInside];
    [button11 setFrame:CGRectMake(0, 0, 20, 20)];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button11];
    self.navigationItem.leftBarButtonItem = barButton;
    
}
- (IBAction) backButtonAction:(id)sender
{
    UIStoryboard *storyBoard12 = [UIStoryboard storyboardWithName:@"Patient" bundle:nil];
    
    UIViewController *vc = [storyBoard12 instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
    [self presentViewController:vc animated:NO completion:nil];
    
}
- (void) downloadImage:(PatientFeedback_TVC*)cell imageUrl1:(NSString *)imgURL1
{
    NSLog(@"Image URL in method=%@",imgURL1);
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       image_manager = [SDWebImageManager sharedManager];
                       [image_manager downloadWithURL:[NSURL URLWithString:imgURL1] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize, NSInteger feedID, NSString *feedString)
                        {
                        } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished)
                        {
                            if (image)
                            {
                                cell.profileImageView.image = image;
                            }
                        }];
                   });
    
}




@end
