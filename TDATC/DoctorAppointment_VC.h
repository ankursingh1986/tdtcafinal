//
//  DoctorAppointment_VC.h
//  TDATC
//
//  Created by iWeb on 9/18/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDWebImageManager.h"
#import "Global.pch"
#import <FSCalendar.h>


@interface DoctorAppointment_VC : UIViewController <UITableViewDelegate, UITableViewDataSource,FSCalendarDelegate,FSCalendarDataSource, UIGestureRecognizerDelegate>
{
    IBOutlet UITableView *tbl_Appointment;
    SDWebImageManager *image_manager;
     UIButton *titleLabelButton;
    //UIButton *titleLabelButton;


}


@property (weak, nonatomic) IBOutlet FSCalendar *calender1;
@property (weak, nonatomic) IBOutlet UIView *view_1;
@property (weak, nonatomic) IBOutlet UILabel *lbl_request;
@property (weak, nonatomic) IBOutlet UILabel *lbl_cancel;

@end
