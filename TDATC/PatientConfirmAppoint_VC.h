//
//  PatientConfirmAppoint_VC.h
//  TDATC
//
//  Created by iWeb on 9/23/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Global.pch"
#import "PayPalMobile.h"


@interface PatientConfirmAppoint_VC : UIViewController
{
    SDWebImageManager *image_manager;
    PayPalConfiguration *payPalConfig;


}

@property (nonatomic, strong) ClinicInfoModel* clinicFeeData;
@property (nonatomic, strong) NSString* str_dat_id;
@property (nonatomic, strong) FindDoctorModel* doctorProfileData;





@property (weak, nonatomic) IBOutlet UITextField *txt_name;
@property (weak, nonatomic) IBOutlet UITextField *txt_email;
@property (weak, nonatomic) IBOutlet UITextField *txt_mobile;
@property (weak, nonatomic) IBOutlet UITextField *txt_sick;
@property (weak, nonatomic) IBOutlet UITextField *txt_mode;
@property (weak, nonatomic) IBOutlet UITextField *txt_fees;

- (IBAction)touch_Confirm:(id)sender;




@property (weak, nonatomic) IBOutlet UITextField *txt_paymentMode;



// paypal start
- (void)setPayPalEnvironment:(NSString *)environment;

@property (strong, nonatomic) IBOutlet UIView *headerView;
@property(nonatomic, strong, readwrite) NSString *environment;
@property(nonatomic, assign, readwrite) BOOL acceptCreditCards;
@property (strong, nonatomic) NSString *productID;
@property(nonatomic, strong, readwrite) NSString *resultText;
@property(nonatomic, strong, readwrite) IBOutlet UIView *successView;


@end
