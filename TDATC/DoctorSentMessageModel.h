//
//  DoctorSentMessageModel.h
//  TDATC
//
//  Created by Vinay on 18/10/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DoctorSentMessageModel : NSObject


@property(nonatomic, strong) NSString* doctor_msg;
@property(nonatomic, strong) NSString* date;
@property(nonatomic, strong) NSString* subject;
@property(nonatomic, strong) NSString* msg_id;
@property(nonatomic, strong) NSString* doctor_full_name;

@property(nonatomic, strong) NSString* patientID;
@property(nonatomic, strong) NSString* attchment;



@property(nonatomic, strong) NSString* patient_full_name;
@property(nonatomic, strong) NSString* comments;

- (id) initWithDictionary:(NSDictionary*)dictionary;

@end
