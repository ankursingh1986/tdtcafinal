//
//  SignupViewController.h
//  TDATC
//
//  Created by iWeb on 9/15/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignupViewController : UIViewController
{
    IBOutlet UIButton *btn_Doctor,*btn_Patient;
    IBOutlet UITextField *txt_fullname,*txt_Email,*txt_passowrd;
    int check;
}
@property (weak, nonatomic) IBOutlet UIButton *btn_Check;

@property (weak, nonatomic) IBOutlet UILabel *lbl_term;
- (IBAction)touch_Doctor:(id)sender;
- (IBAction)touch_Patient:(id)sender;
- (IBAction)touch_Signup:(id)sender;
- (IBAction)touch_TermImage:(id)sender;

@end
