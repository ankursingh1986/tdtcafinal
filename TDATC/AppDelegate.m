//
//  AppDelegate.m
//  TDATC
//
//  Created by iWeb on 9/13/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "AppDelegate.h"
#import "PayPalMobile.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <Accounts/Accounts.h>
#import <Twitter/Twitter.h>
#import "LoginViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    
   // [PayPalMobile initializeWithClientIdsForEnvironments:@{PayPalEnvironmentSandbox : @"AaPcHhDm7D1b0UqEXrG27j9-Ag6ySlWI8tp6QB6hjFpf0shnpZltRzQfuNB3"}];
    
    [PayPalMobile initializeWithClientIdsForEnvironments:@{PayPalEnvironmentProduction : @"AWN_zRBiIzjCU2zixBKPl6AQlUMAOGzPRNckRj2_0osdu3utgTU9cwrg5c6c"}];

    
    
    [[UINavigationBar appearance] setTranslucent:NO];
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:215.0f/255.0f green:29.0f/255.0f blue:32.0f/255.0f alpha:1.0]];
   
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
   
    [[UINavigationBar appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor whiteColor],
      UITextAttributeTextColor,
      [UIColor whiteColor],
      UITextAttributeTextShadowColor,
      [NSValue valueWithUIOffset:UIOffsetMake(0, 0)],
      UITextAttributeTextShadowOffset,
      [UIFont fontWithName:@"Montserrat-Bold" size:20.0],
      UITextAttributeFont,
      nil]];
    
    
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"KHome"])
    {
        NSString *doctorflag = [[NSUserDefaults standardUserDefaults] objectForKey:@"doctorlogin"];
        
        if([doctorflag isEqualToString:@"0"])
        {
           
            [[UINavigationBar appearance] setTranslucent:NO];
            
            [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:5.0f/255.0f green:52.0f/255.0f blue:222.0f/255.0f alpha:1.0]];
           
            
            self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Patient" bundle:nil];
            
            UINavigationController *initialNavigationController = (UINavigationController*)[storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
            
            self.window.rootViewController = initialNavigationController;
            [self.window makeKeyAndVisible];
       
        }
        else
        {
            
            [[UINavigationBar appearance] setTranslucent:NO];
            
            [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:215.0f/255.0f green:29.0f/255.0f blue:32.0f/255.0f alpha:1.0]];
            
            
            self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            
            UINavigationController *initialNavigationController = (UINavigationController*)[storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
            
            self.window.rootViewController = initialNavigationController;
            [self.window makeKeyAndVisible];
        }
    }
    
    [self restoreLastSessionIfExists];

    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}


#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"TDATC"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    
                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                    */
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    
    return _persistentContainer;
}


- (BOOL)application:(UIApplication* )app openURL:(NSURL* )url options:(NSDictionary<NSString *, id>* )options
{

        return [[FBSDKApplicationDelegate sharedInstance] application:app
                                                              openURL:url
                                                    sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                                           annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
   
}
#pragma mark - Twitter SDK
- (void)getTwitterAccountOnCompletion:(void (^)(ACAccount *))completionHandler {
    ACAccountStore *store = [[ACAccountStore alloc] init];
    ACAccountType *twitterType = [store accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    [store requestAccessToAccountsWithType:twitterType withCompletionHandler:^(BOOL granted, NSError *error) {
        if(granted) {
            // Remember that twitterType was instantiated above
            NSArray *twitterAccounts = [store accountsWithAccountType:twitterType];
            
            // If there are no accounts, we need to pop up an alert
            if(twitterAccounts == nil || [twitterAccounts count] == 0) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Twitter Accounts"
                                                                message:@"There are no Twitter accounts configured. You can add or create a Twitter account in Settings."
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            } else {
                //Get the first account in the array
                ACAccount *twitterAccount = [twitterAccounts objectAtIndex:0];
                //Save the used SocialAccountType so it can be retrieved the next time the app is started.
//                [[NSUserDefaults standardUserDefaults] setInteger:SocialAccountTypeTwitter forKey:kSocialAccountTypeKey];
                [[NSUserDefaults standardUserDefaults] synchronize];
                //Call the completion handler so the calling object can retrieve the twitter account.
                completionHandler(twitterAccount);
            }
        }
    }];
}
//#pragma mark - Other
- (void)restoreLastSessionIfExists {
      [self getTwitterAccountOnCompletion:^(ACAccount *twitterAccount){
            //If we successfully retrieved a Twitter account
            if(twitterAccount) {
                //Make sure anything UI related happens on the main queue
                dispatch_async(dispatch_get_main_queue(), ^{
                    
//                    LoginViewController *homeViewController = [[LoginViewController alloc] initWithSocialAccountType:SocialAccountTypeTwitter];
//                    [self.navigationController pushViewController:homeViewController animated:YES];
                });
            }
        }];
}

//#pragma mark - GMAIL login delegates
//
//- (BOOL)application:(UIApplication *)app
//            openURL:(NSURL *)url
//            options:(NSDictionary *)options {
//    return [[GIDSignIn sharedInstance] handleURL:url
//                               sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
//                                      annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
//}



#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}

@end
