//
//  WebServiceManager.m
//  NewsApp
//
//  Created by Vinay Kumar on 24/02/16.
//  Copyright © 2016 Vinay Kumar. All rights reserved.
//

#import "WebServiceManager.h"
#import "Defines.h"

#define ContentType @"application/x-www-form-urlencoded"

@implementation WebServiceManager

#pragma mark -GET JSON requests
+ (NSURLRequest*) requestWithServiceGET:(NSString*)service
{
    NSString* urlString=[kServerURL stringByAppendingString:service];

    return [WebServiceManager requestWithUrlStringGET:urlString];
}
+ (NSMutableURLRequest*) requestWithUrlStringGET:(NSString*)urlString
{
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"GET"];
    [request setTimeoutInterval:120];
    return request;
}

+ (NSURLRequest*) requestWithService:(NSString*)service
{
    NSString* urlString=[kServerURL stringByAppendingString:service];
    
    return [WebServiceManager requestWithUrlString:urlString];
}
+ (NSMutableURLRequest*) requestWithUrlString:(NSString*)urlString;
{
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
  //  [request addValue:ContentType forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"123456789" forHTTPHeaderField:@"X-Oc-Merchant-Id"];
    [request setHTTPMethod:@"GET"];
    [request setTimeoutInterval:120];
    return request;
}
// Post  request value

+ (NSURLRequest*) postRequestWithService:(NSString*)service withPayload:(NSString*)details
{
    NSString* urlString=[kServerURL stringByAppendingString:service];
    return [WebServiceManager postRequestWithUrlString:urlString withPayload:details];
}
+ (NSMutableURLRequest*) postRequestWithUrlString:(NSString*)urlString withPayload:(NSString*)details
{
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
  //  NSData *postData = [details dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    
    NSData *postData = [details dataUsingEncoding:NSUTF8StringEncoding];

    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    [request setHTTPMethod:@"POST"];
  //  [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];

   // [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setTimeoutInterval:120];
    [request setHTTPBody:postData];
    
   // [request setAllHTTPHeaderFields:headers];

    return request;
}



// For WishList
// For Post request
+ (NSURLRequest*) postRequestWithServiceForWishList:(NSString*)service withPayload:(NSDictionary*)dict
{
    NSString* urlString=[kServerURL stringByAppendingString:service];
    return [WebServiceManager postRequestWithUrlDicwithSessionForWishList:urlString withDictionary:dict];
}


+ (NSMutableURLRequest*) postRequestWithUrlDicwithSessionForWishList:(NSString*)urlString withDictionary:(NSDictionary*)dict
{
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    [request setValue:@"123456789" forHTTPHeaderField:@"X-Oc-Merchant-Id"];
    
    [request setValue:@"en-gb" forHTTPHeaderField:@"X-Oc-Merchant-Language"];
    NSUserDefaults *NonVolatile = [NSUserDefaults standardUserDefaults];
    NSString *sessionString = [NonVolatile stringForKey:@"SessionData"];
    
    [request setValue:sessionString forHTTPHeaderField:@"X-Oc-Session"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPMethod:@"POST"];
    [request setTimeoutInterval:60];
    NSData* postData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:nil];
    [request setHTTPBody:postData];
    //  [WebServiceManager addAuthTokenToRequest:request];
    
    return request;
}

// For Get Request
+ (NSURLRequest*) requestWithServiceAndWithSessionForWishList:(NSString*)service
{
    NSString* urlString=[kServerURL stringByAppendingString:service];
    return [WebServiceManager requestWithUrlStringAndWithSessionForWishList:urlString];
}

+ (NSMutableURLRequest*) requestWithUrlStringAndWithSessionForWishList:(NSString*)urlString
{
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    NSUserDefaults *NonVolatile = [NSUserDefaults standardUserDefaults];
    NSString *sessionString = [NonVolatile stringForKey:@"SessionData"];
    [request setValue:sessionString forHTTPHeaderField:@"X-Oc-Session"];
    
   
  //  [request setValue:_gDataManager.sessionString forHTTPHeaderField:@"X-Oc-Session"];
    [request setValue:@"en-gb" forHTTPHeaderField:@"X-Oc-Merchant-Language"];
    [request addValue:@"123456789" forHTTPHeaderField:@"X-Oc-Merchant-Id"];
    [request setHTTPMethod:@"GET"];
    [request setTimeoutInterval:120];
    return request;
}

// For Delete Request

+ (NSURLRequest*) deleteRequestWithServiceForWishList:(NSString*)service withDictionary:(NSDictionary*)dict
{
    NSString* urlString=[kServerURL stringByAppendingString:service];
    return [WebServiceManager deleteRequestWithUrlDicwithSessionForWishList:urlString withDictionary:dict];
}
+ (NSMutableURLRequest*) deleteRequestWithUrlDicwithSessionForWishList:(NSString*)urlString withDictionary:(NSDictionary*)dict
{
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    [request setValue:@"123456789" forHTTPHeaderField:@"X-Oc-Merchant-Id"];
    
    
    NSUserDefaults *NonVolatile = [NSUserDefaults standardUserDefaults];
    NSString *sessionString = [NonVolatile stringForKey:@"SessionData"];
    
    [request setValue:@"en-gb" forHTTPHeaderField:@"X-Oc-Merchant-Language"];
    [request setValue:sessionString forHTTPHeaderField:@"X-Oc-Session"];
  //  [request setValue:_gDataManager.sessionString forHTTPHeaderField:@"X-Oc-Session"];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPMethod:@"DELETE"];
    [request setTimeoutInterval:60];
    NSData* postData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:nil];
    [request setHTTPBody:postData];
    //  [WebServiceManager addAuthTokenToRequest:request];
    
    return request;
}
// Vinay For Fashion
+ (NSURLRequest*) requestWithServiceAndWithSessionPOST:(NSString*)service
{
    NSString* urlString=[kServerURL stringByAppendingString:service];
    return [WebServiceManager requestWithUrlStringAndWithSessionPOST:urlString];
}

+ (NSMutableURLRequest*) requestWithUrlStringAndWithSessionPOST:(NSString*)urlString
{
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    NSUserDefaults *NonVolatile = [NSUserDefaults standardUserDefaults];
    NSString *sessionString = [NonVolatile stringForKey:@"SessionData"];
    [request setValue:sessionString forHTTPHeaderField:@"X-Oc-Session"];
    
  
 //   [request setValue:_gDataManager.sessionString forHTTPHeaderField:@"X-Oc-Session"];
    [request addValue:@"123456789" forHTTPHeaderField:@"X-Oc-Merchant-Id"];
    [request setHTTPMethod:@"POST"];
    [request setTimeoutInterval:120];
    return request;
}

// Vinay For Fashion
+ (NSURLRequest*) requestWithServiceAndWithSession:(NSString*)service
{
    NSString* urlString=[kServerURL stringByAppendingString:service];
    return [WebServiceManager requestWithUrlStringAndWithSession:urlString];
}

+ (NSMutableURLRequest*) requestWithUrlStringAndWithSession:(NSString*)urlString;
{
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    NSUserDefaults *NonVolatile = [NSUserDefaults standardUserDefaults];
    NSString *sessionString = [NonVolatile stringForKey:@"SessionData"];
    [request setValue:sessionString forHTTPHeaderField:@"X-Oc-Session"];
  //  [request setValue:_gDataManager.sessionString forHTTPHeaderField:@"X-Oc-Session"];

    [request addValue:@"123456789" forHTTPHeaderField:@"X-Oc-Merchant-Id"];
    [request setHTTPMethod:@"GET"];
    [request setTimeoutInterval:120];
    return request;
}

// PUT Request
+ (NSURLRequest*) putRequestWithService:(NSString*)service withDictionary:(NSDictionary*)dict
{
    NSString* urlString=[kServerURL stringByAppendingString:service];
    return [WebServiceManager putRequestWithUrlDicwithSession:urlString withDictionary:dict];
}
+ (NSMutableURLRequest*) putRequestWithUrlDicwithSession:(NSString*)urlString withDictionary:(NSDictionary*)dict
{
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    [request setValue:@"123456789" forHTTPHeaderField:@"X-Oc-Merchant-Id"];
    
    
    NSUserDefaults *NonVolatile = [NSUserDefaults standardUserDefaults];
    NSString *sessionString = [NonVolatile stringForKey:@"SessionData"];
    
    [request setValue:sessionString forHTTPHeaderField:@"X-Oc-Session"];
   // [request setValue:_gDataManager.sessionString forHTTPHeaderField:@"X-Oc-Session"];

    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPMethod:@"PUT"];
    [request setTimeoutInterval:60];
    NSData* postData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:nil];
    [request setHTTPBody:postData];
    //  [WebServiceManager addAuthTokenToRequest:request];
    
    return request;
}

#pragma mark- POST JSON requests
+ (NSMutableURLRequest*) postRequestWithServiceDic:(NSString*)service withDictionary:(NSDictionary*)dict
{
    NSString* urlString=[kServerURL stringByAppendingString:service];
    return [WebServiceManager postRequestWithUrlDic:urlString withDictionary:dict];
}

+ (NSMutableURLRequest*) postRequestWithServiceDicwithSesion:(NSString*)service withDictionary:(NSDictionary*)dict
{
    NSString* urlString=[kServerURL stringByAppendingString:service];
    return [WebServiceManager postRequestWithUrlDicwithSession:urlString withDictionary:dict];
}



+ (NSMutableURLRequest*) postRequestWithUrlDicwithSession:(NSString*)urlString withDictionary:(NSDictionary*)dict
{
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    [request setValue:@"123456789" forHTTPHeaderField:@"X-Oc-Merchant-Id"];
    
  
    NSUserDefaults *NonVolatile = [NSUserDefaults standardUserDefaults];
    NSString *sessionString = [NonVolatile stringForKey:@"SessionData"];
    
    [request setValue:sessionString forHTTPHeaderField:@"X-Oc-Session"];
  //  [request setValue:_gDataManager.sessionString forHTTPHeaderField:@"X-Oc-Session"];

    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPMethod:@"POST"];
    [request setTimeoutInterval:60];
    NSData* postData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:nil];
    [request setHTTPBody:postData];
    //  [WebServiceManager addAuthTokenToRequest:request];
    
    return request;
}

+ (NSMutableURLRequest*) postRequestWithUrlDic:(NSString*)urlString withDictionary:(NSDictionary*)dict
{
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    [request setValue:@"123456789" forHTTPHeaderField:@"X-Oc-Merchant-Id"];
    
    NSUserDefaults *NonVolatile = [NSUserDefaults standardUserDefaults];
    NSString *sessionString = [NonVolatile stringForKey:@"SessionData"];
    
   
   [request setValue:sessionString forHTTPHeaderField:@"X-Oc-Session"];
    
  
   // [request setValue:_gDataManager.sessionString forHTTPHeaderField:@"X-Oc-Session"];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPMethod:@"POST"];
    [request setTimeoutInterval:60];
    NSData* postData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:nil];
    [request setHTTPBody:postData];
  //  [WebServiceManager addAuthTokenToRequest:request];
    
    return request;
}


// Delete Request
+ (NSURLRequest*) deleteRequestWithService:(NSString*)service withDictionary:(NSDictionary*)dict
{
    NSString* urlString=[kServerURL stringByAppendingString:service];
    return [WebServiceManager deleteRequestWithUrlDicwithSession:urlString withDictionary:dict];
}
+ (NSMutableURLRequest*) deleteRequestWithUrlDicwithSession:(NSString*)urlString withDictionary:(NSDictionary*)dict
{
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    [request setValue:@"123456789" forHTTPHeaderField:@"X-Oc-Merchant-Id"];
    
    
    NSUserDefaults *NonVolatile = [NSUserDefaults standardUserDefaults];
    NSString *sessionString = [NonVolatile stringForKey:@"SessionData"];
    
    [request setValue:sessionString forHTTPHeaderField:@"X-Oc-Session"];
  //  [request setValue:_gDataManager.sessionString forHTTPHeaderField:@"X-Oc-Session"];

    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPMethod:@"DELETE"];
    [request setTimeoutInterval:60];
    NSData* postData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:nil];
    [request setHTTPBody:postData];
    //  [WebServiceManager addAuthTokenToRequest:request];
    
    return request;
}

+ (void) sendRequest:(NSURLRequest*)request completion: (void (^)(NSData* , NSError*)) callback
{
    if ([WebServiceManager isConnectedToNetwork]==NO)
    {
        callback(nil , [NSError errorWithDomain:kNetworkErrorMeassage code:0 userInfo:nil]);
        return;
    }
//    NSLog(@"Request# \n URL : %@ \n Headers : %@ \n Request Method : %@ \n Post body : %@\n",request.URL.absoluteString, request.allHTTPHeaderFields.description,request.HTTPMethod,request.HTTPBody?[NSJSONSerialization JSONObjectWithData:request.HTTPBody options:0 error:NULL]:request.HTTPBody);
    
     NSLog(@"Request# \n URL : %@ \n Headers : %@ \n Request Method : %@ \n Post body : %@\n",request.URL.absoluteString, request.allHTTPHeaderFields.description,request.HTTPMethod,request.HTTPBody?[[NSString alloc] initWithData:request.HTTPBody encoding:NSASCIIStringEncoding]:request.HTTPBody);
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    NSURLSessionDataTask *dataTask=[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^
                                    (NSData* responseData,NSURLResponse* response,NSError* error)
                                    {
                                        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                        NSHTTPURLResponse* httpResponce=(NSHTTPURLResponse*)response;
                                        NSUInteger responseStatusCode=[httpResponce statusCode];
                                        if(responseStatusCode == 200 || responseStatusCode==201 || responseStatusCode==202 || responseStatusCode == 204)
                                        {
                                            dispatch_async(dispatch_get_main_queue(),^
                                                           {
                                                               callback(responseData,error);
                                                           });
                                        }
                                        else
                                        {
                                            dispatch_async(dispatch_get_main_queue(),^
                                                           {
                                                               callback(nil,error);
                                                           });
                                        }
                                    }];
    [dataTask resume];
}

+ (BOOL) isConnectedToNetwork
{
    // Create zero addy
    struct sockaddr_in zeroAddress;
    bzero(&zeroAddress, sizeof(zeroAddress));
    zeroAddress.sin_len = sizeof(zeroAddress);
    zeroAddress.sin_family = AF_INET;
    
    // Recover reachability flags
    SCNetworkReachabilityRef defaultRouteReachability = SCNetworkReachabilityCreateWithAddress(NULL, (struct sockaddr *)&zeroAddress);
    SCNetworkReachabilityFlags flags;
    
    // synchronous model
    
    BOOL didRetrieveFlags = SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags);
    CFRelease(defaultRouteReachability);
    
    if (!didRetrieveFlags)
    {
        NSLog(@"Error. Could not recover network reachability flags\n");
        return 0;
    }
    
    BOOL isReachable = flags & kSCNetworkFlagsReachable;
    BOOL needsConnection = flags & kSCNetworkFlagsConnectionRequired;
    
    return (isReachable && !needsConnection);
}

@end
