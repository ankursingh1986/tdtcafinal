//
//  DoctorISent_TVC.m
//  TDATC
//
//  Created by iWeb on 9/18/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "DoctorISent_TVC.h"
#import "Global.pch"

@implementation DoctorISent_TVC

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void) setUpData:(DoctorSentMessageModel*)data
{
    self.subjectLabel.text = data.subject;
  //  self.messageLabel.text = data.doctor_msg;
    if([data.attchment isEqualToString:@""])
    {
        self.txtView_sent.text = data.doctor_msg;

    }
    else
    {
        NSString *strData=[NSString stringWithFormat:@"%@ \n %@\%@",data.doctor_msg,_gDataManager.smsImagePath,data.attchment];
        self.txtView_sent.text = strData;
    }
    self.dateLabel.text = [Utility changeDateTimeFormat:data.date];
}

@end
