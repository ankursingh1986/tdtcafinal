//
//  SideMenuProfileTableViewCell.h
//  NicoBeacon
//
//  Created by iWeb on 9/2/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SideMenuProfileTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *img_side;

@property (weak, nonatomic) IBOutlet UILabel *profileName;
@property (weak, nonatomic) IBOutlet UILabel *profileEmail;

@end
