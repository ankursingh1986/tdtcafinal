//
//  DoctorFeedback_VC.m
//  TDATC
//
//  Created by iWeb on 9/18/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "DoctorFeedback_VC.h"
#import "DoctorFeedback_TVC.h"
#import "DataManager.h"
#import "Utility.h"
#import "MyLoader.h"
#import "DataParser.h"
#import "Defines.h"
#import "DoctorFeedback.h"
#import "Defines.h"


@interface DoctorFeedback_VC ()
{
    NSMutableArray *feedbackArray;
    NSMutableArray* _mainArray;
    NSMutableArray* _selectedDateArray;


}

@end

@implementation DoctorFeedback_VC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self leftButton];
    [self rightButton];
    feedbackArray = [[NSMutableArray alloc]init];
  _mainArray=[[NSMutableArray alloc]init];
    _selectedDateArray=[[NSMutableArray alloc]init];

    [tbl_FeedBack setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    self->tbl_FeedBack.tableFooterView = [[UIView alloc] init];


   
    self.gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];

    
    self.view_1.hidden=YES;
    tbl_FeedBack.hidden=NO;
    
    
    _calender1.delegate = self;
    _calender1.dataSource = self;
    
//    UITapGestureRecognizer *singleFingerTap =
//    [[UITapGestureRecognizer alloc] initWithTarget:self
//                                            action:@selector(handleSingleTap:)];
//    [self.view_1 addGestureRecognizer:singleFingerTap];
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    self.view_1.hidden=YES;
    tbl_FeedBack.hidden=NO;

    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    _calender1.appearance.eventDefaultColor = [UIColor greenColor];


    self.navigationItem.title = @"FEEDBACK HISTORY";
    [self getDoctorFeedback];
}

#pragma mark - Calnder delegates

- (BOOL)calendar:(FSCalendar *)calendar hasEventForDate:(NSDate *)date
{
    if(feedbackArray.count!=0)
    {
        for (int k=0; k<feedbackArray.count; k++) {
            DoctorFeedback* data = [feedbackArray objectAtIndex:k];
            NSString *dateStr = [Utility changeDateCalenderFormat:data.crdt];
            
            // Convert string to date object
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"yyyy-MM-dd"];
            NSDate *datenew = [dateFormat dateFromString:dateStr];
            if ([date isEqualToDate:datenew]) {
                return YES;
            }
            
        }
    }
    
    return NO;
}

///FSCalendarDelegate


- (void)calendar:(FSCalendar *)calendar didSelectDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"MMMM dd, YYYY"];
    NSLog(@"did select date %@",[df stringFromDate:date]);
    //NSDate *selectedDate = [df dateFromString:date];
    
    
    [_mainArray removeAllObjects];
    [_selectedDateArray removeAllObjects];
    for (DoctorFeedback* obj in feedbackArray)
    {
        NSLog(@"%@", obj.crdt);
        NSString *dateStr = [Utility changeDateCalenderFormat:obj.crdt];
        
        // Convert string to date object
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd"];
        NSDate *appointmentDate = [dateFormat dateFromString:dateStr];
        
        if ([date isEqualToDate:appointmentDate])
        {
            [_selectedDateArray addObject:obj];
            _mainArray = [_selectedDateArray mutableCopy];
        }
    }
    [tbl_FeedBack reloadData];
    
    self.view_1.hidden=YES;

    tbl_FeedBack.hidden=NO;

}



- (void) getDoctorFeedback
{
    [MyLoader showLoadingView];
    
   // [DataParser doctorFeedback:[NSString stringWithFormat:@"id=%@",_gDataManager.loginModel.user_id] WithCompletionHandler:^(NSArray *array, BOOL isSuccessful)
  //    [DataParser doctorFeedback:[NSString stringWithFormat:@"id=210"] WithCompletionHandler:^(NSArray *array, BOOL isSuccessful)
    [DataParser doctorFeedback:[NSString stringWithFormat:@"id=%@",_gDataManager.loginModel.user_id] WithCompletionHandler:^(NSArray *array, BOOL isSuccessful)
    {
        [MyLoader hideLoadingView];
        if (isSuccessful)
        {
            [MyLoader hideLoadingView];
            feedbackArray = [array mutableCopy];
            _mainArray = [array mutableCopy];
            [tbl_FeedBack reloadData];
            [self.calender1 reloadData];
        }
        else
        {
            [Utility alertWithTitle:kAlertTitle withMessage:kNetworkErrorTitle onVC:self];
        }
    }];
}

- (IBAction)previous:(id)sender
{
    NSDate *currentMonth = self.calender1.currentPage;
    NSDate *previousMonth = [self.gregorian dateByAddingUnit:NSCalendarUnitMonth value:-1 toDate:currentMonth options:0];
    [self.calender1 setCurrentPage:previousMonth animated:YES];
}

- (IBAction)next:(id)sender
{
    NSDate *currentMonth = self.calender1.currentPage;
    NSDate *nextMonth = [self.gregorian dateByAddingUnit:NSCalendarUnitMonth value:1 toDate:currentMonth options:0];
    [self.calender1 setCurrentPage:nextMonth animated:YES];
}

// FSCalendarDelegate
- (void)calendar:(FSCalendar *)calendar didSelectDate:(NSDate *)date
{
    self.view_1.hidden=YES;
    tbl_FeedBack.hidden=NO;
    NSLog(@"date selected");
}

#pragma mark - table view delegates and data socurce


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _mainArray.count;
}
//

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    DoctorFeedback_TVC *cell = [tableView dequeueReusableCellWithIdentifier:@"DoctorFeedback_Cell" forIndexPath:indexPath];
    
    
    cell.view_star.canEdit = NO;
    cell.view_star.maxRating = 5;
    
    
  //  cell.view_star.rating = 3;
    DoctorFeedback *obj=[_mainArray objectAtIndex:indexPath.row];
    cell.lbl_name.text=[obj.patient_name uppercaseString];
    cell.lbl_date.text=[Utility changeDateTimeFormat:obj.crdt];
    cell.view_star.rating = [obj.raiting_id intValue];
    cell.txtView_message.text=obj.pasent_msg;
    NSString *imagePath=[NSString stringWithFormat:@"%@%@",kPatientImagePath,obj.patient_image];
    if([imagePath isEqualToString:@"(null)(null)"])
    {
        
    }
    else
    {
        [self downloadImage:cell imageUrl1:imagePath];
        
    }

   
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    DoctoreMessageDetailViewController* destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"DoctoreMessageDetailViewController_id"];
    //
    //    [self.navigationController pushViewController:destVC animated:YES];
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)rightButton
{
    button =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"filter"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(filtorButtonAction:)forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0, 20, 20)];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.rightBarButtonItem = barButton;
}

- (IBAction) filtorButtonAction:(id)sender
{
    
    if(button.selected)
    {
        self.view_1.hidden=NO;
        tbl_FeedBack.hidden=YES;
    }
    else
    {
        self.view_1.hidden=YES;
        tbl_FeedBack.hidden=NO;
    }
    button.selected = !button.selected;

    
}


-(void)leftButton
{
    UIButton *button11 =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button11 setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [button11 addTarget:self action:@selector(backButtonAction:)forControlEvents:UIControlEventTouchUpInside];
    [button11 setFrame:CGRectMake(0, 0, 20, 20)];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button11];
    self.navigationItem.leftBarButtonItem = barButton;
}

- (IBAction) backButtonAction:(id)sender
{
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
    [self presentViewController:vc animated:NO completion:nil];
    
}
- (void) downloadImage:(DoctorFeedback_TVC*)cell imageUrl1:(NSString *)imgURL1
{
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       image_manager = [SDWebImageManager sharedManager];
                       [image_manager downloadWithURL:[NSURL URLWithString:imgURL1] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize, NSInteger feedID, NSString *feedString)
                        {
                        } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished)
                        {
                            if (image)
                            {
                                cell.img_profile.image = image;
                            }
                        }];
                   });
    
}



@end
