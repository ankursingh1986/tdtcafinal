//
//  DoctorProfileModel.h
//  TDATC
//
//  Created by Vinay on 14/10/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DoctorProfileModel : NSObject

@property(nonatomic, strong) NSString* service_name;
@property(nonatomic, strong) NSString* specility_name;


@property(nonatomic, strong) NSString* course;
@property(nonatomic, strong) NSString* institute;
@property(nonatomic, strong) NSString* year;

@property(nonatomic, strong) NSMutableArray* doctor_registration;
@property(nonatomic, strong) NSMutableArray* doctor_memberships;
@property(nonatomic, strong) NSMutableArray* doctor_awards;

@property(nonatomic, strong) NSMutableArray* doctor_experiences;

@property(nonatomic, strong) NSMutableArray* doctor_educations;

@property(nonatomic, strong) NSMutableArray* doctor_specialities;

@property(nonatomic, strong) NSMutableArray* doctor_services;










@property(nonatomic, strong) NSString* email;
@property(nonatomic, strong) NSString* full_name;
@property(nonatomic, strong) NSString* image;


- (id) initWithDictionary:(NSDictionary*)dictionary;

@end
