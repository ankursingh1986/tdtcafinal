//
//  DoctorProfileModel.m
//  TDATC
//
//  Created by Vinay on 14/10/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "DoctorProfileModel.h"

@implementation DoctorProfileModel


- (id) initWithDictionary:(NSDictionary*)dictionary
{
    self = [super init];
    if (self)
    {
        NSArray* doctor_registration = dictionary[@"doctor_registration"];
        for (NSDictionary* dict in doctor_registration)
        {
            NSLog(@"%@",dict);
        }
        
        NSArray* doctor_memberships = dictionary[@"doctor_memberships"];
        for (NSDictionary* dict in doctor_memberships)
        {
            NSLog(@"%@",dict);
        }
        
        NSArray* doctor_educations = dictionary[@"doctor_educations"];
        for (NSDictionary* dict in doctor_educations)
        {
            if (![dict[@"institute"] isKindOfClass:[NSNull class]])
                self.institute = dict[@"institute"];
            else
                self.institute = @"";
            //self.institute = dict[@"institute"];
            if (![dict[@"course"] isKindOfClass:[NSNull class]])
                self.course = dict[@"course"];
            else
                self.course = @"";
            //self.course = dict[@"course"];
            if (![dict[@"year"] isKindOfClass:[NSNull class]])
                self.year = dict[@"year"];
            else
                self.year = @"";
            //self.year = dict[@"year"];
        }
        
        NSArray* doctor_specialities = dictionary[@"doctor_specialities"];
        for (NSDictionary* dict in doctor_specialities)
        {
            if (![dict[@"speciality"][@"specility_name"] isKindOfClass:[NSNull class]])
                self.specility_name = dict[@"speciality"][@"specility_name"];
            else
                self.specility_name = @"";
            // self.specility_name = dict[@"speciality"][@"specility_name"];
        }
        
        
        
        NSArray* doctor_services = dictionary[@"doctor_services"];
        for (NSDictionary* dict in doctor_services)
        {
            NSDictionary *dicLocal=dict[@"services"];
            if (![dicLocal[@"service_name"] isKindOfClass:[NSNull class]])
                self.service_name = dicLocal[@"service_name"];
            else
                self.service_name = @"";
            // self.service_name = dicLocal[@"service_name"];
        }
        
        if (![dictionary[@"doctor_registration"] isKindOfClass:[NSNull class]])
            self.doctor_registration = dictionary[@"doctor_registration"];
        //        else
        //            self.doctor_registration = @"";
        self.doctor_registration=dictionary[@"doctor_registration"];
        self.doctor_memberships=dictionary[@"doctor_memberships"];
        self.doctor_awards=dictionary[@"doctor_awards"];
        self.doctor_experiences=dictionary[@"doctor_experiences"];
        self.doctor_educations=dictionary[@"doctor_educations"];
        self.doctor_specialities=dictionary[@"doctor_specialities"];
        self.doctor_services=dictionary[@"doctor_services"];
        
        if (![dictionary[@"email"] isKindOfClass:[NSNull class]])
            self.email = dictionary[@"email"];
        else
            self.email = @"";
        //self.email = dictionary[@"email"];
        if (![dictionary[@"full_name"] isKindOfClass:[NSNull class]])
            self.full_name = dictionary[@"full_name"];
        else
            self.full_name = @"";
        if (![dictionary[@"image"] isKindOfClass:[NSNull class]])
            self.image = dictionary[@"image"];
        else
            self.image = @"";
        //self.image = dictionary[@"image"];
    }
    return self;
}

@end
