//
//  PatientFindDoctor_VC.h
//  TDATC
//
//  Created by iWeb on 9/23/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Global.pch"


@interface PatientFindDoctor_VC : UIViewController
{
    UIButton *button;
    SDWebImageManager *image_manager;

}
@property (weak, nonatomic) IBOutlet UITextField *countryTextField;
@property (weak, nonatomic) IBOutlet UITextField *stateTextField;
@property (weak, nonatomic) IBOutlet UITextField *cityTextField;
@property (weak, nonatomic) IBOutlet UITextField *serviceTextField;

- (IBAction)touch_FindDoctor:(id)sender;

@end
