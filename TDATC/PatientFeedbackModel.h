//
//  PatientFeedbackModel.h
//  TDATC
//
//  Created by Vinay on 03/11/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PatientFeedbackModel : NSObject

@property (nonatomic, strong) NSString* doctorId;
@property (nonatomic, strong) NSString* full_name;
@property (nonatomic, strong) NSString* image;
@property (nonatomic, strong) NSString* image_path;
@property (nonatomic, strong) NSString* pasent_msg;
@property (nonatomic, strong) NSString* raiting_id;
@property (nonatomic, strong) NSString* date;

- (id) initWithDictionary:(NSDictionary*)dictionary;

@end
