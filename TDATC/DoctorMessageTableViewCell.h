//
//  DoctorMessageTableViewCell.h
//  TDATC
//
//  Created by iWeb on 9/18/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DoctorMessage.h"
#import "SDWebImageManager.h"



@interface DoctorMessageTableViewCell : UITableViewCell
{
    SDWebImageManager *image_manager;

}
@property (weak, nonatomic) IBOutlet UITextView *txtview_head;
@property (weak, nonatomic) IBOutlet UITextView *txtView_desc;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Title;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Date;
@property (weak, nonatomic) IBOutlet UILabel *lbl_desc;
@property (weak, nonatomic) IBOutlet UIImageView *img_profile;
- (void) setUpData:(DoctorMessage*)data;

@end
