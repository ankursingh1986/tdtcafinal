//
//  DoctorListModel.m
//  TDATC
//
//  Created by Vinay on 07/11/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "DoctorList.h"

@implementation DoctorList

- (id) initWithDictionary:(NSDictionary*)dictionary
{
    self = [super init];
    if (self)
    {
        if (![dictionary[@"doctor_id"] isKindOfClass:[NSNull class]])
            self.doctor_id = dictionary[@"doctor_id"];
        else
            self.doctor_id = @"";
        //self.doctor_id = dictionary[@"doctor_id"];
        if (![dictionary[@"patient_id"] isKindOfClass:[NSNull class]])
            self.patient_id = dictionary[@"patient_id"];
        else
            self.patient_id = @"";
        
        if (![dictionary[@"dr"][@"full_name"] isKindOfClass:[NSNull class]])
            self.full_name = dictionary[@"dr"][@"full_name"];
        else
            self.full_name = @"";
        
        // self.full_name = dictionary[@"dr"][@"full_name"];
        
        if (![dictionary[@"dr"][@"image"] isKindOfClass:[NSNull class]])
            self.image = dictionary[@"dr"][@"image"];
        else
            self.image = @"";
        //self.image = dictionary[@"dr"][@"image"];
        if (![dictionary[@"dr"][@"locality"] isKindOfClass:[NSNull class]])
            self.locality = dictionary[@"dr"][@"locality"];
        else
            self.locality = @"";
        //self.locality = dictionary[@"dr"][@"locality"];
        if (![dictionary[@"fba"][@"raiting_id"] isKindOfClass:[NSNull class]])
            self.rating = dictionary[@"fba"][@"raiting_id"];
        else
            self.rating = @"";
        
        //self.rating = dictionary[@"fba"][@"raiting_id"];
    }
    return self;
}


@end
