//
//  DoctorHome_VC.m
//  TDATC
//
//  Created by iWeb on 9/18/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "DoctorHome_VC.h"
#import "DoctorHome_TVC.h"
#import "DoctorHome_TVC1.h"
#import "SWRevealViewController.h"
#import "DataParser.h"
#import "Utility.h"
#import "DataManager.h"
#import "DoctorProfileModel.h"
#import "MyLoader.h"
#import "SDWebImageManager.h"
#import "Defines.h"


#import "Defines.h"


@interface DoctorHome_VC ()

@end

@implementation DoctorHome_VC


#pragma mark - View controlers life cycle methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    [tbl_Home setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    self->tbl_Home.tableFooterView = [[UIView alloc] init];

    SWRevealViewController *vc=self.revealViewController;
    if (vc)
    {
        _sideBarbtn.target=self.revealViewController;
        //[_sideBarbtn setAction: @selector(revalTogel: )];
        //   [self.sideBarbtn setAction:@selector(revealToggle:)];
        _sideBarbtn.action=@selector(revealToggle:);
        [self.navigationController.navigationBar addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    arrayImage=[[NSMutableArray alloc]initWithObjects:@"services",@"specialization",@"education",@"experience",@"award",@"membership-1",@"registration", nil];
    
    arrayTitle=[[NSMutableArray alloc]initWithObjects:@"SERVICES",@"SPECIALIZATIONS",@"EDUCATION",@"EXPERIENCE",@"AWARDS & RECOGNITION",@"MEMBERSHIP",@"REGISTRATION", nil];
    
    arrayDesc=[[NSMutableArray alloc]initWithObjects:@"",@"",@"",@"",@"",@"",@"", nil];
    
    
    servicesArray = [[NSMutableArray alloc]init];
    specialitiesArray = [[NSMutableArray alloc]init];
    educationsArray = [[NSMutableArray alloc]init];
    experiencesArray = [[NSMutableArray alloc]init];
    awardsArray = [[NSMutableArray alloc]init];
    membershipsArray = [[NSMutableArray alloc]init];
    registrationArray = [[NSMutableArray alloc]init];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
  
    
   
    
     NSString *socialLogin = [[NSUserDefaults standardUserDefaults] objectForKey:@"socialLogin"];
    
    if([socialLogin isEqualToString:@"0"])
    {
         [self userAutoLogin];


    }
    else
    {
        [self userloginwithFB];
    }
    
     self.navigationItem.title = [_gDataManager.loginModel.full_name uppercaseString];
    
}
-(void)userloginwithFB{
    
    NSString *postData = [[NSUserDefaults standardUserDefaults] objectForKey:@"userLoginInfo"];
    
    
    [MyLoader showLoadingView];
    [DataParser facebookAPIHit:postData WithCompletionHandler:^(NSDictionary *dict, BOOL isSuccessful)
     {
         if (isSuccessful)
         {
             [MyLoader hideLoadingView];
             
             if([dict[@"status"] isEqualToString:@"false"])
             {
                
             }
             else
             {
                 self.navigationItem.title = [_gDataManager.loginModel.full_name uppercaseString];
                 [self getDoctorProfile];
             }
         }
         else
         {
             [MyLoader hideLoadingView];
             //[Utility alertWithTitle:@"" withMessage:kNetworkErrorTitle onVC:self];
         }
     }];
    

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void) userAutoLogin
{
    NSString *postData = [[NSUserDefaults standardUserDefaults] objectForKey:@"userLoginInfo"];
  
    
    [MyLoader showLoadingView];
    [DataParser loginAPIHit:postData WithCompletionHandler:^(NSDictionary *dict, BOOL isSuccessful)
     {
         if (isSuccessful)
         {
             [MyLoader hideLoadingView];
             
             if([dict[@"status"] isEqualToString:@"false"])
             {
//                 [Utility alertWithTitle:kAlertTitle withMessage:dict[@"user"] onVC:self];
                 
                 //Ankur
             }
             else
             {
                  self.navigationItem.title = [_gDataManager.loginModel.full_name uppercaseString];
                 [self getDoctorProfile];
             }
         }
         else
         {
             [MyLoader hideLoadingView];
            // [Utility alertWithTitle:@"" withMessage:kNetworkErrorTitle onVC:self];
         }
     }];

    
}

#pragma mark - Get doctor profile
- (void) getDoctorProfile
{
    //NSString *user_id = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"];
    
    [MyLoader showLoadingView];
    [DataParser doctorDashboardProfile:[NSString stringWithFormat:@"id=%@",_gDataManager.loginModel.user_id] WithCompletionHandler:^(NSDictionary* dict, BOOL isSuccessful)
     
    {
        [MyLoader hideLoadingView];
        if(isSuccessful)
        {
            DoctorProfileModel *obj=_gDataManager.doctorModel;
            servicesArray = obj.doctor_services;
            specialitiesArray = obj.doctor_specialities;
            educationsArray = obj.doctor_educations;
            experiencesArray = obj.doctor_experiences;
            awardsArray = obj.doctor_awards;
            membershipsArray = obj.doctor_memberships;
            registrationArray = obj.doctor_registration;
            [tbl_Home reloadData];
        }
        else
        {
            //[Utility alertWithTitle:@"Alert!" withMessage:@"Please check your internet connection" onVC:self];
        }
    }];
}

#pragma mark - table view delegates and data socurce


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section==0)
    {
        return 155;
    }
    else
    {
        int noOfLine = 0;
        if(indexPath.row==0)
        {
            noOfLine = (int)servicesArray.count;
            
           // noOfLine = noOfLine*30;
           // noOfLine = noOfLine+30;
           // return noOfLine;
        }
        if(indexPath.row==1)
        {
            noOfLine = (int)specialitiesArray.count;
//            noOfLine = noOfLine*30;
//            noOfLine = noOfLine+30;
//            return noOfLine;
        }
        if(indexPath.row==2)
        {
            noOfLine = (int)educationsArray.count;
//            noOfLine = noOfLine*30;
//            noOfLine = noOfLine+30;
//            return noOfLine;
        }
        if(indexPath.row==3)
        {
            noOfLine = (int)experiencesArray.count;
//            noOfLine = noOfLine*30;
//            noOfLine = noOfLine+30;
//            return noOfLine;
        }
        if(indexPath.row==4)
        {
            noOfLine = (int)awardsArray.count;
//            noOfLine = noOfLine*30;
//            noOfLine = noOfLine+30;
//            return noOfLine;
        }
        if(indexPath.row==5)
        {
            noOfLine = (int)membershipsArray.count;
            noOfLine = noOfLine*30;
            noOfLine = noOfLine+30;
            return noOfLine;
        }
        if(indexPath.row==6)
        {
            noOfLine = (int)registrationArray.count;
//            noOfLine = noOfLine*30;
//            noOfLine = noOfLine+30;
//            return noOfLine;
        }
        if(noOfLine==0)
        {
            return 80;
        }
        else if (noOfLine==1)
        {
            return 80;

        }
        else
        {
            return 100;
        }
        return 80;
    }
    return 80;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section==0)
    {
        return 1;
    }
    return arrayImage.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section==0)
    {
        DoctorHome_TVC1 *cell = [tableView dequeueReusableCellWithIdentifier:@"DoctorHome_Cell1" forIndexPath:indexPath];
        cell.img_home.layer.cornerRadius = cell.img_home.frame.size.width / 2;
        cell.img_home.clipsToBounds = YES;
        cell.img_home.layer.borderWidth = 6.0f;
        cell.img_home.layer.borderColor = [UIColor whiteColor].CGColor;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        NSString *imagePath=[NSString stringWithFormat:@"%@",_gDataManager.doctorProfileImagePath];
        if([imagePath isEqualToString:@"(null)(null)"])
        {
            
        }
        else
        {
            [self downloadImage:cell imageUrl1:imagePath];
            
        }
        NSLog(@"Iamge=%@",_gDataManager.doctorModel.image);
        return cell;
    }
    else
    {
        
        //if(cell==nil)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"DoctorHome_Cell" forIndexPath:indexPath];
        
        
        cell.img_home.image=[UIImage imageNamed:[arrayImage objectAtIndex:indexPath.row]];
        cell.lbl_titile.text=[arrayTitle objectAtIndex:indexPath.row];
        cell.lbl_detail.text=[arrayDesc objectAtIndex:indexPath.row];
        DoctorProfileModel *obj=_gDataManager.doctorModel;
        
       
        
        if(indexPath.row==0)
        {
            NSString *serviceData;
            for(int i=0;i<servicesArray.count;i++)
            {
                NSDictionary* dict=[servicesArray objectAtIndex:i];
                if(i==0)
                {
                    serviceData= [NSString stringWithFormat:@"%@",dict[@"services"][@"service_name"]];
                }
                else
                {
                    serviceData= [NSString stringWithFormat:@"%@,\n%@",serviceData,dict[@"services"][@"service_name"]];
                }
            }
            
            CGRect labelFrame = CGRectMake(56, 38, 260, 650);
            UILabel *myLabel = [[UILabel alloc] initWithFrame:labelFrame];
            [myLabel setBackgroundColor:[UIColor clearColor]];
            [myLabel setFont:[UIFont fontWithName:@"Montserrat-Light" size:16]];
            myLabel.textColor = [UIColor colorWithRed:(188/255.f) green:(188/255.f) blue:(188/255.f) alpha:1.0];

            
            NSString *labelText = [serviceData uppercaseString];
            [myLabel setText:labelText];
            [myLabel setNumberOfLines:0];
            [myLabel sizeToFit];
            
            [cell addSubview:myLabel];
            
            cell.lbl_detail.numberOfLines = 0;
            [cell.lbl_detail sizeToFit];
            
            
            
           
         
        }
        if (indexPath.row==1)
        {
            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(10, 0, 300, 1)];
            view.backgroundColor = [UIColor colorWithRed:(188/255.f) green:(188/255.f) blue:(188/255.f) alpha:1.0];
            [cell addSubview:view];
            
            NSString *specialisData;
            for(int i=0;i<specialitiesArray.count;i++)
            {
                NSDictionary* dict=[specialitiesArray objectAtIndex:i];
                if(i==0)
                {
                    specialisData= [NSString stringWithFormat:@"%@",dict[@"speciality"][@"specility_name"]];
                }
                else
                {
                    specialisData= [NSString stringWithFormat:@"%@,\n%@",specialisData,dict[@"speciality"][@"specility_name"]];
                }
            }
            
            CGRect labelFrame = CGRectMake(56, 38, 260, 650);
            UILabel *myLabel = [[UILabel alloc] initWithFrame:labelFrame];
            [myLabel setBackgroundColor:[UIColor clearColor]];
            [myLabel setFont:[UIFont fontWithName:@"Montserrat-Light" size:16]];
            myLabel.textColor = [UIColor colorWithRed:(188/255.f) green:(188/255.f) blue:(188/255.f) alpha:1.0];
            
            NSString *labelText = [specialisData uppercaseString];
            [myLabel setText:labelText];
            [myLabel setNumberOfLines:0];
            [myLabel sizeToFit];
            
            [cell addSubview:myLabel];
            
           // cell.lbl_detail.text=obj.specility_name;

        }
        if (indexPath.row==2)
        {
            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(10, 0, 300, 1)];
            view.backgroundColor = [UIColor colorWithRed:(188/255.f) green:(188/255.f) blue:(188/255.f) alpha:1.0];
            [cell addSubview:view];

            NSString *localData;
            for(int i=0;i<educationsArray.count;i++)
            {
                NSDictionary* dict=[educationsArray objectAtIndex:i];
                if(i==0)
                {
                    localData= [NSString stringWithFormat:@"%@ (%@, %@, %@)",dict[@"course"],dict[@"institute"],dict[@"city"],dict[@"year"]];
                }
                else
                {
                    localData= [NSString stringWithFormat:@"%@,\n%@ (%@, %@, %@)",localData,dict[@"course"],dict[@"institute"],dict[@"city"],dict[@"year"]];
                }
            }
            
            CGRect labelFrame = CGRectMake(56, 38, 260, 650);
            UILabel *myLabel = [[UILabel alloc] initWithFrame:labelFrame];
            [myLabel setBackgroundColor:[UIColor clearColor]];
            [myLabel setFont:[UIFont fontWithName:@"Montserrat-Light" size:16]];
            myLabel.textColor = [UIColor colorWithRed:(188/255.f) green:(188/255.f) blue:(188/255.f) alpha:1.0];
            
            NSString *labelText = [localData uppercaseString];
            [myLabel setText:labelText];
            [myLabel setNumberOfLines:0];
            [myLabel sizeToFit];
            
            [cell addSubview:myLabel];
            
            //cell.lbl_detail.text=obj.course;
        }
        if (indexPath.row==3)
        {
            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(10, 0, 300, 1)];
            view.backgroundColor = [UIColor colorWithRed:(188/255.f) green:(188/255.f) blue:(188/255.f) alpha:1.0];
            [cell addSubview:view];

            
            NSString *localData;
            for(int i=0;i<experiencesArray.count;i++)
            {
                NSDictionary* dict=[experiencesArray objectAtIndex:i];
                if(i==0)
                {
                    localData= [NSString stringWithFormat:@"%@, %@ - %@",dict[@"organizationname"],dict[@"datefrom"],dict[@"dateto"]];
                }
                else
                {
                    localData= [NSString stringWithFormat:@"%@,\n%@, %@ - %@",localData,dict[@"organizationname"],dict[@"datefrom"],dict[@"dateto"]];
                }
            }
            
            CGRect labelFrame = CGRectMake(56, 38, 260, 650);
            UILabel *myLabel = [[UILabel alloc] initWithFrame:labelFrame];
            [myLabel setBackgroundColor:[UIColor clearColor]];
            [myLabel setFont:[UIFont fontWithName:@"Montserrat-Light" size:16]];
            myLabel.textColor = [UIColor colorWithRed:(188/255.f) green:(188/255.f) blue:(188/255.f) alpha:1.0];
            
            NSString *labelText = [localData uppercaseString];
            [myLabel setText:labelText];
            [myLabel setNumberOfLines:0];
            [myLabel sizeToFit];
            
            [cell addSubview:myLabel];
            
           // cell.lbl_detail.text=obj.year;
            
        }
        if (indexPath.row==4)
        {
            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(10, 0, 300, 1)];
            view.backgroundColor = [UIColor colorWithRed:(188/255.f) green:(188/255.f) blue:(188/255.f) alpha:1.0];
            [cell addSubview:view];

            NSString *localData;
            for(int i=0;i<awardsArray.count;i++)
            {
                NSDictionary* dict=[awardsArray objectAtIndex:i];
                if(i==0)
                {
                    localData= [NSString stringWithFormat:@"%@ - %@",dict[@"award_name"],dict[@"year"]];
                }
                else
                {
                    localData= [NSString stringWithFormat:@"%@,\n%@ - %@",localData,dict[@"award_name"],dict[@"year"]];
                }
            }
            
            CGRect labelFrame = CGRectMake(56, 38, 260, 650);
            UILabel *myLabel = [[UILabel alloc] initWithFrame:labelFrame];
            [myLabel setBackgroundColor:[UIColor clearColor]];
            [myLabel setFont:[UIFont fontWithName:@"Montserrat-Light" size:16]];
            myLabel.textColor = [UIColor colorWithRed:(188/255.f) green:(188/255.f) blue:(188/255.f) alpha:1.0];
            
            NSString *labelText = [localData uppercaseString];
            [myLabel setText:labelText];
            [myLabel setNumberOfLines:0];
            [myLabel sizeToFit];
            
            [cell addSubview:myLabel];
            
           // cell.lbl_detail.text=obj.institute;
            
        }
        if (indexPath.row==5)
        {
            
            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(10, 0, 300, 1)];
            view.backgroundColor = [UIColor colorWithRed:(188/255.f) green:(188/255.f) blue:(188/255.f) alpha:1.0];
            [cell addSubview:view];

            
            NSString *localData;
            for(int i=0;i<membershipsArray.count;i++)
            {
                NSDictionary* dict=[membershipsArray objectAtIndex:i];
                if(i==0)
                {
                    localData= [NSString stringWithFormat:@"%@ ",dict[@"mem_name"]];
                }
                else
                {
                    localData= [NSString stringWithFormat:@"%@,\n%@",localData,dict[@"mem_name"]];
                }
            }
            
            CGRect labelFrame = CGRectMake(56, 38, 260, 650);
            UILabel *myLabel = [[UILabel alloc] initWithFrame:labelFrame];
            [myLabel setBackgroundColor:[UIColor clearColor]];
            [myLabel setFont:[UIFont fontWithName:@"Montserrat-Light" size:16]];
            myLabel.textColor = [UIColor colorWithRed:(188/255.f) green:(188/255.f) blue:(188/255.f) alpha:1.0];
            
            NSString *labelText = [localData uppercaseString];
            [myLabel setText:labelText];
            [myLabel setNumberOfLines:0];
            [myLabel sizeToFit];
          //  if(cell == nil)
            {
                [cell addSubview:myLabel];
                
            }
            
            //[cell addSubview:myLabel];
        }
        if (indexPath.row==6)
        {
            
            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(10, 0, 300, 1)];
            view.backgroundColor = [UIColor colorWithRed:(188/255.f) green:(188/255.f) blue:(188/255.f) alpha:1.0];
            [cell addSubview:view];

            
            NSString *localData;
            for(int i=0;i<registrationArray.count;i++)
            {
                NSDictionary* dict=[registrationArray objectAtIndex:i];
                if(i==0)
                {
                    localData= [NSString stringWithFormat:@"%@",dict[@"registration_name"]];
                }
                else
                {
                    localData= [NSString stringWithFormat:@"%@,\n%@",localData,dict[@"registration_name"]];
                }
            }
            
            CGRect labelFrame = CGRectMake(56, 38, 260, 650);
            UILabel *myLabel = [[UILabel alloc] initWithFrame:labelFrame];
            [myLabel setBackgroundColor:[UIColor clearColor]];
            [myLabel setFont:[UIFont fontWithName:@"Montserrat-Light" size:16]];
            myLabel.textColor = [UIColor colorWithRed:(188/255.f) green:(188/255.f) blue:(188/255.f) alpha:1.0];
            
            NSString *labelText = [localData uppercaseString];
            [myLabel setText:labelText];
            [myLabel setNumberOfLines:0];
            [myLabel sizeToFit];
           // if(cell == nil)
            {
                [cell addSubview:myLabel];

            }
            
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
        }
        return nil;
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}
- (void) downloadImage:(DoctorHome_TVC1*)cell imageUrl1:(NSString *)imgURL1
{
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       image_manager = [SDWebImageManager sharedManager];
                       [image_manager downloadWithURL:[NSURL URLWithString:imgURL1] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize, NSInteger feedID, NSString *feedString)
                        {
                        } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished)
                        {
                            if (image)
                            {
                                cell.img_home.image = image;
                            }
                        }];
                   });
    
}




@end
