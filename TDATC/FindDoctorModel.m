//
//  FindDoctorModel.m
//  TDATC
//
//  Created by Vinay on 02/11/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "FindDoctorModel.h"

@implementation FindDoctorModel

- (id) initWithDictionary:(NSDictionary*)dictionary
{
    self = [super init];
    if (self)
    {
        self.user_id = dictionary[@"user_id"];
        
        if (![dictionary[@"full_name"] isKindOfClass:[NSNull class]])
            self.full_name = dictionary[@"full_name"];
        else
            self.full_name = @"";
        
        if (![dictionary[@"locality"] isKindOfClass:[NSNull class]])
            self.locality = dictionary[@"locality"];
        else
            self.locality = @"";
        
        if (![dictionary[@"image"] isKindOfClass:[NSNull class]])
            self.image = dictionary[@"image"];
        else
            self.image = @"";
        
        if (![dictionary[@"rating"] isKindOfClass:[NSNull class]])
            self.rating = [NSString stringWithFormat:@"%@",dictionary[@"rating"]];
        else
            self.rating = @"";
        
        
        NSArray* dataArray = dictionary[@"doctor_educations"];
        
        for (NSDictionary* dict in dataArray)
        {
            if (![dict[@"course"] isKindOfClass:[NSNull class]])
                self.course = dict[@"course"];
            else
                self.course = @"";
            
            if (![dict[@"institute"] isKindOfClass:[NSNull class]])
                self.institute = dict[@"institute"];
            else
                self.institute = @"";
            
            if (![dict[@"city"] isKindOfClass:[NSNull class]])
                self.city = dict[@"city"];
            else
                self.city = @"";
            
            if (![dict[@"year"] isKindOfClass:[NSNull class]])
                self.year = dict[@"year"];
            else
                self.year = @"";
        }
        NSArray* expArray = dictionary[@"doctor_experiences"];
        
        for (NSDictionary* dict in expArray)
        {
            int expData=[dict[@"dateto"] intValue] - [dict[@"datefrom"] intValue];
            self.exp = [NSString stringWithFormat:@"%d",expData];
           
        }
        
        NSArray* awardArray = dictionary[@"doctor_awards"];
        
        for (NSDictionary* dict in awardArray)
        {
            
            if (![dict[@"award_name"] isKindOfClass:[NSNull class]])
                self.award = [NSString stringWithFormat:@"%@",dict[@"award_name"]];
            else
                self.award = @"";
            
            self.awardYear = [NSString stringWithFormat:@"%@",dict[@"year"]];
            
           
        }
        
        
    }
    return self;
}

@end
