//
//  PatientAppointment_CVC.m
//  TDATC
//
//  Created by iWeb on 9/25/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "PatientAppointment_CVC.h"
#import "Global.pch"

@implementation PatientAppointment_CVC
-(void)setupUI
{
   // _view_colection.layer.cornerRadius=30;
    
    [_view_colection.layer setCornerRadius:10.0f];
    
    // border
    [_view_colection.layer setBorderColor:[UIColor whiteColor].CGColor];
    [_view_colection.layer setBorderWidth:1.5f];
    
    // drop shadow
    [_view_colection.layer setShadowColor:[UIColor blueColor].CGColor];
    [_view_colection.layer setShadowOpacity:0.8];
    [_view_colection.layer setShadowRadius:3.0];
    [_view_colection.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
}

- (void) setUpData:(PatientAppointmentModel*)data
{
    self.patientNameLabel.text = [data.doctorName uppercaseString];
    self.locationLabel.text = data.locality;
    self.sicknessLabel.text = data.sickness;
    ////avalableTime
    
    
    NSString *str1=[NSString stringWithFormat:@"%@-%@",[Utility changeDateFormat1:data.availabledate],[Utility changeTimeFormat:data.avalableTime]];
    self.dateLabel.text=str1;
   // self.dateLabel.text = [Utility changeDateTimeFormat:data.availabledate];
   // self.dateLabel.text = data.createddate;
}

@end
