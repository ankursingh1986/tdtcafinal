//
//  DoctorPatientModel.m
//  TDATC
//
//  Created by iWeb on 10/31/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import "DoctorPatientModel.h"
#import "Utility.h"

@implementation DoctorPatientModel

- (id) initWithDictionary:(NSDictionary*)dict
{
    self = [super init];
    if (self)
    {
        self.doctor_id = [NSString stringWithFormat:@"%@",dict[@"doctor_id"]];
        self.patient_id =[NSString stringWithFormat:@"%@",dict[@"patient_id"]];
        
        if ([dict[@"full_name"] isKindOfClass:[NSNull class]])
            self.full_name = @"";
        else
            self.full_name=[NSString stringWithFormat:@"%@",dict[@"full_name"]];
        
        NSString *urlString = [dict[@"patient_image"] stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        self.patient_image= urlString;
        NSString *urlString1 = [dict[@"profile_image"] stringByReplacingOccurrencesOfString:@" " withString:@"%20"];

        self.profile_image=urlString1;
        self.gender = dict[@"gender"];
        
        if ([dict[@"gender"] isKindOfClass:[NSNull class]])
            self.gender = @"MALE";
        else
            self.gender = dict[@"gender"];
        
        if ([dict[@"age"] isKindOfClass:[NSNull class]])
            self.age = @"";
        else
            self.age = [NSString stringWithFormat:@"%@",dict[@"age"]];

        if ([dict[@"locality"] isKindOfClass:[NSNull class]])
            self.locality = @"";
        else
            self.locality = dict[@"locality"];
        
        if ([dict[@"email"] isKindOfClass:[NSNull class]])
            self.email = @"";
        else
            self.email = dict[@"email"];

        
        if ([dict[@"mobile"] isKindOfClass:[NSNull class]])
            self.mobile = @"";
        else
            self.mobile = [NSString stringWithFormat:@"%@",dict[@"mobile"]];

        
        if ([dict[@"alt_mobile"] isKindOfClass:[NSNull class]])
            self.alt_mobile = @"";
        else
            self.alt_mobile = [NSString stringWithFormat:@"%@",dict[@"alt_mobile"]];

        
        if ([dict[@"married_status"] isKindOfClass:[NSNull class]])
            self.married_status = @"";
        else
            self.married_status = dict[@"married_status"];
        
        if ([dict[@"referrd_by"] isKindOfClass:[NSNull class]])
            self.referrd_by = @"";
        else
            self.referrd_by = dict[@"referrd_by"];
        
        
        if ([dict[@"guarantor"] isKindOfClass:[NSNull class]])
            self.guarantor = @"";
        else
            self.guarantor = dict[@"guarantor"];
        
        if ([dict[@"dob"] isKindOfClass:[NSNull class]])
        {
            
            self.dob = @"";
        }
        else
        {
            
            self.dob = [Utility getAgeFromDOBFormat:dict[@"dob"]];
        }

        
        if ([dict[@"guarantor_relation"] isKindOfClass:[NSNull class]])
            self.guarantor_relation = @"";
        else
            self.guarantor_relation = dict[@"guarantor_relation"];
        
        
        if ([dict[@"g_contact"] isKindOfClass:[NSNull class]])
            self.g_contact = @"";
        else
            self.g_contact = [NSString stringWithFormat:@"%@",dict[@"g_contact"]];
        
    }
    return self;
}


@end
