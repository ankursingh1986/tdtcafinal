//
//  DoctorListModel.h
//  TDATC
//
//  Created by Vinay on 07/11/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DoctorList : NSObject

@property (nonatomic, strong) NSString* doctor_id;
@property (nonatomic, strong) NSString* patient_id;
@property (nonatomic, strong) NSString* full_name;
@property (nonatomic, strong) NSString* image;
@property (nonatomic, strong) NSString* locality;
@property (nonatomic, strong) NSString* rating;


- (id) initWithDictionary:(NSDictionary*)dictionary;

@end
