//
//  PatientInboxMessageTableViewCell.h
//  TDATC
//
//  Created by Vinay on 07/11/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PatientMessageModel.h"
#import "Global.pch"


@interface PatientInboxMessageTableViewCell : UITableViewCell
{
    SDWebImageManager *image_manager;

}

@property (weak, nonatomic) IBOutlet UITextView *txt_title;

@property (weak, nonatomic) IBOutlet UILabel *lbl_date;
@property (weak, nonatomic) IBOutlet UITextView *txt_desc;
@property (weak, nonatomic) IBOutlet UIImageView *img_profile;

- (void) setUpData:(PatientMessageModel*)data;

@end
